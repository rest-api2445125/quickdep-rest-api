<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230603233639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB32576E0FD');
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB3A76ED395');
        $this->addSql('DROP INDEX IDX_3369BBB32576E0FD ON accepted_contract');
        $this->addSql('DROP INDEX IDX_3369BBB3A76ED395 ON accepted_contract');
        $this->addSql('ALTER TABLE accepted_contract DROP user_id, DROP contract_id');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1F2576E0FD');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1FA76ED395');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1F2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1FA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE client CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE owner owner VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285919EB6921');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285975E23604');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28598C03F15C');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859BE04EA9');
        $this->addSql('ALTER TABLE contract DROP invoices_id, DROP parent_id, CHANGE paid paid TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859AA23F6C8 FOREIGN KEY (next_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285975E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28598C03F15C FOREIGN KEY (employee_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E98F2859AA23F6C8 ON contract (next_id)');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EA76ED395');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A768C03F15C');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76C54C8C93');
        $this->addSql('ALTER TABLE document CHANGE message message LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A768C03F15C FOREIGN KEY (employee_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76C54C8C93 FOREIGN KEY (type_id) REFERENCES document_type (id)');
        $this->addSql('ALTER TABLE experience DROP FOREIGN KEY FK_590C103BE04EA9');
        $this->addSql('ALTER TABLE experience DROP FOREIGN KEY FK_590C103A76ED395');
        $this->addSql('DROP INDEX IDX_590C103BE04EA9 ON experience');
        $this->addSql('ALTER TABLE experience DROP job_id');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744A76ED395');
        $this->addSql('ALTER TABLE invoice CHANGE start start DATE NOT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `like` DROP FOREIGN KEY FK_AC6340B3A76ED395');
        $this->addSql('ALTER TABLE `like` ADD CONSTRAINT FK_AC6340B3A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA2576E0FD');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709E2576E0FD');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709EA76ED395');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709E2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E15AD669');
        $this->addSql('ALTER TABLE user ADD initdelete DATETIME DEFAULT NULL, DROP invoices_id');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E15AD669 FOREIGN KEY (usercontract_id) REFERENCES user_contract (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64919EB6921 ON user (client_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accepted_contract ADD user_id INT DEFAULT NULL, ADD contract_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB32576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_3369BBB32576E0FD ON accepted_contract (contract_id)');
        $this->addSql('CREATE INDEX IDX_3369BBB3A76ED395 ON accepted_contract (user_id)');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1FA76ED395');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1F2576E0FD');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1F2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client CHANGE owner owner VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859AA23F6C8');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285919EB6921');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28598C03F15C');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859BE04EA9');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285975E23604');
        $this->addSql('DROP INDEX UNIQ_E98F2859AA23F6C8 ON contract');
        $this->addSql('ALTER TABLE contract ADD invoices_id INT DEFAULT NULL, ADD parent_id INT DEFAULT NULL, CHANGE paid paid TINYINT(1) DEFAULT 0');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285919EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28598C03F15C FOREIGN KEY (employee_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285975E23604 FOREIGN KEY (town_id) REFERENCES town (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EA76ED395');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A768C03F15C');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76C54C8C93');
        $this->addSql('ALTER TABLE document CHANGE message message TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A768C03F15C FOREIGN KEY (employee_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76C54C8C93 FOREIGN KEY (type_id) REFERENCES document_type (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE experience DROP FOREIGN KEY FK_590C103A76ED395');
        $this->addSql('ALTER TABLE experience ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_590C103BE04EA9 ON experience (job_id)');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744A76ED395');
        $this->addSql('ALTER TABLE invoice CHANGE start start DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `like` DROP FOREIGN KEY FK_AC6340B3A76ED395');
        $this->addSql('ALTER TABLE `like` ADD CONSTRAINT FK_AC6340B3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA2576E0FD');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709E2576E0FD');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709EA76ED395');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709E2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64919EB6921');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649E15AD669');
        $this->addSql('DROP INDEX UNIQ_8D93D64919EB6921 ON `user`');
        $this->addSql('ALTER TABLE `user` ADD invoices_id INT DEFAULT NULL, DROP initdelete');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649E15AD669 FOREIGN KEY (usercontract_id) REFERENCES user_contract (id) ON UPDATE NO ACTION ON DELETE SET NULL');
    }
}
