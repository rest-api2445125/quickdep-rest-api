<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230104220221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD owner VARCHAR(255) DEFAULT NULL, ADD payment INT DEFAULT NULL, DROP oowner, CHANGE factureaddress factureaddress VARCHAR(255) DEFAULT NULL, CHANGE zipcode zipcode VARCHAR(255) DEFAULT NULL, CHANGE facturezipcode facturezipcode VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28592989F1FD');
        $this->addSql('DROP INDEX IDX_E98F28592989F1FD ON contract');
        $this->addSql('ALTER TABLE contract CHANGE houre houre INT DEFAULT NULL, CHANGE pause pause INT DEFAULT NULL, CHANGE bonus bonus INT DEFAULT NULL, CHANGE parking parking TINYINT(1) DEFAULT NULL, CHANGE invoice_id invoices_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28592454BA75 FOREIGN KEY (invoices_id) REFERENCES invoice (id)');
        $this->addSql('CREATE INDEX IDX_E98F28592454BA75 ON contract (invoices_id)');
        $this->addSql('ALTER TABLE invoice ADD code VARCHAR(255) NOT NULL, ADD paid TINYINT(1) NOT NULL, CHANGE date date DATETIME NOT NULL, CHANGE total total NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE time_sheet CHANGE pause pause INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD oowner VARCHAR(255) NOT NULL, DROP owner, DROP payment, CHANGE factureaddress factureaddress VARCHAR(255) NOT NULL, CHANGE zipcode zipcode VARCHAR(255) NOT NULL, CHANGE facturezipcode facturezipcode VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28592454BA75');
        $this->addSql('DROP INDEX IDX_E98F28592454BA75 ON contract');
        $this->addSql('ALTER TABLE contract CHANGE houre houre INT NOT NULL, CHANGE parking parking TINYINT(1) NOT NULL, CHANGE pause pause TIME DEFAULT NULL, CHANGE bonus bonus NUMERIC(10, 0) DEFAULT NULL, CHANGE invoices_id invoice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28592989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id)');
        $this->addSql('CREATE INDEX IDX_E98F28592989F1FD ON contract (invoice_id)');
        $this->addSql('ALTER TABLE invoice DROP code, DROP paid, CHANGE date date DATE NOT NULL, CHANGE total total NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE time_sheet CHANGE pause pause TIME NOT NULL');
    }
}
