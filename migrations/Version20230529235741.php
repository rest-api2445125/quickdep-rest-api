<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230529235741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB32576E0FD');
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB3A76ED395');
        $this->addSql('DROP INDEX IDX_3369BBB3A76ED395 ON accepted_contract');
        $this->addSql('DROP INDEX IDX_3369BBB32576E0FD ON accepted_contract');
        $this->addSql('ALTER TABLE accepted_contract DROP user_id, DROP contract_id');
        $this->addSql('ALTER TABLE client CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE owner owner VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285919EB6921');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285975E23604');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28598C03F15C');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859BE04EA9');
        $this->addSql('ALTER TABLE contract DROP invoices_id, DROP parent_id, CHANGE paid paid TINYINT(1) NOT NULL, CHANGE sent2 sent2 TINYINT(1) DEFAULT NULL, CHANGE sent24 sent24 TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859AA23F6C8 FOREIGN KEY (next_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285975E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28598C03F15C FOREIGN KEY (employee_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E98F2859AA23F6C8 ON contract (next_id)');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EA76ED395');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE invoice CHANGE start start DATE NOT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA2576E0FD');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709E2576E0FD');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709EA76ED395');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709E2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accepted_contract ADD user_id INT DEFAULT NULL, ADD contract_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB32576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_3369BBB3A76ED395 ON accepted_contract (user_id)');
        $this->addSql('CREATE INDEX IDX_3369BBB32576E0FD ON accepted_contract (contract_id)');
        $this->addSql('ALTER TABLE client CHANGE owner owner VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859AA23F6C8');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285919EB6921');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28598C03F15C');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859BE04EA9');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285975E23604');
        $this->addSql('DROP INDEX UNIQ_E98F2859AA23F6C8 ON contract');
        $this->addSql('ALTER TABLE contract ADD invoices_id INT DEFAULT NULL, ADD parent_id INT DEFAULT NULL, CHANGE paid paid TINYINT(1) DEFAULT 0, CHANGE sent2 sent2 INT DEFAULT NULL, CHANGE sent24 sent24 INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285919EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28598C03F15C FOREIGN KEY (employee_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285975E23604 FOREIGN KEY (town_id) REFERENCES town (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EA76ED395');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744A76ED395');
        $this->addSql('ALTER TABLE invoice CHANGE start start DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA2576E0FD');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709E2576E0FD');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709EA76ED395');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709E2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }
}
