<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230120213830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD validated TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD rate NUMERIC(10, 0) NOT NULL, ADD clientrate NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE user ADD phonecode VARCHAR(255) DEFAULT NULL, ADD phoneverified TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP validated');
        $this->addSql('ALTER TABLE contract DROP rate, DROP clientrate');
        $this->addSql('ALTER TABLE `user` DROP phonecode, DROP phoneverified');
    }
}
