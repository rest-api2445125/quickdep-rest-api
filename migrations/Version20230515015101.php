<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230515015101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE accepted_contract (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, contract_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_3369BBB3A76ED395 (user_id), INDEX IDX_3369BBB32576E0FD (contract_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE apply (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, contract_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', consecutive TINYINT(1) DEFAULT NULL, consecutivecount INT DEFAULT NULL, startdate DATETIME DEFAULT NULL, enddate DATETIME DEFAULT NULL, INDEX IDX_BD2F8C1FA76ED395 (user_id), INDEX IDX_BD2F8C1F2576E0FD (contract_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, town_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, image_path VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', factureaddress VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(255) DEFAULT NULL, facturezipcode VARCHAR(255) DEFAULT NULL, owner VARCHAR(255) DEFAULT NULL, payment INT DEFAULT NULL, validated TINYINT(1) DEFAULT NULL, INDEX IDX_C744045575E23604 (town_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, employee_id INT DEFAULT NULL, job_id INT DEFAULT NULL, town_id INT DEFAULT NULL, next_id INT DEFAULT NULL, houre INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', status VARCHAR(255) DEFAULT NULL, code VARCHAR(255) NOT NULL, applies_count INT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, startdate DATETIME NOT NULL, enddate DATETIME NOT NULL, cloth VARCHAR(255) DEFAULT NULL, parking TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, paid TINYINT(1) NOT NULL, pause INT DEFAULT NULL, bonus INT DEFAULT NULL, rate NUMERIC(10, 0) NOT NULL, clientrate NUMERIC(10, 0) NOT NULL, late INT DEFAULT NULL, sent2 TINYINT(1) DEFAULT NULL, sent24 TINYINT(1) DEFAULT NULL, INDEX IDX_E98F285919EB6921 (client_id), INDEX IDX_E98F28598C03F15C (employee_id), INDEX IDX_E98F2859BE04EA9 (job_id), INDEX IDX_E98F285975E23604 (town_id), UNIQUE INDEX UNIQ_E98F2859AA23F6C8 (next_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract_invoice (contract_id INT NOT NULL, invoice_id INT NOT NULL, INDEX IDX_98346A8C2576E0FD (contract_id), INDEX IDX_98346A8C2989F1FD (invoice_id), PRIMARY KEY(contract_id, invoice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, INDEX IDX_92FB68EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, type_id INT DEFAULT NULL, statut VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', text VARCHAR(255) DEFAULT NULL, message LONGTEXT DEFAULT NULL, INDEX IDX_D8698A768C03F15C (employee_id), INDEX IDX_D8698A76C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, required TINYINT(1) NOT NULL, type VARCHAR(10) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experience (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, work VARCHAR(255) NOT NULL, time VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_590C103A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invoice (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, date DATETIME NOT NULL, total NUMERIC(10, 0) NOT NULL, paid TINYINT(1) NOT NULL, start DATE NOT NULL, INDEX IDX_90651744A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, rate NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `like` (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, contract_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_AC6340B3A76ED395 (user_id), INDEX IDX_AC6340B32576E0FD (contract_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, contract_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_BF5476CA2576E0FD (contract_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_user (notification_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_35AF9D73EF1A9D84 (notification_id), INDEX IDX_35AF9D73A76ED395 (user_id), PRIMARY KEY(notification_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_seen (id INT AUTO_INCREMENT NOT NULL, notification_id INT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, seen TINYINT(1) NOT NULL, INDEX IDX_1C6E4122EF1A9D84 (notification_id), INDEX IDX_1C6E4122A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE password_reset (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(10) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_sheet (id INT AUTO_INCREMENT NOT NULL, contract_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', totaltimes INT NOT NULL, amount NUMERIC(10, 0) NOT NULL, status VARCHAR(255) NOT NULL, startdate DATETIME NOT NULL, enddate DATETIME NOT NULL, pause INT DEFAULT NULL, UNIQUE INDEX UNIQ_C24E709E2576E0FD (contract_id), INDEX IDX_C24E709EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE town (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, usercontract_id INT DEFAULT NULL, client_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', image_path VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, register_completed TINYINT(1) NOT NULL, work VARCHAR(255) DEFAULT NULL, birthday DATE DEFAULT NULL, bio LONGTEXT DEFAULT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', verification_code INT DEFAULT NULL, email_verified TINYINT(1) NOT NULL, new TINYINT(1) DEFAULT NULL, documents_status VARCHAR(255) NOT NULL, signed TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, phonecode VARCHAR(255) DEFAULT NULL, phoneverified TINYINT(1) DEFAULT NULL, notworked INT DEFAULT NULL, completed INT DEFAULT NULL, canceled INT DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649E15AD669 (usercontract_id), UNIQUE INDEX UNIQ_8D93D64919EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_town (user_id INT NOT NULL, town_id INT NOT NULL, INDEX IDX_36678B6DA76ED395 (user_id), INDEX IDX_36678B6D75E23604 (town_id), PRIMARY KEY(user_id, town_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_contract (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, signature_path VARCHAR(255) DEFAULT NULL, date DATE NOT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB3A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE accepted_contract ADD CONSTRAINT FK_3369BBB32576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1FA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE apply ADD CONSTRAINT FK_BD2F8C1F2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045575E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28598C03F15C FOREIGN KEY (employee_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285975E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859AA23F6C8 FOREIGN KEY (next_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE contract_invoice ADD CONSTRAINT FK_98346A8C2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract_invoice ADD CONSTRAINT FK_98346A8C2989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A768C03F15C FOREIGN KEY (employee_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76C54C8C93 FOREIGN KEY (type_id) REFERENCES document_type (id)');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `like` ADD CONSTRAINT FK_AC6340B3A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `like` ADD CONSTRAINT FK_AC6340B32576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_user ADD CONSTRAINT FK_35AF9D73EF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_user ADD CONSTRAINT FK_35AF9D73A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_seen ADD CONSTRAINT FK_1C6E4122EF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification (id)');
        $this->addSql('ALTER TABLE notification_seen ADD CONSTRAINT FK_1C6E4122A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709E2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE time_sheet ADD CONSTRAINT FK_C24E709EA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D649E15AD669 FOREIGN KEY (usercontract_id) REFERENCES user_contract (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D64919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE user_town ADD CONSTRAINT FK_36678B6DA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_town ADD CONSTRAINT FK_36678B6D75E23604 FOREIGN KEY (town_id) REFERENCES town (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB3A76ED395');
        $this->addSql('ALTER TABLE accepted_contract DROP FOREIGN KEY FK_3369BBB32576E0FD');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1FA76ED395');
        $this->addSql('ALTER TABLE apply DROP FOREIGN KEY FK_BD2F8C1F2576E0FD');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045575E23604');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285919EB6921');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28598C03F15C');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859BE04EA9');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285975E23604');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859AA23F6C8');
        $this->addSql('ALTER TABLE contract_invoice DROP FOREIGN KEY FK_98346A8C2576E0FD');
        $this->addSql('ALTER TABLE contract_invoice DROP FOREIGN KEY FK_98346A8C2989F1FD');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EA76ED395');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A768C03F15C');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76C54C8C93');
        $this->addSql('ALTER TABLE experience DROP FOREIGN KEY FK_590C103A76ED395');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744A76ED395');
        $this->addSql('ALTER TABLE `like` DROP FOREIGN KEY FK_AC6340B3A76ED395');
        $this->addSql('ALTER TABLE `like` DROP FOREIGN KEY FK_AC6340B32576E0FD');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA2576E0FD');
        $this->addSql('ALTER TABLE notification_user DROP FOREIGN KEY FK_35AF9D73EF1A9D84');
        $this->addSql('ALTER TABLE notification_user DROP FOREIGN KEY FK_35AF9D73A76ED395');
        $this->addSql('ALTER TABLE notification_seen DROP FOREIGN KEY FK_1C6E4122EF1A9D84');
        $this->addSql('ALTER TABLE notification_seen DROP FOREIGN KEY FK_1C6E4122A76ED395');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709E2576E0FD');
        $this->addSql('ALTER TABLE time_sheet DROP FOREIGN KEY FK_C24E709EA76ED395');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649E15AD669');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64919EB6921');
        $this->addSql('ALTER TABLE user_town DROP FOREIGN KEY FK_36678B6DA76ED395');
        $this->addSql('ALTER TABLE user_town DROP FOREIGN KEY FK_36678B6D75E23604');
        $this->addSql('DROP TABLE accepted_contract');
        $this->addSql('DROP TABLE apply');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE contract_invoice');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE document_type');
        $this->addSql('DROP TABLE experience');
        $this->addSql('DROP TABLE invoice');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE `like`');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE notification_user');
        $this->addSql('DROP TABLE notification_seen');
        $this->addSql('DROP TABLE password_reset');
        $this->addSql('DROP TABLE time_sheet');
        $this->addSql('DROP TABLE town');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_town');
        $this->addSql('DROP TABLE user_contract');
    }
}
