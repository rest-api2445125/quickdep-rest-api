<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109162334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contract_invoice (contract_id INT NOT NULL, invoice_id INT NOT NULL, INDEX IDX_98346A8C2576E0FD (contract_id), INDEX IDX_98346A8C2989F1FD (invoice_id), PRIMARY KEY(contract_id, invoice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contract_invoice ADD CONSTRAINT FK_98346A8C2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract_invoice ADD CONSTRAINT FK_98346A8C2989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F28592454BA75');
        $this->addSql('DROP INDEX IDX_E98F28592454BA75 ON contract');
        $this->addSql('ALTER TABLE contract DROP invoices_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE contract_invoice');
        $this->addSql('ALTER TABLE contract ADD invoices_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28592454BA75 FOREIGN KEY (invoices_id) REFERENCES invoice (id)');
        $this->addSql('CREATE INDEX IDX_E98F28592454BA75 ON contract (invoices_id)');
    }
}
