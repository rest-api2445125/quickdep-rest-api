<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221130234058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_contract (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, signature_path VARCHAR(255) DEFAULT NULL, date DATE NOT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client ADD town_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045575E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('CREATE INDEX IDX_C744045575E23604 ON client (town_id)');
        $this->addSql('ALTER TABLE time_sheet ADD startdate DATETIME NOT NULL, ADD enddate DATETIME NOT NULL, DROP start_hour, DROP end_hour');
        $this->addSql('ALTER TABLE user ADD usercontract_id INT DEFAULT NULL, ADD type VARCHAR(255) NOT NULL, DROP signature_path');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E15AD669 FOREIGN KEY (usercontract_id) REFERENCES user_contract (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E15AD669 ON user (usercontract_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D649E15AD669');
        $this->addSql('DROP TABLE user_contract');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045575E23604');
        $this->addSql('DROP INDEX IDX_C744045575E23604 ON client');
        $this->addSql('ALTER TABLE client DROP town_id');
        $this->addSql('ALTER TABLE time_sheet ADD start_hour TIME NOT NULL, ADD end_hour TIME NOT NULL, DROP startdate, DROP enddate');
        $this->addSql('DROP INDEX UNIQ_8D93D649E15AD669 ON `user`');
        $this->addSql('ALTER TABLE `user` ADD signature_path VARCHAR(255) DEFAULT NULL, DROP usercontract_id, DROP type');
    }
}
