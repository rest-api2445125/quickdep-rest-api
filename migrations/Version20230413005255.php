<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230413005255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE antecedent_value');
        $this->addSql('ALTER TABLE contract ADD next_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859AA23F6C8 FOREIGN KEY (next_id) REFERENCES contract (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E98F2859AA23F6C8 ON contract (next_id)');
        $this->addSql('ALTER TABLE experience DROP FOREIGN KEY FK_590C103BE04EA9');
        $this->addSql('DROP INDEX IDX_590C103BE04EA9 ON experience');
        $this->addSql('ALTER TABLE experience ADD title VARCHAR(255) NOT NULL, DROP job_id, CHANGE time time VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE notification ADD date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user ADD notworked INT DEFAULT NULL, ADD completed INT DEFAULT NULL, ADD canceled INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE antecedent_value (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, bool TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859AA23F6C8');
        $this->addSql('DROP INDEX UNIQ_E98F2859AA23F6C8 ON contract');
        $this->addSql('ALTER TABLE contract DROP next_id');
        $this->addSql('ALTER TABLE experience ADD job_id INT DEFAULT NULL, DROP title, CHANGE time time INT NOT NULL');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C103BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_590C103BE04EA9 ON experience (job_id)');
        $this->addSql('ALTER TABLE notification DROP date');
        $this->addSql('ALTER TABLE `user` DROP notworked, DROP completed, DROP canceled');
    }
}
