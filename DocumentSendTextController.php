<?php

namespace App\Controller;

use App\Configuration\DocumentStatus;
use App\Entity\Document;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DocumentSendTextController extends AbstractController
{
    public function __invoke(Document $data, Request $request)
    {
        $text = $request->get('text');
        $data->setText($text);

        $data->setStatut(DocumentStatus::$WAITING);

        $data->setUpdatedAt(new DateTimeImmutable());

        return $data;

        return [
            'success' => true,
            'message' => 'Document enrégistré avec succès',
            'document' => $data
        ];
    }
}
