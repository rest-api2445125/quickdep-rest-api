# QuickDep

QuickDep est une application web/mobile permettant d'automatiser le processus de recommandantion des employés à des entréprises.

Dans ce dépot se trouve l'API de l'application.

## Fonctionnalités

- Gestion des client (Entréprises)
- Gestion des contrats
- Gestion et suivi des employés
- Un tableau de bord administrateur intuitif

## Prerequis Environement

Pour lancer ce projet, vous devez avoir certaines librairies d'installées dans votre pc, entre autre :

`PHP >= 8` avec la possibilité de s'exécuter en ligne de commande

`Composer`

`MySQL`

## Lancer le projet en local

Clonner le projet sur github

```bash
  git clone https://github.com/dmwema/QuickDep-API.git
```

Se dirriger dans le dossier du projet

```bash
  cd QuickDep-API
```

Installer dependences

```bash
  composer install
```

Après avoir installé les dépendances, vous devez modifier le fichier .env en y ajoutant les information de la base de données tout en le decommentant (enlever le # au début) avant

```bash
  DATABASE_URL="mysql://username:password@127.0.0.1:3306/quickdep?serverVersion=mariadb-10.4.17"
```

Remplacer `mariadb-10.4.17` Par la version installée de mariadb ou mysql, `username` par le nom d'utilisateur de mysql/mysql et `password` par le mot de passe de l'utilisateur de la base de données.

Une fois cela fait, créer la base de donnée en exécutant la commande suivante:

```bash
  php bin/console doctrine:database:create
```

Créer les migration et les exécuter

```bash
  php bin/console make:migration
```

Lancer le serveur interne de php

```bash
  php -S localhost:8000 -t public
```

Rendez vous sur http://localhost:8000/api dans un navigateur web
et si tout s'est bien passé, vous serez face à la documentation et les endpoints de l'API

## Feedback

Si vous avez des question ou des suggestions à nous faire, contactez-nous sur contact@quickdep.ca
