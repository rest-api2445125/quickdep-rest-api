<?php

namespace App\Form\ClientWeb;

use App\Entity\Town;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('owner', TextType::class)
            ->add('phone', TextType::class)
            ->add('address', TextType::class)
            ->add(
                'town', 
                EntityType::class,
                [
                    'class' => Town::class,
                    'choice_label' => 'name',
                ]
            )
            ->add('zipcode', TextType::class)
            ->add('facturezipcode', TextType::class, [
                'required' => false
            ])
            ->add('factureaddress', TextType::class, [
                'required' => false
            ])
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('passwordconfirm', PasswordType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'S\'inscrire',
            ])
            ->setMethod('POST');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
