<?php

namespace App\Form\ClientWeb;

use App\Entity\TimeSheet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeSheetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startdate', TimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('enddate', TimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('pause', ChoiceType::class, [
                'choices'  => [
                    'Pas de pause' => null,
                    '10min' => 10,
                    '15min' => 15,
                    '20min' => 20,
                    '25min' => 25,
                    '30min' => 30,
                    '35min' => 35,
                    '40min' => 40,
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer et Approuver',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TimeSheet::class,
        ]);
    }
}
