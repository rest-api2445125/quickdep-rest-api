<?php

namespace App\Form\Admin;

use App\Configuration\ContractConfig;
use App\Entity\Client;
use App\Entity\Contract;
use App\Entity\Job;
use App\Entity\Town;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', TextareaType::class, [
                'required'   => false,
            ])
            ->add('startdate', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('enddate', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('cloth', TextType::class, [
                'required'   => false,
            ])
            ->add('parking', CheckboxType::class, [
                'label' => 'Stationnement',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'required'   => false,
            ])
            ->add('pause', ChoiceType::class, [
                'choices'  => [
                    'Pas de pause' => 0,
                    '10min' => 10,
                    '15min' => 15,
                    '20min' => 20,
                    '25min' => 25,
                    '30min' => 30,
                    '35min' => 35,
                    '40min' => 40,
                ],
            ])
            ->add('bonus', ChoiceType::class, [
                'choices'  => [
                    'Pas de bonus' => 0,
                    '1$/H' => 1,
                    '2$/H' => 2,
                    '3$/H' => 3,
                    '4$/H' => 4,
                    '5$/H' => 5,
                    '6$/H' => 6,
                    '7$/H' => 7,
                    '8$/H' => 8,
                    '9$/H' => 9,
                    '10$/H' => 10,
                ],
            ])
            ->add('job', EntityType::class, [
                'class' => Job::class,
                'choice_label' => function($job) {
                    return $job->getTitle() . " (" . $job->getRate() + ContractConfig::$CLIENT_MERGE . "$/H)";
                },
            ], [
                'required'   => false,
            ])
            ->add('town', EntityType::class, [
                'required' => false,
                'class' => Town::class,
                'choice_label' => 'name',
            ])
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'name',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer et publier',
            ])
            ->setMethod('POST')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}