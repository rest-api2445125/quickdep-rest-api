<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Entity\Document;
use App\Entity\DocumentType;
use App\Entity\User;
use App\Entity\UserContract;
use App\Repository\UserContractRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;
    private $_passwordEncoder;
    private $_userContractRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordEncoder,
        UserContractRepository $userContractRepository
    ) {
        $this->_passwordEncoder = $passwordEncoder;
        $this->_entityManager = $entityManager;
        $this->_userContractRepository = $userContractRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    /**
     * @param User $data
     */
    public function persist($data, array $context = [])
    {

        if ($data->getPlainPassword()) {
            $data->setPassword(
                $this->_passwordEncoder->hashPassword(
                    $data,
                    $data->getPlainPassword()
                )
            );

            $data->eraseCredentials();
        }

        $user_contract = $data->getUsercontract();

        if ($user_contract == null) {
            $user_contract = new UserContract();
            $user_contract->settype($data->getType());
            $user_contract->setDate(new DateTime());
            $user_contract->setUpdatedAt(new DateTimeImmutable());

            $this->_entityManager->persist($user_contract);
            $this->_entityManager->flush();

            $data->setUsercontract($user_contract);
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}
