<?php

namespace App\Components;

use App\Components\Contract as ComponentsContract;
use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Apply;
use App\Entity\Contract as EntityContract;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use App\Entity\Contract;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class EmployeeCard
{
    public String $image;
    public String $name;
    public String $email;
    public String $phone;
    public String $applyStatus = '';
    public bool $applied = false;
    public bool $showDetails = false;
    public $experiences = [];
    public $stats = [];
    public ?Apply $apply = null;
    Public Contract $contract;

    public function __construct(
        private UserRepository $userRepository
    ) {}

    public function mount(User $employee, Contract $contract)
    {
        $this->contract = $contract;
        $this->image = $employee->getImagePath();
        $this->name = $employee->getFirstname() . " " . $employee->getLastname();
        $this->email = $employee->getEmail();
        $this->phone = $employee->getPhone();
        foreach ($employee->getExperiences() as $experience) {
            $exp = [];
            $exp["name"] = $experience->getTitle();
            $exp["time"] = $experience->getTime();
        }
        $this->showDetails = $contract->getStatus() != ContractStatus::$PUBLISHED;
        $this->stats['completed'] = $employee->getCompleted();
        $this->stats['canceled'] = $employee->getCanceled();
        $this->stats['notWorked'] = $employee->getNotworked();

        if ($contract->getAppliesCount() != null && $contract->getAppliesCount() > 0) {
            foreach ($contract->getApplies() as $apply) {
                if ($employee == $apply->getUser()) {
                    $this->applied = true;
                    $this->apply = $apply;
                    if ($contract->getStatus() == ContractStatus::$PUBLISHED && $contract->getNext() != null) {
                        if ($apply->isConsecutive() and ($apply->getConsecutivecount() == null or $apply->getConsecutivecount() == 0)) {
                            $this->applyStatus = 'Postulé à tous les shifts';
                        } else if (!$apply->isConsecutive()) {
                            if ($apply->getStartdate() != null) {
                                $this->applyStatus = 'Postulé au shift du ' . ContractConfig::full_date($apply->getStartdate());
                            }
                        } else {
                            if ($apply->getStartdate() != null && $apply->getEnddate() != null) {
                                $this->applyStatus = 'Postulé aux shifts du ' . ContractConfig::full_date($apply->getStartdate()) . " au " . ContractConfig::full_date($apply->getEnddate());
                            }
                        }
                    }
                }
            }
        }

    }
}