<?php

namespace App\Components;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Contract as EntityContract;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use App\Entity\Contract as AppEntityContract;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class Contract
{
    public String $image;
    public String $title;
    public bool $consecutive = false;
    public bool $today = false;
    public bool $applied = false;
    public bool $approuved = false;
    public String $status = '';
    public String $rate;
    public String $bonus;
    public String $date;
    public String $hours;
    public String $path;
    public String $client;
    public EntityContract $contract;

    public function __construct(
        private UserRepository $userRepository,
    ) {}

    public function mount(EntityContract $contractData, User $user)
    {
        $session = new Session();

        if ($session->get('type') == 'admin') {
            $this->path = 'app_admin_shift_detail';
        } else {
            $this->path = 'app_client_web_shift_detail';
        }

        $this->contract = $contractData;
        $this->image = $this->userRepository->findOneBy([
            'client' => $contractData->getClient()
        ])->getImagePath();

        if ($this->image == null) {
           $this->image = 'clientweb/img/logo.png'; 
        } else {
            $this->image = 'users/images/' . $this->image;;
        }

        $this->client = $contractData->getClient()->getName();

        $this->consecutive = self::isConsecutive($contractData);

        $this->title = $contractData->getJob()->getTitle();
        
        // date
        $today = new DateTime();
        if ($this->consecutive) {
            $end = 
            $this->date = "Du " . ContractConfig::full_date($contractData->getStartdate(), false, true) . " au " . ContractConfig::full_date(self::endDate($contractData), false, true);
        } else {
            if ($contractData->getStartdate()->format('d-m-Y') == $today->format('d-m-Y')) {
                $this->today = true;
                $this->date = "Aujourd'hui";
            } else {
                $this->date = ContractConfig::full_date($contractData->getStartdate(), false, true);
            }
        }

        if ($contractData->getAppliesCount() != null && $contractData->getAppliesCount() > 0) {
            $this->status = "En attente d'approbation";
        } 

        if ($contractData->getStatus() == ContractStatus::$APPROUVED) {
            $this->status = "Validé";
        }

        $this->hours = "De " . $contractData->getStartdate()->format('G:i')
            . " à " . $contractData->getEnddate()->format('G:i')
        ;

        $this->applied = 
            $contractData->getStatus() == ContractStatus::$PUBLISHED
            && $contractData->getAppliesCount() != null
            && $contractData->getAppliesCount() > 0
        ;

        $this->approuved = $contractData->getStatus() == ContractStatus::$APPROUVED;

        $this->rate = $contractData->getClientrate() . "$/H";
        $this->bonus = $contractData->getBonus() ? $contractData->getBonus() . "$/H" : '';

    }

    private static function endDate(EntityContract $contract): DateTime {
        $end = $contract->getEnddate();
        if (self::isConsecutive($contract)) {
            return self::endDate($contract->getNext());
        }
        return $end;
    }

    private static function isConsecutive (AppEntityContract $contractData): bool {
        if (
            $contractData->getNext() != null 
            && $contractData->getNext()->getStatus() == $contractData->getStatus()
        ) {
            if ($contractData->getStatus() == ContractStatus::$PUBLISHED) {
                return true;
            } else {
                if ($contractData->getEmployee() == $contractData->getNext()->getEmployee()) {
                    return true;
                }
            }
        }
        return false;
    }
}