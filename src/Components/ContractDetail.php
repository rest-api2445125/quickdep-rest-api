<?php

namespace App\Components;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Contract as EntityContract;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class ContractDetail
{
    public String $image;
    public String $title;
    public String $client;
    public bool $consecutive = false;
    public bool $today = false;
    public bool $applied = false;
    public bool $approuved = false;
    public String $status = '';
    public String $rate;
    public String $bonus;
    public String $date;
    public String $hours;
    public String $parking;
    public String $pause;
    public String $address;
    public EntityContract $contract;
    public $applies = [];

    public function __construct(
        private UserRepository $userRepository
    ) {}

    public function mount(EntityContract $contractData, User $user)
    {
        $this->contract = $contractData;
        $this->image = $this->userRepository->findOneBy([
            'client' => $contractData->getClient()
        ])->getImagePath();

        if (
            $contractData->getNext() != null 
            && $contractData->getNext()->getStatus() == $contractData->getStatus()
        ) {
            if ($contractData->getStatus() == ContractStatus::$PUBLISHED) {
                $this->consecutive = true;
            } else {
                if ($contractData->getEmployee() == $contractData->getNext()->getEmployee()) {
                    $this->consecutive = true;
                }
            }
        }

        $this->title = $contractData->getJob()->getTitle();
        
        // date
        $today = new DateTime();
        if ($contractData->getStartdate()->format('d-m-Y') == $today->format('d-m-Y')) {
            $this->today = true;
            $this->date = "Aujourd'hui";
        } else {
            $this->date = ContractConfig::full_date($contractData->getStartdate(), true, true);
        }

        if ($this->consecutive) {
            $this->date = "Du " . ContractConfig::full_date($contractData->getStartdate(), false, true)
                . " au " . ContractConfig::full_date($contractData->getEnddate(), false, true)
            ;
        }

        if ($contractData->getAppliesCount() != null && $contractData->getAppliesCount() > 0) {
            $this->status = "En attente d'approbation";
        } 

        if ($contractData->getStatus() == ContractStatus::$APPROUVED) {
            $this->status = "Validé";
        }

        $this->hours = "De " . $contractData->getStartdate()->format('G:i')
            . " à " . $contractData->getEnddate()->format('G:i')
        ;

        $this->applied = 
            $contractData->getStatus() == ContractStatus::$PUBLISHED
            && $contractData->getAppliesCount() != null
            && $contractData->getAppliesCount() > 0
        ;

        $this->client = $contractData->getClient()->getName();

        $this->approuved = $contractData->getStatus() == ContractStatus::$APPROUVED;

        if ($contractData->getPause() == null) {
            $this->pause = "Pas de pause";
        } else {
            $this->pause = $contractData->getPause() . "min de pause.";
        }

        if ($contractData->isParking() != true) {
            $this->parking = "Pas de stationnement.";
        } else {
            $this->parking = "Stationnement permis.";
        }
        $town = $contractData->getTown();
        if ($town == null) {
            $town = $contractData->getClient()->getTown();
        }
        $this->address = $contractData->getAddress() . ", " . $town->getName();

        $this->rate = $contractData->getClientrate() . "$/H";
        $this->bonus = $contractData->getBonus() ? $contractData->getBonus() . "$/H" : '';

    }
}