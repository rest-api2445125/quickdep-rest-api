<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Contract;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        /*for ($i = 0; $i < 5; $i++) {
            $client = new Client();
            $client->setName($this->faker->company())
                ->setEmail('test@company.ca')
                ->setPhone($this->faker->e164PhoneNumber())
                ->setAddress($this->faker->address());

            $manager->persist($client);
            $manager->flush();
        }
        */
        for ($i = 0; $i < 20; $i++) {
            $client_repository = $manager->getRepository(Client::class);
            $client = $client_repository->findOneBy(['id' => rand(1, 5)]);

            $contract = new Contract();
            $contract->setClient($client)
                ->setTitle($this->faker->jobTitle())
                ->setDescription($this->faker->realTextBetween($minNbChars = 50, $maxNbChars = 200, $indexSize = 2))
                ->setRequirement($this->faker->realTextBetween($minNbChars = 150, $maxNbChars = 200, $indexSize = 2))
                ->setHoure(rand(1, 5))
                ->setHourlyRate(rand(10, 25))
                ->setDate(new DateTime());

            $manager->persist($contract);
            $manager->flush();
        }

        $manager->flush();
    }
}
