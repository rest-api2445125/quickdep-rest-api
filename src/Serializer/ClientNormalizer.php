<?php

namespace App\Serializer;

use App\Entity\Client;
use App\Repository\UserRepository;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Vich\UploaderBundle\Storage\StorageInterface;

class ClientNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'AppClientNormalizerAlreadyCalled';

    private $storage;
    private $_userRepository;

    public function __construct(StorageInterface $storage, UserRepository $userRepository)
    {
        $this->storage = $storage;
        $this->_userRepository = $userRepository;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return !isset($context[self::ALREADY_CALLED]) && $data instanceof Client;
    }

    /**
     * @param Document $object
     */
    public function normalize(mixed $object, ?string $format = null, array $context = [])
    {
        $user = $this->_userRepository->findOneBy(
            ["client" => $object]
        );
        $object->setImageUrl($this->storage->resolveUri($user, 'image'));
        if ($user != null) {
            $object->setUserId($user->getId());
            $object->setEmail($user->getEmail());
        }
        $context[self::ALREADY_CALLED] = true;
        return $this->normalizer->normalize($object, $format, $context);
    }
}
