<?php

namespace App\Serializer;

use App\Configuration\ContractStatus;
use App\Entity\Contract;
use App\Repository\UserRepository;
use DateTime;
use DateTimeInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Vich\UploaderBundle\Storage\StorageInterface;

class ContractNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'AppContractNormalizerAlreadyCalled';

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return !isset($context[self::ALREADY_CALLED]) && $data instanceof Contract;
    }

    /**
     * @param Contract $object
     */
    public function normalize(mixed $object, ?string $format = null, array $context = [])
    {
        if (
            $object->getNext() != null
            && ($object->getStatus() == ContractStatus::$PUBLISHED || $object->getStatus() == ContractStatus::$APPROUVED)
            && $object->getNext()->getStatus() === $object->getStatus()
        ) {
            if ($object->getApplyconsecutivecount()) {
                $object->setEnddate($this->lastDate($object, $object->getApplyconsecutivecount()));
            } else {
                $object->setEnddate($this->lastDate($object));
            }
        }
        $object->setStartdate(new DateTime($object->getStartdate()->format("Y-m-d H:i:s")));
        $object->setEnddate(new DateTime($object->getEnddate()->format("Y-m-d H:i:s")));

        $context[self::ALREADY_CALLED] = true;
        return $this->normalizer->normalize($object, $format, $context);
    }

    private function lastDate(Contract $contract, $limit = NULL, $current = 1): DateTimeInterface
    {
        $next = $contract->getNext();
        if ($next->getNext() != null) {
            if ($limit != null) {
                if ($current  == $limit) {
                    return $next->getEnddate();
                } else {
                    return $this->lastDate($next, $limit, $current + 1);
                }
            }
            return $this->lastDate($next);
        }
        return $next->getEnddate();
    }
}
