<?php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class TownsListingController extends AbstractController
{
    public function __invoke(Paginator $data, Request $request, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        if ($user == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", 1);
        }
        $users_towns = $user->getTown();

        foreach ($data as $town) {
            foreach ($users_towns as $users_town) {
                if ($town->getId() == $users_town->getId()) {
                    $town->setSelected(true);
                }
            }
        }

        return $data;
    }
}
