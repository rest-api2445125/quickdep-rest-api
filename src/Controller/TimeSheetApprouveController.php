<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Configuration\TimeSheetStatus;
use App\Entity\TimeSheet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TimeSheetApprouveController extends AbstractController
{
    public function __invoke(TimeSheet $data, EntityManagerInterface $em)
    {
        $data->setStatus(TimeSheetStatus::$APPROUVED);
        $data->getContract()->setStatus(ContractStatus::$TIME_APPROUVED);

        $employee = $data->getContract()->getEmployee();

        if ($employee->getCompleted() != null)
            $employee->setCompleted($employee->getCompleted() + 1);
        else
            $employee->setCompleted(1);

        $em->persist($employee);
        $em->flush();

        return $data;
    }
}
