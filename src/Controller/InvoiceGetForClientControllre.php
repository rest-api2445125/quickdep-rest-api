<?php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Configuration\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class InvoiceGetForClientControllre extends AbstractController
{
    public function __invoke(array $data, Request $request)
    {
        $return_data = [];
        foreach ($data as $invoice) {
            if ($invoice->getUser()->getType() == UserType::$SOCIETY) {
                $return_data[] = $invoice;
            }
        }
        return $return_data;
    }
}
