<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Contract;
use App\Entity\User;
use App\Repository\ApplyRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ContractListingController extends AbstractController
{
    public function __invoke(array $data, UserRepository $userRepository, ApplyRepository $applyRepository)
    {
        if ($this->getUser() == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", "Vous devez vous connecter pour voir les contrats");
        }

        /**
         * @var User
         */
        $user = $this->getUser();

        $user_towns = $user->getTown();

        $validate_data = [];

        foreach ($data as $contract) {
            if ($contract->isPublic($user, $applyRepository)) {
                if (!in_array("ROLE_ADMIN", $user->getRoles())) {
                    $in_towns = false;
                    foreach ($user_towns as $town) {
                        if ($town->getId() == $contract->getTown()->getId()) {
                            $in_towns = true;
                        }
                    }
                    if ($in_towns) {
                        if (!$this->appliedAll($contract, $user, $applyRepository))
                            $validate_data[] = $contract;
                    }
                } else {
                    $validate_data[] = $contract;
                }
            }
        }

        return [
            "contracts" => ContractConfig::listing($validate_data, $this->getUser()),
            "user" => $userRepository->find($user->getId())
        ];
    }

    private function appliedAll(Contract $contract, User $user, ApplyRepository $applyRepository)
    {
        if ($contract->getParent() != null) {
            return $this->appliedAll($contract->getParent(), $user, $applyRepository);
        }

        $apply = $applyRepository->findOneBy([
            'user' => $user,
            'contract' => $contract
        ]);

        if ($apply != null && $apply->isConsecutive() && ($apply->getConsecutivecount() == 0 || $apply->getConsecutivecount() == null)) {
            return true;
        }

        return false;
    }
}
