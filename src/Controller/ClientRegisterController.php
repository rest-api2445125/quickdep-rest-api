<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\UserType;
use App\Entity\Client;
use App\Entity\Device;
use App\Entity\UserContract;
use App\Repository\ClientRepository;
use App\Repository\DeviceRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ClientRegisterController extends AbstractController
{
    public function __invoke(
        Client $data,
        TownRepository $townRepository,
        DeviceRepository $deviceRepository,
        EntityManagerInterface $em,
        JWTTokenManagerInterface $jWTTokenManager,
        UserPasswordHasherInterface $encoder,
        UserRepository $userRepository,
        Request $request
    ) {
        $request_datas = json_decode(
            preg_replace(
                '/[\x00-\x1F\x80-\xFF]/',
                '',
                $request->getContent()
            ),
            true
        );

        $email = $request_datas['email'];
        $password = $request_datas['password'];

        $devicetoken = $request_datas["device"];

        $town_id = $request_datas["town_id"];


        $exist = $userRepository->findOneBy(["email" => $email]);

        if ($exist != null) {
            return [
                "success" => false,
                "message" => "Un compte existe déjà avec cette adresse mail."
            ];
        }

        $user = $userRepository->createNewUser([
            'email' => $email,
            'password' => $password,
            'type' => UserType::$SOCIETY
        ], $em, $encoder);

        $user = ContractConfig::clientRegister(
            $data, $townRepository, $town_id, $user, $userRepository, $em, $devicetoken, $deviceRepository
        );

        $token = $jWTTokenManager->create($user);
        

        return [
            "success" => true,
            'id' => $user->getId(),
            'token' => $token,
            'type' => UserType::$SOCIETY,
            'user' => $user
        ];
    }
}
