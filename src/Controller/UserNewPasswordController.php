<?php

namespace App\Controller;

use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserNewPasswordController extends AbstractController
{
    public function __invoke(User $data, Request $request, UserPasswordHasherInterface $encoder, EntityManagerInterface $manager)
    {
        $plain_password = $data->getPassword();
        $data->setPassword($encoder->hashPassword($data, $plain_password));
        $data->setNew(false);

        $manager->persist($data);
        $manager->flush();

        return [
            "success" => true,
            "message" => "Mot de passe modifié avec succès"
        ];
    }
}
