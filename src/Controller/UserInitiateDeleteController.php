<?php

namespace App\Controller;

use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserInitiateDeleteController extends AbstractController
{
    public function __invoke(User $data, EntityManagerInterface $em)
    {
        $data->setInitdelete(new DateTime());
        $em->persist($data);
        $em->flush();

        return [
            'success' => true
        ];  
    }
}
