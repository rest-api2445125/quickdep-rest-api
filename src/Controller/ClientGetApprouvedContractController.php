<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Client;
use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class ClientGetApprouvedContractController extends AbstractController
{
    public function __invoke(Client $data, ContractRepository $contractRepository)
    {
        return ContractConfig::contractApprouved(
            $data, $contractRepository
        );
    }
}
