<?php

namespace App\Controller;

use App\Configuration\Notification;
use App\Configuration\NotificationConfig;
use App\Configuration\NotificationType;
use App\Entity\Client;
use App\Entity\Contract;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Twilio\Rest\Client as TwilioClient;

class ContractBonusController extends AbstractController
{
    public function __invoke(Contract $data, Request $request, EntityManagerInterface $em, MailerInterface $mailer, UserRepository $userRepository, HttpClientInterface $httpClient, DeviceRepository $deviceRepository)
    {
        $em->persist($data);
        $em->flush();

        $twilioClient = new TwilioClient(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        NotificationConfig::create(
            $data,
            $twilioClient,
            NotificationType::$BONUS,
            $em,
            $userRepository,
            $httpClient,
            $deviceRepository
        );

        return [
            "success" => true,
            "Message" => "Bonus ajouté avec succès"
        ];
    }
}
