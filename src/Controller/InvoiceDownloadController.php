<?php

namespace App\Controller;

use App\Configuration\UserType;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceDownloadController extends AbstractController
{
    public function __invoke(Invoice $data, MailerInterface $mailer, Request $request)
    {
        $email = $data->getUser()->getEmail();
        $template = "email/pay.html.twig";
        $template_datas = [
            "invoice" => $data,
            "startdate" => date('Y-m-d', strtotime('-7 day', strtotime($data->getDate()->format("Y-m-d")))),
            "name" => $data->getUser()->getType() == UserType::$EMPLOYEE ? $data->getUser()->getLastname() . " " . $data->getUser()->getFirstname() : $data->getUser()->getClient()->getName(),
            "society" => $data->getUser()->getType() == UserType::$EMPLOYEE ? "" : $data->getUser()->getClient()->getName(),
            "asked" => true
        ];
        $subject = "Vous avez une nouvelle facture de QuickDep";

        if ($data->getUser()->getType() == UserType::$EMPLOYEE) {
            $file_path = "invoices/employee.xlsx";
            $from = "facturation@quickdep.ca";
        } else {
            $file_path = "invoices/client.xlsx";
            $from = "facturation@quickdep.ca";
        }

        $pdf_path = "invoices/pdf/" . $data->getCode() . ".pdf";

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject, [
                "path" => $pdf_path,
                "name" => "Facture " . $data->getCode()
            ], $from);
        }

        return [
            "success" => true,
            "message" => "Fichier envoyé par mail avec succès"
        ];
    }
}
