<?php

namespace App\Controller\Admin;

use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Configuration\UserType;
use App\Entity\Town;
use App\Entity\User;
use App\Form\Admin\EmployeeType;
use App\Repository\ClientRepository;
use App\Repository\ContractRepository;
use App\Repository\DocumentRepository;
use App\Repository\DocumentTypeRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/admin/travailleurs', name: 'app_admin_user')]
    public function index(
        UserRepository $userRepository,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $users = $userRepository->findBy([
            'type' => UserType::$EMPLOYEE
        ]);

        $return_users = [];
        $validated = [];
        $waiting = [];

        foreach ($users as $user) {
            if (!in_array("ROLE_ADMIN", $user->getRoles())) {
                $return_users[] = $user;
                if (
                    $user->isSigned()
                    && $user->getDocumentsStatus() == DocumentStatus::$VALIDATED
                    && $user->isEmailVerified()
                    && $user->isPhoneverified()
                ) {
                    $validated[] = $user;
                } else {
                    $waiting[] = $user;
                }
            }
        }
        return $this->render('admin/user/index.html.twig', [
            'all' => $return_users,
            'validated' => $validated,
            'waiting' => $waiting,
        ]);
    }

    #[Route('/admin/delete-user-{id}', name: 'app_user_delete_user', methods: ['POST'])]
    public function deleteUser(
        Request $request, 
        UserRepository $userRepository,
        EntityManagerInterface $em,
        DocumentRepository $documentRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $employee = $userRepository->find($request->get('id'));

        $documents = $documentRepository->findBy([
            'employee' => $employee
        ]);

        foreach ($documents as $document) {
            $document->setEmployee(null);
            $em->persist($document);
        }

        $em->remove($employee);
        $em->flush();

        $this->addFlash('success', 'Travailleur supprimé avec succès');

        return $this->redirectToRoute('app_admin_user');

    }

    #[Route('/admin/employee/new', name: 'app_user_new_user', methods: ['POST', 'GET'])]
    public function newUser(
        Request $request, 
        EntityManagerInterface $em,
        UserPasswordHasherInterface $encoder,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        
        $employeeForm = $this->createForm(EmployeeType::class);
        $employeeForm->handleRequest($request);

        $employee = new User();
        
        if ($employeeForm->isSubmitted() && $employeeForm->isValid()) {
            $data = $employeeForm->getData();

            if ($data['password'] != $data['password_confirm']) {
                $this->addFlash('notice', 'Les deux mots de passes ne correspondent pas');
                return $this->redirectToRoute('app_user_new_user');
            }

            $employee = $userRepository->createNewUser(
                [
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'type' => UserType::$EMPLOYEE
                ],
                $em,
                $encoder
            )
                ->setFirstname($data['firstname'])
                ->setLastname($data['lastname'])
                ->setWork($data['work'])
                ->setBirthday($data['birthday'])
                ->setAddress($data['address'])
                ->setPhone($data['phone'])
            ;

            $em->persist($employee);
            $em->flush();

            return $this->redirectToRoute('app_user_town', [
                'id' => $employee->getId()
            ]);
        }


        return $this->render('admin/user/new.html.twig', [
            'employeeForm' => $employeeForm->createView(),
            'edit' => false,
            'employee' => $employee
        ]);

    }

    #[Route('/admin/town/{id}', name: 'app_user_town', methods: ['POST', 'GET'])]
    public function town(
        $id,
        Request $request, 
        TownRepository $townRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $towns = $townRepository->findAll();
        $employee = $userRepository->find($id);

        $twonFormBuilder = $this->createFormBuilder();
        $twonFormBuilder
            ->add('submit', SubmitType::class, [
                'label' => 'Continuer'
            ])
        ;
        
        $twonForm = $twonFormBuilder->getForm();

        $twonForm->handleRequest($request);
        
        if ($twonForm->isSubmitted() && $twonForm->isValid()) {
            foreach ($towns as $town) {
                $employee->removeTown($town);
            }

            foreach ($_POST as $town_id => $value) {
                if ($value == "on") {
                    $employee->addTown(
                        $townRepository->find($town_id)
                    );
                }
            }

            $em->persist($employee);
            $em->flush();

            return $this->redirectToRoute('app_admin_user');
        }

        return $this->render('admin/user/town.html.twig', [
            'towns' => $towns,
            'townForm' => $twonForm->createView(),
            'employee' => $employee
        ]);

    }

    #[Route('/admin/documents/{id}', name: 'app_user_documents', methods: ['POST', 'GET'])]
    public function documents(
        $id,
        Request $request, 
        DocumentTypeRepository $documentTypeRepository,
        UserRepository $userRepository,
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $types = $documentTypeRepository->findAll();
        $employee = $userRepository->find($id);

        $builder = $this->createFormBuilder();
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Continuer'
            ])
        ;

        foreach ($types as $type) {
            $builder
                ->add($type->getId(), $type->getType() == 'text' ? TextType::class : FileType::class, [
                    'label' => $type->getTitle()
                ])
            ;
        }
        
        $form = $builder->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            dd($data);

            // $em->persist($employee);
            // $em->flush();

            return $this->redirectToRoute('app_admin_user');
        }

        return $this->render('admin/user/document.html.twig', [
            'types' => $types,
            'employee' => $employee,
            'form' => $form->createView()
        ]);

    }

    #[Route('/admin/travailleur/{id}', name: 'app_user_detail', methods: ['POST', 'GET'])]
    public function detail(
        $id,
        Request $request, 
        DocumentTypeRepository $documentTypeRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $employee = $userRepository->find($id);


        $infoBuilder = $this->createFormBuilder([
            'firstname' => $employee->getFirstname(),
            'lastname' => $employee->getLastname(),
            'email' => $employee->getEmail(),
            'phone' => $employee->getPhone(),
            'address' => $employee->getAddress(),
            'work' => $employee->getWork(),
            'birthday' => $employee->getBirthday(),
            'bio' => $employee->getBio(),
        ]);

        $infoBuilder
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', TextType::class)
            ->add('address', TextType::class)
            ->add('work', TextType::class)
            ->add('birthday', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('bio', TextType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $infoForm = $infoBuilder->getForm();
        $infoForm->handleRequest($request);

        if ($infoForm->isSubmitted() && $infoForm->isValid()) {
            $data = $infoForm->getData();
            if ($data['firstname'] == null) {
                $this->addFlash('error', 'Le prénom est obligatoire');
            } else if ($data['lastname'] == null) {
                $this->addFlash('error', 'Le nom est obligatoire');
            } else if ($data['email'] == null) {
                $this->addFlash('error', 'L\'adresse E-mail est obligatoire');
            } else if ($data['address'] == null) {
                $this->addFlash('error', 'L\'adresse est obligatoire');
            } else if ($data['phone'] == null) {
                $this->addFlash('error', 'Le numéro de téléphone est obligatoire');
            } else if ($data['birthday'] == null) {
                $this->addFlash('error', 'La date de naissance est obligatoire');
            } else {
                $employee
                    ->setFirstname($data['firstname'])
                    ->setLastname($data['lastname'])
                    ->setEmail($data['email'])
                    ->setPhone($data['phone'])
                    ->setAddress($data['address'])
                    ->setWork($data['work'])
                    ->setBirthday($data['birthday'])
                    ->setbio($data['bio'])
                ;
                $em->persist($employee);
                $em->flush();
                $this->addFlash('success', 'Modifications enrégistrées avec succès');
            }
        }

        return $this->render('admin/user/detail.html.twig', [
            'user' => $employee,
            'infoForm' => $infoForm->createView()
        ]);

    }

    #[Route('/admin/document/unvalid/{id}', name: 'app_user_document_unvalid', methods: ['POST'])]
    public function unvalidDoc(
        $id,
        Request $request, 
        DocumentRepository $documentRepository,
        EntityManagerInterface $em
    ): Response
    {
        $data = $documentRepository->find($id);
        $data->setStatut(DocumentStatus::$REFUSED);

        $user = $data->getEmployee();
        $user->setDocumentsStatus(DocumentStatus::$REFUSED);

        $em->persist($user);
        $em->persist($data);
        $em->flush();

        $this->addFlash('success', 'Document refusé avec succès');

        return $this->redirect($request->headers->get('referer'));
    }

    #[Route('/admin/document/valid/{id}', name: 'app_user_document_valid', methods: ['POST'])]
    public function validDoc(
        $id,
        Request $request, 
        DocumentRepository $documentRepository,
        EntityManagerInterface $em
    ): Response
    {
        $data = $documentRepository->find($id);

        $user = $data->getEmployee();
        $data->setStatut(DocumentStatus::$VALIDATED);

        if (DocumentConfig::check_validated($user)) {
            $user->setDocumentsStatus(DocumentStatus::$VALIDATED);
        }

        if (DocumentConfig::check_waiting($user)) {
            $user->setDocumentsStatus(DocumentStatus::$WAITING);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_not_sent($user)) {
            $user->setDocumentsStatus(DocumentStatus::$NOT_SENT);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_refused($user)) {
            $user->setDocumentsStatus(DocumentStatus::$REFUSED);

            $em->persist($user);
            $em->flush();
        }

        $em->persist($data);
        $em->persist($user);
        $em->flush();

        $this->addFlash('success', 'Document validé avec succès');

        return $this->redirect($request->headers->get('referer'));
    }

}
