<?php

namespace App\Controller\Admin;

use App\Controller\MailerController;
use App\Entity\Town;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ClientController extends AbstractController
{
    #[Route('/admin/entreprises', name: 'app_admin_client')]
    public function index(
        ClientRepository $clientRepository,
        UserRepository $userRepository,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $emails = [];
        $users = [];
        $clients = $clientRepository->findAll();
        
        $all = [];
        $validated = [];
        $waiting = [];
        
        foreach ($clients as $client) {
            $userClient = $userRepository->findOneBy([
                'client' => $client
            ]);
            $emails[$client->getId()] = $userClient->getEmail();
            $users[$client->getId()] = $userClient;
            $all[] = $client;
            if ($client->isValidated()) {
                $validated[] = $client;
            } else {
                $waiting[] = $client;
            }
        }
        return $this->render('admin/client/index.html.twig', [
            'clients' => $clients,
            'emails' => $emails,
            'users' => $users,
            'all' => $all,
            'validated' => $validated,
            'waiting' => $waiting,
        ]);
    }

    #[Route('/admin/delete-client-{id}', name: 'app_user_delete_client', methods: ['POST'])]
    public function deleteClient(
        HttpFoundationRequest $request, 
        ClientRepository $clientRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em,
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $client = $clientRepository->find($request->get('id'));
        $user = $userRepository->findOneBy([
            'client' => $client
        ]);

        $em->remove($user);
        $em->remove($client);
        $em->flush();

        $this->addFlash('success', 'Entréprise supprimée avec succès');

        return $this->redirectToRoute('app_admin_client');

    }

    #[Route('/admin/client/detail/{id}', name: 'app_user_client_detail')]
    public function profile(
        $id,
        Request $request, 
        UserPasswordHasherInterface $encoder,
        UserRepository $userRepository,
        ClientRepository $clientRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if (!($userSession != null && in_array('ROLE_ADMIN', $userSession->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $client = $clientRepository->find($id);
        $user = $userRepository->findOneBy([
            'client' => $client
        ]);

        $factureBuilder = $this->createFormBuilder([
            'factureaddress' => $user->getClient()->getFactureaddress(),
            'facturezipcode' => $user->getClient()->getFacturezipcode()
        ]);
        
        $factureBuilder
            ->add('factureaddress', TextType::class)
            ->add('facturezipcode', TextType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $factureForm = $factureBuilder->getForm();

        $factureForm->handleRequest($request);

        if ($factureForm->isSubmitted() && $factureForm->isValid()) {
            /** @var Client $data */
            $data = $factureForm->getData();
            $client
                ->setFactureaddress($data['factureaddress'])
                ->setFacturezipcode($data['facturezipcode'])
            ;
            $em->persist($client);
            $em->flush();

            $this->addFlash('success', 'Informations enrégistrées avec succès');
        }

        $infoBuilder = $this->createFormBuilder([
            'owner' => $client->getOwner(),
            'address' => $client->getAddress(),
            'zipcode' => $client->getZipcode(),
            'town' => $client->getTown()
        ]);
        $infoBuilder
            ->add('owner', TextType::class)
            ->add('address', TextType::class)
            ->add('zipcode', TextType::class)
            ->add('town', EntityType::class, [
                'class' => Town::class,
                'choice_label' => 'name',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $infoForm = $infoBuilder->getForm();
        $infoForm->handleRequest($request);

        if ($infoForm->isSubmitted() && $infoForm->isValid()) {
            /** @var Client $data */
            $data = $infoForm->getData();
            $client
                ->setOwner($data['owner'])
                ->setAddress($data['address'])
                ->setZipcode($data['zipcode'])
                ->setTown($data['town'])
            ;
            $em->persist($client);
            $em->flush();

            $this->addFlash('success', 'Informations enrégistrées avec succès');
        }

        return $this->render('admin/client/detail.html.twig', [
            'client' => $client,
            'user' => $user,
            'infoForm' => $infoForm->createView(),
            'factureForm' => $factureForm->createView(),
        ]);
    }

    #[Route('/admin/validate-client-{id}', name: 'app_user_validate_client', methods: ['POST'])]
    public function validateClient(
        $id,
        HttpFoundationRequest $request, 
        ClientRepository $clientRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        MailerInterface $mailer
    ): Response
    {

        $data = $clientRepository->find($id);

        $data->setValidated(true);


        $template = 'email/validation.html.twig';

        $name = $data->getName();

        $template_datas = [
            'name' =>  $name,
        ];

        $client_user = $userRepository->findOneBy([
            'client' => $data
        ]);

        if ($client_user != null) {
            MailerController::sendEmail($mailer, $client_user->getEmail(), $template, $template_datas, "Validation du compte " . $data->getName());
        }

        $this->addFlash('success', 'Entréprise validée avec succès');

        $em->persist($data);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));

    }

    #[Route('/admin/unvalidate-client-{id}', name: 'app_user_unvalidate_client', methods: ['POST'])]
    public function unValidateClient(
        $id,
        HttpFoundationRequest $request, 
        ClientRepository $clientRepository,
        EntityManagerInterface $em
    ): Response
    {

        $data = $clientRepository->find($id);

        $data->setValidated(false);

        $em->persist($data);
        $em->flush();

        $this->addFlash('success', 'Entréprise bloquée avec succès');

        return $this->redirect($request->headers->get('referer'));

    }
}
