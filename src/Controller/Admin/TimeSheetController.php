<?php

namespace App\Controller\Admin;

use App\Configuration\ContractConfig;
use App\Entity\TimeSheet;
use App\Entity\User;
use App\Form\ClientWeb\TimeSheetType;
use App\Repository\ContractRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;

class TimeSheetController extends AbstractController
{

    #[Route('/admin/todo', name: 'app_admin_timesheet_todo')]
    public function todo(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        
        $dataReturn = ContractConfig::adminTimeSheetTodo(
            $contractRepository,
        );
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }
        
        
        return $this->render('admin/time_sheet/todo.html.twig',[
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }

    #[Route('/admin/timesheet', name: 'app_admin_timesheet')]
    public function index(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        
        $dataReturn = ContractConfig::userTimeSheet(
            $user,
            $contractRepository,
            true
        );
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }
        
        return $this->render('admin/time_sheet/index.html.twig',[
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }

    #[Route('/admin/timesheet-approuved', name: 'app_admin_timesheet_approuved')]
    public function approuved(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        
        $dataReturn = ContractConfig::userApprouvedTimeSheets(
            $user,
            $contractRepository,
            true,
            true
        );
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }

        return $this->render('admin/time_sheet/approuved.html.twig',[
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }
}
