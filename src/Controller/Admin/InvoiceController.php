<?php

namespace App\Controller\Admin;

use App\Configuration\UserType;
use App\Repository\InvoiceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    #[Route('/admin/client', name: 'app_admin_invoice_client')]
    public function client(
        InvoiceRepository $invoiceRepository,
        Request $request,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $invoices = $invoiceRepository->findAll();
        $wait = [];
        $all = [];
        $paid = [];

        $images = [];

        foreach ($invoices as $invoice) {
            if ($invoice->getUser()->getType() == UserType::$SOCIETY) {
                $all[] = $invoice;
                if ($invoice->isPaid()) {
                    $paid[] = $invoice;
                } else {
                    $wait[] = $invoice;
                }
            }
        }
        return $this->render('admin/invoice/client.html.twig', [
            'paid' => $paid,
            'wait' => $wait,
            'all' => $all,
        ]);
    }

    #[Route('/admin/employee', name: 'app_admin_invoice_employee')]
    public function employee(
        InvoiceRepository $invoiceRepository,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        $invoices = $invoiceRepository->findAll();
        $wait = [];
        $paid = [];
        $all = [];

        foreach ($invoices as $invoice) {
            if ($invoice->getUser()->getType() == UserType::$EMPLOYEE) {
                $all[] = $invoice;
                if ($invoice->isPaid()) {
                    $paid[] = $invoice;
                } else {
                    $wait[] = $invoice;
                }
            }
        }
        return $this->render('admin/invoice/employee.html.twig', [
            'paid' => $paid,
            'wait' => $wait,
            'all' => $all,
        ]);
    }

    #[Route('/admin/validate-invoice-{id}', name: 'app_user_validate_invoice', methods: ['POST'])]
    public function paid(
        $id,
        Request $request,
        InvoiceRepository $invoiceRepository,
        EntityManagerInterface $em,
    ): Response
    {

        $data = $invoiceRepository->find($id);

        $data->setPaid(true);

        $this->addFlash('success', 'Facture payée avec succès');

        $em->persist($data);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));

    }

    #[Route('/admin/cancel-invoice-{id}', name: 'app_user_cancel_invoice', methods: ['POST'])]
    public function cancel(
        $id,
        Request $request,
        InvoiceRepository $invoiceRepository,
        EntityManagerInterface $em,
    ): Response
    {

        $data = $invoiceRepository->find($id);

        $data->setPaid(false);

        $this->addFlash('success', 'Paiement de la facture annulée avec succès');

        $em->persist($data);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));

    }
}
