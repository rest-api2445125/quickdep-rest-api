<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\ClientWeb\LoginType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    #[Route('/admin/login', name: 'app_admin_auth')]
    public function index(
        Request $request,
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if ($user != null && in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->redirectToRoute('app_admin_contract');
        }

        $loginForm = $this->createForm(LoginType::class);
        $loginForm->handleRequest($request);

        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $data = $loginForm->getData();

            $user = $userRepository->findOneBy([
                "email" => $data["email"]
            ]);

            $password = $data['password'];

            if ($user != null && in_array("ROLE_ADMIN", $user->getRoles())) {
                if ($encoder->isPasswordValid($user, $password)) {
                    $isnew = false;
                    if ($user->isNew() == true || $user->isNew() === null) {
                        $isnew = true;
                        $user->setNew(false);
    
                        $em->persist($user);
                        $em->flush();
                    }

                    $session->set('user', $user);
                    $session->set('type', 'admin');
                    
                    if ($isnew) {
                        return $this->redirectToRoute('app_admin_new_password');
                    }
    
                    $data = [
                        'success' => true,
                        'new' => $isnew,
                        'user' => $user
                    ];
                    return $this->redirectToRoute('app_admin_contract');
                }
            } 
            
            $this->addFlash(
                'notice',
                'Adresse email ou mot de passe incorrect!'
            );
        }

        return $this->render('admin/auth/login.html.twig', [
            'loginForm' => $loginForm->createView()
        ]);
    }

    #[Route('/admin/new-password', name: 'app_admin_new_password')]
    public function newPassword(
        Request $request,
        UserPasswordHasherInterface $encoder,
        EntityManagerInterface $em,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $newPassFormBuilder = $this->createFormBuilder();
        $newPassFormBuilder
            ->add('password', PasswordType::class, [
                'label' => 'Nouveau mot de passe'
            ])
            ->add('password_confirm', PasswordType::class, [
                'label' => 'Confirmer le nouveau mot de passe'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $newPassForm = $newPassFormBuilder->getForm();

        /** @var Form */
        $newPassForm->handleRequest($request);

        if ($newPassForm->isSubmitted() && $newPassForm->isValid()) {
            $data = $newPassForm->getData();

            if ($data['password'] != $data['password_confirm']) {
                $this->addFlash('notice', 'Les deux mots de passes ne correspondent pas');
                return $this->redirectToRoute('app_admin_new_password');
            }

            if (strlen($data['password']) < 6) {
                $this->addFlash('notice', 'Le mot de passe doit avoir au mois 6 carractères');
                return $this->redirectToRoute('app_admin_new_password');
            }

            $user = $userRepository->find($user->getId());
            
            $plain_password = $data['password'];
            $user->setPassword($encoder->hashPassword($user, $plain_password));
            $user->setNew(false);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_admin_contract');
        }

        return $this->render('admin/auth/new_pass.html.twig', [
            'form' => $newPassForm->createView()
        ]);
    }
}
