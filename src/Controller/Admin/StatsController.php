<?php

namespace App\Controller\Admin;

use App\Configuration\ContractStatus;
use App\Configuration\TimeSheetStatus;
use App\Configuration\UserType;
use App\Repository\ClientRepository;
use App\Repository\ContractRepository;
use App\Repository\InvoiceRepository;
use App\Repository\TimeSheetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{

    #[Route('/admin/stats', name: 'app_admin_stats')]
    public function employee(
        ContractRepository $contractRepository,
        UserRepository $userRepository,
        TimeSheetRepository $timeSheetRepository,
        ClientRepository $clientRepository,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

       // completed shifts
       $compl_shifts = count($contractRepository->findBy([
            "status" => [ContractStatus::$TIME_APPROUVED, ContractStatus::$PAID]
        ]));

        // users 
        $users = count($userRepository->findAll());

        // total hours 
        $hours_times =
            $timeSheetRepository->findBy([
                "status" => TimeSheetStatus::$APPROUVED
            ]);

        $times = 0;

        foreach ($hours_times as $time) {
            $times += $time->getTotaltimes();
        }

        $total_houres = ceil($times / 60);

        // uncompleted shifts
        $uncompleted_shifts = count($contractRepository->findBy([
            'status' => [ContractStatus::$NOT_WORKED]
        ]));

        // requests 
        $request = ($compl_shifts / ($compl_shifts + $uncompleted_shifts)) * 100;

        // clients
        $clients = count($clientRepository->findAll());

        return $this->render('admin/stats/stats.html.twig', [
            "completed_shifts" => $compl_shifts,
            "total_users" => $users,
            "completed_hours" => $total_houres,
            "completed_requests" => $request,
            "uncompleted_shifts" => $uncompleted_shifts,
            "clients" => $clients
        ]);
    }

    #[Route('/admin/finance', name: 'app_admin_finance')]
    public function finance(
        Request $request,
        InvoiceRepository $invoiceRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

       // paid_emp 
       $paid_emps = $invoiceRepository->findBy([
            "paid" => true
        ]);

        $paid_emp_amount = 0;

        foreach ($paid_emps as $p) {
            if ($p->getUser()->getType() == UserType::$EMPLOYEE) {
                $paid_emp_amount += $p->getTotal();
            }
        }

        // paid_cl 
        $paid_ecls = $invoiceRepository->findBy([
            "paid" => true
        ]);

        $paid_cl_amount = 0;

        foreach ($paid_ecls as $p) {
            if ($p->getUser()->getType() == UserType::$SOCIETY) {
                $paid_cl_amount += $p->getTotal();
            }
        }

        // unpaid_emp 
        $unpaid_emps = $invoiceRepository->findBy([
            "paid" => false
        ]);

        $unpaid_emp_amount = 0;

        foreach ($unpaid_emps as $p) {
            if ($p->getUser()->getType() == UserType::$EMPLOYEE) {
                $unpaid_emp_amount += $p->getTotal();
            }
        }

        // unpaid_cl 
        $unpaid_ecls = $invoiceRepository->findBy([
            "paid" => false
        ]);

        $unpaid_cl_amount = 0;

        foreach ($unpaid_ecls as $p) {
            if ($p->getUser()->getType() == UserType::$SOCIETY) {
                $unpaid_cl_amount += $p->getTotal();
            }
        }

        // benefit 
        $benefit = $paid_cl_amount + $unpaid_cl_amount - $paid_emp_amount - $unpaid_emp_amount;

        return $this->render('admin/stats/finance.html.twig', [
            "paid_employee" => $paid_emp_amount,
            "paid_clients" => $paid_cl_amount,
            "unpaid_employee" => $unpaid_emp_amount,
            "unpaid_clients" => $unpaid_cl_amount,
            "benefits" => $benefit,
        ]);
    }
}
