<?php

namespace App\Controller\Admin;

use App\Configuration\ContractConfig;
use App\Entity\User;
use App\Form\Admin\ContractType;
use App\Repository\ApplyRepository;
use App\Repository\ContractRepository;
use App\Repository\DeviceRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

class ContractController extends AbstractController
{
    #[Route('/admin', name: 'app_admin_contract')]
    public function index(
        Request $request,
        ContractRepository $contractRepository,
        ApplyRepository $applyRepository,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $data = $contractRepository->findAll();
        $validate_data = [];

        foreach ($data as $contract) {
            if ($contract->isPublic($user, $applyRepository)) {
                $validate_data[] = $contract;
            }
        }

        $dataReturn = ContractConfig::listing($validate_data, $this->getUser(), true);
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }

        return $this->render('admin/contract/index.html.twig', [
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }

    #[Route('/admin/shift', name: 'app_admin_new_shift')]
    public function newShift(
        Request $request, 
        TownRepository $townRepository,
        EntityManagerInterface $em,
        DeviceRepository $deviceRepository,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        UserRepository $userReository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_contract');
        }
    
        $type = $session->get('type');

        $contractForm = $this->createForm(ContractType::class);
        $contractForm->handleRequest($request);
        
        if ($contractForm->isSubmitted() && $contractForm->isValid()) {
            $data = $contractForm->getData();
            $consecutive = false;

            if (array_key_exists('consecutive', $_POST)) {
                $consecutive = $_POST['consecutive'] == "on";    
            }

            $twilioClient = new Client(
                $this->getParameter("app.twilio_ssd"),
                $this->getParameter("app.twilio_token")
            );

            ContractConfig::createContract(
                $data,
                $consecutive,
                $em,
                $twilioClient,
                $deviceRepository,
                $userRepository,
                $httpClient,
            );

            $this->addFlash(
                'success',
                'Contrat publié avec succés!'
            );

            return $this->redirectToRoute('app_admin_contract');
        }

        $town = null;

        return $this->render('admin/contract/new-shift.html.twig', [
            'contractForm' => $contractForm->createView(),
            'user' => $user,
            'edit' => false,
            'town' => $town,
            'address' => $type == 'client' ? $userRepository->find($user->getId())->getClient()->getAddress() : ''
        ]);
    }
    
    #[Route('/admin/waiting', name: 'app_admin_contract_waiting')]
    public function waiting(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        
        $dataReturn = ContractConfig::adminApplies(
            $contractRepository,
            true
        );
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }

        return $this->render('admin/contract/waiting.html.twig',[
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }


    #[Route('/amin/approuved', name: 'app_admin_contract_approuved')]
    public function approuved(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }
        
        $dataReturn = ContractConfig::adminApprouved(
            $contractRepository,
            true
        );
        
        $images = [];
        if (count($dataReturn) > 1) {
            foreach ($dataReturn as $contract) {
                if (!is_array($contract)) {
                    $user = $userRepository->findOneBy([
                        'client' => $contract->getClient()
                    ]);
                    $images[$contract->getId()] = $user->getImagePath();
                }
            }
        }

        return $this->render('admin/contract/approuved.html.twig',[
            'contracts' => $dataReturn,
            'images' => $images
        ]);
    }

    #[Route('/admin/shift-{id}', name: 'app_admin_shift_detail')]
    public function detail(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && in_array('ROLE_ADMIN', $user->getRoles()))) {
            return $this->redirectToRoute('app_admin_auth');
        }

        $contract = $contractRepository->find($request->get('id'));

        return $this->render('admin/contract/detail.html.twig', [
            'contract' => $contract,
            'image' => $userRepository->findOneBy([
                'client' => $contract->getClient()
            ])->getImagePath()
        ]);
    }
}
