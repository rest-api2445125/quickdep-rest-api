<?php

namespace App\Controller;

use App\Repository\InvoiceRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class DownloadInvoiceController extends AbstractController
{
    #[Route('/download/invoice/{id}', name: 'app_download_invoice')]
    public function index(Request $request, InvoiceRepository $invoiceRepository): Response
    {
        $id = $request->attributes->get('id');
        $data = $invoiceRepository->find($id);

        $filename = $data->getCode() . '.xlsx';
        $streamedResponse = new StreamedResponse();
        $streamedResponse->setCallback(function () use ($filename) {
            $spreadsheet = IOFactory::load('invoices/' . $filename);

            $writer =  new Xlsx($spreadsheet);
            $writer->save('php://output');
        });

        $streamedResponse->setStatusCode(200);
        $streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $streamedResponse->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        return $streamedResponse->send();
    }
}
