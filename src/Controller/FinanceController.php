<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Configuration\TimeSheetStatus;
use App\Configuration\UserType;
use App\Entity\Contract;
use App\Repository\ClientRepository;
use App\Repository\ContractRepository;
use App\Repository\InvoiceRepository;
use App\Repository\TimeSheetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FinanceController extends AbstractController
{
    public function __invoke(array $data, InvoiceRepository $invoiceRepository, TimeSheetRepository $timeSheetRepository, ContractRepository $contractRepository, UserRepository $userRepository)
    {
        // paid_emp 
        $paid_emps = $invoiceRepository->findBy([
            "paid" => true
        ]);

        $paid_emp_amount = 0;

        foreach ($paid_emps as $p) {
            if ($p->getUser()->getType() == UserType::$EMPLOYEE) {
                $paid_emp_amount += $p->getTotal();
            }
        }

        // paid_cl 
        $paid_ecls = $invoiceRepository->findBy([
            "paid" => true
        ]);

        $paid_cl_amount = 0;

        foreach ($paid_ecls as $p) {
            if ($p->getUser()->getType() == UserType::$SOCIETY) {
                $paid_cl_amount += $p->getTotal();
            }
        }

        // unpaid_emp 
        $unpaid_emps = $invoiceRepository->findBy([
            "paid" => false
        ]);

        $unpaid_emp_amount = 0;

        foreach ($unpaid_emps as $p) {
            if ($p->getUser()->getType() == UserType::$EMPLOYEE) {
                $unpaid_emp_amount += $p->getTotal();
            }
        }

        // unpaid_cl 
        $unpaid_ecls = $invoiceRepository->findBy([
            "paid" => false
        ]);

        $unpaid_cl_amount = 0;

        foreach ($unpaid_ecls as $p) {
            if ($p->getUser()->getType() == UserType::$SOCIETY) {
                $unpaid_cl_amount += $p->getTotal();
            }
        }

        // benefit 
        $benefit = $paid_cl_amount + $unpaid_cl_amount - $paid_emp_amount - $unpaid_emp_amount;

        return [
            "paid_employee" => $paid_emp_amount,
            "paid_clients" => $paid_cl_amount,
            "unpaid_employee" => $unpaid_emp_amount,
            "unpaid_clients" => $unpaid_cl_amount,
            "benefits" => $benefit,
        ];
    }
}
