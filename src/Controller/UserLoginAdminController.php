<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Entity\Device;
use App\Entity\User;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserLoginAdminController extends AbstractController
{
    public function __invoke(
        User $data,
        Request $request,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        JWTTokenManagerInterface $jWTTokenManager,
        UserPasswordHasherInterface $encoder
    ) {
        $request_datas = json_decode(
            preg_replace(
                '/[\x00-\x1F\x80-\xFF]/',
                '',
                $request->getContent()
            ),
            true
        );

        $email = $request_datas['email'];
        $password = $request_datas['password'];

        $user = $userRepository->findOneBy([
            'email' => $email
        ]);

        if ($user != null && in_array("ROLE_ADMIN", $user->getRoles())) {
            if ($encoder->isPasswordValid($user, $password)) {
                $token = $jWTTokenManager->create($user);

                $isnew = false;

                if ($user->isNew() == true || $user->isNew() === null) {
                    $isnew = true;
                    $user->setNew(false);

                    $em->persist($user);
                    $em->flush();
                }

                return [
                    'success' => true,
                    'new' => $isnew,
                    'token' => $token,
                    'user' => $user
                ];
            }
        }

        return [
            'success' => false,
            'message' => 'Adresse mail ou mot de passe incorrect'
        ];
    }
}
