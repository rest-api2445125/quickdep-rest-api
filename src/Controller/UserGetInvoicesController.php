<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\InvoiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetInvoicesController extends AbstractController
{
    public function __invoke(User $data, Request $request, UserRepository $userRepository, InvoiceRepository $invoiceRepository)
    {
        return $data->getInvoices();
    }
}
