<?php

namespace App\Controller;

use App\Configuration\Notification;
use App\Entity\AcceptedContract;
use App\Entity\Contract;
use App\Repository\AcceptedContractRepository;
use App\Repository\ContractRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

class NotifyUnAcceptedContractController extends AbstractController
{
    public function __invoke(Contract $data, UserRepository $userRepository, ContractRepository $contractRepository, AcceptedContractRepository $acceptedContractRepository, MailerInterface $mailer)
    {
        // get all users
        $users = $userRepository->findAll();
        // get all contracts
        $contracts = $contractRepository->findAll();

        // if (user not accept contract)
        // if contract date = today
        // send notification

        foreach ($contracts as $contract) {
            if ($contract->getStatus() != "ACCEPTED" && $contract->getStatus() != "VALIDATED" && $contract->getStatus() != "COMPLETED") {
                $date = $contract->getDate()->format("d-m-Y");
                $today = new DateTime();
                $today = $today->format("d-m-Y");

                if ($data == $today) {
                    Notification::shifNotification($contract, $userRepository, $mailer);
                }

                return [
                    "date" => $date,
                    "today" => $today
                ];
            }
        }

        return [
            "success" => true,
        ];
    }
}
