<?php

namespace App\Controller\ClientWeb;

use App\Configuration\ContractConfig;
use App\Configuration\UserType;
use App\Entity\Contract;
use App\Entity\User;
use App\Form\ClientWeb\ContractType;
use App\Repository\ContractRepository;
use App\Repository\DeviceRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Client as AppEntityClient;
use App\Entity\Town;
use App\Repository\ApplyRepository;
use App\Repository\ClientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

use function PHPSTORM_META\type;

class HomeController extends AbstractController
{
    #[Route('/entreprise', name: 'app_client_web_home')]
    public function index(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository,
        ApplyRepository $applyRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $user = $userRepository->find($user->getId());
        $session->set('user', $user);

        $dataReturn = ContractConfig::clientContracts(
            $user->getClient(),
            $userRepository,
            $applyRepository,
            $contractRepository,
            true
        );

        return $this->render('client_web/home/index.html.twig', $dataReturn);
    }

    #[Route('/entreprise/shift-{id}', name: 'app_client_web_shift_detail')]
    public function detail(
        Request $request, 
        ContractRepository $contractRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $contract = $contractRepository->find($request->get('id'));

        return $this->render('client_web/home/detail.html.twig', [
            'contract' => $contract,
        ]);
    }

    #[Route('/entreprise/shift', name: 'app_client_web_new_shift')]
    public function newShift(
        Request $request, 
        TownRepository $townRepository,
        EntityManagerInterface $em,
        DeviceRepository $deviceRepository,
        UserRepository $userRepository,
        HttpClientInterface $httpClient
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        if (!($user->isSigned() && $user->getClient()->isValidated() && $user->isEmailVerified() && $user->isPhoneverified())) {
            return $this->redirectToRoute('app_client_web_home');
        }

        $type = $session->get('type');
        
        $town = $townRepository->find($user->getClient()->getTown()->getId());

        $contractForm = $this->createForm(ContractType::class);
        $contractForm->handleRequest($request);

        if ($contractForm->isSubmitted() && $contractForm->isValid()) {
            $data = $contractForm->getData();
            $consecutive = false;

            if (array_key_exists('consecutive', $_POST)) {
                $consecutive = $_POST['consecutive'] == "on";    
            }

            $data->setClient($em->getRepository(AppEntityClient::class)->find($user->getClient()->getId()));
            if ($data->getAddress() == null) {
                $data->setAddress($user->getClient()->getAddress());
                $data->setTown($town);
            }

            $twilioClient = new Client(
                $this->getParameter("app.twilio_ssd"),
                $this->getParameter("app.twilio_token")
            );

            ContractConfig::createContract(
                $data,
                $consecutive,
                $em,
                $twilioClient,
                $deviceRepository,
                $userRepository,
                $httpClient,
            );

            $this->addFlash(
                'success',
                'Contrat publié avec succés!'
            );

            return $this->redirectToRoute('app_client_web_home');
        }

        return $this->render('client_web/home/new-shift.html.twig', [
            'contractForm' => $contractForm->createView(),
            'user' => $user,
            'town' => $town,
            'edit' => false,
            'type' => $type,
            'address' => $type == 'client' ? $userRepository->find($user->getId())->getClient()->getAddress() : ''
        ]);
    }

    #[Route('/contract/edit-shift/{id}', name: 'app_client_web_edit_shift')]
    public function editShift(
        $id,
        Request $request, 
        TownRepository $townRepository,
        EntityManagerInterface $em,
        DeviceRepository $deviceRepository,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        ContractRepository $contractRepository
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if ($user == null) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $type = $session->get('type');
        
        
        $contract = $em->getRepository(Contract::class)->find($id);

        $town = $townRepository->find(
            $contract->getClient()->getTown()->getId()
        );

        $contractForm = $this->createForm(ContractType::class, $contract);

        $contractForm->handleRequest($request);

        if ($contractForm->isSubmitted() && $contractForm->isValid()) {
        
            $twilioClient = new Client(
                $this->getParameter("app.twilio_ssd"),
                $this->getParameter("app.twilio_token")
            );

            $data = $contractForm->getData();

            $data->setNewbonus($data->getBonus());

            ContractConfig::editContract(
                $contractRepository,
                $data,
                $twilioClient,
                $em,
                $userRepository,
                $httpClient,
                $deviceRepository
            );

            $this->addFlash(
                'success',
                'Contrat modifié avec succés!'
            );

            if ($type == 'admin') {
                return $this->redirectToRoute('app_admin_contract');
            }
            return $this->redirectToRoute('app_client_web_home');
        }

        return $this->render('client_web/home/new-shift.html.twig', [
            'contractForm' => $contractForm->createView(),
            'user' => $user,
            'town' => $town,
            'data' => $contract,
            'edit' => true,
            'type' => $type,
            'contract' => $contract,
            'address' => $type == 'client' ? $userRepository->find($user->getId())->getClient()->getAddress() : ''
        ]);
    }

    #[Route('/entreprise/delete-shift-{id}', name: 'app_client_web_delete_shift_detail', methods: ['POST'])]
    public function deleteContract(
        Request $request, 
        ContractRepository $contractRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if ($user == null) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        $type = $session->get('type');

        $contract = $contractRepository->find($request->get('id'));
        $em->remove($contract);
        $em->flush();

        $this->addFlash('success', 'Contrat supprimé avec succès');

        return $this->redirectToRoute($type == 'admin' ? 'app_admin_contract' : 'app_client_web_home');

    }

    #[Route('/entreprise/profile', name: 'app_client_web_home_profile')]
    public function profile(
        Request $request, 
        UserPasswordHasherInterface $encoder,
        UserRepository $userRepository,
        ApplyRepository $applyRepository,
        ClientRepository $clientRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $user = $userRepository->find($user->getId());
        $session->set('user', $user);

        $factureBuilder = $this->createFormBuilder([
            'factureaddress' => $user->getClient()->getFactureaddress(),
            'facturezipcode' => $user->getClient()->getFacturezipcode()
        ]);
        
        $factureBuilder
            ->add('factureaddress', TextType::class)
            ->add('facturezipcode', TextType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $factureForm = $factureBuilder->getForm();

        $factureForm->handleRequest($request);

        if ($factureForm->isSubmitted() && $factureForm->isValid()) {
            /** @var Client $data */
            $data = $factureForm->getData();
            $client = $clientRepository->find($user->getClient()->getId());
            $client
                ->setFactureaddress($data['factureaddress'])
                ->setFacturezipcode($data['facturezipcode'])
            ;
            $em->persist($client);
            $em->flush();

            $this->addFlash('success', 'Informations enrégistrées avec succès');
            return $this->redirectToRoute('app_client_web_home_profile');
        }


        $passBuilder = $this->createFormBuilder();
        $passBuilder
            ->add('password', PasswordType::class)
            ->add('password_c', PasswordType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $passForm = $passBuilder->getForm();
        $passForm->handleRequest($request);

        if ($passForm->isSubmitted() && $passForm->isValid()) {
            $data = $passForm->getData();
            $pass = $data['password'];
            $pass_c = $data['password_c'];

            if (strlen($pass) < 6) {
                $this->addFlash('error', 'Le mot de passe doit avoir au moins 6 carractères');
                return $this->redirectToRoute('app_client_web_home_profile');
            } else if ($pass != $pass_c) {
                $this->addFlash('error', 'Les deux mot de passes ne correspondent pas');
                return $this->redirectToRoute('app_client_web_home_profile');
            } else {
                $user = $userRepository->find($user->getId());
                $user->setPassword($encoder->hashPassword($user, $data['password']));

                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Mot de passe modifié avec succès');
                return $this->redirectToRoute('app_client_web_home_profile');
            }
        }

        $infoBuilder = $this->createFormBuilder([
            'owner' => $user->getClient()->getOwner(),
            'address' => $user->getClient()->getAddress(),
            'zipcode' => $user->getClient()->getZipcode(),
            'town' => $user->getClient()->getTown()
        ]);
        $infoBuilder
            ->add('owner', TextType::class)
            ->add('address', TextType::class)
            ->add('zipcode', TextType::class)
            ->add('town', EntityType::class, [
                'class' => Town::class,
                'choice_label' => 'name',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $infoForm = $infoBuilder->getForm();
        $infoForm->handleRequest($request);

        if ($infoForm->isSubmitted() && $infoForm->isValid()) {
            /** @var Client $data */
            $data = $infoForm->getData();
            $client = $clientRepository->find($user->getClient()->getId());
            $client
                ->setOwner($data['owner'])
                ->setAddress($data['address'])
                ->setZipcode($data['zipcode'])
                ->setTown($data['town'])
            ;
            $em->persist($client);
            $em->flush();

            $this->addFlash('success', 'Informations enrégistrées avec succès');
            return $this->redirectToRoute('app_client_web_home_profile');
        }

        return $this->render('client_web/home/profile.html.twig', [
            'user' => $user,
            'infoForm' => $infoForm->createView(),
            'factureForm' => $factureForm->createView(),
            'passForm' => $passForm->createView()
        ]);
    }

    #[Route('/entreprise/contact', name: 'app_client_web_home_contact')]
    public function contact(
        Request $request, 
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        return $this->render('client_web/home/contact.html.twig', [
            'user' => $user,
        ]);
    }
}
