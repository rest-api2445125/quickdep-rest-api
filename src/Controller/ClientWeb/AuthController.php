<?php

namespace App\Controller\ClientWeb;

use App\Configuration\ContractConfig;
use App\Configuration\NotificationConfig;
use App\Configuration\UserType;
use App\Controller\MailerController;
use App\Entity\Client;
use App\Entity\Device;
use App\Entity\PasswordReset;
use App\Entity\Town;
use App\Entity\User;
use App\Form\ClientWeb\LoginType;
use App\Form\ClientWeb\RegisterFormType;
use App\Repository\PasswordResetRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client as RestClient;

class AuthController extends AbstractController
{
    #[Route('/entreprise/login', name: 'app_client_web_auth')]
    public function index(
        Request $request, 
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder,
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if ($user != null && $user->getType() == UserType::$SOCIETY) {
            return $this->redirectToRoute('app_client_web_home');
        }

        $loginForm = $this->createForm(LoginType::class);
        $loginForm->handleRequest($request);

        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $data = $loginForm->getData();

            $user = $userRepository->findOneBy([
                "email" => $data["email"],
                "type" => UserType::$SOCIETY
            ]);


            $password = $data['password'];

            if ($user != null && $encoder->isPasswordValid($user, $password)) {
                $session->set('user', $user);
                $session->set('type', 'client');
                return $this->redirectToRoute('app_client_web_home');
            } else {
                $this->addFlash(
                    'notice',
                    'Adresse email ou mot de passe incorrect!'
                );
            }
        }



        return $this->render('client_web/auth/index.html.twig', [
            'loginForm' => $loginForm->createView(),
        ]);
    }


    #[Route('/entreprise/register', name: 'app_client_web_auth_register')]
    public function register(
        Request $request, 
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if ($user != null && $user->getType() == UserType::$SOCIETY) {
            return $this->redirectToRoute('app_client_web_home');
        }

        $registerForm = $this->createForm(RegisterFormType::class);
        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $request_datas = $registerForm->getData();
    
            $email = $request_datas['email'];
            $password = $request_datas['password'];
    
            $town_id = $request_datas["town"]->getId();
    
            $exist = $userRepository->findOneBy(["email" => $email]);
    
            if ($exist != null) {
                $this->addFlash(
                    'notice',
                    'Un compte existe déjà avec cette adresse E-mail.'
                );
            } else {
                $user = $userRepository->createNewUser([
                    'email' => $email,
                    'password' => $password,
                    'type' => UserType::$SOCIETY
                ], $em, $encoder);
    
                $data = (new Client())
                    ->setName($request_datas['name'])
                    ->setOwner($request_datas['owner'])
                    ->setPhone($request_datas['phone'])
                    ->setAddress($request_datas['address'])
                    ->setTown($request_datas['town'])
                    ->setZipcode($request_datas['zipcode'])
                    ->setEmail($request_datas['email'])
                ;
    
                $em->persist($data);
                $em->flush();
    
                if ($request_datas['factureaddress'] != null) {
                    $data
                        ->setFactureaddress($request_datas['factureaddress'])
                        ->setFacturezipcode($request_datas['facturezipcode'])
                    ;
                }
    
                $user = ContractConfig::clientRegister(
                    $data, $em->getRepository(Town::class), $town_id, $user, $userRepository, $em, "", $em->getRepository(Device::class)
                );
    
                $session->set('user', $user);
                $session->set('type', 'client');
                return $this->redirectToRoute('app_client_web_auth_paiement');
            }
        }

        return $this->render('client_web/auth/register.html.twig', [
            'registerForm' => $registerForm->createView(),
        ]);
    }


    #[Route('/entreprise/paiement/{profile?}', name: 'app_client_web_auth_paiement')]
    public function paiement(
        Request $request, 
        EntityManagerInterface $em,
        $profile
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $builder = $this->createFormBuilder();
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        /** @var Form */
        $paiementForm = $builder->getForm();
        $paiementForm->handleRequest($request);

        if ($paiementForm->isSubmitted() && $paiementForm->isValid()) {
            $payment = $_POST['paiement'];
            $client = $em->getRepository(Client::class)->find($user->getClient()->getId());

            $client->setPayment((int) $payment);

            $user = $em->getRepository(User::class)->find($user->getId());
            $user->setClient($client);
            $session->set('user', $user);
            $session->set('type', 'client');

            $em->persist($client);
            $em->flush();

            $this->addFlash(
                'success',
                'Mode de paiement enrégistré avec succès'
            );

            if ($profile) {
                return $this->redirectToRoute('app_client_web_home_profile');
            } else {
                return $this->redirectToRoute('app_client_web_auth_contract');
            }
        }

        return $this->render('client_web/auth/paiement.html.twig', [
            'paiementForm' => $paiementForm->createView(),
            'profile' => $profile == 1 ? true : false
        ]);
    }

    #[Route('/entreprise/contract/{profile?}', name: 'app_client_web_auth_contract')]
    public function contract(
        Request $request, 
        EntityManagerInterface $em,
        $profile
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');
        //$session->clear();

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        return $this->render('client_web/auth/contract.html.twig', [
            'user' => $user,
            'profile' => $profile == 1 ? true : false
        ]);
    }

    #[Route('/entreprise/logout', name: 'logout', methods: ['POST'])]
    public function logout(
        Request $request, 
    ): Response
    {
        $session = $request->getSession();
        
        $session->clear();
        return $this->redirectToRoute('app_client_web_auth');
    }

    #[Route('/entreprise/forget-pass', name: 'app_client_web_auth_forget_pass')]
    public function forgetPass(
        Request $request, 
        EntityManagerInterface $em,
        UserRepository $userRepository,
        PasswordResetRepository $passwordResetRepository,
         MailerInterface $mailer
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if ($user != null && $user->getType() == UserType::$SOCIETY) {
            return $this->redirectToRoute('app_client_web_home');
        }

        $emailFormBuilder = $this->createFormBuilder();
        $emailFormBuilder
            ->add('email', EmailType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Valider'
            ])
        ;

        $emailForm = $emailFormBuilder->getForm();
        $emailForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $data = $emailForm->getData();
            
            $userData = $userRepository->findOneBy([
                'email' => $data['email'],
                'type' => UserType::$SOCIETY
            ]);

            if ($userData == null) {
                $this->addFlash('notice', 'Aucun compte entréprise ne correspond à cette adresse E-mail');
            } else {
                $passwordReset = (new PasswordReset())  
                    ->setEmail($data['email'])            
                ;

                $dataReturn = ContractConfig::resetPassword(
                    $data['email'],
                    $passwordResetRepository,
                    $passwordReset,
                    $em,
                    $mailer,
                    $userRepository
                );

                if (!$dataReturn['success']) {
                    $this->addFlash('notice', $dataReturn['message']);
                } else {
                    return $this->redirectToRoute('app_client_web_auth_confirm_code', ['resetId' => $passwordReset->getId()]);
                }
            }

        }

        return $this->render('client_web/auth/forget-pass.html.twig', [
            'user' => $user,
            'emailForm' => $emailForm->createView()
        ]);
    }

    #[Route('/entreprise/confirm-code/{resetId}', name: 'app_client_web_auth_confirm_code')]
    public function confirmCode(
        Request $request, 
        UserRepository $userRepository,
        PasswordResetRepository $passwordResetRepository,
        $resetId
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if ($userSession != null && $userSession->getType() == UserType::$SOCIETY) {
            return $this->redirectToRoute('app_client_web_home');
        }

        $passwordReset = $passwordResetRepository->find($resetId);

        if ($passwordReset == null) {
            $this->addFlash('notice', 'Une erreur s\'est produite');
            return $this->redirectToRoute('app_client_web_auth');
        }

        $userId = $userRepository->findOneBy([
            'email' => $passwordReset->getEmail()
        ])->getId();

        $codeFormBuilder = $this->createFormBuilder();
        $codeFormBuilder
            ->add('submit', SubmitType::class, [
                'label' => 'Valider'
            ])
        ;

        $codeForm = $codeFormBuilder->getForm();
        $codeForm->handleRequest($request);

        if ($codeForm->isSubmitted() && $codeForm->isValid()) {
            $code = "";
            foreach ($_POST as $field) {
                if(!is_array($field)) {
                    $code .= $field;
                }
            }

            if ($code != "" && strlen($code) < 5) {
                $this->addFlash('notice', 'Le code entré est invalide');
            } else {;

                $dataReturn = ContractConfig::resetPasswordConfirm(
                    $passwordReset,
                    $code,
                    $userRepository,
                    $passwordReset->getEmail()
                );

                if (!$dataReturn['success']) {
                    $this->addFlash('notice', $dataReturn['message']);
                } else {
                    $session->set('codeVerified', true);
                    $session->set('type', 'client');
                    return $this->redirectToRoute('app_client_web_auth_new_pass', ['userId' => $userId]);
                }

            }
        }

        return $this->render('client_web/auth/confirm-code.html.twig', [
            'reset' => $passwordReset,
            'codeForm' => $codeForm->createView()
        ]);
    }

    #[Route('/entreprise/new-pass/{userId}', name: 'app_client_web_auth_new_pass')]
    public function newPass(
        Request $request, 
        EntityManagerInterface $em,
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder,
        $userId
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if ($userSession != null && $userSession->getType() == UserType::$SOCIETY) {
            return $this->redirectToRoute('app_client_web_home');
        }

        if ($session->get('codeVerified') != true) {
            $this->addFlash('notice', 'Une erreur s\'est produite');
            return $this->redirectToRoute('app_client_web_auth');
        }

        $passBuilder = $this->createFormBuilder();
        $passBuilder
            ->add('password', PasswordType::class)
            ->add('password_c', PasswordType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Enrégistrer'
            ])
        ;

        $user = $userRepository->find($userId);

        $passForm = $passBuilder->getForm();
        $passForm->handleRequest($request);

        if ($passForm->isSubmitted() && $passForm->isValid()) {
            $data = $passForm->getData();
            $pass = $data['password'];
            $pass_c = $data['password_c'];

            if (strlen($pass) < 6) {
                $this->addFlash('notice', 'Le mot de passe doit avoir au moins 6 carractères');
            } else if ($pass != $pass_c) {
                $this->addFlash('notice', 'Les deux mot de passes ne correspondent pas');
            } else {
                $user = $userRepository->find($user->getId());
                $user->setPassword($encoder->hashPassword($user, $data['password']));

                $em->persist($user);
                $em->flush();

                $session->remove('codeVerified');
                $this->addFlash('success', 'Mot de passe modifié avec succès. Veuillez vous connecter avec le nouveau mot de passe');
                return $this->redirectToRoute('app_client_web_auth');
            }
        }

        return $this->render('client_web/auth/newpass.html.twig', [
            'user' => $user,
            'passForm' => $passForm->createView()
        ]);
    }

    #[Route('/entreprise/email-verif', name: 'mailVerif', methods: ['POST'])]
    public function mailVerif(
        Request $request, 
        UserRepository $userRepository,
        EntityManagerInterface $em,
        MailerInterface $mailer
    ): Response
    { 
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if (!($userSession != null && $userSession->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $data = $userRepository->find($userSession->getId());

        $otp = ContractConfig::generateOtp(5);

        // check if code exist
        if ($data->getVerificationCode() != null) {
            $otp = $data->getVerificationCode();
        } else {
            $data->setVerificationCode($otp);
            $em->persist($data);
            $em->flush();
        }

        $template = 'email/verification.html.twig';

        $username = $data->getFirstname() . " " . $data->getLastname();

        $template_datas = [
            'username' =>  $username,
            'code' => ContractConfig::codeSpaced($otp)
        ];

        // send mail
        MailerController::sendEmail($mailer, $data->getEmail(), $template, $template_datas, "Vérification de l'adresse mail");
        return $this->redirectToRoute('app_client_web_auth_confirm_code_verif');
    }

    #[Route('/entreprise/confirm-code-verif', name: 'app_client_web_auth_confirm_code_verif')]
    public function confirmCodeVerif(
        Request $request, 
        UserRepository $userRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if (!($userSession != null && $userSession->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $codeFormBuilder = $this->createFormBuilder();
        $codeFormBuilder
            ->add('submit', SubmitType::class, [
                'label' => 'Valider'
            ])
        ;

        $codeForm = $codeFormBuilder->getForm();
        $codeForm->handleRequest($request);

        $data = $userRepository->find($userSession->getId());
            
        if ($codeForm->isSubmitted() && $codeForm->isValid()) {
            $code = "";
            foreach ($_POST as $field) {
                if(!is_array($field)) {
                    $code .= $field;
                }
            }

            if ($code != "" && strlen($code) < 5) {
                $this->addFlash('notice', 'Le code entré est invalide');
            } else {
                if ($data->getVerificationCode() == $code) {
                    $data->setEmailVerified(true);
                    $em->persist($data);
                    $em->flush();
                    return $this->redirectToRoute('app_client_web_home');
                } else {
                    $this->addFlash('notice', 'Le code entré est invalide');
                }
            }
        }

        return $this->render('client_web/auth/confirm-code-verif.html.twig', [
            'codeForm' => $codeForm->createView(),
            'email' => $data->getEmail()
        ]);
    }

    #[Route('/entreprise/phone-verif', name: 'phoneVerif', methods: ['POST'])]
    public function phoneVerif(
        Request $request, 
        UserRepository $userRepository,
        EntityManagerInterface $em,
        MailerInterface $mailer
    ): Response
    { 
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');
        if (!($userSession != null && $userSession->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        $data = $userRepository->find($userSession->getId());

        $otp = ContractConfig::generateOtp(5);

        // check if code exist
        if ($data->getPhonecode() != null) {
            $otp = $data->getPhonecode();
        } else {
            $data->setPhonecode($otp);
            $em->persist($data);
            $em->flush();
        }

        $twilio = new RestClient(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        $phone = $data->getPhone();

        if ($data->getType() == UserType::$SOCIETY) {
            $phone = $data->getClient()->getPhone();
        }

        NotificationConfig::sendSms($twilio, ContractConfig::format_number($phone), $otp . " Est le code de vérification de votre numéro de téléphone");
        
        return $this->redirectToRoute('app_client_web_auth_confirm_code_verif_phone');
    }

    #[Route('/entreprise/confirm-phone-verif', name: 'app_client_web_auth_confirm_code_verif_phone')]
    public function confirmPhoneCodeVerif(
        Request $request, 
        UserRepository $userRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $userSession = $session->get('user');

        if (!($userSession != null && $userSession->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $codeFormBuilder = $this->createFormBuilder();
        $codeFormBuilder
            ->add('submit', SubmitType::class, [
                'label' => 'Valider'
            ])
        ;

        $codeForm = $codeFormBuilder->getForm();
        $codeForm->handleRequest($request);

        $data = $userRepository->find($userSession->getId());
            
        if ($codeForm->isSubmitted() && $codeForm->isValid()) {
            $code = "";
            foreach ($_POST as $field) {
                if(!is_array($field)) {
                    $code .= $field;
                }
            }

            if ($code != "" && strlen($code) < 5) {
                $this->addFlash('notice', 'Le code entré est invalide');
            } else {
                if ($data->getPhonecode() == $code) {
                    $data->setPhoneverified(true);
                    $em->persist($data);
                    $em->flush();
                    return $this->redirectToRoute('app_client_web_home');
                } else {
                    $this->addFlash('notice', 'Le code entré est invalide');
                }
            }
        }

        return $this->render('client_web/auth/confirm-code-verif-phone.html.twig', [
            'codeForm' => $codeForm->createView(),
            'phone' => $data->getClient()->getPhone()
        ]);
    }
}
