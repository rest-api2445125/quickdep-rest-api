<?php

namespace App\Controller\ClientWeb;

use App\Configuration\ContractConfig;
use App\Configuration\UserType;
use App\Entity\TimeSheet;
use App\Entity\User;
use App\Form\ClientWeb\TimeSheetType;
use App\Repository\ContractRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;

class TimeSheetController extends AbstractController
{
    #[Route('/entreprise/timesheet', name: 'app_client_web_timesheet')]
    public function index(Request $request, ContractRepository $contractRepository): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $dataReturn = ContractConfig::userTimeSheet(
            $user,
            $contractRepository,
            true,
            true
        );
        
        return $this->render('client_web/time_sheet/index.html.twig',['contracts' => $dataReturn]);
    }

    #[Route('/entreprise/timesheet-approuved', name: 'app_client_web_timesheet_approuved')]
    public function approuved(Request $request, ContractRepository $contractRepository): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $dataReturn = ContractConfig::userApprouvedTimeSheets(
            $user,
            $contractRepository,
            true
        );
        
        return $this->render('client_web/time_sheet/approuved.html.twig',['contracts' => $dataReturn]);
    }

    #[Route('/timesheet/detail-{id}', name: 'app_client_web_timesheet_detail')]
    public function detail(
        Request $request, 
        ContractRepository $contractRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em
    ): Response
    {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!$user != null) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $type = $session->get('type');

        $contract = $contractRepository->find($request->get('id'));

        $timeSheetForm = $this->createForm(TimeSheetType::class, $contract->getTimeSheet());

        if ($contract->getTimeSheet() == null) {

            $start = $contract->getStartdate();
            $end = $contract->getEnddate();
            $pause = $contract->getPause();

            $diff = $start->diff($end);
            $totalT = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i - $pause;
            $totalRate = ($contract->getClientrate() / 60) * $totalT;

            $ts = (new TimeSheet())
                ->setStartdate($contract->getStartdate())
                ->setEnddate($contract->getEnddate())
                ->setPause($contract->getPause())
                ->setContract($contract)
                ->setUser($contract->getEmployee())
                ->setTotaltimes($totalT)
                ->setAmount($totalRate)
            ;

            $em->persist($ts);
            $em->flush();

            $timeSheetForm = $this->createForm(TimeSheetType::class, $ts);
        }

        $timeSheetForm->handleRequest($request);

        if ($timeSheetForm->isSubmitted() && $timeSheetForm->isValid()) {
            $data = $timeSheetForm->getData();

            $twilio = new Client(
                $this->getParameter("app.twilio_ssd"),
                $this->getParameter("app.twilio_token")
            );

            $start = $data->getStartdate();
            $end = $data->getEnddate();
            $pause = $data->getPause();

            $diff = $start->diff($end);
            $totalTime = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i - $pause;

            if ($pause != null) {
                $totalTime -= $pause;
            }
            $totalRate = ($contract->getClientrate() / 60) * $totalTime;

            ContractConfig::contractSendTime(
                $contract,
                $totalTime,
                $totalRate,
                $start->format('H:i'),
                $end->format('H:i'),
                $pause,
                $user,
                $em,
                $twilio
            );

            $this->addFlash(
                'success',
                'Feuille de temps approuvée avec succés!'
            );
        }

        $userClient = $userRepository->findOneBy(['client' => $contract->getClient()]);

        return $this->render('client_web/time_sheet/detail.html.twig', [
            'contract' => $contract,
            'userClient' => $userClient,
            'type' => $type == 'admin' ? 'admin/base.html.twig' : 'client_web/base.html.twig',
            'timeSheetForm' => $timeSheetForm->createView()
        ]);
    }
}
