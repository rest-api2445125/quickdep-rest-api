<?php

namespace App\Controller\ClientWeb;

use App\Configuration\UserType;
use App\Repository\InvoiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    #[Route('/entreprise/invoice', name: 'app_client_web_invoice')]
    public function index(
        InvoiceRepository $invoiceRepository,
        Request $request,
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $invoices = $invoiceRepository->findBy([
            'user' => $user,
            'paid' => false
        ]);

        return $this->render('client_web/invoice/index.html.twig', [
            'invoices' => $invoices,
        ]);
    }

    #[Route('/entreprise/invoice/paid', name: 'app_client_web_invoice_paid')]
    public function paid(
        InvoiceRepository $invoiceRepository,
        Request $request
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $invoices = $invoiceRepository->findBy([
            'user' => $user,
            'paid' => true
        ]);

        return $this->render('client_web/invoice/paid.html.twig', [
            'invoices' => $invoices,
        ]);
    }

    #[Route('/facturation/invoice/{id}', name: 'app_client_web_invoice_detail')]
    public function detail(
        $id,
        InvoiceRepository $invoiceRepository,
        Request $request,
        UserRepository $userRepository
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if ($user == null) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $invoice = $invoiceRepository->find($id);

        return $this->render('client_web/invoice/detail.html.twig', [
            'invoice' => $invoice,
            'type' => $session->get('type'),
        ]); 
    }
}
