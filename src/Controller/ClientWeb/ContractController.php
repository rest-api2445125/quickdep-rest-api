<?php

namespace App\Controller\ClientWeb;

use App\Configuration\ContractConfig;
use App\Configuration\UserType;
use App\Entity\Contract;
use App\Entity\User;
use App\Repository\ApplyRepository;
use App\Repository\ContractRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;

class ContractController extends AbstractController
{
    #[Route('/entreprise/waiting', name: 'app_client_web_contract_waiting')]
    public function index(
        Request $request, 
        ContractRepository $contractRepository,
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $dataReturn = ContractConfig::clientApplies(
            $user->getClient(),
            $contractRepository,
            true
        );

        return $this->render('client_web/contract/waiting.html.twig',['contracts' => $dataReturn]);
    }

    #[Route('/entreprise/approuved', name: 'app_client_web_contract_approuved')]
    public function approuved(
        Request $request, 
        ContractRepository $contractRepository,
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }
        
        $dataReturn = ContractConfig::contractApprouved(
            $user->getClient(),
            $contractRepository,
            true
        );

        
        return $this->render('client_web/contract/approuved.html.twig',['contracts' => $dataReturn]);
    }

    #[Route('/entreprise/confirm/{user_id}/{contract_id}', name: 'app_client_web_contract_confirm', methods: ['POST'])]
    public function confirm(
        $user_id,
        $contract_id,
        Request $request, 
        UserRepository $userRepository,
        ContractRepository $contractRepository,
        ApplyRepository $applyRepository,
        EntityManagerInterface $em
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $employee = $userRepository->find($user_id);
        
        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractConfirm(
            $contractRepository->find($contract_id),
            $twilio,
            $applyRepository,
            $employee,
            $em
        );

        $this->addFlash(
            'success',
            'Shift attribué à avec succès'
        );

        
        return $this->redirectToRoute('app_client_web_home');
    }

    #[Route('/entreprise/cancel/{contract_id}', name: 'app_client_web_contract_cancel', methods: ['POST'])]
    public function cancel(
        $contract_id,
        Request $request, 
        EntityManagerInterface $em
    ): Response {
        $session = $request->getSession();
        /** @var User */
        $user = $session->get('user');

        if (!($user != null && $user->getType() == UserType::$SOCIETY)) {
            return $this->redirectToRoute('app_client_web_auth');
        }

        $data = $em->getRepository(Contract::class)->find($contract_id);

        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractCancel(
            $data,
            $user,
            $em,
            $twilio
        );

        $this->addFlash(
            'success',
            'Shift rétiré à avec succès'
        );

        
        return $this->redirectToRoute('app_client_web_home');
    }
}
