<?php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Configuration\ContractStatus;
use App\Repository\ContractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TimeSheetToSendController extends AbstractController
{
    public function __invoke(Paginator $data, ContractRepository $contractRepository)
    {
        $contracts = $contractRepository->findBy([
            'status' => [
                ContractStatus::$WORKED,
            ]
        ]);

        if (count($contracts) == 0) {
            return [
                "Aucune feuille de temps trouvée"
            ];
        }

        return $contracts;
    }
}
