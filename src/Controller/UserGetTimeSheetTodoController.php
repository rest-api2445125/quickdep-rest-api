<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Entity\User;
use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetTimeSheetTodoController extends AbstractController
{
    public function __invoke(User $data, Request $request, ContractRepository $contractRepository)
    {
        $user_contracts = null;

        if ($data->getType() == UserType::$EMPLOYEE) {
            $user_contracts = $data->getContracts();
        } else {
            $user_contracts = $contractRepository->findBy([
                "client" => $data->getClient()
            ]);
        }

        $data_return = [];
        $perpage = 100;
        $page = $request->get('current_page');
        $last_date = null;
        $j = 1;

        $validate_data = [];

        if ($page > 1) {
            foreach ($user_contracts as $contract) {
                $end_prev = ($page - 1) * $perpage;
                if ($j == $end_prev) {
                    $last_date = $contract->getDate()->format('n') . ' ' . $contract->getDate()->format('Y');
                    break;
                }
                $j++;
            }
        }

        foreach ($user_contracts as $contract) {
            if ($contract->getStatus() == ContractStatus::$WORKED) {
                $validate_data[] = $contract;
            }
        }

        $data_return = ContractConfig::listing($validate_data, $data);

        return $data_return;
    }
}
