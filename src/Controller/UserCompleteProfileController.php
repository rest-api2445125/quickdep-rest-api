<?php

namespace App\Controller;

use App\Configuration\DocumentStatus;
use App\Configuration\UserType;
use App\Entity\Device;
use App\Entity\User;
use App\Entity\UserContract;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserCompleteProfileController extends AbstractController
{
    public function __invoke(User $data, DeviceRepository $deviceRepository, EntityManagerInterface $em, JWTTokenManagerInterface $jWTTokenManager, UserRepository $userRepository, Request $request, UserPasswordHasherInterface $encoder, EntityManagerInterface $manager)
    {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $devicetoken = $request_datas["device"];

        $email = $request_datas['email'];
        $password = $request_datas['password'];

        $exist = $userRepository->findOneBy(["email" => $email]);
        if ($exist != null) {
            return [
                "success" => false,
                "message" => "Un compte existe déjà avec cette adresse mail."
            ];
        }

        $user = $userRepository->createNewUser([
            'email' => $email,
            'password' => $password,
            'type' => UserType::$EMPLOYEE
        ], $em, $encoder);

        $token = $jWTTokenManager->create($user);

        $user_contract = new UserContract();
        $user_contract->settype(UserType::$EMPLOYEE);
        $user_contract->setDate(new DateTime());
        $user_contract->setUpdatedAt(new DateTimeImmutable());

        $em->persist($user_contract);
        $em->flush();

        $user->setUsercontract($user_contract);

        $user->setFirstname($data->getFirstname());
        $user->setLastname($data->getLastname());
        $user->setAddress($data->getAddress());
        $user->setWork($data->getWork());
        $user->setPhone($data->getPhone());
        $user->setBirthday($data->getBirthday());
        $user->setBio($data->getBio());

        $em->persist($user);
        $em->flush();

        if ($devicetoken != null) {
            // save user token
            $existDevice = $deviceRepository->findOneBy(["token" => $devicetoken]);

            if ($existDevice != null) {
                $existDevice->setUser($user);
                $em->persist($existDevice);
                $em->flush();
            } else {
                $device = new Device();
                $device->setToken($devicetoken);
                $device->setUser($user);
                $em->persist($device);
                $em->flush();
            }
        }


        return [
            "success" => true,
            'id' => $user->getId(),
            'token' => $token,
            'type' => UserType::$EMPLOYEE,
            'user' => $user
        ];
    }
}
