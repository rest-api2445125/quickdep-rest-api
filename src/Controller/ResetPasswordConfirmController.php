<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Entity\PasswordReset;
use App\Repository\PasswordResetRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ResetPasswordConfirmController extends AbstractController
{
    public function __invoke(
        PasswordReset $data,
        UserRepository $userRepository,
        PasswordResetRepository $repository,
    ) {
        $email = $data->getEmail();

        // check if account exists
        $reset = $repository->findOneBy(['email' => $email]);

        $code = $data->getCode();

        return ContractConfig::resetPasswordConfirm(
            $reset,
            $code,
            $userRepository,
            $email
        );
    }
}
