<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetLikesController extends AbstractController
{
    public function __invoke(User $data, Request $request)
    {
        $user_contracts = $data->getLikes();

        $data_return = [];
        $perpage = 100;
        $page = $request->get('current_page');
        $last_date = null;
        $j = 1;

        $validate_data = [];


        if ($page > 1) {
            foreach ($user_contracts as $contract) {
                $end_prev = ($page - 1) * $perpage;
                if ($j == $end_prev) {
                    $last_date = $contract->getDate()->format('n') . ' ' . $contract->getDate()->format('Y');
                    break;
                }
                $j++;
            }
        }

        foreach ($user_contracts as $like) {
            $contract = $like->getContract();
            if ($contract->getStatus() == ContractStatus::$PUBLISHED) {
                $validate_data[] = $contract;
            }
        }

        //return $validate_data;

        $data_return = ContractConfig::listing($validate_data, $data, $last_date, $page, $perpage);

        return $data_return;
    }
}
