<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\TownRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserGetTownsController extends AbstractController
{
    public function __invoke(User $data, Request $request, TownRepository $townRepository, EntityManagerInterface $manager)
    {
        return $data->getTown();
    }
}
