<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

class ClientValidationController extends AbstractController
{
    public function __invoke(Client $data, MailerInterface $mailer, UserRepository $userRepository)
    {
        $data->setValidated(true);


        $template = 'email/validation.html.twig';

        $name = $data->getName();

        $template_datas = [
            'name' =>  $name,
        ];

        $client_user = $userRepository->findOneBy([
            'client' => $data
        ]);

        if ($client_user != null) {
            MailerController::sendEmail(
                $mailer, 
                $client_user->getEmail(), 
                $template,
                 $template_datas, 
                 "Validation du compte " . $data->getName(), null,
                 "info@quickdep.com"
            );
        }


        return $data;
    }
}
