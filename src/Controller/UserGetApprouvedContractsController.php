<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetApprouvedContractsController extends AbstractController
{
    public function __invoke(User $data, Request $request)
    {
        $user_contracts = $data->getContracts();

        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if (
                $contract->getStatus() == ContractStatus::$APPROUVED
                && ($contract->getParent() == null || $contract->getParent()->getEmployee()->getId() != $data->getId())
            ) {
                $validate_data[] = $contract;
            }
        }

        return ContractConfig::listing($validate_data, $data);
    }
}
