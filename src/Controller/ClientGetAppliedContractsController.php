<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Client;
use App\Entity\Contract;
use App\Repository\ContractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ClientGetAppliedContractsController extends AbstractController
{
    public function __invoke(Client $data, Request $request, ContractRepository $contractRepository)
    {
        return ContractConfig::clientApplies(
            $data, $contractRepository
        );
    }
}
