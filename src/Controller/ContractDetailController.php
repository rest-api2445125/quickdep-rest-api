<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Entity\Apply;
use App\Entity\Contract;
use App\Entity\TimeSheet;
use App\Entity\User;
use App\Repository\ApplyRepository;
use App\Repository\TimeSheetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ContractDetailController extends AbstractController
{
    private $doctrine;
    private $em;
    private $timeSheetRepository;

    public function __construct(
        ManagerRegistry $doctrine, 
        EntityManagerInterface $em, 
        TimeSheetRepository $timeSheetRepository)
    {
        $this->doctrine = $doctrine;
        $this->em = $em;
        $this->timeSheetRepository = $timeSheetRepository;
    }

    public function __invoke(Contract $data, Request $request)
    {
        /**
         * @var User
         */
        $user = $this->getUser();

        if ($user == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", 1);
        }

        if (
            $user->getType() == UserType::$EMPLOYEE
            &&
            !(
                $data->isPublic($user, $this->em->getRepository(Apply::class))
                || (
                    $data->getStatus() != ContractStatus::$PUBLISHED 
                    && $data->getEmployee()->getId() == $user->getId()
                )
            )
        ) {
            return [
                "success" => true,
                "message" => "Ce shift n'est plus disponible"
            ];
        }

        if ($data->getStatus() == ContractStatus::$PUBLISHED) {
            if ($user->getType() == UserType::$EMPLOYEE) {
                $user_applies = $user->getApplies();

                foreach ($user_applies as $apply) {
                    if ($apply->getContract()->getId() == $data->getId()) {
                        $data->setApplied(true);
                    }
                }
            }
        }

        if ($data->getStatus() == ContractStatus::$TIME_SENT || $data->getStatus() == ContractStatus::$TIME_APPROUVED) {
            $time_sheet = $data->getTimeSheet();

            $data->setStartdate($time_sheet->getStartdate());
            $data->setEnddate($time_sheet->getEnddate());
            $data->setPause($time_sheet->getPause());
        }

        return [
            "success" => true,
            "data" => $data
        ];
    }
}
