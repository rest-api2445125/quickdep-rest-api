<?php

namespace App\Controller;

use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Entity\Document;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DocumentSendFileController extends AbstractController
{
    public function __invoke(Document $data, Request $request, EntityManagerInterface $em)
    {
        $user = $data->getEmployee();

        if ($data->getType() == "text") {
            $text = $request->get('text');
            $data->setText($text);
        } else {
            $file = $request->files->get('file');
            $data->setFile($file);
        }

        $data->setStatut(DocumentStatus::$WAITING);

        $data->setUpdatedAt(new DateTimeImmutable());

        $em->persist($data);
        $em->flush();

        if (DocumentConfig::check_waiting($user)) {
            $user->setDocumentsStatus(DocumentStatus::$WAITING);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_not_sent($user)) {
            $user->setDocumentsStatus(DocumentStatus::$NOT_SENT);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_refused($user)) {
            $user->setDocumentsStatus(DocumentStatus::$REFUSED);

            $em->persist($user);
            $em->flush();
        }


        return [
            'success' => true,
            'message' => 'Document enrégistré avec succès',
            'document' => $data,
            'user' => $user
        ];
    }
}
