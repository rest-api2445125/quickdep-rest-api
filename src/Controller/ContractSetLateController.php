<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\NotificationConfig;
use App\Entity\Contract;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Twilio\Rest\Client;

class ContractSetLateController extends AbstractController
{
    public function __invoke(Contract $data, EntityManagerInterface $em, Request $request)
    {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $late = $request_datas["late"];

        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );
        
        if ($data->getStartdate() < new DateTime()) {
            return [
                'error' => false,
                'message' => "Vous ne pouvez pas signaler un retard après l'heure de début prévu."
            ];
        }

        $data->setLate($late);

        $em->persist($data);
        $em->flush();
        
        // Send sms
        $to = ContractConfig::format_number($data->getClient()->getPhone());

        $message = $data->getEmployee()->getFirstname() . " " . $data->getEmployee()->getLastname()
            . " vient de signaler qu'il sera en retard de " . $late . "min pour le shift prévu pour le " . 
            ContractConfig::full_date($data->getStartdate(), true) . " de "
            . $data->getStartdate()->format("H:i") . " à " . $data->getEnddate()->format("H:i")
            . " au poste de " . $data->getJob()->getTitle();
        NotificationConfig::sendSms($twilio, $to, $message);

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
