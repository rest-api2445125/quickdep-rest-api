<?php

namespace App\Controller;

use App\Configuration\Notification;
use App\Configuration\NotificationType;
use App\Configuration\UserType;
use App\Entity\Client;
use App\Entity\Contract;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ClientPostController extends AbstractController
{
    public function __invoke(Client $data, Request $request, EntityManagerInterface $em, MailerInterface $mailer, UserRepository $userRepository, HttpClientInterface $httpClient, DeviceRepository $deviceRepository)
    {
        $user = $this->getUser();

        if ($user != null && $user->getType() == UserType::$SOCIETY) {
            $user->setClient($data);
            $user->setRegisterCompleted(true);
            $user->setPhone($data->getPhone());

            $data->setValidated(false);

            $em->persist($user);
            $em->flush();

            $em->persist($data);
            $em->flush();

            return $userRepository->find($user->getId());
        }

        return $data;
    }
}
