<?php

namespace App\Controller;

use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Entity\Document;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DocumentSendTextController extends AbstractController
{
    public function __invoke(Document $data, Request $request, EntityManagerInterface $em)
    {
        $user = $data->getEmployee();

        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $data->setText($request_datas["text"]);

        $data->setStatut(DocumentStatus::$VALIDATED);

        $data->setUpdatedAt(new DateTimeImmutable());

        $em->persist($data);
        $em->flush();

        if (DocumentConfig::check_waiting($user)) {
            $user->setDocumentsStatus(DocumentStatus::$WAITING);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_not_sent($user)) {
            $user->setDocumentsStatus(DocumentStatus::$NOT_SENT);

            $em->persist($user);
            $em->flush();
        }

        if (DocumentConfig::check_refused($user)) {
            $user->setDocumentsStatus(DocumentStatus::$REFUSED);

            $em->persist($user);
            $em->flush();
        }

        return [
            'success' => true,
            'message' => 'Document enrégistré avec succès',
            'document' => $data,
            'user' => $user
        ];
    }
}
