<?php

namespace App\Controller;

use App\Configuration\DocumentStatus;
use App\Entity\Contract;
use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DocumentUnValidatedController extends AbstractController
{
    public function __invoke(Document $data, EntityManagerInterface $em): Document
    {
        $data->setStatut(DocumentStatus::$REFUSED);
        $user = $data->getEmployee();
        $user->setDocumentsStatus(DocumentStatus::$REFUSED);

        $em->persist($user);
        $em->flush();

        return $data;
    }
}
