<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\NotificationConfig;
use App\Configuration\UserType;
use App\Entity\CanceledContract;
use App\Entity\Contract;
use App\Entity\User;
use App\Repository\ApplyRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Twilio\Rest\Client;


class ContractCanceledController extends AbstractController
{
    private $doctrine;
    private $em;
    private $applyRepository;

    public function __construct(ManagerRegistry $doctrine, EntityManagerInterface $em, ApplyRepository $applyRepository)
    {
        $this->doctrine = $doctrine;
        $this->em = $em;
        $this->applyRepository = $applyRepository;
    }

    public function __invoke(Contract $data)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter");
        }

        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractCancel(
            $data,
            $user,
            $this->em,
            $twilio
        );

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
