<?php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Entity\Client;
use App\Entity\Invoice;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class InvoicesGetForEmployeesController extends AbstractController
{
    public function __invoke(Paginator $data, Request $request)
    {
        $return_data = [];
        foreach ($data as $invoice) {
            if ($invoice->getUser()->getType() == UserType::$EMPLOYEE) {
                $return_data[] = $invoice;
            }
        }
        return $return_data;
    }
}
