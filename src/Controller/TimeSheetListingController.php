<?php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Configuration\ContractStatus;
use App\Repository\ContractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TimeSheetListingController extends AbstractController
{
    public function __invoke(Paginator $data, ContractRepository $contractRepository)
    {
        $contracts = $contractRepository->findBy([
            'status' => [
                ContractStatus::$TIME_SENT,
                ContractStatus::$TIME_APPROUVED,
            ]
        ]);

        $return_dats = [];

        foreach ($contracts as $contract) {
            if ($contract->getTimeSheet() != null) {
                $total = $contract->getTimeSheet()->getTotaltimes();
                $houre = intdiv($total, 60);
                $reste = ($total / 60) - $houre;
                $mins = $reste * 60;

                $return_dats[] = [
                    "id" => $contract->getTimeSheet()->getId(),
                    "user" => $contract->getEmployee(),
                    "contract" => $contract,
                    "startdate" => $contract->getTimeSheet()->getStartdate(),
                    "enddate" => $contract->getTimeSheet()->getEnddate(),
                    "pause" => $contract->getTimeSheet()->getPause(),
                    "houres" => $houre . "h" . $mins,
                    "amount" => $contract->getTimeSheet()->getAmount(),
                    "status" => $contract->getTimeSheet()->getStatus(),
                ];
            }
        }

        if (count($return_dats) == 0) {
            return [
                "Aucune feuille de temps trouvée"
            ];
        }

        return $return_dats;
    }
}
