<?php

namespace App\Controller;

use App\Configuration\UserType;
use App\Entity\Invoice;
use App\Entity\User;
use App\Entity\UserContract;
use App\Repository\InvoiceRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContractFileDownloadContoller extends AbstractController
{
    public function __invoke(User $data, MailerInterface $mailer, Request $request)
    {
        $email = $data->getEmail();
        $template = "email/user_contract.html.twig";
        $template_datas = [
            "invoice" => $data,
            "name" => $data->getType() == UserType::$EMPLOYEE ? $data->getLastname() . " " . $data->getFirstname() : $data->getClient()->getOwner(),
            "society" => $data->getType() == UserType::$EMPLOYEE ? "" : $data->getClient()->getName(),
            "asked" => true
        ];
        $subject = "Contract " . $data->getType() == UserType::$EMPLOYEE ? "Contract du travailleu QuickDep" : "Contract de facturation QuickDep";

        $from = "";
        $file_path = "";
        if ($data->getType() == UserType::$EMPLOYEE) {
            $file_path = "contracts/employee/pdf" . $data->getUsercontract()->getId() . ".pdf";
            $from = "facturation@quickdep.ca";
        } else {
            $file_path = "contracts/society/pdf/" . $data->getUsercontract()->getId() . ".pdf";
            $from = "facturation@quickdep.ca";
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject, [
                "path" => $file_path,
                "name" => "Contract  " . UserType::$EMPLOYEE ?  $data->getFirstname() . " " . $data->getFirstname() : $data->getClient()->getName()
            ], $from);
        }

        return [
            "success" => true,
            "message" => "Fichier envoyé par mail avec succès"
        ];
    }
}
