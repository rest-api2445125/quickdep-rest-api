<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\NotificationConfig;
use App\Configuration\UserType;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Twilio\Rest\Client;

class UserPhoneVerificationController extends AbstractController
{
    public function __invoke(User $data, Request $request, MailerInterface $mailer, EntityManagerInterface $entityManagerInterface)
    {
        $otp = ContractConfig::generateOtp(5);

        // check if code exist
        if ($data->getPhonecode() != null) {
            $otp = $data->getPhonecode();
        } else {
            $data->setPhonecode($otp);
            $entityManagerInterface->persist($data);
            $entityManagerInterface->flush();
        }

        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        $phone = $data->getPhone();

        if ($data->getType() == UserType::$SOCIETY) {
            $phone = $data->getClient()->getPhone();
        }

        NotificationConfig::sendSms($twilio, ContractConfig::format_number($phone), $otp . " Est le code de vérification de votre numéro de téléphone");

        return [
            "success" => true,
            "message" => "Code de vérification envoyé avec succès"
        ];
    }
}
