<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\DocumentConfig;
use App\Configuration\NotificationConfig;
use App\Entity\Contract;
use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Twilio\Rest\Client;

class ContractNotWorkedController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(Contract $data)
    {
        if ($data->getEmployee() != null) {
            if ($data->getEmployee()->getNotworked() != null)
                $data->getEmployee()->setNotworked($data->getEmployee()->getNotworked() + 1);
            else
                $data->getEmployee()->setNotworked(1);

            $data->setStatus(ContractStatus::$NOT_WORKED);

            $this->em->persist($data);
            $this->em->flush();

            $twilio = new Client(
                $this->getParameter("app.twilio_ssd"),
                $this->getParameter("app.twilio_token")
            );
            
            // Send sms
            $to = ContractConfig::format_number($data->getEmployee()->getPhone());

            $message = $data->getClient()->getName()
                . " vient de signaler que vous ne vous êtes pas presenté pour le shift prévu pour le " .
                ContractConfig::full_date($data->getStartdate(), true) . " à "
                . $data->getStartdate()->format("H:i") . " au poste de " . $data->getJob()->getTitle()
                . ". Deux absences non justifiées conduiront au blocage de votre comptes.";
            NotificationConfig::sendSms($twilio, $to, $message);

            foreach (DocumentConfig::$ADMIN_NUMBERS as $toAdmin) {
                $message = $data->getClient()->getName()
                    . " vient de signaler que " 
                    . $data->getEmployee()->getFirstname() . " " . $data->getEmployee()->getLastname() 
                    . " ne s'est pas presenté pour le shift prévu pour le " .
                    ContractConfig::full_date($data->getStartdate(), true) . " de "
                    . $data->getStartdate()->format("H:i") . " au poste de " . $data->getJob()->getTitle()
                    . ". code : " . $data->getCode();
                NotificationConfig::sendSms($twilio, $toAdmin, $message);
            }

            return [
                'success' => true,
                'data' => $data
            ];
        }

        return [
            "success" => false,
            "message" => "Aucun utilisateur n'a postulé pour ce contrat"
        ];
    }
}
