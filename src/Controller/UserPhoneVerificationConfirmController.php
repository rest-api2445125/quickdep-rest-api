<?php

namespace App\Controller;

use App\Entity\PasswordReset;
use App\Entity\User;
use App\Repository\PasswordResetRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

class UserPhoneVerificationConfirmController extends AbstractController
{
    public function __invoke(User $data, Request $request, UserRepository $userRepository, PasswordResetRepository $repository, EntityManagerInterface $em, MailerInterface $mailer)
    {
        $code = json_decode($request->getContent())->code;
        if ($data->getPhonecode() == $code) {
            $data->setPhoneverified(true);
            $em->persist($data);
            $em->flush();
            return [
                "success" => true,
                "message" => "Numéro de téléphone vérifié avec succès !",
                "user" => $data
            ];
        }
        return [
            "success" => false,
            "message" => "Code invalide"
        ];
    }
}
