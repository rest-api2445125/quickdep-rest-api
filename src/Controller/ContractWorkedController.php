<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\NotificationConfig;
use App\Configuration\NotificationType;
use App\Entity\CanceledContract;
use App\Entity\Contract;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

class ContractWorkedController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(
        Contract $data,
    ) {
        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractWorked(
            $data,
            $this->em,
            $twilio
        );

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
    