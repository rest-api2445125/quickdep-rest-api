<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\Notification;
use App\Configuration\NotificationConfig;
use App\Configuration\NotificationType;
use App\Entity\CanceledContract;
use App\Entity\Contract;
use App\Repository\ContractRepository;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

class ContractEditController extends AbstractController
{
    public function __invoke(
        Contract $data,
        DeviceRepository $deviceRepository,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        Request $request,
        ContractRepository $contractRepository
    ) {
        
        $twilioClient = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::editContract(
            $contractRepository,
            $data,
            $twilioClient,
            $em,
            $userRepository,
            $httpClient,
            $deviceRepository
        );

        return [
            'success' => true,
            'data' => $data,
            'message' => 'Contrat modifié avec succès'
        ];
    }
}
