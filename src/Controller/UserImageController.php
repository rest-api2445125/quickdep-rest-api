<?php

namespace App\Controller;

use App\Entity\User;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserImageController extends AbstractController
{
    public function __invoke(User $data, Request $request): User
    {
        $image = $request->files->get('image');
        $data->setImage($image);
        $data->setUpdatedAt(new DateTimeImmutable());

        return $data;
    }
}
