<?php

namespace App\Controller;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Address;

class MailerController extends AbstractController
{
    public static function sendEmail($mailer, $to, $template, $datas, $subject, $attachment = null, $from = "facturation@quickdep.ca")
    {
        $email = (new TemplatedEmail())
            ->from(new Address("facturation@quickdep.ca", "QuickDep"))
            ->to($to)
            ->subject($subject)

            // path of the Twig template to render
            ->htmlTemplate($template)

            // pass variables (name => value) to the template
            ->context($datas);

        if ($attachment != null) {
            $email->attachFromPath($attachment["path"], $attachment["name"]);
        }

        if ($mailer->send($email)) {
            return true;
        } else {
            return false;
        }
    }
}
