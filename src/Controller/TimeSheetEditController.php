<?php

namespace App\Controller;

use App\Configuration\TimeSheetStatus;
use App\Entity\TimeSheet;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Math;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TimeSheetEditController extends AbstractController
{
    public function __invoke(TimeSheet $data, EntityManagerInterface $em)
    {
        $start = $data->getStartdate()->format('H:i');
        $end = $data->getEnddate()->format('H:i');
        $pause = $data->getPause() == null ? null : $data->getPause();

        $start_arr = explode(':', $start);
        $end_arr = explode(':', $end);
        $pause_arr = null;

        $total_start = (int) $start_arr[0] * 60 + (int) $start_arr[1];
        $total_end = (int) $end_arr[0] * 60 + (int) $end_arr[1];

        $total = $total_end - $total_start;

        if ($pause != null) {
            $pause_arr = explode(':', $pause);
            $total_pause = (int) $pause_arr[0] * 60 + (int) $pause_arr[1];
            $total = $total_end - $total_start - $total_pause;
        }

        $total_h = $total / 60;

        $total_rate = $total_h * $data->getContract()->getJob()->getRate();

        $data->setTotaltimes($total);
        $data->setAmount($total_rate);

        $em->persist($data);
        $em->flush();

        return $data;
    }
}
