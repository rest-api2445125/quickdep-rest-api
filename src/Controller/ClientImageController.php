<?php

namespace App\Controller;

use App\Entity\Client;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ClientImageController extends AbstractController
{
    public function __invoke(Client $data, Request $request): Client
    {
        $image = $request->files->get('image');
        $data->setImage($image);
        $data->setUpdatedAt(new DateTimeImmutable());

        return $data;
    }
}
