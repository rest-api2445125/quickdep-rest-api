<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Entity\Document;
use App\Entity\Invoice;
use App\Repository\ContractRepository;
use App\Repository\InvoiceRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InvoiceGenerateController extends AbstractController
{
    public function __invoke(
        UserRepository $userRepository,
        ContractRepository $contractRepository,
        MailerInterface $mailer,
        EntityManagerInterface $em
    ) {
        $all_time_approuved_contracts = $contractRepository->findBy([
            'status' => ContractStatus::$TIME_APPROUVED,
            'paid' => false
        ]);

        $today = new DateTime();
        $lastWeek = $today->modify('-1 week');
        $lastMonday = $lastWeek->modify('last monday');
        $lastSunday = $lastWeek->modify('next sunday');

        $invoices_user = new ArrayCollection();

        foreach ($all_time_approuved_contracts as $contract) {
            if ($lastMonday <= $contract->getStartdate() && $contract->getEnddate() <= $lastSunday) {
                $client_user = $userRepository->findOneBy([
                    "client" => $contract->getClient()
                ]);
    
                // employee invoice
                if ($contract->getEmployee() != null) {
                    if ($invoices_user[$contract->getEmployee()->getEmail()] == null) {
    
                        $invoice = (new Invoice())
                            ->addContract($contract)
                            ->setDate(new DateTime())
                            ->setUser($contract->getEmployee())
                            ->setStart($lastMonday)
                            ->setEnd($lastSunday)
                        ;
                        
                        if ($contract->getBonus() == null) {
                            $invoice->setTotal(($contract->getTimeSheet()->getTotaltimes() / 60) * $contract->getRate());
                        } else {
                            $invoice->setTotal(($contract->getTimeSheet()->getTotaltimes() / 60) * ($contract->getRate() + $contract->getBonus()));
                        }
    
                        $invoice
                            ->setCode("QD")
                            ->setPaid(false)
                        ;
                        
                        $em->persist($invoice);
                        $em->flush();
    
                        $invoice->setCode($this->generate_code($invoice));
                        $invoices_user[$contract->getEmployee()->getEmail()] = $invoice;
    
                        $em->persist($invoice);
                        $em->flush();
                    } else {
                        $invoices_user[
                            $contract
                                ->getEmployee()
                                ->getEmail()
                        ]->addContract($contract);

                        if ($contract->getBonus() == null) {
                            $invoices_user[
                                $contract
                                    ->getEmployee()
                                    ->getEmail()
                            ]->setTotal(
                                $invoices_user[
                                    $contract
                                        ->getEmployee()
                                        ->getEmail()
                                    ]->getTotal() + ($contract->getTimeSheet()->getTotaltimes() / 60) * $contract->getRate());
                        } else {
                            $invoices_user[
                                $contract
                                    ->getEmployee()
                                    ->getEmail()
                            ]->setTotal(
                                $invoices_user[
                                    $contract
                                        ->getEmployee()
                                        ->getEmail()
                                    ]->getTotal() + (
                                        $contract
                                            ->getTimeSheet()
                                            ->getTotaltimes() / 60
                                    ) * ($contract->getRate() + $contract->getBonus()));
                        }
                    }
                }
    
                // invoices client
                if ($contract->getClient() != null && $client_user != null) {
                    if ($invoices_user[$client_user->getEmail()] == null) {
                        $invoice = (new Invoice())
                            ->addContract($contract)
                            ->setDate(new DateTime())
                            ->setUser($client_user)
                        ;

                        if ($contract->getBonus() == null) {
                            $invoice->setTotal(($contract->getTimeSheet()->getTotaltimes() / 60) * $contract->getClientrate());
                        } else {
                            $invoice->setTotal(($contract->getTimeSheet()->getTotaltimes() / 60) * ($contract->getClientrate() + $contract->getBonus()));
                        }

                        $invoice
                            ->setCode("QD")
                            ->setPaid(false)
                        ;

                        $em->persist($invoice);
                        $em->flush();

                        $invoice->setCode($this->generate_code($invoice));
                        $invoices_user[$client_user->getEmail()] = $invoice;
    
                        $em->persist($invoice);
                        $em->flush();
                    } else {
                        $invoices_user[$client_user->getEmail()]->addContract($contract);
                        if ($contract->getBonus() == null) {
                            $invoices_user[$client_user->getEmail()]->setTotal($invoices_user[$client_user->getEmail()]->getTotal() + ($contract->getTimeSheet()->getTotaltimes() / 60) * $contract->getClientrate());
                        } else {
                            $invoices_user[$client_user->getEmail()]->setTotal($invoices_user[$client_user->getEmail()]->getTotal() + ($contract->getTimeSheet()->getTotaltimes() / 60) * ($contract->getClientrate() + $contract->getBonus()));
                        }
                    }
                }
    
                $contract->setPaid(true);
                $contract->setStatus(ContractStatus::$PAID);
    
                $em->persist($contract);
                $em->flush();
            }
        }

        $template = "email/pay.html.twig";
        $subject = "Vous avez une nouvelle facture de QuickDep";

        $file_path = "invoices/client.xlsx";
        $from = "facturation@quickdep.ca";

        /** @var Invoice $invoice */
        foreach ($invoices_user as $email => $invoice) {
            $template_datas = [
                "invoice" => $invoice,
                "startdate" => date('Y-m-d', strtotime('-7 day', strtotime($invoice->getDate()->format("Y-m-d")))),
                "name" => $invoice->getUser()->getType() == UserType::$EMPLOYEE ? $invoice->getUser()->getLastname() . " " . $invoice->getUser()->getFirstname() : $invoice->getUser()->getClient()->getOwner(),
                "society" => $invoice->getUser()->getType() == UserType::$EMPLOYEE ? "" : $invoice->getUser()->getClient()->getName(),
            ];

            if ($invoice->getUser()->getType() == UserType::$EMPLOYEE) {
                $file_path = "invoices/employee.xlsx";
                $from = "facturation@quickdep.ca";
            } else {
                $file_path = "invoices/client.xlsx";
                $from = "facturation@quickdep.ca";
            }

            $spreadsheet = IOFactory::load($file_path);
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('G3', '#' . $invoice->getCode());

            if ($invoice->getUser()->getType() == UserType::$EMPLOYEE) {

                $today = new DateTime();

                $first_row = 22;
                $index = 0;

                foreach ($invoice->getContracts() as $invoice_contract) {
                    $hours = $invoice_contract->getTimeSheet()->getTotaltimes() % 60 == 0 ? $invoice_contract->getTimeSheet()->getTotaltimes() / 60 . "H" : floor($invoice_contract->getTimeSheet()->getTotaltimes() / 60) . "H" . ($invoice_contract->getTimeSheet()->getTotaltimes() / 60 - floor($invoice_contract->getTimeSheet()->getTotaltimes() / 60)) * 60;

                    $sheet->setCellValue('B' . $first_row + $index, "#" . $invoice_contract->getCode());

                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('B' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('C' . $first_row + $index, $invoice_contract->getClient()->getName() . " (" . $invoice_contract->getJob()->getTitle() . ")");

                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('C' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('D' . $first_row + $index, ContractConfig::full_date(date: $invoice_contract->getStartdate(), dayName: false, year: true));

                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('D' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('E' . $first_row + $index, $hours);

                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('E' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('F' . $first_row + $index, $invoice_contract->getRate() . "$/H");

                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    if ($invoice_contract->getBonus() == null) {
                        $sheet->setCellValue('G' . $first_row + $index, "-");
                    } else {
                        $sheet->setCellValue('G' . $first_row + $index, $invoice_contract->getBonus() . "$/H");
                    }

                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    if ($invoice_contract->getBonus() == null) {
                        $sheet->setCellValue('H' . $first_row + $index, ($invoice_contract->getTimeSheet()->getTotaltimes() / 60) * $invoice_contract->getRate() . "$");
                    } else {
                        $sheet->setCellValue('H' . $first_row + $index, ($invoice_contract->getTimeSheet()->getTotaltimes() / 60) * ($invoice_contract->getRate() + $invoice_contract->getBonus()) . "$");
                    }

                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setIndent(1);

                    $index++;
                }

                $sheet->setCellValue('C3', 'Prénom et nom: : ' . $invoice->getUser()->getFirstname() . " " . $invoice->getUser()->getLastname());
                $sheet->setCellValue('C4', 'Adresse postale : ' . $invoice->getUser()->getAddress());
                $sheet->setCellValue('C5', 'Tél : ' . $invoice->getUser()->getPhone());
                $sheet->setCellValue('C6', 'Courriel : ' . $invoice->getUser()->getEmail());
                $sheet->setCellValue('H7', ContractConfig::full_date(date: $today, dayName: true, year: true));
                $sheet->setCellValue('C34', ContractConfig::full_date(date: $today, dayName: true, year: true));
                $sheet->setCellValue('G12', " " .  ContractConfig::full_date($invoice->getStart()) . " au " . ContractConfig::full_date($invoice->getEnd()));

                $sheet->mergeCells('F' . $first_row + $index . ':G' . $first_row + $index);
                $sheet->mergeCells('F' . $first_row + $index + 1 . ':G' . $first_row + $index + 1);
                $sheet->mergeCells('F' . $first_row + $index + 2 . ':G' . $first_row + $index + 2);

                $sheet->setCellValue('F' . $first_row + $index, "Total");

                $sheet->setCellValue('H' . $first_row + $index, round($invoice->getTotal(), 2) . "$");
                $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('H' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('H' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->setCellValue('F' . $first_row + $index + 1, "Ajustements");

                $sheet->setCellValue('H' . $first_row + $index + 1, "0" . "$");
                $sheet->getStyle('H' . $first_row + $index + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index + 1)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('H' . $first_row + $index + 1)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('H' . $first_row + $index + 1)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('F' . $first_row + $index + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->setCellValue('F' . $first_row + $index + 2, "Total à payer");

                $sheet->setCellValue('H' . $first_row + $index + 2, round($invoice->getTotal(), 2) . "$");
                $sheet->getStyle('H' . $first_row + $index + 2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index + 2)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

                $sheet->getStyle('G' . $first_row + $index + 2)->getFont()->setBold(true);
                $sheet->getStyle('H' . $first_row + $index + 2)->getFont()->setBold(true);
                $sheet->getStyle('F' . $first_row + $index + 2)->getFont()->setSize(15);
                $sheet->getStyle('H' . $first_row + $index + 2)->getFont()->setSize(15);

                $sheet->getStyle('F' . $first_row + $index + 2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index + 2)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


                $sheet->mergeCells('B' . $first_row + $index + 8 . ':H' . $first_row + $index + 8);
                $sheet->setCellValue('B' . $first_row + $index + 8, "Merci de faire confiance à QuickDep");
                $sheet->getStyle('B' . $first_row + $index + 8)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $sheet
                    ->getStyle('B' . $first_row + $index + 8)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('29216B');

                $sheet->getStyle('B' . $first_row + $index + 8)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B' . $first_row + $index + 8)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            } else {
                $today = new DateTime();

                $first_row = 22;
                $index = 0;

                foreach ($invoice->getContracts() as $invoice_contract) {
                    $hours = $invoice_contract->getTimeSheet()->getTotaltimes() % 60 == 0 ? $invoice_contract->getTimeSheet()->getTotaltimes() / 60 . "H" : floor($invoice_contract->getTimeSheet()->getTotaltimes() / 60) . "H" . ($invoice_contract->getTimeSheet()->getTotaltimes() / 60 - floor($invoice_contract->getTimeSheet()->getTotaltimes() / 60)) * 60;

                    $sheet->setCellValue('B' . $first_row + $index, "#" . $invoice_contract->getCode());

                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('B' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('B' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('C' . $first_row + $index, $invoice_contract->getJob()->getTitle());

                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('C' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('C' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('D' . $first_row + $index, ContractConfig::full_date(date: $invoice_contract->getStartdate(), dayName: false, year: true));

                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('D' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('D' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('E' . $first_row + $index, $hours);

                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('E' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('E' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->setCellValue('F' . $first_row + $index, $invoice_contract->getClientrate() . "$/H");

                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    if ($invoice_contract->getBonus() == null) {
                        $sheet->setCellValue('G' . $first_row + $index, "-");
                    } else {
                        $sheet->setCellValue('G' . $first_row + $index, $invoice_contract->getBonus() . "$/H");
                    }

                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('G' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    if ($invoice_contract->getBonus() == null) {
                        $sheet->setCellValue('H' . $first_row + $index, ($invoice_contract->getTimeSheet()->getTotaltimes() / 60) * $invoice_contract->getClientrate() . "$");
                    } else {
                        $sheet->setCellValue('H' . $first_row + $index, ($invoice_contract->getTimeSheet()->getTotaltimes() / 60) * ($invoice_contract->getClientrate() + $invoice_contract->getBonus()) . "$");
                    }

                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setIndent(1);

                    $index++;
                }

                $sheet->setCellValue('C3', 'Nom entreprise : ' . $invoice->getUser()->getClient()->getName());
                $sheet->setCellValue('C4', 'Adresse postale : ' . $invoice->getUser()->getClient()->getAddress());
                $sheet->setCellValue('C5', 'Tél : ' . $invoice->getUser()->getClient()->getPhone());
                $sheet->setCellValue('C6', 'Courriel : ' . $invoice->getUser()->getEmail());
                $sheet->setCellValue('G12', ContractConfig::full_date($invoice->getStart()) . " au " . ContractConfig::full_date($invoice->getEnd()));

                $sheet->setCellValue('D34', ContractConfig::full_date(date: $today, dayName: true, year: true));

                $sheet->mergeCells('F' . $first_row + $index . ':G' . $first_row + $index);
                $sheet->mergeCells('F' . $first_row + $index + 1 . ':G' . $first_row + $index + 1);
                $sheet->mergeCells('F' . $first_row + $index + 2 . ':G' . $first_row + $index + 2);

                $sheet->setCellValue('F' . $first_row + $index, "Total");

                $sheet->setCellValue('H' . $first_row + $index, round($invoice->getTotal(), 2) . "$");
                $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('H' . $first_row + $index)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('H' . $first_row + $index)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->setCellValue('F' . $first_row + $index + 1, "Ajustements");

                $sheet->setCellValue('H' . $first_row + $index + 1, "0" . "$");
                $sheet->getStyle('H' . $first_row + $index + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index + 1)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('F' . $first_row + $index + 1)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('H' . $first_row + $index + 1)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getStyle('H' . $first_row + $index + 1)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet->getStyle('F' . $first_row + $index + 1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index + 1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->setCellValue('F' . $first_row + $index + 2, "Total à payer");

                $sheet->setCellValue('H' . $first_row + $index + 2, round($invoice->getTotal(), 2) . "$");
                $sheet->getStyle('H' . $first_row + $index + 2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H' . $first_row + $index + 2)->getAlignment()->setIndent(1);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('F' . $first_row + $index + 2)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
                $sheet->getStyle('H' . $first_row + $index + 2)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

                $sheet->getStyle('G' . $first_row + $index + 2)->getFont()->setBold(true);
                $sheet->getStyle('H' . $first_row + $index + 2)->getFont()->setBold(true);
                $sheet->getStyle('F' . $first_row + $index + 2)->getFont()->setSize(15);
                $sheet->getStyle('H' . $first_row + $index + 2)->getFont()->setSize(15);

                $sheet->getStyle('F' . $first_row + $index + 2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F' . $first_row + $index + 2)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


                $sheet->mergeCells('B' . $first_row + $index + 8 . ':H' . $first_row + $index + 8);
                $sheet->setCellValue('B' . $first_row + $index + 8, "Merci de faire confiance à QuickDep");
                $sheet->getStyle('B' . $first_row + $index + 8)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $sheet
                    ->getStyle('B' . $first_row + $index + 8)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('29216B');

                $sheet->getStyle('B' . $first_row + $index + 8)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B' . $first_row + $index + 8)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            }

            $path = "invoices/" . $invoice->getCode() . ".xlsx";
            $writer = new Xlsx($spreadsheet);
            $writer->save($path);

            $pdf_path = "invoices/pdf/" . $invoice->getCode() . ".pdf";

            $pdf_writer = IOFactory::createWriter($spreadsheet, 'Mpdf');

            $pdf_writer->save($pdf_path);

            if ($invoice != null) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject, [
                        "path" => $pdf_path,
                        "name" => "Facture " . $invoice->getCode()
                    ], $from);
                }
            }
        }

        return [
            'success' => true,
            'Message' => "Factures générées avec succès"
        ];
    }

    public function generate_code(Invoice $invoice)
    {
        $code = "QD-F";
        $code .= date_format($invoice->getDate(), "mdy") . '-';
        $id = $invoice->getId();
        $code .= $id;

        return $code;
    }
}
