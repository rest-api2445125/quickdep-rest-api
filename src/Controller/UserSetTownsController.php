<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\TownRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserSetTownsController extends AbstractController
{
    public function __invoke(User $data, Request $request, TownRepository $townRepository, EntityManagerInterface $manager)
    {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        // remove all users towns
        foreach ($data->getTown() as $town) {
            $data->removeTown($town);
        }

        foreach ($request_datas as $key => $value) {
            if ($value) {
                $id = explode('_', $key)[1];
                $town = $townRepository->find((int) $id);
                $data->addTown($town);
            }
        }

        $manager->persist($data);
        $manager->flush();

        return [
            "success" => true,
            "messages" => "Villes de travail enrégistrées avec succès"
        ];
    }
}
