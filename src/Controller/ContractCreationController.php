<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\Notification;
use App\Configuration\NotificationConfig;
use App\Configuration\NotificationType;
use App\Entity\CanceledContract;
use App\Entity\Contract;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

class ContractCreationController extends AbstractController
{
    public function __invoke(
        Contract $data,
        DeviceRepository $deviceRepository,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        Request $request
    ) {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);
        $consecutive = false;
        if (array_key_exists("consecutive", $request_datas)) {
            $consecutive = $request_datas["consecutive"];
        }

        $twilioClient = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::createContract(
            $data,
            $consecutive,
            $entityManager,
            $twilioClient,
            $deviceRepository,
            $userRepository,
            $httpClient,
        );

        return [
            'success' => true,
            'data' => $data,
            'message' => 'Contrat publié avec succès'
        ];
    }

}
