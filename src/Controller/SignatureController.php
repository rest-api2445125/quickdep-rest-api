<?php

namespace App\Controller;

use App\Configuration\UserType;
use App\Entity\User;
use App\Entity\UserContract;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SignatureController extends AbstractController
{
    public function __invoke(
        UserContract $data,
        Request $request,
        EntityManagerInterface $em,
        UserRepository $userRepository
    ) {
        $signature = $request->files->get('signature');

        $user = $userRepository->findOneBy(["usercontract" => $data]);

        $data->setSignature($signature);

        $user->setSigned(true);

        $data->setDate(new DateTime());
        $data->setUpdatedAt(new DateTimeImmutable());

        if ($user->getType() == UserType::$EMPLOYEE) {
            $file_path = "contracts/employee/model.xlsx";
        } else {
            $file_path = "contracts/society/model.xlsx";
        }

        $spreadsheet = IOFactory::load($file_path);
        $sheet = $spreadsheet->getActiveSheet();

        $em->persist($user);
        $em->persist($data);
        $em->flush();

        $sheet->setCellValue('C7', $data->getDate()->format("Y-m-d"));
        if ($user->getType() == UserType::$EMPLOYEE) {
            $sheet->setCellValue('C9', $user->getLastname() . " " . $user->getFirstname() . " ");
            $sheet->setCellValue('E39', $user->getLastname() . " " . $user->getFirstname());
        } else if ($user->getType() == UserType::$SOCIETY) {
            $sheet->setCellValue('C9', $user->getClient()->getName() . "  ");
            $sheet->setCellValue('E39', $user->getClient()->getName());
        }

        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('signature');
        $drawing->setDescription('signature');
        $drawing->setPath('users/signatures/' . $data->getSignaturePath()); // put your path and image here
        $drawing->setCoordinates('G35');
        $drawing->setHeight(250);
        $drawing->setWidth(200);
        $drawing->setWorksheet($sheet);

        $writer = new Xlsx($spreadsheet);
        $writer->save('image.xlsx');

        $path = "";
        if ($user->getType() == UserType::$EMPLOYEE) {
            $path = "contracts/employee/" . $data->getId() . ".xlsx";
        } else if ($user->getType() == UserType::$SOCIETY) {
            $path = "contracts/society/" . $data->getId() . ".xlsx";
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($path);

        $pdf_path = "";
        if ($user->getType() == UserType::$EMPLOYEE) {
            $pdf_path = "contracts/employee/pdf/" . $data->getId() . ".pdf";
        } else if ($user->getType() == UserType::$SOCIETY) {
            $pdf_path = "contracts/society/pdf/" . $data->getId() . ".pdf";
        }

        $pdf_writer = IOFactory::createWriter($spreadsheet, 'Mpdf');

        $pdf_writer->save($pdf_path);


        return [
            'success' => true,
            'message' => 'Contrat signé avec succès',
            'user' => $data
        ];
    }
}
