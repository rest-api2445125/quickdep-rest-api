<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\Notification;
use App\Configuration\NotificationConfig;
use App\Entity\Apply;
use App\Entity\CanceledContract;
use App\Entity\Contract;
use App\Entity\User;
use App\Repository\ApplyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Twilio\Rest\Client;

class ContractConfirmController extends AbstractController
{
    private $doctrine;
    private $em;

    public function __construct(ManagerRegistry $doctrine, EntityManagerInterface $em)
    {
        $this->doctrine = $doctrine;
        $this->em = $em;
    }

    public function __invoke(Contract $data, ApplyRepository $applyRepository, UserRepository $userRepository, Request $request)
    {

        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $user = $userRepository->find($request_datas["user_id"]);

        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractConfirm(
            $data,
            $twilio,
            $applyRepository,
            $user,
            $this->em
        );

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
