<?php

namespace App\Controller;

use App\Entity\Contract;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContractDeleteController extends AbstractController
{
    public function __invoke(
        Contract $data,
        EntityManagerInterface $em,
    ) {
        if ($data->getStartdate() > new DateTime()) {
            if ($data->getNext() == null) {
                $em->remove($data);
                $em->flush();
            } else {
                $this->deleteNext($data, $em);
            }
            return [
                'success' => true,
                'message' => 'Contrat supprimé avec succès'
            ];
        }
        return [
            'success' => false,
            'message' => 'Vous ne pouvez pas supprimer un contrat après son heure de début'
        ];
    }

    private function deleteNext(Contract $contract, EntityManager $em): bool
    {
        $next = $contract->getNext();
        if ($next != null) {
            return $this->deleteNext($next, $em);
        }

        $em->remove($contract);
        $em->flush();
        return true;
    }
}
