<?php

namespace App\Controller;

use App\Entity\Experience;
use App\Entity\User;
use App\Repository\JobRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserAddExperiencesController extends AbstractController
{
    public function __invoke(
        User $data,
        Request $request,
        JobRepository $jobRepository,
        EntityManagerInterface $manager
    ) {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        foreach ($data->getExperiences() as $experience) {
            $data->removeExperience($experience);
            $manager->remove($experience);
            $manager->flush();
        }

        foreach ($request_datas as $value) {
            if ($value) {
                $expereience = (new Experience())
                    ->setTitle($value["title"])
                    ->setUser($data)
                    ->setTime($value["time"])
                    ->setWork($value["work"]);
                $manager->persist($expereience);
                $data->addExperience($expereience);
                $manager->persist($data);
                $manager->flush();
            }
        }


        return  $data;
    }
}
