<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

class UserEmailVerificationController extends AbstractController
{
    public function __invoke(User $data, Request $request, MailerInterface $mailer, EntityManagerInterface $entityManagerInterface)
    {
        $otp = ContractConfig::generateOtp(5);

        // check if code exist
        if ($data->getVerificationCode() != null) {
            $otp = $data->getVerificationCode();
        } else {
            $data->setVerificationCode($otp);
            $entityManagerInterface->persist($data);
            $entityManagerInterface->flush();
        }

        // generate OTP

        $template = 'email/verification.html.twig';

        $username = $data->getFirstname() . " " . $data->getLastname();

        $template_datas = [
            'username' =>  $username,
            'code' => ContractConfig::codeSpaced($otp)
        ];

        // send mail
        MailerController::sendEmail($mailer, $data->getEmail(), $template, $template_datas, "Vérification de l'adresse mail");

        return [
            "success" => true,
            "message" => "Code de vérification envoyé avec succès"
        ];
    }
}
