<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Client;
use App\Entity\Contract;
use App\Repository\ApplyRepository;
use App\Repository\ContractRepository;
use App\Repository\UserRepository;
use DateTime;
use App\Entity\Client as EntityClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ClientGetContractsController extends AbstractController
{
    public function __invoke(
        EntityClient $data,
        UserRepository $userRepository,
        ApplyRepository $applyRepository,
        ContractRepository $contractRepository
    ) {
        return ContractConfig::clientContracts(
            $data,
            $userRepository,
            $applyRepository,
            $contractRepository
        );
    }
}
