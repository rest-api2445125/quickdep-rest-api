<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\User;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserLoginController extends AbstractController
{
    public function __invoke(
        User $data,
        Request $request,
        DeviceRepository $deviceRepository,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        JWTTokenManagerInterface $jWTTokenManager,
        UserPasswordHasherInterface $encoder
    ) {
        $request_datas = json_decode(
            preg_replace(
                '/[\x00-\x1F\x80-\xFF]/',
                '',
                $request->getContent()
            ),
            true
        );

        $devicetoken = null;

        if (array_key_exists('device', $request_datas)) {
            $devicetoken = $request_datas["device"];
        }

        $email = $request_datas['email'];
        $password = $request_datas['password'];

        $user = $userRepository->findOneBy([
            'email' => $email
        ]);

        if ($user != null) {
            if ($encoder->isPasswordValid($user, $password)) {
                $token = $jWTTokenManager->create($user);

                if ($devicetoken != null) {
                    foreach ($user->getDevices() as $device) {
                        $em->remove($device);
                        $em->flush();
                    }

                    $device = new Device();
                    $device->setToken($devicetoken);
                    $device->setUser($user);

                    $em->persist($device);

                    if ($data->getInitdelete() != null) {
                        $data->setInitdelete(null);
                        $em->persist($data);
                    }

                    $em->flush();
                }

                return [
                    'success' => true,
                    'token' => $token,
                    'user' => $userRepository->find($user->getId()),
                    'type' => $data->getType()
                ];
            }
        }


        return [
            'success' => false,
            'message' => 'Adresse mail ou mot de passe incorrect'
        ];
    }
}
