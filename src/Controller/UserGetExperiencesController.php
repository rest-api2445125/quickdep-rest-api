<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserGetExperiencesController extends AbstractController
{
    public function __invoke(User $data)
    {
        $return = [];
        $experiences = $data->getExperiences();

        foreach ($experiences as $key => $value) {
            $return[$key . ""] = $value;
        }
        return $return;
    }
}
