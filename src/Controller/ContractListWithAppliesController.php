<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Entity\Contract;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ContractListWithAppliesController extends AbstractController
{
    public function __invoke(array $data, UserRepository $userRepository)
    {
        if ($this->getUser() == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", "Vous devez vous connecter pour voir les contrats");
        }

        if (!in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", "Vous n'avez pas droit à acceder à ces informations");
        }

        $validate_data = [];

        foreach ($data as $contract) {
            if ($contract->getStatus() == ContractStatus::$PUBLISHED || $contract->getStatus() == ContractStatus::$APPROUVED) {
                if ($contract->getStartdate() > new DateTime()) {
                    $data_contract = [
                        "contract" => $contract,
                        "applies" => []
                    ];

                    $applies = $contract->getApplies();
                    if ($contract->getStatus() == ContractStatus::$APPROUVED) {
                        $applies = [
                            $contract->getEmployee()
                        ];
                    }

                    if ($contract->getStatus() == ContractStatus::$PUBLISHED) {
                        foreach ($applies as $apply) {
                            $user = $apply->getUser();
                            $data_contract["applies"][] = [
                                "id" => $user->getId(),
                                "firstname" => $user->getFirstname(),
                                "lastname" => $user->getLastname(),
                                "image" => $user->getImage(),
                            ];
                        }
                    } else {
                        foreach ($applies as $apply) {
                            $user = $contract->getEmployee();
                            if ($user != null) {
                                $data_contract["applies"][] = [
                                    "id" => $user->getId(),
                                    "firstname" => $user->getFirstname(),
                                    "lastname" => $user->getLastname(),
                                    "image" => $user->getImage(),
                                ];
                            }
                        }
                    }

                    if (count($data_contract["applies"]) > 0) {
                        $validate_data[] = $data_contract;
                    }
                }
            }
        }


        return $validate_data;
    }
}
