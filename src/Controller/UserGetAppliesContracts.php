<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Entity\Contract;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetAppliesContracts extends AbstractController
{
    public function __invoke(User $data, Request $request)
    {
        $user_contracts = $data->getApplies();

        $data_return = [];
        $perpage = 100;
        $page = $request->get('current_page');
        $last_date = null;
        $j = 1;

        $validate_data = [];

        if ($page > 1) {
            foreach ($user_contracts as $acc_contract) {
                $contract = $acc_contract->getContract();
                $end_prev = ($page - 1) * $perpage;
                if ($j == $end_prev) {
                    $last_date = $contract->getStartdate()->format('n') . ' ' . $contract->getStartdate()->format('Y');
                    break;
                }
                $j++;
            }
        }

        foreach ($user_contracts as $acc_contract) {
            $contract = $acc_contract->getContract();
            if (
                $contract->getStatus() == ContractStatus::$PUBLISHED
                && $contract->getEmployee() == null
                && ($contract->getParent() == null || $contract->getParent()->getStatus() != ContractStatus::$PUBLISHED)
                && !$this->itemExists($validate_data, $contract)
                && !$contract->startDatePassed()
            ) {
                $contract->setApplyconsecutive($acc_contract->isConsecutive());
                $contract->setApplyconsecutivecount($acc_contract->getConsecutivecount());
                $validate_data[] = $contract;
            }
        }

        $data_return = ContractConfig::listing($validate_data, $data);
        return $data_return;
    }

    private function itemExists(array $array, Contract $contract): bool
    {
        $exists = false;
        foreach ($array as $contractItem) {
            if ($contractItem->getId() == $contract->getId()) {
                $exists = true;
            }
        }
        return $exists;
    }
}
