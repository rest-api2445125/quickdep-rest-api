<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Entity\PasswordReset;
use App\Repository\PasswordResetRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

class ResetPasswordController extends AbstractController
{
    public function __invoke(
        PasswordReset $data,
        UserRepository $userRepository,
        PasswordResetRepository $passwordResetRepository,
        MailerInterface $mailer,
        EntityManagerInterface $em
    ) { 
        $email = $data->getEmail();

        return ContractConfig::resetPassword(
            $email,
            $passwordResetRepository,
            $data,
            $em,
            $mailer,
            $userRepository
        );
    }
}
