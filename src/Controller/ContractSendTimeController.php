<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\Notification;
use App\Configuration\NotificationConfig;
use App\Configuration\NotificationType;
use App\Configuration\TimeSheetStatus;
use App\Configuration\UserType;
use App\Entity\Client;
use App\Entity\Contract;
use App\Entity\TimeSheet;
use App\Repository\DeviceRepository;
use App\Repository\TimeSheetRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\VarDumper\Cloner\Data;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Twilio\Rest\Client as RestClient;

class ContractSendTimeController extends AbstractController
{
    public function __invoke(Contract $data, Request $request, EntityManagerInterface $em, TimeSheetRepository $timeSheetRepository, MailerInterface $mailer, UserRepository $userRepository, HttpClientInterface $httpClient, DeviceRepository $deviceRepository)
    {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        /** @var User */
        $user = $this->getUser();

        if ($user == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", 1);
        }
        
        $t_start = $request_datas["t_start"];
        $t_end = $request_datas["t_end"];
        $t_pause = null;
        if (array_key_exists('t_pause', $request_datas)) {
            $t_pause = $request_datas["t_pause"];
        }
        $total_time = $request_datas["t_time_total"];

        $total_rate = $request_datas["t_total_rate"];

        $twilio = new RestClient(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        ContractConfig::contractSendTime(
            $data,
            $total_time,
            $total_rate,
            $t_start,
            $t_end,
            $t_pause,
            $this->getUser(),
            $em,
            $twilio
        );

        return [
            "success" => true,
            "Message" => "Feuille de temps envoyé avec succès"
        ];
    }
}
