<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\NotificationConfig;
use App\Repository\ContractRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;

class CronController extends AbstractController
{
    #[Route('/cron/check/{h}', name: 'app_cron')]
    public function check24(
        Request $request,
        ContractRepository $contractRepository,
        EntityManagerInterface $em
    ) {
        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        $houres = $request->attributes->get('h');

        $confirmed_shifts = $contractRepository->findBy([
            'status' => ContractStatus::$APPROUVED
        ]);
        
        foreach ($confirmed_shifts as $contract) {
            $now = new DateTime();
            if ($now > $contract->getEnddate()) {
                ContractConfig::contractWorked(
                    $contract,
                    $em,
                    $twilio
                );
            } else {
                if (
                    $now->format("d m Y")
                    == $contract->getStartdate()->format("d m Y")
                ) {
                    $interval = $now->diff($contract->getStartdate());
                    if (
                        (int) $interval->format("i") >  ((int) $houres) * 60
                        && (int) $interval->format("i") <  ((int) $houres) * 60 + 15
                        && (
                            ($houres == 2 && !$contract->isSent2())
                            || ($houres == 24 && !$contract->isSent24())
                        )
                    ) {
                        if ($houres == 2) 
                            $contract->setSent2(true);
                        elseif ($houres == 24)
                            $contract->setSent24(true);
    
                        $em->persist($contract);
                        $em->flush();
                    
                        $message = "Votre shift chez "
                            . $contract->getClient()->getName()
                            . " de " . $contract->getStartdate()->format("H:i")
                            . " à " . $contract->getEnddate()->format("H:i")
                            . " le " . ContractConfig::full_date($contract->getStartdate(), true)
                            . " débute dans $houres heures";
                        NotificationConfig::sendSms(
                            $twilio,
                            ContractConfig::format_number($contract->getEmployee()->getPhone()),
                            $message
                        );
                    }
                }
            }

            
        }

        return new Response("OK");
    }

    #[Route('/cron/check/ts', name: 'app_cron_ts')]
    public function checkTs(
        Request $request,
        ContractRepository $contractRepository,
    ) {
        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        $employeeTs = $contractRepository->findBy([
            'status' => ContractStatus::$WORKED
        ]);

        foreach ($employeeTs as $contract) {
            $now = new DateTime();
            $lastMonday = date('Y-m-d', strtotime('last monday'));

            if ($contract->getEnddate() >= $lastMonday && $contract->getEnddate() <= $now) {
                $message = "Veuillez déclarer votre feuille de temps pour le shift du "
                    . ContractConfig::full_date($contract->getStartdate(), true)
                    . " chez " . $contract->getClient()->getName() . ", au poste de " . $contract->getJob()->getTitle() . ". Veuillez vous rendre au menu feuilles de temps de votre application pour le faire.";
                
                NotificationConfig::sendSms(
                    $twilio, 
                    ContractConfig::format_number($contract->getEmployee()->getPhone()),
                    $message
                );
            } 
        }

        return new Response("OK");
    }

    #[Route('/cron/check/client-ts', name: 'app_cron_ts')]
    public function checkTClient(
        Request $request,
        ContractRepository $contractRepository,
    ) {
        $twilio = new Client(
            $this->getParameter("app.twilio_ssd"),
            $this->getParameter("app.twilio_token")
        );

        $clientTs = $contractRepository->findBy([
            'status' => ContractStatus::$TIME_SENT
        ]);
        
        foreach ($clientTs as $contract) {
            $now = new DateTime();
            $lastMonday = date('Y-m-d', strtotime('last monday'));

            if ($contract->getEnddate() >= $lastMonday && $contract->getEnddate() <= $now) {
                $message = "Veuillez valider la feuille de temps pour le shift du "
                    . ContractConfig::full_date($contract->getStartdate(), true)
                    . ", au poste de " . $contract->getJob()->getTitle()
                    . ", effectué par " . $contract->getEmployee()->getFirstname() . " " . $contract->getEmployee()->getLastname() .  ".";
                
                NotificationConfig::sendSms(
                    $twilio, 
                    ContractConfig::format_number($contract->getClient()->getPhone()),
                    $message
                );
            } 
        }

        return new Response("OK");
    }
}
