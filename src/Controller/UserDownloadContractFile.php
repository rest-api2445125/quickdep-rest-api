<?php

namespace App\Controller;

use App\Configuration\UserType;
use App\Entity\Invoice;
use App\Entity\User;
use App\Repository\InvoiceRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserDownloadContractFile extends AbstractController
{
    public function __invoke(User $data, MailerInterface $mailer, Request $request)
    {
        $email = $data->getEmail();
        $template = "email/pay.html.twig";
        $template_datas = [
            "name" => $data->getType() == UserType::$EMPLOYEE ? $data->getLastname() . " " . $data->getFirstname() : $data->getClient()->getName(),
        ];
        $subject = "Contract QuickDep";

        $file_path = "";
        $from = "";
        if ($data->getType() == UserType::$EMPLOYEE) {
            $file_path = "contracts/employee/pdf/" . $data->getUsercontract()->getId() . ".pdf";
            $from = "facturation@quickdep.ca";

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject, [
                    "path" => $file_path,
                    "name" => "Contract-QuickDep-" . $data->getLastname() . "-" . $data->getFirstname()
                ], $from);
            }
        } else {
            $file_path = "contracts/society/pdf/" . $data->getUsercontract()->getId() . ".pdf";
            $from = "facturation@quickdep.ca";

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject, [
                    "path" => $file_path,
                    "name" => "Contract-QuickDep-" . $data->getClient()->getName()
                ], $from);
            }
        }

        return [
            "success" => true,
            "message" => "Contrat envoyé par mail avec succès"
        ];
    }
}
