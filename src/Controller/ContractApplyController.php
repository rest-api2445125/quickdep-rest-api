<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\DocumentConfig;
use App\Configuration\DocumentErrorType;
use App\Configuration\DocumentStatus;
use App\Configuration\NotificationConfig;
use App\Entity\Apply;
use App\Entity\Contract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\ApplyRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Twilio\Rest\Client;

class ContractApplyController extends AbstractController
{

    private $doctrine;
    private $em;

    public function __construct(ManagerRegistry $doctrine, EntityManagerInterface $em)
    {
        $this->doctrine = $doctrine;
        $this->em = $em;
    }

    public function __invoke(Contract $data, Request $request, ApplyRepository $applyRepository)
    {
        $request_datas = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $consecutive = false;

        if (is_array($request_datas) && array_key_exists("consecutive", $request_datas)) {
            $consecutive = $request_datas["consecutive"];
        }

        /**
         * @var User 
         */
        $user = $this->getUser(); // get authenticated user

        if ($user == null) {
            throw new UnauthorizedHttpException("Vous devez vous connecter", 1);
        }

        if ($user->getDocumentsStatus() == DocumentStatus::$NOT_SENT) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$DOCUMENT_NOT_SENT,
                "message" => 'Veuillez envoyer vos documents administratifs afin de pouvoir postuler au contrat.'
            ];
        } else if (!$user->isSigned()) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$CONTRACT_NOT_SIGNED,
                "message" => 'Vous devez Signer le contrat du travailleur avant de postuler au contrat.'
            ];
        } else if ($user->getImagePath() == null) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$NO_PROFILE_PICTURE,
                "message" => 'Vous devez ajouter une photo de profil.'
            ];
        } else if (!$user->isEmailVerified()) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$EMAIL_NOT_VERIFIED,
                "message" => 'Vous devez vérifier votre adresse email avant de postuler au contrat.'
            ];
        } else if (!$user->isPhoneverified()) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$EMAIL_NOT_VERIFIED,
                "message" => 'Vous devez vérifier votre numéro de téléphone avant de postuler au contrat.'
            ];
        } else if ($user->getDocumentsStatus() == DocumentStatus::$WAITING) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$DOCUMENT_WAITING,
                "message" => 'Vos documents administratifs sont en cours de validation. Vous recevrez un mail vous informant du résultat de la validation'
            ];
        } else if ($user->getDocumentsStatus() == DocumentStatus::$REFUSED) {
            return [
                "success" => false,
                "type" => DocumentErrorType::$DOCUMENT_REFUSED,
                "message" => 'Vos documents sont non valides. Veuillez consulter votre profile pour voir quel document a été réjété.'
            ];
        }

        if ($user->getDocumentsStatus() == DocumentStatus::$VALIDATED) {

            if ($data->is_published()) {
                if ($data->getAppliesCount() >= ContractConfig::$MAX_APPLIES || $data->getStatus() != ContractStatus::$PUBLISHED) {
                    return [
                        "success" => false,
                        "message" => 'Vous ne pouvez pas postuler à ce contrat.'
                    ];
                }

                $data->setAppliesCount($data->getAppliesCount() + 1);

                if (($data->getParent() != null && $data->getParent()->getStatus() == ContractStatus::$PUBLISHED) || $consecutive) {

                    $apply = $applyRepository->findOneBy([
                        "user" => $user,
                        "contract" => Contract::getFistParent($data)
                    ]);

                    if ($apply == null) {
                        $apply = (new Apply())
                            ->setUser($user)
                            ->setContract($data)
                            ->setConsecutive($consecutive);
                    } else {
                        $apply->setConsecutive(true);
                    }

                    $apply
                        ->setStartdate($data->getStartdate())
                        ->setEnddate($consecutive ? $this->lastDate($data) : $data->getEnddate())
                        ->setConsecutivecount($consecutive ? false : Contract::posInConsecutive($data, Contract::getFistParent($data)));

                    $this->em->persist($apply);
                    $this->em->flush();
                } else {
                    $apply = (new Apply())
                        ->setUser($user)
                        ->setContract($data);

                    $this->em->persist($apply);
                    $this->em->flush();
                }

                $twilio = new Client(
                    $this->getParameter("app.twilio_ssd"),
                    $this->getParameter("app.twilio_token")
                );
                
                $message = $user->getFirstName() . " " .$user->getLastName() . " vient de postulé au shift au poste de "
                    . $data->getJob()->getTitle()
                    . " prévu pour le " .
                    ContractConfig::full_date($data->getStartdate(), true) . " De "
                    . $data->getStartdate()->format("H:i") . " à " . $data->getEnddate()->format("H:i");
                NotificationConfig::sendSms($twilio, $data->getClient()->getPhone(), $message);

                return [
                    'success' => true,
                    'data' => $data
                ];
            }

            return [
                "success" => false,
                "message" => 'Vous ne pouvez pas postuler à ce contrat.'
            ];
        }

        return [
            "success" => false,
            "message" => "Erreur due à la validation des documents."
        ];
    }

    private function lastDate(Contract $contract, $limit = NULL, $current = 1): DateTimeInterface
    {
        $next = $contract->getNext();
        if ($next->getNext() != null) {
            if ($limit != null) {
                if ($current  == $limit) {
                    return $next->getEnddate();
                } else {
                    return $this->lastDate($next, $limit, $current + 1);
                }
            }
            return $this->lastDate($next);
        }
        return $next->getEnddate();
    }
}
