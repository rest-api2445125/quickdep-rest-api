<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Configuration\TimeSheetStatus;
use App\Entity\Contract;
use App\Repository\ClientRepository;
use App\Repository\ContractRepository;
use App\Repository\TimeSheetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StatsController extends AbstractController
{
    public function __invoke(array $data, ClientRepository $clientRepository, TimeSheetRepository $timeSheetRepository, ContractRepository $contractRepository, UserRepository $userRepository)
    {
        // completed shifts
        $compl_shifts = count($contractRepository->findBy([
            "status" => [ContractStatus::$TIME_APPROUVED, ContractStatus::$PAID]
        ]));

        // users 
        $users = count($userRepository->findAll());

        // total hours 
        $hours_times =
            $timeSheetRepository->findBy([
                "status" => TimeSheetStatus::$APPROUVED
            ]);

        $times = 0;

        foreach ($hours_times as $time) {
            $times += $time->getTotaltimes();
        }

        $total_houres = ceil($times / 60);

        // uncompleted shifts
        $uncompleted_shifts = count($contractRepository->findBy([
            'status' => [ContractStatus::$NOT_WORKED]
        ]));

        // requests 
        $request = ($compl_shifts / ($compl_shifts + $uncompleted_shifts)) * 100;

        // clients
        $clients = count($clientRepository->findAll());

        return [
            "completed_shifts" => $compl_shifts,
            "total_users" => $users,
            "completed_hours" => $total_houres,
            "completed_requests" => $request,
            "uncompleted_shifts" => $uncompleted_shifts,
            "clients" => $clients
        ];
    }
}
