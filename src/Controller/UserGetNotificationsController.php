<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\NotificationRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetNotificationsController
{
    public function __invoke(User $data)
    {
        return $data->getNotifications();
    }
}
