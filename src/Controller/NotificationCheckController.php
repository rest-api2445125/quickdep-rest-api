<?php

namespace App\Controller;

use App\Configuration\Notification;
use App\Entity\NotificationSeen;
use App\Entity\User;
use App\Repository\NotificationRepository;
use App\Repository\NotificationSeenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class NotificationCheckController extends AbstractController
{
    public function __invoke(User $data, Request $request, EntityManagerInterface $em, NotificationRepository $notificationRepository, NotificationSeenRepository $notificationSeenRepository)
    {
        $tosee = [];

        $user = $this->getUser();

        if ($user == null) {
            return [
                "hasNews" => count($tosee) > 0,
                "datas" => $tosee
            ];
        }

        $notifications = $notificationRepository->findAll();

        foreach ($notifications as $notification) {
            if ($notification->getUsers()->contains($user)) {
                $seen = $notificationSeenRepository->findBy([
                    "user" => $user,
                    "notification" => $notification
                ]);

                if ($seen == null) {
                    $notificationSeen = new NotificationSeen();
                    $notificationSeen->setUser($user);
                    $notificationSeen->setNotification($notification);
                    $notificationSeen->setSeen(false);

                    $em->persist($notificationSeen);
                    $em->flush();
                    $tosee[] = $notification;
                }
            }
        }

        return [
            "hasNews" => count($tosee) > 0,
            "datas" => $tosee
        ];
    }
}
