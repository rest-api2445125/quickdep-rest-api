<?php

namespace App\Controller;

use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Entity\Contract;
use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DocumentValidatedController extends AbstractController
{
    public function __invoke(Document $data, EntityManagerInterface $em): Document
    {
        $user = $data->getEmployee();

        $data->setStatut(DocumentStatus::$VALIDATED);

        if (DocumentConfig::check_validated($user)) {
            $user->setDocumentsStatus(DocumentStatus::$VALIDATED);
        }

        if (DocumentConfig::check_waiting($user)) {
            $user->setDocumentsStatus(DocumentStatus::$WAITING);

            $em->persist($user);
        }

        if (DocumentConfig::check_not_sent($user)) {
            $user->setDocumentsStatus(DocumentStatus::$NOT_SENT);

            $em->persist($user);
        }

        if (DocumentConfig::check_refused($user)) {
            $user->setDocumentsStatus(DocumentStatus::$REFUSED);

            $em->persist($user);
        }

        $em->persist($user);
        $em->flush();

        return $data;
    }
}
