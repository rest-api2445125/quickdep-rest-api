<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\User;
use App\Repository\DeviceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserRegisterController extends AbstractController
{
    public function __invoke(
        User $data,
        Request $request,
        UserRepository $userRepository,
        DeviceRepository $deviceRepository,
        EntityManagerInterface $em,
        JWTTokenManagerInterface $jWTTokenManager,
        UserPasswordHasherInterface $encoder
    ) {
        $request_datas
            = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $request->getContent()), true);

        $devicetoken = $request_datas["device"];

        $email = $request_datas['email'];
        $password = $request_datas['password'];

        $exist = $userRepository->findOneBy(["email" => $email]);

        if ($exist != null) {
            return [
                "success" => false,
                "message" => "Un compte existe déjà avec cette adresse mail."
            ];
        }

        $user = $userRepository->createNewUser(
            [
                'email' => $data->getEmail(),
                'password' => $password,
                'type' => $data->getType()
            ],
            $em,
            $encoder
        );

        $token = $jWTTokenManager->create($user);

        if ($devicetoken != null) {
            $device = new Device();
            $device->setToken($devicetoken);
            $device->setUser($user);

            $em->persist($device);
            $em->flush();
        }

        return [
            "success" => true,
            'id' => $user->getId(),
            'token' => $token,
            'type' => $data->getType(),
            'user' => $userRepository->find($user->getId()),
        ];
    }
}
