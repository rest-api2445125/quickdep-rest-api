<?php

namespace App\Controller;

use App\Configuration\ContractStatus;
use App\Entity\Document;
use App\Entity\Invoice;
use App\Repository\ContractRepository;
use App\Repository\InvoiceRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

class InvoicePayController extends AbstractController
{
    public function __invoke(Invoice $data, UserRepository $userRepository, ContractRepository $contractRepository, MailerInterface $mailer, InvoiceRepository $invoiceRepository, EntityManagerInterface $em)
    {
        $data->setPaid(true);
        $em->persist($data);
        $em->flush();

        return [
            'success' => true,
            'Message' => "Succes ! facture payé"
        ];
    }
}
