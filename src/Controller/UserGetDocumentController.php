<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserGetDocumentController extends AbstractController
{
    public function __invoke(User $data, DocumentRepository $documentRepository)
    {
        $documents = $documentRepository->findBy([
            "employee" => $data
        ]);
        return $documents;
    }
}
