<?php

namespace App\Controller;

use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Entity\User;
use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserGetTimeSheetController extends AbstractController
{
    public function __invoke(User $data, Request $request, ContractRepository $contractRepository)
    {
       return ContractConfig::userTimeSheet($data, $contractRepository);
    }
}
