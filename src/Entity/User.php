<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\NotificationCheckController;
use App\Controller\UserAddExperiencesController;
use App\Controller\UserCompleteProfileController;
use App\Controller\UserDownloadContractFile;
use App\Controller\UserEmailVerificationConfirmController;
use App\Controller\UserEmailVerificationController;
use App\Controller\UserGetAppliesContracts;
use App\Controller\UserGetApprouvedContractsController;
use App\Controller\UserGetDocumentController;
use App\Controller\UserGetExperiencesController;
use App\Controller\UserGetInvoicesController;
use App\Controller\UserGetLikesController;
use App\Controller\UserGetNotificationsController;
use App\Controller\UserGetTimeSheetController;
use App\Controller\UserGetTimeSheetsApprouvedController;
use App\Controller\UserGetTimeSheetTodoController;
use App\Controller\UserGetTownsController;
use App\Controller\UserImageController;
use App\Controller\UserInitiateDeleteController;
use App\Controller\UserLoginAdminController;
use App\Controller\UserLoginController;
use App\Controller\UserNewPasswordController;
use App\Controller\UserPhoneVerificationConfirmController;
use App\Controller\UserPhoneVerificationController;
use App\Controller\UserRegisterController;
use App\Controller\UserSetTownsController;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @Vich\Uploadable()
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    attributes: ["order" => [
        "id" => "DESC"
    ]],
    forceEager: false,
    normalizationContext: [
        "groups" => ["user:read"],
        "enable_max_depth" => true
    ],
    denormalizationContext: [
        "groups" => ["user:write"],
        "enable_max_depth" => true
    ],
    collectionOperations: [
        'get',
        'post' => [
            'method' => 'POST',
            'path' => '/register',
            'controller' => UserRegisterController::class,
            'openapi_context' => [
                "summary" => "Register as user",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email' => [
                                        'type' => 'string'
                                    ],
                                    'password' => [
                                        'type' => 'string'
                                    ],
                                    'type' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'login' => [
            'method' => 'POST',
            'path' => '/login',
            'controller' => UserLoginController::class,
            'openapi_context' => [
                "summary" => "Login as user",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email' => [
                                        'type' => 'string'
                                    ],
                                    'password' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'login-admin' => [
            'method' => 'POST',
            'path' => '/admin/login',
            'controller' => UserLoginAdminController::class,
            'openapi_context' => [
                "summary" => "Login as admin",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email' => [
                                        'type' => 'string'
                                    ],
                                    'password' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'check-notifications' => [
            'method' => 'POST',
            'path' => '/users/check-notifications',
            'controller' => NotificationCheckController::class,
            'openapi_context' => [
                "summary" => "Check if there are new notifications",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => []
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'register' => [
            'method' => 'POST',
            'path' => '/users/register',
            'controller' => UserCompleteProfileController::class,
            'openapi_context' => [
                "summary" => "User Register"
            ],
        ],
    ],
    itemOperations: [
        'delete',
        'patch',
        'get',
        'contracts' => [
            'method' => 'GET',
            'path' => '/users/{id}/applies',
            'controller' => UserGetAppliesContracts::class,
            'openapi_context' => [
                "summary" => "User | Get all user's applies",
            ],
        ],
        'approuvments' => [
            'method' => 'GET',
            'path' => '/users/{id}/approuved',
            'controller' => UserGetApprouvedContractsController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's approuved contracts",
            ],
        ],
        'initiate_deletion' => [
            'method' => 'POST',
            'path' => '/users/{id}/initiate-delete',
            'controller' => UserInitiateDeleteController::class,
            'openapi_context' => [
                "summary" => "initiate delete",
            ]
        ],
        'invoices' => [
            'method' => 'GET',
            'path' => '/users/{id}/invoices',
            'controller' => UserGetInvoicesController::class,
            'openapi_context' => [
                "summary" => "client, get all invoices",
            ]
        ],
        'time_sheet' => [
            'method' => 'GET',
            'path' => '/users/{id}/time-sheets',
            'controller' => UserGetTimeSheetController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's time sheets",
            ],
        ],
        'time_sheet_approuved' => [
            'method' => 'GET',
            'path' => '/users/{id}/time-sheets-approuved',
            'controller' => UserGetTimeSheetsApprouvedController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's time sheets",
            ],
        ],
        'time_sheet_todo' => [
            'method' => 'GET',
            'path' => '/users/{id}/time-sheets-todo',
            'controller' => UserGetTimeSheetTodoController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's time sheets to do",
            ],
        ],
        'files' => [
            'method' => 'GET',
            'path' => '/users/{id}/files',
            'controller' => UserGetDocumentController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's files",
            ],
        ],
        'towns' => [
            'method' => 'POST',
            'path' => '/users/{id}/towns',
            'controller' => UserSetTownsController::class,
            'openapi_context' => [
                "summary" => "User | set towns",
            ],
        ],
        'towns-list' => [
            'method' => 'GET',
            'path' => '/users/{id}/towns-list',
            'controller' => UserGetTownsController::class,
            'openapi_context' => [
                "summary" => "User | get towns",
            ],
        ],
        'experiences-list' => [
            'method' => 'GET',
            'path' => '/users/{id}/experiences-list',
            'controller' => UserGetExperiencesController::class,
            'openapi_context' => [
                "summary" => "User | get experiences",
            ],
        ],
        'experiences' => [
            'method' => 'POST',
            'path' => '/users/{id}/experiences',
            'controller' => UserAddExperiencesController::class,
            'openapi_context' => [
                "summary" => "User | add experiences",
            ],
        ],
        'likes' => [
            'method' => 'GET',
            'path' => '/users/{id}/likes',
            'controller' => UserGetLikesController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's likes",
            ],
        ],
        'notifications' => [
            'method' => 'GET',
            'path' => '/users/{id}/notifications',
            'controller' => UserGetNotificationsController::class,
            'openapi_context' => [
                "summary" => "User | Get all user's notifications",
            ],
        ],
        'email_verification' => [
            'method' => 'POST',
            'path' => '/users/{id}/email-verifivation',
            'controller' => UserEmailVerificationController::class,
            'openapi_context' => [
                "summary" => "User | Email verification",
            ],
        ],
        'phone_verification' => [
            'method' => 'POST',
            'path' => '/users/{id}/phone-verifivation',
            'controller' => UserPhoneVerificationController::class,
            'openapi_context' => [
                "summary" => "User | Phone verification",
            ],
        ],
        'image' => [
            'method' => 'POST',
            'path' => '/users/{id}/image',
            'deserialize' => false,
            'controller' => UserImageController::class,
            'openapi_context' => [
                "summary" => "add user image",
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'image' => [
                                        'type' => 'string',
                                        'format' =>  'binary'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'email_verification_confirm' => [
            'method' => 'POST',
            'path' => '/users/{id}/email-verifivation-confirm',
            'controller' => UserEmailVerificationConfirmController::class,
            'openapi_context' => [
                "summary" => "Confirm email verification",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'code' => [
                                        'type' => 'string',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'phone_verification_confirm' => [
            'method' => 'POST',
            'path' => '/users/{id}/phone-verifivation-confirm',
            'controller' => UserPhoneVerificationConfirmController::class,
            'openapi_context' => [
                "summary" => "Confirm phone verification",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'code' => [
                                        'type' => 'string',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'new-password' => [
            'method' => 'POST',
            'path' => '/users/{id}/password',
            'controller' => UserNewPasswordController::class,
            'openapi_context' => [
                "summary" => "Create a new password",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'password' => [
                                        'type' => 'string',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'download_file_contract' => [
            'method' => 'POST',
            'path' => '/users/{id}/contract-file-download',
            'controller' => UserDownloadContractFile::class,
            'openapi_context' => [
                "summary" => "Download contract file",
            ]
        ]
    ],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user:read", "document:read", "contract:read", "client:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read", "invoice:read"])]
    private ?string $email = null;

    #[ORM\Column]
    #[Groups("user:read")]
    private array $roles = [];

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $imagePath = null;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping= "users_images", fileNameProperty= "imagePath")
     */
    private $image;

    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read", "invoice:read"])]
    private ?string $imageUrl = null;

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(["user:read", "user:write"])]
    private ?string $password = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "invoice:read"])]
    private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "invoice:read"])]
    private ?string $lastname = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "invoice:read"])]
    private ?string $phone = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read"])]
    private ?string $address = null;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: Document::class, cascade: ['detach'])]
    #[Groups("user:read")]
    private Collection $documents;

    #[Groups("user:write")]
    #[SerializedName("password")]
    private ?string $plainPassword = null;

    #[ORM\Column]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?bool $register_completed = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read"])]
    private ?string $work = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read"])]
    private ?\DateTimeInterface $birthday = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read"])]
    private ?string $bio = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $updated_at = null;

    #[ORM\Column(nullable: true)]
    private ?int $verification_code = null;

    #[ORM\Column]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?bool $email_verified = null;

    #[ORM\ManyToMany(targetEntity: Notification::class, mappedBy: 'users')]
    private Collection $notifications;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: Contract::class)]
    private Collection $contracts;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Device::class)]
    #[Groups(["user:read", "user:write"])]
    private Collection $devices;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Apply::class)]
    private Collection $applies;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: TimeSheet::class)]
    private Collection $timeSheets;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "user:write"])]
    private ?bool $new = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?string $documents_status = null;

    #[ORM\Column]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?bool $signed = null;

    #[ORM\ManyToMany(targetEntity: Town::class, inversedBy: 'users')]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private Collection $town;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?string $type = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?UserContract $usercontract = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'], fetch: 'EAGER')]
    #[Groups(["user:read", "user:write", "document:read", "contract:read"])]
    private ?Client $client = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Invoice::class, fetch: 'EAGER')]
    private Collection $invoices;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $phonecode = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?bool $phoneverified = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Experience::class, fetch: 'EAGER')]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private Collection $experiences;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?int $notworked = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?int $completed = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "user:write", "document:read", "contract:read", "client:read"])]
    private ?int $canceled = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $initdelete = null;

    public function __construct()
    {
        $this->documents = new ArrayCollection();

        $this->updated_at = new DateTimeImmutable();
        $this->notifications = new ArrayCollection();
        $this->contracts = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->applies = new ArrayCollection();
        $this->timeSheets = new ArrayCollection();
        $this->town = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->experiences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function isSigned(): ?bool
    {
        return $this->signed;
    }

    public function setSigned(?bool $signed): self
    {
        $this->signed = $signed;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setEmployee($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getEmployee() === $this) {
                $document->setEmployee(null);
            }
        }

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function isRegisterCompleted(): ?bool
    {
        return $this->register_completed;
    }

    public function setRegisterCompleted(bool $register_completed): self
    {
        $this->register_completed = $register_completed;

        return $this;
    }

    public function getWork(): ?string
    {
        return $this->work;
    }

    public function setWork(?string $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function setBio(string $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getVerificationCode(): ?int
    {
        return $this->verification_code;
    }

    public function setVerificationCode(?int $verification_code): self
    {
        $this->verification_code = $verification_code;

        return $this;
    }

    public function isEmailVerified(): ?bool
    {
        return $this->email_verified;
    }

    public function setEmailVerified(bool $email_verified): self
    {
        $this->email_verified = $email_verified;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->addUser($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            $notification->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Contract>
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts->add($contract);
            $contract->setEmployee($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getEmployee() === $this) {
                $contract->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Device>
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices->add($device);
            $device->setUser($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->removeElement($device)) {
            // set the owning side to null (unless already changed)
            if ($device->getUser() === $this) {
                $device->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Apply>
     */
    public function getApplies(): Collection
    {
        return $this->applies;
    }

    public function addApply(Apply $apply): self
    {
        if (!$this->applies->contains($apply)) {
            $this->applies->add($apply);
            $apply->setUser($this);
        }

        return $this;
    }

    public function removeApply(Apply $apply): self
    {
        if ($this->applies->removeElement($apply)) {
            // set the owning side to null (unless already changed)
            if ($apply->getUser() === $this) {
                $apply->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TimeSheet>
     */
    public function getTimeSheets(): Collection
    {
        return $this->timeSheets;
    }

    public function addTimeSheet(TimeSheet $timeSheet): self
    {
        if (!$this->timeSheets->contains($timeSheet)) {
            $this->timeSheets->add($timeSheet);
            $timeSheet->setUser($this);
        }

        return $this;
    }

    public function removeTimeSheet(TimeSheet $timeSheet): self
    {
        if ($this->timeSheets->removeElement($timeSheet)) {
            // set the owning side to null (unless already changed)
            if ($timeSheet->getUser() === $this) {
                $timeSheet->setUser(null);
            }
        }

        return $this;
    }

    public function isNew(): ?bool
    {
        return $this->new;
    }

    public function setNew(?bool $new): self
    {
        $this->new = $new;

        return $this;
    }

    public function getDocumentsStatus(): ?string
    {
        return $this->documents_status;
    }

    public function setDocumentsStatus(string $documents_status): self
    {
        $this->documents_status = $documents_status;

        return $this;
    }

    /**
     * @return Collection<int, Town>
     */
    public function getTown(): Collection
    {
        return $this->town;
    }

    public function addTown(Town $town): self
    {
        if (!$this->town->contains($town)) {
            $this->town->add($town);
        }

        return $this;
    }

    public function removeTown(Town $town): self
    {
        $this->town->removeElement($town);

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUsercontract(): ?UserContract
    {
        return $this->usercontract;
    }

    public function setUsercontract(?UserContract $usercontract): self
    {
        $this->usercontract = $usercontract;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
            $invoice->setUser($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getUser() === $this) {
                $invoice->setUser(null);
            }
        }

        return $this;
    }

    public function getPhonecode(): ?string
    {
        return $this->phonecode;
    }

    public function setPhonecode(?string $phonecode): self
    {
        $this->phonecode = $phonecode;

        return $this;
    }

    public function isPhoneverified(): ?bool
    {
        return $this->phoneverified;
    }

    public function setPhoneverified(?bool $phoneverified): self
    {
        $this->phoneverified = $phoneverified;

        return $this;
    }

    /**
     * @return Collection<int, Experience>
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences->add($experience);
            $experience->setUser($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->removeElement($experience)) {
            // set the owning side to null (unless already changed)
            if ($experience->getUser() === $this) {
                $experience->setUser(null);
            }
        }

        return $this;
    }

    public function getNotworked(): ?int
    {
        return $this->notworked;
    }

    public function setNotworked(?int $notworked): self
    {
        $this->notworked = $notworked;

        return $this;
    }

    public function getCompleted(): ?int
    {
        return $this->completed;
    }

    public function setCompleted(?int $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    public function getCanceled(): ?int
    {
        return $this->canceled;
    }

    public function setCanceled(?int $canceled): self
    {
        $this->canceled = $canceled;

        return $this;
    }

    public function getInitdelete(): ?\DateTimeInterface
    {
        return $this->initdelete;
    }

    public function setInitdelete(?\DateTimeInterface $initdelete): self
    {
        $this->initdelete = $initdelete;

        return $this;
    }
}
