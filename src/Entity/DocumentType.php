<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\DocumentTypeCreationController;
use App\Repository\DocumentTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DocumentTypeRepository::class)]
#[ApiResource(
    itemOperations: [
        'delete',
        'patch',
        'get',
    ],
)]
class DocumentType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(length: 255)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["document:write", "user:read", "document:read"])]
    private ?string $title = null;

    #[ORM\Column]
    #[Groups(["document:write", "user:read", "document:read"])]
    private ?bool $required = null;

    #[ORM\Column(length: 10, nullable: true)]
    #[Groups(["document:write", "user:read", "document:read"])]
    private ?string $type = null;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Document::class)]
    private Collection $documents;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["document:write", "user:read", "document:read"])]
    private ?string $description = null;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function isRequired(): ?bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setType($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getType() === $this) {
                $document->setType(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
