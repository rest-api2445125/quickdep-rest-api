<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Configuration\ContractConfig;
use App\Configuration\ContractStatus;
use App\Configuration\UserType;
use App\Controller\ContractApplyController;
use App\Controller\ContractBonusController;
use App\Controller\ContractCanceledController;
use App\Controller\ContractConfirmController;
use App\Controller\ContractCreationController;
use App\Controller\ContractDeleteController;
use App\Controller\ContractDetailController;
use App\Controller\ContractEditController;
use App\Controller\ContractListingController;
use App\Controller\ContractListWithAppliesController;
use App\Controller\ContractNotWorkedController;
use App\Controller\ContractSendTimeController;
use App\Controller\ContractSetLateController;
use App\Controller\ContractWorkedController;
use App\Controller\FinanceController;
use App\Controller\NotifyUnAcceptedContractController;
use App\Controller\StatsController;
use App\Repository\ApplyRepository;
use App\Repository\ContractRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ContractRepository::class)]
#[ApiResource(
    attributes: [
        "order" => [
            "startdate" => "ASC"
        ]
    ],
    itemOperations: [
        'delete' => [
            'method' => 'DELETE',
            'path' => '/contracts/{id}',
            'controller' => ContractDeleteController::class,
            'openapi_context' => [
                "summary" => "Delete contract",
            ],
        ],
        'get' => [
            'method' => 'GET',
            'path' => '/contracts/{id}',
            'controller' => ContractDetailController::class,
            'openapi_context' => [
                "summary" => "Get contract detail",
            ],
        ],
        'apply' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/apply',
            'controller' => ContractApplyController::class,
            'openapi_context' => [
                "summary" => "User | apply contract",
            ],
        ],
        'send_time' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/time-send',
            'controller' => ContractSendTimeController::class,
            'openapi_context' => [
                "summary" => "User | send time sheet",
            ],
        ],
        'cancel' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/cancel',
            'openapi_context' => [
                "summary" => "User or Client | cancel or refuse applied contract",
            ],
            'controller' => ContractCanceledController::class
        ],
        'edit' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/edit',
            'openapi_context' => [
                "summary" => "User or Client | edit contract",
            ],
            'controller' => ContractEditController::class
        ],
        'confirm' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/confirm',
            'openapi_context' => [
                "summary" => "Client | confirm a user to a contract",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'user_id' => [
                                        'type' => 'string'
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'controller' => ContractConfirmController::class
        ],
        'worked' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/worked',
            'openapi_context' => [
                "summary" => "Client | confirm that user has worked on the contract",
            ],
            'controller' => ContractWorkedController::class
        ],
        'not_worked' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/not-worked',
            'openapi_context' => [
                "summary" => "Client | set user has not worked",
            ],
            'controller' => ContractNotWorkedController::class
        ],
        'bonus' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/bonus',
            'openapi_context' => [
                "summary" => "Add a bonus to the contract",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'bonus' => [
                                        'type' => 'string',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'controller' => ContractBonusController::class
        ],
        'late' => [
            'method' => 'POST',
            'path' => '/contracts/{id}/late',
            'openapi_context' => [
                "summary" => "User notify for being late",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'late' => [
                                        'type' => 'integer',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'controller' => ContractSetLateController::class
        ]
    ],
    collectionOperations: [
        'post' => [
            'method' => 'POST',
            'path' => '/contracts',
            'controller' => ContractCreationController::class
        ],
        'get' => [
            'method' => 'GET',
            'path' => '/contracts',
            'controller' => ContractListingController::class
        ],
        'waiting' => [
            'method' => 'GET',
            'path' => '/contracts/with-applies',
            'controller' => ContractListWithAppliesController::class
        ],
        'unaccepted_notification' => [
            'method' => 'POST',
            'path' => '/contracts/notify-unaccepted',
            'controller' => NotifyUnAcceptedContractController::class
        ],
        'stats' => [
            'method' => 'GET',
            'path' => '/contracts/stats',
            'controller' => StatsController::class
        ],
        'finance' => [
            'method' => 'GET',
            'path' => '/contracts/finance',
            'controller' => FinanceController::class
        ],
    ],
    normalizationContext: [
        'groups' => [
            "contract:read"
        ]
    ],
    denormalizationContext: [
        'groups' => [
            "contract:write"
        ]
    ],
    paginationEnabled: false
)]
class Contract
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?int $houre = null;

    #[ORM\ManyToOne(inversedBy: 'contracts', fetch: 'EAGER', cascade: ['persist'])]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read", "invoice:read"])]
    private ?Client $client = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;
    #[ORM\OneToMany(mappedBy: 'contract', targetEntity: Like::class)]
    private Collection $likes;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "user:read", "client:read"])]
    private ?string $status = null;

    #[Groups(["contract:read", "user:read", "client:read"])]
    private bool $liked = false;

    #[Groups(["contract:read", "user:read", "client:read"])]
    private bool $applied = false;

    #[Groups(["contract:read", "user:read", "client:read", "contract:write"])]
    private ?int $newbonus;

    #[Groups(["contract:read", "user:read", "client:read"])]
    private ?int $likeId = 0;

    #[ORM\ManyToOne(inversedBy: 'contracts')]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?User $employee = null;

    #[ORM\OneToOne(mappedBy: 'contract', cascade: ['persist', 'remove'])]
    #[Groups(["invoice:read"])]
    private ?TimeSheet $timeSheet = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "user:read", "client:read"])]
    private ?string $code = null;

    #[ORM\OneToMany(mappedBy: 'contract', targetEntity: Apply::class, fetch: 'EAGER')]
    #[Groups(["contract:read"])]
    private Collection $applies;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "user:read", "client:read"])]
    private ?int $applies_count = null;

    #[ORM\ManyToOne(inversedBy: 'contracts')]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read", "invoice:read"])]
    private ?Job $job = null;

    #[ORM\ManyToOne(inversedBy: 'contracts', cascade: ['persist'])]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?Town $town = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?string $address = null;

    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $startdate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?\DateTimeInterface $enddate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?string $cloth = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?bool $parking = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?bool $paid = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?int $pause = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "contract:write", "user:read", "client:read"])]
    private ?int $bonus = null;

    #[ORM\ManyToMany(targetEntity: Invoice::class, inversedBy: 'contracts')]
    private Collection $invoices;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?string $rate = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?string $clientrate = null;

    #[ORM\OneToOne(inversedBy: 'parent', targetEntity: self::class, cascade: ['persist', 'remove'])]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?self $next = null;

    #[ORM\OneToOne(mappedBy: 'next', targetEntity: self::class, cascade: ['persist', 'remove'])]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?self $parent = null;

    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?bool $applyconsecutive = null;

    private ?int $applyconsecutivecount = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?int $late = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sent2 = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sent24 = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
        $this->likes = new ArrayCollection();
        $this->applies = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function startDatePassed()
    {
        return $this->startdate < new DateTime();
    }

    public function isPublic(User $user, ApplyRepository $applyRepository): bool
    {
        $applied = $applyRepository->findOneBy([
            'contract' => $this,
            'user' => $user
        ]) != null;

        if ($user->getType() == UserType::$SOCIETY || in_array('ROLE_ADMIN', $user->getRoles())) {
            $applied = $this->getAppliesCount() != null && $this->getAppliesCount() > 0;
        }

        return
            $this->getStatus() == ContractStatus::$PUBLISHED
            && $this->getAppliesCount() < ContractConfig::$MAX_APPLIES
            && !$this->startDatePassed()
            && !$applied
            && ($this->getParent() == null
                || $this->getParent()->getStatus() != ContractStatus::$PUBLISHED
                || ($this->getParent() != null && $this->appliedParent($this, $user, $applyRepository))
                || $this->getParent()->startDatePassed()
            )
        ;
    }

    public static function getFistParent(Contract $contract)
    {
        $parent = $contract->getParent();

        if ($parent != null && $parent->getParent() != null) {
            return $contract->getFistParent($parent);
        }

        return $parent;
    }

    private function appliedParent(Contract $contract, User $user, ApplyRepository $applyRepository, Contract $init = null, $i = 0)
    {
        $firstParent = self::getFistParent($contract);

        $apply = $applyRepository->findOneBy([
            'user' => $user,
            'contract' => $firstParent
        ]);


        if ($user->getType() == UserType::$SOCIETY) {
            $apply = $applyRepository->findOneBy([
                'user' => $user,
                'contract' => $firstParent
            ]);

            if ($apply != null) {
                return false;
            }
        }

        if ($apply != null) {
            if ($apply->isConsecutive() == true) {
                if ($apply->getConsecutivecount() != null && self::posInConsecutive($this, self::getFistParent($this)) == $apply->getConsecutivecount() + 1)
                    return true;
            } else if ($contract->getParent()->getId() == self::getFistParent($this)->getId()) {
                return true;
            }
        }

        return false;
    }

    public static function posInConsecutive(Contract $contract, Contract $parent, $i = 1): int
    {
        if ($parent->getNext() != null && $parent->getNext()->getId() != $contract->getId()) {
            return self::posInConsecutive($contract, $parent->getNext(), $i + 1);
        }
        return $i;
    }

    public function getHoure(): ?int
    {
        return $this->houre;
    }

    public function getNewbonus(): ?int
    {
        return $this->newbonus;
    }

    public function setHoure(int $houre): self
    {
        $this->houre = $houre;

        return $this;
    }

    public function setNewbonus(?int $bonus): self
    {
        $this->newbonus = $bonus;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setContract($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getContract() === $this) {
                $like->setContract(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function isLiked(): ?bool
    {
        return $this->liked;
    }

    public function setLiked(bool $liked): self
    {
        $this->liked = $liked;
        return $this;
    }

    public function isApplied(): ?bool
    {
        return $this->applied;
    }

    public function setApplied(?bool $applied): self
    {
        $this->applied = $applied;

        return $this;
    }

    public function getLikeId(): ?int
    {
        return $this->likeId;
    }

    public function setLikeId(int $likeId): self
    {
        $this->likeId = $likeId;

        return $this;
    }

    public function publish()
    {
        $this->setAppliesCount(0);
        $this->setStatus(ContractStatus::$PUBLISHED);
    }

    public function time_approuve()
    {
        $this->setStatus(ContractStatus::$TIME_APPROUVED);
    }

    // verifications
    public function is_published()
    {
        return $this->getStatus() == ContractStatus::$PUBLISHED;
    }

    public function is_time_approuved()
    {
        return $this->getStatus() == ContractStatus::$TIME_APPROUVED;
    }

    public function getEmployee(): ?User
    {
        return $this->employee;
    }

    public function setEmployee(?User $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getTimeSheet(): ?TimeSheet
    {
        return $this->timeSheet;
    }

    public function setTimeSheet(?TimeSheet $timeSheet): self
    {
        // unset the owning side of the relation if necessary
        if ($timeSheet === null && $this->timeSheet !== null) {
            $this->timeSheet->setContract(null);
        }

        // set the owning side of the relation if necessary
        if ($timeSheet !== null && $timeSheet->getContract() !== $this) {
            $timeSheet->setContract($this);
        }

        $this->timeSheet = $timeSheet;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection<int, Apply>
     */
    public function getApplies(): Collection
    {
        return $this->applies;
    }

    public function addApply(Apply $apply): self
    {
        if (!$this->applies->contains($apply)) {
            $this->applies->add($apply);
            $apply->setContract($this);
        }

        return $this;
    }

    public function removeApply(Apply $apply): self
    {
        if ($this->applies->removeElement($apply)) {
            // set the owning side to null (unless already changed)
            if ($apply->getContract() === $this) {
                $apply->setContract(null);
            }
        }

        return $this;
    }

    public function getAppliesCount(): ?int
    {
        return $this->applies_count;
    }

    public function setAppliesCount(?int $applies_count): self
    {
        $this->applies_count = $applies_count;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(?Town $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getCloth(): ?string
    {
        return $this->cloth;
    }

    public function setCloth(?string $cloth): self
    {
        $this->cloth = $cloth;

        return $this;
    }

    public function isParking(): ?bool
    {
        return $this->parking;
    }

    public function setParking(bool $parking): self
    {
        $this->parking = $parking;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getPause(): ?int
    {
        return $this->pause;
    }

    public function setPause(?int $pause): self
    {
        $this->pause = $pause;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(?int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        $this->invoices->removeElement($invoice);

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getClientrate(): ?string
    {
        return $this->clientrate;
    }

    public function setClientrate(string $clientrate): self
    {
        $this->clientrate = $clientrate;

        return $this;
    }

    public function getNext(): ?self
    {
        return $this->next;
    }

    public function setNext(?self $next): self
    {
        $this->next = $next;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        // unset the owning side of the relation if necessary
        if ($parent === null && $this->parent !== null) {
            $this->parent->setNext(null);
        }

        // set the owning side of the relation if necessary
        if ($parent !== null && $parent->getNext() !== $this) {
            $parent->setNext($this);
        }

        $this->parent = $parent;

        return $this;
    }

    public function isApplyconsecutive(): ?bool
    {
        return $this->applyconsecutive;
    }

    public function setApplyconsecutive(?bool $applyconsecutive): self
    {
        $this->applyconsecutive = $applyconsecutive;

        return $this;
    }

    public function getApplyconsecutivecount(): ?int
    {
        return $this->applyconsecutivecount;
    }

    public function setApplyconsecutivecount(?int $applyconsecutivecount): self
    {
        $this->applyconsecutivecount = $applyconsecutivecount;

        return $this;
    }

    public function getLate(): ?int
    {
        return $this->late;
    }

    public function setLate(?int $late): self
    {
        $this->late = $late;

        return $this;
    }

    public function isSent2(): ?bool
    {
        return $this->sent2;
    }

    public function setSent2(?bool $sent2): self
    {
        $this->sent2 = $sent2;

        return $this;
    }

    public function isSent24(): ?bool
    {
        return $this->sent24;
    }

    public function setSent24(bool $sent24): self
    {
        $this->sent24 = $sent24;

        return $this;
    }
}
