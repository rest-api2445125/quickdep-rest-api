<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\TimeSheetApprouveController;
use App\Controller\TimeSheetEditController;
use App\Controller\TimeSheetListingController;
use App\Controller\TimeSheetToSendController;
use App\Repository\TimeSheetRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TimeSheetRepository::class)]
#[ApiResource(
    attributes: ["order" => [
        "id" => "DESC"
    ]],
    itemOperations: [
        'get',
        'approuve' => [
            'method'     => 'POST',
            'path'       => '/time_sheets/{id}/approuve',
            'controller' => TimeSheetApprouveController::class
        ],
        'edit' => [
            'method'     => 'POST',
            'path'       => '/time_sheets/{id}/edit',
            'controller' => TimeSheetEditController::class,
            'openapi_context' => [
                "summary" => "Update the timesheet ressource",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'startdate' => [
                                        'type' => 'string',
                                    ],
                                    'enddate' => [
                                        'type' => 'string',
                                    ],
                                    'pause' => [
                                        'type' => 'string',
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'delete',
        'patch'
    ],
    collectionOperations: [
        'get' => [
            'method' => 'GET',
            'path' => '/time_sheets',
            'controller' => TimeSheetListingController::class
        ],
        'wait' => [
            'method' => 'GET',
            'path' => '/time_sheets_to_send',
            'controller' => TimeSheetToSendController::class
        ],
        'post'
    ]
)]
class TimeSheet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["contract:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'timeSheet', cascade: ['persist', 'remove'])]
    private ?Contract $contract = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(type: Types::INTEGER)]
    #[Groups(["contract:read", "invoice:read"])]
    private ?int $totaltimes = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    #[Groups(["contract:read", "invoice:read"])]
    private ?string $amount = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read"])]
    private ?string $status = null;

    #[ORM\ManyToOne(inversedBy: 'timeSheets', cascade: ['persist'])]
    #[Groups(["contract:read"])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["contract:read", "invoice:read"])]
    private ?\DateTimeInterface $startdate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["contract:read", "invoice:read"])]
    private ?\DateTimeInterface $enddate = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "invoice:read"])]
    private ?int $pause = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTotaltimes(): ?int
    {
        return $this->totaltimes;
    }

    public function setTotaltimes(int $totaltimes): self
    {
        $this->totaltimes = $totaltimes;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getPause(): ?int
    {
        return $this->pause;
    }

    public function setPause(?int $pause): self
    {
        $this->pause = $pause;

        return $this;
    }
}
