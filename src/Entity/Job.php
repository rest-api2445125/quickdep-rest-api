<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\JobRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: JobRepository::class)]
#[ApiResource()]
class Job
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?string $title = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?string $rate = null;

    #[ORM\OneToMany(mappedBy: 'job', targetEntity: Contract::class)]
    private Collection $contracts;

    #[Groups(["contract:read", "user:read", "client:read", "invoice:read"])]
    private ?string $merge = null;

    public function __construct()
    {
        $this->contracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return Collection<int, Contract>
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts->add($contract);
            $contract->setJob($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getJob() === $this) {
                $contract->setJob(null);
            }
        }

        return $this;
    }

    public function getMerge(): ?string
    {
        return $this->merge;
    }

    public function setMerge(?string $merge): self
    {
        $this->merge = $merge;

        return $this;
    }
}
