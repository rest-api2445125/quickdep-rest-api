<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\TownsListingController;
use App\Repository\TownRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TownRepository::class)]
#[ApiResource(
    collectionOperations: [
        'post',
        'get' => [
            'method' => 'GET',
            'path' => '/towns',
            'controller' => TownsListingController::class
        ],
    ],
)]
class Town
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["contract:read", "user:read", "client:read", "client:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "user:read", "client:read", "client:read"])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'town', targetEntity: Contract::class)]
    private Collection $contracts;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'town')]
    private Collection $users;

    #[Groups(["contract:read", "user:read", "client:read"])]
    private bool $selected = false;

    #[ORM\OneToMany(mappedBy: 'town', targetEntity: Client::class)]
    private Collection $clients;

    public function __construct()
    {
        $this->contracts = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->clients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isSelected(): ?bool
    {
        return $this->selected;
    }

    public function setSelected(bool $selected): self
    {
        $this->selected = $selected;
        return $this;
    }

    /**
     * @return Collection<int, Contract>
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts->add($contract);
            $contract->setTown($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getTown() === $this) {
                $contract->setTown(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addTown($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeTown($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Client>
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients->add($client);
            $client->setTown($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->removeElement($client)) {
            // set the owning side to null (unless already changed)
            if ($client->getTown() === $this) {
                $client->setTown(null);
            }
        }

        return $this;
    }
}
