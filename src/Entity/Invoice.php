<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\InvoiceDownloadController;
use App\Controller\InvoiceGenerateController;
use App\Controller\InvoiceGetForClientControllre;
use App\Controller\InvoicesGetForEmployeesController;
use App\Controller\InvoicePayController;
use App\Repository\InvoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
#[ApiResource(
    attributes: ["order" => [
        "id" => "ASC"
    ]],
    collectionOperations: [
        'get',
        'generatte' => [
            'method' => 'POST',
            'path' => '/invoices/generate',
            'controller' => InvoiceGenerateController::class,
            'openapi_context' => [
                "summary" => "Admin (auto) | generate invoices",
            ],
        ],
        'employee' => [
            'method' => 'GET',
            'path' => '/invoices/employee',
            'controller' => InvoicesGetForEmployeesController::class,
            'openapi_context' => [
                "summary" => "client, get all employee's invoices",
            ]
        ],
        'client' => [
            'method' => 'GET',
            'path' => '/invoices/client',
            'controller' => InvoiceGetForClientControllre::class,
            'openapi_context' => [
                "summary" => "client, get all client's invoices",
            ]
        ],
    ],
    itemOperations: [
        'get',
        'delete',
        'pay' => [
            'method' => 'POST',
            'path' => '/invoices/{id}/pay',
            'controller' => InvoicePayController::class,
            'openapi_context' => [
                "summary" => "Admin (auto) | mark invoice as paid",
            ],
        ],
        'download' => [
            'method' => 'POST',
            'path' => '/invoices/{id}/download',
            'controller' => InvoiceDownloadController::class,
            'openapi_context' => [
                "summary" => "Admin (auto) | download invoice",
            ],
        ],
    ],
    normalizationContext: [
        'groups' => [
            "invoice:read"
        ]
    ],
)]
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "invoice:read"])]
    private ?string $code = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["user:read", "invoice:read"])]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    #[Groups(["user:read", "invoice:read"])]
    private ?string $total = null;

    #[ORM\ManyToOne(inversedBy: 'invoices')]
    #[Groups(["invoice:read"])]
    private ?User $user = null;

    #[ORM\Column]
    #[Groups(["user:read", "invoice:read"])]
    private ?bool $paid = null;

    #[ORM\ManyToMany(targetEntity: Contract::class, mappedBy: 'invoices')]
    #[Groups(["user:read", "invoice:read"])]
    private Collection $contracts;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $start = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $end = null;

    public function __construct()
    {
        $this->contracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * @return Collection<int, Contract>
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts->add($contract);
            $contract->addInvoice($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            $contract->removeInvoice($this);
        }

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }
}
