<?php

namespace App\Entity;

use App\Repository\ExperienceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ExperienceRepository::class)]
class Experience
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user:read", "contract:read", "client:read", "document:read"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'experiences')]
    private ?User $user = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "contract:read", "client:read", "document:read"])]
    private ?string $work = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "contract:read", "client:read", "document:read"])]
    private ?string $time = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "contract:read", "client:read", "document:read"])]
    private ?string $title = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getWork(): ?string
    {
        return $this->work;
    }

    public function setWork(string $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
