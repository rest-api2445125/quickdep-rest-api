<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ResetPasswordConfirmController;
use App\Controller\ResetPasswordController;
use App\Repository\PasswordResetRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PasswordResetRepository::class)]

#[ApiResource(
    collectionOperations: [
        'post',
        'get',
        'reset-request' => [
            'method' => 'POST',
            'path' => '/password-reset/reset-request',
            'controller' => ResetPasswordController::class,
            'openapi_context' => [
                "summary" => "Reset password request",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'reset-confirm' => [
            'method' => 'POST',
            'path' => '/password-reset/reset-confirm',
            'controller' => ResetPasswordConfirmController::class,
            'openapi_context' => [
                "summary" => "Reset password request",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'email' => [
                                        'type' => 'string'
                                    ],
                                    'code' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ],
    normalizationContext: [
        "groups" => ["passwordReset:read"]
    ]
)]
class PasswordReset
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["passwordReset:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 10)]
    #[Groups(["passwordReset:read"])]
    private ?string $code = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(length: 255)]
    #[Groups(["passwordReset:read"])]
    private ?string $email = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
