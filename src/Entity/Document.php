<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Configuration\DocumentStatus;
use App\Controller\DocumentCreateController;
use App\Controller\DocumentSendFileController;
use App\Controller\DocumentSendTextController;
use App\Controller\DocumentUnValidatedController;
use App\Controller\DocumentValidatedController;
use App\Repository\DocumentRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @Vich\Uploadable() 
 */
#[ORM\Entity(repositoryClass: DocumentRepository::class), ApiResource(
    itemOperations: [
        'delete',
        'get',
        'patch',
        'validate' => [
            'method' => 'POST',
            'path' => '/documents/{id}/valid',
            'controller' => DocumentValidatedController::class,
            'openapi_context' => [
                "summary" => "Admin | validate user's document sent",
            ],
        ],
        'unvalidate' => [
            'method' => 'POST',
            'path' => '/documents/{id}/unvalid',
            'controller' => DocumentUnValidatedController::class,
            'openapi_context' => [
                "summary" => "Admin | unvalidate user's document sent",
            ],
        ],
        'file' => [
            'method' => 'POST',
            'path' => '/documents/{id}/file',
            'deserialize' => false,
            'controller' => DocumentSendFileController::class,
            'openapi_context' => [
                "summary" => "document send file",
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' =>  'binary'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'text' => [
            'method' => 'POST',
            'path' => '/documents/{id}/text',
            'deserialize' => false,
            'controller' => DocumentSendTextController::class,
            'openapi_context' => [
                "summary" => "document send document text",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'string',
                                'properties' => [
                                    'text' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ],
    denormalizationContext: [
        "groups" => ["document:write"]
    ],
    normalizationContext: [
        "groups" => ["document:read"]
    ]
)]
class Document
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["document:read", "user:read"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'documents')]
    #[Groups(["document:write", "document:read"])]
    private ?User $employee = null;

    #[ORM\Column(length: 255)]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?string $statut = null;

    #[ORM\ManyToOne(inversedBy: 'documents')]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?DocumentType $type = null;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="documents_files", fileNameProperty="filePath")
     */
    private $file;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $filePath = null;

    /**
     * @var string|null
     */

    #[Groups(["document:write", "document:read", "user:read"])]
    private ?string $fileUrl = null;

    #[ORM\Column]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?\DateTimeImmutable $updated_at = null;

    #[ORM\Column]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?string $text = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["document:write", "document:read", "user:read"])]
    private ?string $message = null;

    public function __construct()
    {
        $this->setStatut(DocumentStatus::$WAITING);
        $this->created_at = new DateTimeImmutable();
        $this->updated_at = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmployee(): ?User
    {
        return $this->employee;
    }

    public function setEmployee(?User $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getType(): ?DocumentType
    {
        return $this->type;
    }

    public function setType(?DocumentType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }

    public function setFileUrl(?string $fileUrl): self
    {
        $this->fileUrl = $fileUrl;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
