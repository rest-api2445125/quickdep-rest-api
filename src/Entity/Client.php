<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ClientGetAppliedContractsController;
use App\Controller\ClientGetApprouvedContractController;
use App\Controller\ClientGetContractsController;
use App\Controller\ClientImageController;
use App\Controller\ClientPostController;
use App\Controller\ClientRegisterController;
use App\Controller\ClientValidationController;
use App\Repository\ClientRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable() 
 */
#[ORM\Entity(repositoryClass: ClientRepository::class), ApiResource(
    attributes: ["order" => [
        "id" => "DESC"
    ]],
    itemOperations: [
        'get',
        'patch',
        'delete',
        'image' => [
            'method' => 'POST',
            'path' => '/clients/{id}/image',
            'deserialize' => false,
            'controller' => ClientImageController::class,
            'openapi_context' => [
                "summary" => "add client image",
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'image' => [
                                        'type' => 'string',
                                        'format' =>  'binary'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'contracts' => [
            'method' => 'GET',
            'path' => '/clients/{id}/contracts',
            'controller' => ClientGetContractsController::class,
            'openapi_context' => [
                "summary" => "client, get all contracts",
            ]
        ],
        'applies' => [
            'method' => 'GET',
            'path' => '/clients/{id}/applies',
            'controller' => ClientGetAppliedContractsController::class,
            'openapi_context' => [
                "summary" => "client, get all applied contracts",
            ]
        ],
        'approuved' => [
            'method' => 'GET',
            'path' => '/clients/{id}/approuved',
            'controller' => ClientGetApprouvedContractController::class,
            'openapi_context' => [
                "summary" => "client, get all approuved contracts",
            ]
        ],
        'validate' => [
            'method' => 'POST',
            'path' => '/clients/{id}/validate',
            'controller' => ClientValidationController::class,
            'openapi_context' => [
                "summary" => "Admin, Validate client account",
            ]
        ],
    ],
    collectionOperations: [
        'get',
        'post' => [
            'method' => 'POST',
            'controller' => ClientRegisterController::class
        ],
        'register' => [
            'method' => 'POST',
            'path' => '/clients/register',
            'controller' => ClientRegisterController::class,
            'openapi_context' => [
                "summary" => "Register as society",
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'name' => [
                                        'type' => 'string'
                                    ],
                                    'password' => [
                                        'type' => 'string'
                                    ],
                                    'phone' => [
                                        'type' => 'string'
                                    ],
                                    'address' => [
                                        'type' => 'string'
                                    ],
                                    'town_id' => [
                                        'type' => 'string'
                                    ],
                                    'factureaddress' => [
                                        'type' => 'string'
                                    ],
                                    'zipcode' => [
                                        'type' => 'string'
                                    ],
                                    'facturezipcode' => [
                                        'type' => 'string'
                                    ],
                                    'owner' => [
                                        'type' => 'string'
                                    ],
                                    'payment' => [
                                        'type' => 'string'
                                    ],
                                    'device' => [
                                        'type' => 'string'
                                    ],
                                    'email' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    denormalizationContext: [
        "groups" => ["client:write"]
    ],
    normalizationContext: [
        "groups" => ["client:read"]
    ]
)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "invoice:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write", "invoice:read"])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write", "invoice:read"])]
    private ?string $phone = null;

    #[ORM\Column(length: 255)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write", "invoice:read"])]
    private ?string $address = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Contract::class)]
    private Collection $contracts;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $imagePath = null;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping= "clients_images", fileNameProperty= "imagePath")
     */
    private $image;

    #[ORM\Column]
    private ?\DateTimeImmutable $updated_at = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[Groups(["contract:read", "client:read", "user:read"])]
    private ?string $imageUrl = null;

    #[ORM\ManyToOne(inversedBy: 'clients', fetch: 'EAGER')]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?Town $town = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?string $factureaddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?string $zipcode = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?string $facturezipcode = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?string $owner = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write"])]
    private ?int $payment = null;

    #[Groups(["contract:read", "client:read", "user:read", "invoice:read"])]
    private ?int $userId = null;

    #[Groups(["contract:read", "client:read", "user:read", "invoice:read"])]
    #[ORM\Column(nullable: true)]
    private ?String $email = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["contract:read", "client:read", "user:read", "like:read", "client:write", "invoice:read"])]
    private ?bool $validated = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
        $this->updated_at = new DateTimeImmutable();
        $this->contracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Contract>
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts->add($contract);
            $contract->setClient($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->removeElement($contract)) {
            // set the owning side to null (unless already changed)
            if ($contract->getClient() === $this) {
                $contract->setClient(null);
            }
        }

        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(?Town $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getFactureaddress(): ?string
    {
        return $this->factureaddress;
    }

    public function setFactureaddress(string $factureaddress): self
    {
        $this->factureaddress = $factureaddress;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getFacturezipcode(): ?string
    {
        return $this->facturezipcode;
    }

    public function setFacturezipcode(string $facturezipcode): self
    {
        $this->facturezipcode = $facturezipcode;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getPayment(): ?int
    {
        return $this->payment;
    }

    public function setPayment(int $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getEmail(): ?String
    {
        return $this->email;
    }

    public function setEmail(?String $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function isValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(?bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }
}
