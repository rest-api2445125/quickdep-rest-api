<?php

namespace App\Entity;

use App\Repository\ApplyRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ApplyRepository::class)]
class Apply
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user:read", "contract:read"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'applies', fetch: 'EAGER')]
    #[Groups(["user:read", "contract:read"])]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'applies')]
    #[Groups(["user:read"])]
    private ?Contract $contract = null;

    #[ORM\Column]
    #[Groups(["user:read", "contract:read"])]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "contract:read"])]
    private ?bool $consecutive = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["user:read", "contract:read"])]
    private ?int $consecutivecount = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["user:read", "contract:read"])]
    private ?\DateTimeInterface $startdate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["user:read", "contract:read"])]
    private ?\DateTimeInterface $enddate = null;

    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function isConsecutive(): ?bool
    {
        return $this->consecutive;
    }

    public function setConsecutive(?bool $consecutive): self
    {
        $this->consecutive = $consecutive;

        return $this;
    }

    public function getConsecutivecount(): ?int
    {
        return $this->consecutivecount;
    }

    public function setConsecutivecount(?int $consecutivecount): self
    {
        $this->consecutivecount = $consecutivecount;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(?\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }
}
