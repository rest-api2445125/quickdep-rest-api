<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\SignatureController;
use App\Repository\UserContractRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable()
 */
#[ORM\Entity(repositoryClass: UserContractRepository::class)]
#[ApiResource(
    itemOperations: [
        'patch',
        'get',
        'delete',
        'sign' => [
            'method' => 'POST',
            'path' => '/user_contracts/{id}/sign',
            'deserialize' => false,
            'controller' => SignatureController::class,
            'openapi_context' => [
                "summary" => "add signature",
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'signature' => [
                                        'type' => 'string',
                                        'format' =>  'binary'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ]
)]
class UserContract
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?string $type = null;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="sign_files", fileNameProperty="signaturePath")
     */
    private $signature;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $signaturePath = null;

    /**
     * @var string|null
     */
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?string $signatureUrl = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Groups(["user:read", "user:write", "document:read", "client:read"])]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $updated_at = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSignaturePath(): ?string
    {
        return $this->signaturePath;
    }

    public function setSignaturePath(?string $signaturePath): self
    {
        $this->signaturePath = $signaturePath;

        return $this;
    }

    public function getSignature(): ?File
    {
        return $this->signature;
    }

    public function setSignature(?File $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getSignatureUrl(): ?string
    {
        return $this->signatureUrl;
    }

    public function setSignatureUrl(?string $signatureUrl): self
    {
        $this->signatureUrl = $signatureUrl;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
