<?php

namespace App\Configuration;

use App\Entity\Document;
use App\Entity\DocumentType;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DocumentConfig
{

    public static $ADMIN_NUMBERS = ["+15817771486"];

    public static function check_not_sent(User $user): bool
    {
        $docs_not_sent = false;

        foreach ($user->getDocuments() as $user_doc) {
            if ($user_doc->getStatut() == DocumentStatus::$NOT_SENT) {
                $docs_not_sent = true;
            }
        }

        return $docs_not_sent;
    }

    public static function check_waiting(User $user): bool
    {
        $total = count($user->getDocuments());
        $i = 0;
        $j = 0;

        foreach ($user->getDocuments() as $user_doc) {
            if ($user_doc->getStatut() == DocumentStatus::$WAITING || $user_doc->getStatut() == DocumentStatus::$VALIDATED) {
                $i++;
            }
            if ($user_doc->getStatut() == DocumentStatus::$WAITING) {
                $j++;
            }
        }

        return $i == $total && $j > 0;
    }

    public static function check_validated(User $user): bool
    {
        $docs_validated = true;

        foreach ($user->getDocuments() as $user_doc) {
            if ($user_doc->getStatut() != DocumentStatus::$VALIDATED) {
                $docs_validated = false;
            }
        }

        return $docs_validated;
    }

    public static function check_refused(User $user): bool
    {
        $docs_refused = false;

        foreach ($user->getDocuments() as $user_doc) {
            if ($user_doc->getStatut() == DocumentStatus::$REFUSED) {
                $docs_refused = true;
            }
        }

        return $docs_refused;
    }

    public static function create_all_documents(User $user, EntityManagerInterface $em)
    {
        $document_types = $em->getRepository(DocumentType::class)->findAll();

        foreach ($document_types as $type) {
            $document = new Document();
            $document->setEmployee($user)
                ->settype($type)
                ->setStatut(DocumentStatus::$NOT_SENT);
            $em->persist($document);
            $em->flush();
        }
    }
}
