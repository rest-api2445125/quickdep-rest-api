<?php

namespace App\Configuration;

/**
 * Contract Flow
 * -> Quand une entreprise publie un contract, il est publié avec un status "PUBLISHED"
 *            -> Si l'entreprise accepte la candidature de la personne, le contrat prend le statut "APPROUVED"
 *            -> Si l'entréprise signale que la personne a travaillé, le contrat prend le statu "WORKED"  
 *            -> Si l'entréprise signale que la personne n'a pas travaillé, le contrat prend le statu "NOT_WORKED"
 *            -> Si la feuille de temps est validé, le contrat prend le statut "TIME_APPROUVED" 
 */
class ContractStatus
{
    public static $PUBLISHED = "contract_published";
    public static $APPROUVED = "contract_approuved";
    public static $WORKED = "contract_worked";
    public static $NOT_WORKED = "contract_not_worked";
    public static $TIME_SENT = "contract_time_sent";
    public static $TIME_APPROUVED = "contract_time_approuved";
    public static $PAID = "contract_paid";
}
