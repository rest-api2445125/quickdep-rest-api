<?php

namespace App\Configuration;

class UserType
{
    public static $EMPLOYEE = "user_employee";
    public static $SOCIETY = "user_society";
}
