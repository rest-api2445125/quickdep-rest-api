<?php

namespace App\Configuration;

use App\Entity\Notification;
use App\Repository\DeviceRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FirebaseConfig
{
    public static function sendToAll(
        Notification $notification,
        HttpClientInterface $client,
        DeviceRepository $deviceRepository
    ) {
        $devicesTokens = [];
        foreach ($notification->getUsers() as $user) {
            foreach ($user->getDevices() as $device) {
                $devicesTokens[] = $device->getToken();
            }
        }

        $title = "";

        switch ($notification) {
            case NotificationType::$BONUS:
                $title = "Un bonus vient d'être ajouté";
                break;

            case NotificationType::$NEW_SHIFT:
                $title = "Nouveau Shif publié";
                break;

            default:
                $title = "Quickdep";
                break;
        }
        
        $fields_string = '{
            "registration_ids": ' . json_encode($devicesTokens) . ',
            "notification": {
              "title": "' . $title . '",
              "body": "' . $notification->getMessage() . '",
              "mutable_content": true,
              "sound": "Tri-tone"
              },
        
           "data": {
            "url": "",
            "dl": ""
            }
        }';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "content-type:application/json;charset=utf-8",
            'Authorization: key=AAAAPH06tbI:APA91bGtDNr92B8FgISmzww7MeBDmWeDCBdnslQqWwvA3O1yh0C2rT5iR8Uy38THBG3dxKQNZVTimcBWarTj2mIhOGyhQ8y3pWw7zHK3yGXapS5f0HkV2QvMNgKPQlQDrVKCZRp5jUR4'
        ));
        curl_exec($ch);
    }

    public function send(array $users)
    {
    }
}
