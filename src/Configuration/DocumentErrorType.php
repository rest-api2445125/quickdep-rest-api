<?php

namespace App\Configuration;

class DocumentErrorType
{
    public static $DOCUMENT_NOT_SENT = 1;
    public static $DOCUMENT_WAITING = 2;
    public static $DOCUMENT_REFUSED = 3;
    public static $CONTRACT_NOT_SIGNED = 4;
    public static $EMAIL_NOT_VERIFIED = 5;
    public static $NO_PROFILE_PICTURE = 6;
}
