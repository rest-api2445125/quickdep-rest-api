<?php

namespace App\Configuration;

use App\Controller\MailerController;
use App\Entity\Contract;
use App\Entity\Notification as EntityNotification;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use App\Configuration\NotificationType;
use App\Entity\Invoice;
use App\Repository\DeviceRepository;
use DateTimeImmutable;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client as TwilioClient;

class NotificationConfig
{
    private static $days = [
        "dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"
    ];

    private static $months = [
        "janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "decembre"
    ];

    public static function shifNotification(Contract $contract, UserRepository $userRepository, MailerInterface $mailer, $type = "CREATION")
    {
        $users = $userRepository->findAll();
        $template = "";


        switch ($type) {
            case 'CREATION':
                $template = "email/ShiftCreated.html.twig";
                $subject = "Nouveau shift publié";
                break;
            case "BONUS":
                $template = "email/bonus.html.twig";
                $subject = "Ajout d'un bonus de " . $contract->getBonus() . "$/heure";
                break;
            case "UNACCEPTED":
                $template = "email/unaccepted.html.twig";
                $subject = "Shift à compléter aujourd'hui !";
                break;
            default:
                break;
        }

        $client = $contract->getClient();

        foreach ($users as $user) {
            $email = $user->getEmail();
            $username = $user->getFirstname() . " " . $user->getLastname();
            $template_datas = [
                "contract" => $contract,
                "client" => $client,
                "username" => $username
            ];
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                MailerController::sendEmail($mailer, $email, $template, $template_datas, $subject);
            }
        }
    }

    public static function create(
        Contract $contract,
        TwilioClient $twilioClient,
        String $type,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        DeviceRepository $deviceRepository,
        bool $consecutif = false,
        $endDate = null
    ) {
        $notification = new EntityNotification();

        if ($contract != null) {
            $notification->setContract($contract);
        }

        $notification->setType($type);
        $notification->setDate(new DateTimeImmutable());

        $messsage = null;

        $users = $userRepository->findBy([
            "type" => UserType::$EMPLOYEE
        ]);

        if ($type == NotificationType::$WORKED) {
            $users = $userRepository->findBy([
                "id" => $contract->getEmployee()->getId()
            ]);
        }

        if ($type == NotificationType::$BONUS) {
            $num_day = date_format($contract->getStartdate(), "w");
            $num_month = date_format($contract->getStartdate(), "n");

            $day = self::$days[(int) $num_day];
            $day_d =  date_format($contract->getStartdate(), "d");
            $month = self::$months[(int) $num_month - 1];
            $year = date_format($contract->getStartdate(), "Y");

            $messsage = "Un bonus de " .
                $contract->getBonus() .
                "$/heure vient d’être ajouté au shift de " .
                $contract->getJob()->getTitle() .
                " prévu pour le $day $day_d $month $year";

            if ($contract->getStartdate() != null && $contract->getEnddate() != null) {
                $messsage .= " de " .
                    date_format($contract->getStartdate(), "H:i") .
                    " à " .
                    date_format($contract->getEnddate(), "H:i");
            }

            $messsage .= " chez " . $contract->getClient()->getName();
        } elseif ($type == NotificationType::$NEW_SHIFT) {
            if ($consecutif) {
                $messsage = "Un ensemble de shifts vient d'être publié chez ";
            } else {
                $messsage = "Un shift vient d'être publié chez ";
            }

            $messsage .= $contract->getClient()->getName();

            if ($consecutif) {
                $messsage .= " du " . ContractConfig::full_date($contract->getStartdate(), true)
                    . " au " . ContractConfig::full_date($endDate, true);
            } else {
                $messsage .= " pour le " . ContractConfig::full_date($contract->getStartdate(), true);
            }
            $messsage .= " de " . $contract->getStartdate()->format("H:i") . " à " . $contract->getEnddate()->format("H:i") .
                " payé " . $contract->getJob()->getRate() . "$/H";
        } elseif ($type == NotificationType::$SHIFT_UNACCEPTED) {
            $messsage =
                $contract->getClient()->getName() .
                " offre un shift Aujourd'hui ! " .
                " de " . $contract->getStartdate()->format("H:i") . " à " . $contract->getEnddate()->format("H:i") .
                " payé " . $contract->getJob()->getRate() . "$/H";

            if ($contract->getBonus() != null) {
                $messsage .= ". Avec un bonus de " . $contract->getBonus() . "$/H.";
            }
        } elseif ($type == NotificationType::$WORKED) {
            $messsage =
                "Vous avez une nouvelle feuille de temps à déclarer pour le shift du ";

            $messsage .= ContractConfig::full_date($contract->getStartdate(), true);
            $messsage .= " chez " . $contract->getClient()->getName();
        }

        $notification->setMessage($messsage);

        $em->persist($notification);
        $em->flush();

        foreach ($users as $user) {
            $user_towns = $user->getTown();
            $in_towns = false;
            foreach ($user_towns as $town) {
                if ($town != null && $contract->getTown() != null) {
                    if ($town->getId() == $contract->getTown()->getId()) {
                        $in_towns = true;
                    }
                }
            }
            if ($in_towns) {
                $user->addNotification($notification);
                $em->persist($user);
                $em->flush();
            }
        }


        // send the notification
        FirebaseConfig::sendToAll($notification, $httpClient, $deviceRepository);
    }

    public static function payInvoice(Invoice $invoice, MailerInterface $mailer)
    {
        $success = true;
        $user = $invoice->getUser();
        $template = "email/payInvoice.html.twig";
        $template_datas = [
            "invoice" => $invoice,
        ];
        $subject = "Vous avez une nouvelle facture payé chez QuickDep";
        if (filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            if (!MailerController::sendEmail($mailer, $user->getEmail(), $template, $template_datas, $subject)) {
                $success = false;
            }
        }
        return $success;
    }

    public static function sendSms(TwilioClient $client, String $to, String $message)
    {
        try {
            $message = $client->messages
                ->create(
                    ContractConfig::format_number($to), // to
                    [
                        "body" => "QuickDep\r\n---------\r\n" . $message
                            . "\r\n---------\r\nCeci est un message automatique, veuillez ne pas y répondre.",
                        "from" => "+16692794696"
                    ]
                );
        } catch (\Throwable $th) {
            // 
        }
    }
}
