<?php

namespace App\Configuration;

class DocumentStatus
{
    public static $NOT_SENT = "document_not_sent";
    public static $WAITING = "document_waiting"; // en attente
    public static $VALIDATED = "document_validated";
    public static $REFUSED = "document_refused";
}
