<?php

namespace App\Configuration;

class NotificationType
{
    public static $BONUS = "bonus";
    public static $NEW_SHIFT = "new";
    public static $SHIFT_24H = "h24";
    public static $SHIFT_2H = "h2";
    public static $SHIFT_ACCEPTED = "Saccepted";
    public static $SHIFT_UNACCEPTED = "unaccepted_shift";
    public static $WORKED = "shift_worked";
    public static $DOC_ACCEPTED = "Daccepted";
}
