<?php

namespace App\Configuration;

use App\Controller\MailerController;
use App\Entity\Apply;
use App\Entity\Client as EntityClient;
use App\Entity\Contract;
use App\Entity\TimeSheet;
use App\Entity\User;
use App\Repository\ApplyRepository;
use App\Repository\ContractRepository;
use App\Repository\DeviceRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\Entity\Client as AppEntityClient;
use App\Entity\Device;
use App\Entity\PasswordReset;
use App\Entity\UserContract;
use App\Repository\PasswordResetRepository;
use DateTimeImmutable;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twilio\Rest\Client;

class ContractConfig
{
    public static $MAX_APPLIES = 3;

    public static $CLIENT_MERGE = 6;

    public static $DAYS = [
        "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"
    ];

    public static $MONTHS = [
        "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Décembre",
    ];

    public static function format_number(String $number): String
    {
        return str_replace([' ', '-', '(', ')'], '', $number);
    }

    public static function contractWorked(Contract $data, EntityManagerInterface $em, Client $twilio)
    {
        $data->setStatus(ContractStatus::$WORKED);

        if ($data->getNext() != null) {
            $data->getNext()->setParent(null);
            $data->setNext(null);
        }

        $em->persist($data);
        $em->flush();

        // Send sms
        $to = ContractConfig::format_number($data->getEmployee()->getPhone());

        $message = "Vous avez une nouvelle feuille de temps à déclarer pour le shift du "
            . ContractConfig::full_date($data->getStartdate(), true)
            . " chez " . $data->getClient()->getName() . ", au poste de " . $data->getJob()->getTitle() . ". Veuillez vous rendre au menu feuilles de temps de votre application pour le faire.";
        NotificationConfig::sendSms($twilio, $to, $message);
    }

    public static function full_date(DateTime $date, bool $dayName = false, bool $year = false): String
    {
        $new_date = "";
        if ($dayName) {
            $new_date .= self::$DAYS[(int) $date->format("N") - 1]
                . " ";
        }

        $new_date .= $date->format("d") . " "
            . self::$MONTHS[(int) $date->format("n") - 1];

        if ($year) {
            $new_date .= " " . $date->format("Y");
        }

        return $new_date;
    }

    public static function listing(
        $contracts, 
        ?User $user,
        bool $stringAsArray = false
    )
    {
        $return = [];
        $last_date = null;

        foreach ($contracts as $contract) {
            if ($user != null) {
                $user_applies = $user->getApplies();
                foreach ($user_applies as $apply) {
                    if ($apply->getContract()->getId() == $contract->getId()) {
                        $contract->setApplied(true);
                    }
                }
            }

            $year = $contract->getStartdate()->format('Y');
            $month = $contract->getStartdate()->format('n');
            if ($stringAsArray) {
                if (!in_array([$month . ' ' . $year], $return)) {
                    if ($last_date != [$month . ' ' . $year]) {
                        $return[] = [$month . ' ' . $year];
                        $last_date = [$month . ' ' . $year];
                    }
                    $return[] = $contract;
                } else {
                    $return[] = $contract;
                }
            } else {
                if (!in_array($month . ' ' . $year, $return)) {
                    if ($last_date != $month . ' ' . $year) {
                        $return[] = $month . ' ' . $year;
                        $last_date = $month . ' ' . $year;
                    }
                    $return[] = $contract;
                } else {
                    $return[] = $contract;
                }
            }
        }

        if (count($contracts) == 0) {
            return [
                "Aucun contrat trouvé"
            ];
        }

        return $return;
    }

    public static function createContract (
        Contract $data, 
        bool $consecutive, 
        EntityManagerInterface $entityManager, 
        Client $twilioClient,
        DeviceRepository $deviceRepository,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
    ) {
        $data->setCode("QD");

        $data->setRate($data->getJob()->getRate());
        $data->setClientrate($data->getJob()->getRate() + ContractConfig::$CLIENT_MERGE);

        $data->setPaid(false);

        $entityManager->persist($data);
        $entityManager->flush();

        $data->setCode(self::generate_code($data));

        $data->setBonus($data->getBonus() != null && $data->getBonus() != 0 ? $data->getBonus() : null);

        $data->setStatus(ContractStatus::$PUBLISHED);

        if ($data->getPause() == 0 || $data->getPause() == "") {
            $data->setPause(null);
        }

        if (trim($data->getDescription()) == "") {
            $data->setDescription(null);
        }

        if (trim($data->getCloth()) == "") {
            $data->setCloth(null);
        }

        if ($data->getAddress() == null) {
            $data->setAddress($data->getClient()->getAddress());
        }

        if ($consecutive) {
            $startdate = DateTime::createFromFormat(
                "Y-m-d",
                $data
                    ->getStartdate()
                    ->format('Y-m-d')
            );
            $enddate = DateTime::createFromFormat(
                "Y-m-d",
                $data
                    ->getEnddate()
                    ->format('Y-m-d')
            );

            $starttime = $data->getStartdate()->format('H:i:s');
            $endtime = $data->getEnddate()->format('H:i:s');

            $data->setEnddate(
                DateTime::createFromFormat(
                    "Y-m-d H:i:s",
                    $data->getEnddate()->format("Y-m-d") . $data->getEnddate()->format("H:i:s"),
                )
            );

            if ($data->getTown() == null) {
                $data->setTown($data->getClient()->getTown());
            }

            $entityManager->persist($data);
            $entityManager->flush();

            self::addNexts(
                contract: $data,
                length: (int) $startdate->diff($enddate)->days,
                entityManager: $entityManager,
                endtime: $endtime
            );

            NotificationConfig::create(
                $data,
                $twilioClient,
                NotificationType::$NEW_SHIFT,
                $entityManager,
                $userRepository,
                $httpClient,
                $deviceRepository,
                true,
                $enddate,
            );
        } else {
            if (
                date_format($data->getStartdate(), "d/m/Y")
                != date_format(new DateTime("today"), "d/m/Y")
            ) {
                NotificationConfig::create(
                    $data,
                    $twilioClient,
                    NotificationType::$NEW_SHIFT,
                    $entityManager,
                    $userRepository,
                    $httpClient,
                    $deviceRepository
                );
            } else {
                NotificationConfig::create(
                    $data,
                    $twilioClient,
                    NotificationType::$SHIFT_UNACCEPTED,
                    $entityManager,
                    $userRepository,
                    $httpClient,
                    $deviceRepository
                );
            }

            $entityManager->persist($data);
            $entityManager->flush();

        }
    }

    public static function editContract (
        ContractRepository $contractRepository,
        Contract $data,
        Client $twilioClient,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        HttpClientInterface $httpClient,
        DeviceRepository $deviceRepository,
    ) {
        $previousBonus = $contractRepository->find($data->getId())->getBonus();
        $newBonus = $data->getNewbonus();

        if ($previousBonus != $newBonus) {
            $data->setBonus($newBonus);
            if (
                ($previousBonus == null && $newBonus != 0)
                ||
                ($newBonus != 0 && $newBonus > $previousBonus)
            ) {
                NotificationConfig::create(
                    $data,
                    $twilioClient,
                    NotificationType::$BONUS,
                    $em,
                    $userRepository,
                    $httpClient,
                    $deviceRepository
                );
            }
        }

        $data->publish();

        foreach ($data->getApplies() as $apply) {
            $em->remove($apply);
            $em->flush();
        }

        $em->persist($data);
        $em->flush();
    }

    public static function clientContracts (
        EntityClient $data,
        UserRepository $userRepository,
        ApplyRepository $applyRepository,
        ContractRepository $contractRepository,
        bool $stringAsArray = false
    ) {

        $user_contracts = $contractRepository->findBy(
            [
                "client" => $data,
            ],
            ["startdate" => "ASC"]
        );

        $user = $userRepository->findOneBy([
            "client" => $data
        ]);


        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if ($contract->getStartdate() > new DateTime()) {
                if ($contract->isPublic($user, $applyRepository)) {
                    $contract->setApplied(false);
                    $validate_data[] = $contract;
                }
            }
        }

        $data_return = ContractConfig::listing($validate_data, null, $stringAsArray);
        
        return [
            "contracts" => $data_return,
            "user" => $user,
            "count" => count($validate_data)
        ];
    }

    public static function clientApplies (
        EntityClient $data,
        ContractRepository $contractRepository,
        bool $stringAsArray = false,
    ) {

        $user_contracts = $contractRepository->findBy(
            [
                "client" => $data,
            ],
            ["startdate" => "ASC"]
        );
        
        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if (
                $contract->getStatus() == ContractStatus::$PUBLISHED
                && $contract->getEmployee() == null
                && ($contract->getParent() == null || $contract->getParent()->getStatus() != ContractStatus::$PUBLISHED)
                && !$contract->startDatePassed()
                && !self::itemExists($validate_data, $contract)
            ) {
                if ($contract->getAppliesCount() && $contract->getAppliesCount() > 0) {
                    $contract->setApplied(true);
                    $validate_data[] = $contract;
                } else {
                    $contract->setApplied(false);
                }
            }
        }

        return ContractConfig::listing($validate_data, null, $stringAsArray);
    }

    public static function adminApplies (
        ContractRepository $contractRepository,
        bool $stringAsArray = true,
    ) {

        $user_contracts = $contractRepository->findAll();
        
        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if (
                $contract->getStatus() == ContractStatus::$PUBLISHED
                && $contract->getEmployee() == null
                && ($contract->getParent() == null || $contract->getParent()->getStatus() != ContractStatus::$PUBLISHED)
                && !self::itemExists($validate_data, $contract)
                && !$contract->startDatePassed()
            ) {
                if ($contract->getAppliesCount() && $contract->getAppliesCount() > 0) {
                    $contract->setApplied(true);
                    $validate_data[] = $contract;
                } else {
                    $contract->setApplied(false);
                }
            }
        }

        return ContractConfig::listing($validate_data, null, $stringAsArray);
    }

    public static function contractConfirm (
        Contract $data,
        Client $twilio,
        ApplyRepository $applyRepository,
        User $user,
        EntityManagerInterface $em
    ) {
        if ($data->getAppliesCount() == 0) {
            return [
                "success" => false,
                "message" => "Aucun utilisateur n'a postulé pour ce contrat"
            ];
        }

        $apply = $applyRepository->findOneBy([
            'contract' => $data,
            'user' => $user
        ]);

        if ($apply != null && $apply->isConsecutive()) {
            if ($apply->getConsecutivecount()) {
                self::applyNext($data, $user, $twilio, $em, $apply, 0, $apply->getConsecutivecount());
            } else {
                self::applyNext($data, $user, $twilio, $em, $apply);
            }
        } else {
            $applies = $applyRepository->findBy([
                'contract' => $data
            ]);

            foreach ($applies as $apply) {
                $applyRepository->remove($apply);
            }

            $data->setAppliesCount(null);
            $data->setStatus(ContractStatus::$APPROUVED);
            $data->setEmployee($user);

            // Send sms
            $to = ContractConfig::format_number($user->getPhone());

            $message = "Le shift prévu chez "
                . $data->getClient()->getName()
                . " pour le " .
                ContractConfig::full_date($data->getStartdate(), true) . " De "
                . $data->getStartdate()->format("H:i") . " à " . $data->getEnddate()->format("H:i")
                . " vous a été confirmé. SVP avisez d'un retard si vous pensez pas arriver à l'heure.";
            NotificationConfig::sendSms($twilio, $to, $message);

            $em->persist($data);
        }

        $em->flush();
    }

    public static function contractCancel (
        Contract $data,
        User $user,
        EntityManagerInterface $em,
        Client $twilio
    ) {
        if ($data->getStatus() == ContractStatus::$APPROUVED) {
            if ($user->getType() == UserType::$EMPLOYEE) {
                if ($data->getStartdate() <= new DateTime()) {
                    return [
                        'success' => false,
                        'message' => "Vous ne pouvez pas annuler le contrat car l'heure du début est déjà depassée."
                    ];
                }
                if ($data->getEmployee()->getCanceled() != null)
                    $data->getEmployee()->setCanceled($data->getEmployee()->getCanceled() + 1);
                else
                    $data->getEmployee()->setCanceled(1);
                $data->setEmployee(null);

                // Send sms
                $to = ContractConfig::format_number($data->getClient()->getPhone());

                $message = $data->getEmployee()->getFirstname() . " " . $data->getEmployee()->getLastname() .
                    " vient d'annuler sa présence ai shift prévu pour le " .
                    ContractConfig::full_date($data->getStartdate(), true) . " De "
                    . $data->getStartdate()->format("H:i") . " à " . $data->getEnddate()->format("H:i")
                    . " au poste de " . $data->getJob()->getTitle();

                NotificationConfig::sendSms($twilio, $to, $message);
            }

            $next = $data->getNext();

            if ($next != null && $next->getStatus() == $data->getStatus() && $next->getEmployee() == $data->getEmployee()) {
                self::cancelPrev($data, $em);
            } else {

                $applies = $em->getRepository(Apply::class)->findBy([
                    "contract" => $data,
                    "user" => $user
                ]);

                $data->publish();

                $em->persist($data);
                $em->flush();

                foreach ($applies as $apply) {
                    $em->remove($apply);
                    $em->flush();
                }
            }

            if ($user->getType() == UserType::$SOCIETY) {
                // Send sms
                $to = ContractConfig::format_number($data->getEmployee()->getPhone());

                $message = "Le shift prévu chez "
                    . $data->getClient()->getName()
                    . " pour le " .
                    ContractConfig::full_date($data->getStartdate(), true) . " De "
                    . $data->getStartdate()->format("H:i") . " à " . $data->getEnddate()->format("H:i")
                    . " vous a été retiré.";

                NotificationConfig::sendSms($twilio, $to, $message);
            }
        } else if ($data->getStatus() == ContractStatus::$PUBLISHED) {

            $data->setAppliesCount($data->getAppliesCount() - 1);

            $applies = $em->getRepository(Apply::class)->findBy([
                "contract" => $data,
                "user" => $user
            ]);

            foreach ($applies as $apply) {
                $em->remove($apply);
                $em->flush();
            }
        }
    }

    public static function userTimeSheet (
        User $data,
        ContractRepository $contractRepository,
        bool $stringAsArray = false,
        bool $admin = false
    ) {
        $user_contracts = null;

        if ($data->getType() == UserType::$EMPLOYEE) {
            $user_contracts = $data->getContracts();
        } else {
            $user_contracts = $contractRepository->findBy([
                "client" => $data->getClient()
            ]);
        }

        if ($admin) {
            $user_contracts = $contractRepository->findAll();
        }

        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if ($data->getType() == UserType::$SOCIETY) {
                if ($contract->getStatus() == ContractStatus::$TIME_SENT || $contract->getStatus() == ContractStatus::$WORKED) {
                    $validate_data[] = $contract;
                }
            } else {
                if ($contract->getStatus() == ContractStatus::$TIME_SENT) {
                    $validate_data[] = $contract;
                }
            }
        }

        return ContractConfig::listing($validate_data, $data, $stringAsArray);
    }

    public static function adminTimeSheetTodo (
        ContractRepository $contractRepository,
    ) {
        $user_contracts = null;
        $user_contracts = $contractRepository->findAll();

        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if ($contract->getStatus() == ContractStatus::$WORKED) {
                $validate_data[] = $contract;
            }
        }

        return ContractConfig::listing($validate_data, null, true);
    }

    public static function userApprouvedTimeSheets (
        User $data,
        ContractRepository $contractRepository,
        bool $stringAsArray = false,
        bool $admin = false
    ) {
        $user_contracts = null;
        $validate_data = [];

        if ($data->getType() == UserType::$EMPLOYEE) {
            $user_contracts = $data->getContracts();
        } else {
            $user_contracts = $contractRepository->findBy([
                "client" => $data->getClient()
            ]);
        }

        if ($admin) {
            $user_contracts = $contractRepository->findAll();
        }

        foreach ($user_contracts as $contract) {
            if ($contract->getStatus() == ContractStatus::$TIME_APPROUVED) {
                $validate_data[] = $contract;
            }
        }

        return ContractConfig::listing($validate_data, $data, $stringAsArray);
    }

    public static function contractSendTime (
        Contract $data,
        float $total_time,
        float $total_rate,
        string $t_start,
        string $t_end,
        ?float $t_pause,
        User $user,
        EntityManagerInterface $em,
        Client $twilio
    ) {
        $time_sheet = $data->getTimeSheet();

        if ($time_sheet == null) {
            $time_sheet = new TimeSheet();
            $time_sheet->setUser($data->getEmployee());
        }

        $time_sheet->setContract($data);
        $time_sheet->setStartdate(new DateTime($t_start));
        $time_sheet->setEnddate(new DateTime($t_end));
        if ($data->getPause() != null) {
            $time_sheet->setPause((int)$t_pause);
        }
        $time_sheet->setTotalTimes($total_time);
        $time_sheet->setAmount($total_rate);
        $time_sheet->setStatus(TimeSheetStatus::$WAITING);

        if ($user->getType() == UserType::$EMPLOYEE && !in_array('ROLE_ADMIN', $user->getRoles())) {
            $data->setStatus(ContractStatus::$TIME_SENT);
        } else {
            $data->setStatus(ContractStatus::$TIME_APPROUVED);
            $time_sheet->setStatus(TimeSheetStatus::$APPROUVED);

            $employee = $data->getEmployee();
            $employee->setCompleted(
                $employee->getCompleted() == null ? 1: $employee->getCompleted() + 1
            );
        }

        $em->persist($time_sheet);
        $em->persist($data);
        $em->persist($employee);
        $em->flush();

        // Send sms
        $to = ContractConfig::format_number($data->getEmployee()->getPhone());

        $message = "Vous avez une nouvelle feuille de temps à valider pour le shift du "
            . ContractConfig::full_date($data->getStartdate(), true)
            . ", au poste de " . $data->getJob()->getTitle()
            . ", effectué par " . $data->getEmployee()->getFirstname() . " " . $data->getEmployee()->getLastname() .  ".";
        NotificationConfig::sendSms($twilio, $to, $message);
    }

    private static function cancelPrev(Contract $contract, EntityManagerInterface $em): bool
    {
        $next = $contract->getNext();

        if (
            $next != null
            && $next->getStatus() == $contract->getStatus()
            && $next->getEmployee() == $contract->getEmployee()
        ) {
            $contract->setEmployee(null);
            $contract->publish();

            $applies = $em->getRepository(Apply::class)->findBy([
                "contract" => $contract,
            ]);

            foreach ($applies as $apply) {
                $em->remove($apply);
                $em->flush();
            }

            $em->persist($contract);
            $em->flush();

            return self::cancelPrev($next, $em);
        }

        $contract->setEmployee(null);
        $contract->publish();

        $applies = $em->getRepository(Apply::class)->findBy([
            "contract" => $contract,
        ]);

        foreach ($applies as $apply) {
            $em->remove($apply);
            $em->flush();
        }

        $em->persist($contract);
        $em->flush();

        return true;
    }

    private static function applyNext(Contract $contract, User $user, Client $twilio, EntityManagerInterface $em, Apply $apply, $i = 0, $limit = null)
    {

        $contract->setAppliesCount(null);
        $contract->setStatus(ContractStatus::$APPROUVED);
        $contract->setEmployee($user);

        $em->persist($contract);
        $em->flush();
        if ($contract->getNext() == null) {
            if ($i == 0)
                $message = "Le shift prévu chez "
                    . $contract->getClient()->getName()
                    . " pour le " .
                    ContractConfig::full_date($contract->getStartdate(), true) . " De "
                    . $contract->getStartdate()->format("H:i") . " à " . $contract->getEnddate()->format("H:i")
                    . " vous a été confirmé. SVP avisez d'un retard si vous pensez pas arriver à l'heure.";
            else
                $message = "Les shifts prévus chez "
                    . $contract->getClient()->getName()
                    . " du " .
                    ContractConfig::full_date($apply->getStartdate(), true)
                    . " au " .
                    ContractConfig::full_date($apply->getEnddate(), true) . " De "
                    . $contract->getStartdate()->format("H:i") . " à " . $contract->getEnddate()->format("H:i")
                    . " vous ont été confirmés. SVP avisez d'un retard si vous pensez pas arriver à l'heure.";

            // Send sms
            $to = ContractConfig::format_number($user->getPhone());

            NotificationConfig::sendSms($twilio, $to, $message);
            return true;
        }

        if ($limit != null && $limit == $i)
            return true;

        return self::applyNext($contract->getNext(), $user, $twilio, $em, $apply, $i + 1);
    }

    public static function contractApprouved(
        EntityClient $data,
        ContractRepository $contractRepository,
        bool $stringAsArray = false
    ) {
        $user_contracts = $contractRepository->findBy([
            'client' => $data,
        ], [
            'startdate' => 'ASC'
        ]);

        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if (
                $contract->getStatus() == ContractStatus::$APPROUVED
                && ($contract->getParent() == null || $contract->getParent()->getEmployee()->getId() != $contract->getEmployee()->getId())
            ) {
                $validate_data[] = $contract;
            }
        }

        return ContractConfig::listing($validate_data, null, $stringAsArray);
    }

    public static function adminApprouved(
        ContractRepository $contractRepository,
        bool $stringAsArray = true
    ) {
        $user_contracts = $contractRepository->findAll();

        $validate_data = [];

        foreach ($user_contracts as $contract) {
            if (
                $contract->getStatus() == ContractStatus::$APPROUVED
                && ($contract->getParent() == null || $contract->getParent()->getEmployee()->getId() != $contract->getEmployee()->getId())
            ) {
                $validate_data[] = $contract;
            }
        }

        return ContractConfig::listing($validate_data, null, $stringAsArray);
    }

    private static function itemExists(array $array, Contract $contract): bool
    {
        $exists = false;
        foreach ($array as $contractItem) {
            if ($contractItem->getId() == $contract->getId()) {
                $exists = true;
            }
        }
        return $exists;
    }


    // QD-2001212-0001
    public static function generate_code(Contract $contract)
    {
        $code = "QD-";
        $code .= date_format($contract->getStartdate(), "mdy") . '-';
        $id = $contract->getId();
        $code .= $id;

        return $code;
    }

    private static function addNexts(Contract $contract, int $length, $entityManager, String $endtime): bool
    {
        if ($length != 0) {
            $next = clone $contract;

            $next->setCode("QD");

            $startdate = DateTime::createFromFormat(
                "d-m-Y H:i:s",
                $contract
                    ->getStartdate()
                    ->format("d-m-Y H:i:s")
            )->add(DateInterval::createFromDateString('+1 day'));

            $enddate = DateTime::createFromFormat(
                "d-m-Y H:i:s",
                $contract
                    ->getStartdate()
                    ->format("d-m-Y H:i:s")
            )->add(DateInterval::createFromDateString('+1 day'))->setTime(substr($endtime, 0, 2), substr($endtime, 3, 2), substr($endtime, 6, 2));

            $next
                ->setStartdate($startdate)
                ->setEnddate($enddate);

            $entityManager->persist($next);
            $entityManager->flush();

            $next->setCode(self::generate_code($next));

            $entityManager->persist($next);

            $contract->setNext($next);

            $entityManager->persist($contract);
            $entityManager->flush();

            self::addNexts($next, --$length, $entityManager, $endtime);
        }

        return true;
    }

    public static function clientRegister (
        AppEntityClient $data,
        TownRepository $townRepository, 
        int $town_id, 
        User $user,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        String $devicetoken,
        DeviceRepository $deviceRepository
    ) {
        $town = $townRepository->find((int) $town_id);

        if ($devicetoken != null) {
            // save user device token
            $existDevice
                = $deviceRepository->findOneBy(["token" => $devicetoken]);

            if ($existDevice != null) {
                $existDevice->setUser($user);
                $em->persist($existDevice);
                $em->flush();
            } else {
                $device = (new Device())
                    ->setToken($devicetoken)
                    ->setUser($user);

                $em->persist($device);
                $em->flush();
            }
        }

        $user
            ->setClient($data)
            ->setRegisterCompleted(true)
            ->setPhone($data->getPhone());

        $data
            ->setTown($town)
            ->setValidated(false);

        $user_contract = (new UserContract())
            ->settype($user->getType())
            ->setDate(new DateTime())
            ->setUpdatedAt(new DateTimeImmutable());

        $em->persist($user_contract);
        $user->setUsercontract($user_contract);
        $em->persist($user);
        $em->persist($data);

        $em->flush();

        return $user;
    }

    public static function resetPassword(
        String $email, 
        PasswordResetRepository $passwordResetRepository,
        PasswordReset $data,
        EntityManagerInterface $em,
        MailerInterface $mailer,
        UserRepository $userRepository
    ) {

        // check if account exists
        $user = $userRepository->findOneBy(['email' => $email]);

        if ($user == null) {
            return [
                "success" => false,
                "message" => "Aucun compte ne correspond à l'adresse e-mail entré"
            ];
        }

        $username = $user->getFirstname() . " " . $user->getLastname();

        // generate OTP
        $otp = self::generateOtp(5);

        $template = 'email/reset.html.twig';


        $template_datas = [
            'username' =>  $username,
            'code' => $otp
        ];

        // send mail
        MailerController::sendEmail(
            $mailer,
            $email,
            $template,
            $template_datas,
            "Réinitialisation du mot de passe"
        );

        // delete olds
        $olds = $passwordResetRepository->findBy(["email" => $email]);

        foreach ($olds as $old) {
            $em->remove($old);
            $em->flush();
        }

        // save the reset password request
        $data->setCode($otp);

        $em->persist($data);
        $em->flush();

        return [
            "success" => true,
            "message" => "Un mail de récupération a été envoyé à l'adresse " . $email,
        ];
    }

    public static function generateOtp($length)
    {
        $digits = [];
        $i = 0;
        do {
            $digit = rand(1, 9);
            if (!in_array($digit, $digits)) {
                $digits[] = $digit;
                $i++;
            }
        } while ($i < $length);

        return implode("", $digits);
    }

    public static function codeSpaced($code)
    {
        $code_arr = [];
        foreach (str_split($code) as $char) {
            $code_arr[] = $char;
        }
        return implode(" ", $code_arr);
    }

    public static function resetPasswordConfirm(
        ?PasswordReset $reset,
        String $code,
        UserRepository $userRepository,
        String $email,
    ) {
        if ($reset == null) {
            return [
                "success" => false,
                "message" => "Code invalide"
            ];
        }

        if ($code != $reset->getCode()) {
            return [
                "success" => false,
                "message" => "Code invalide"
            ];
        }

        // check expired
        $now = new DateTimeImmutable();
        $exire = $reset->getCreatedAt()->modify("+60 second");

        if ($now > $exire) {
            return [
                "success" => false,
                "message" => "Le code entré est déjà expiré, veuillez réessayer"
            ];
        }

        $user = $userRepository->findOneBy(["email" => $email]);

        return [
            "success" => true,
            "message" => "Succès! Vous pouvez modifier votre mot de passe",
            "user_id" => $user->getId()
        ];
    }
}
