<?php

namespace App\Configuration;

class TimeSheetStatus
{
    public static $WAITING = "time_waiting";
    public static $APPROUVED = "time_validated";
    public static $CONFLIT = "time_refused";
}
