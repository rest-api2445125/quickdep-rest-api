<?php

namespace App\Repository;

use App\Entity\UnHonoredContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UnHonoredContract>
 *
 * @method UnHonoredContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnHonoredContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnHonoredContract[]    findAll()
 * @method UnHonoredContract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnHonoredContractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnHonoredContract::class);
    }

    public function add(UnHonoredContract $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UnHonoredContract $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return UnHonoredContract[] Returns an array of UnHonoredContract objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UnHonoredContract
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
