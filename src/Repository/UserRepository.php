<?php

namespace App\Repository;

use App\Configuration\DocumentConfig;
use App\Configuration\DocumentStatus;
use App\Configuration\UserType;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function add(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    /**
     * Create a new user
     * @param $data
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createNewUser($data, $manager, $encoder)
    {
        $user = new User();
        $user->setEmail($data['email'])
            ->setPassword($encoder->hashPassword($user, $data['password']))
            ->setRegisterCompleted(true)
            ->setEmailVerified(false)
            ->setSigned(false)
            ->setEmailVerified(false)
            ->setPhoneverified(false)
            ->setNew(true)
            ->setCompleted(0)
            ->setCanceled(0)
            ->setNotworked(0)
            ->setType($data['type'])
            ->setDocumentsStatus(DocumentStatus::$NOT_SENT);

        $manager->persist($user);
        $manager->flush();

        if ($data['type'] == UserType::$EMPLOYEE) {
            DocumentConfig::create_all_documents($user, $manager);
        }

        return $user;
    }
}
