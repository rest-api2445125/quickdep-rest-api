<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/client/detail.html.twig */
class __TwigTemplate_4f100f36ea8aede8423074ca0e15eb03 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/client/detail.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/client/detail.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Detail de l'entréprise";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div style=\"display:flex; align-items:center;justify-content:space-between\">
        <a href=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", [], "any", false, false, false, 7), "headers", [], "any", false, false, false, 7), "get", [0 => "referer"], "method", false, false, false, 7), "html", null, true);
        echo "\" class=\"contract-card-link\">
            <div class=\"card\" style=\"display:inline-block; margin:0\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
                </div>
            </div>
        </a>
        ";
        // line 14
        if ((twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 14, $this->source); })()), "validated", [], "any", false, false, false, 14) != true)) {
            // line 15
            echo "        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_validate_client", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15)]), "html", null, true);
            echo "\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i> Valider</button>
        </form>
        ";
        } else {
            // line 19
            echo "        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_unvalidate_client", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 19, $this->source); })()), "id", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i> Bloquer</button>
        </form>
        ";
        }
        // line 23
        echo "    </div>
    <br>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 style=\"display:inline; margin-right:10px\" class=\"card-title mb-0\">Details de l'entréprise ";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 27, $this->source); })()), "name", [], "any", false, false, false, 27), "html", null, true);
        echo "</h5>
        ";
        // line 28
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 28, $this->source); })()), "emailVerified", [], "any", false, false, false, 28) != true)) {
            // line 29
            echo "        <span class=\"badge bg-danger\">E-mail non vérifié</span>
        ";
        }
        // line 31
        echo "        
        ";
        // line 32
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 32, $this->source); })()), "phoneVerified", [], "any", false, false, false, 32) != true)) {
            // line 33
            echo "        <span class=\"badge bg-danger\">Phone non vérifié</span>
        ";
        }
        // line 35
        echo "        
        ";
        // line 36
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 36, $this->source); })()), "signed", [], "any", false, false, false, 36) != true)) {
            // line 37
            echo "        <span class=\"badge bg-danger\">Contrat non signé</span>
        ";
        }
        // line 39
        echo "        
        ";
        // line 40
        if (((((twig_get_attribute($this->env, $this->source,         // line 41
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 41, $this->source); })()), "signed", [], "any", false, false, false, 41) == true) && (twig_get_attribute($this->env, $this->source,         // line 42
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "emailVerified", [], "any", false, false, false, 42) == true)) && (twig_get_attribute($this->env, $this->source,         // line 43
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 43, $this->source); })()), "phoneVerified", [], "any", false, false, false, 43) == true)) && (twig_get_attribute($this->env, $this->source,         // line 44
(isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 44, $this->source); })()), "validated", [], "any", false, false, false, 44) != true))) {
            // line 46
            echo "        <span class=\"badge bg-warning\">En attente de validation</span>
        ";
        }
        // line 48
        echo "        
        ";
        // line 49
        if (((((twig_get_attribute($this->env, $this->source,         // line 50
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 50, $this->source); })()), "signed", [], "any", false, false, false, 50) == true) && (twig_get_attribute($this->env, $this->source,         // line 51
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "emailVerified", [], "any", false, false, false, 51) == true)) && (twig_get_attribute($this->env, $this->source,         // line 52
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 52, $this->source); })()), "phoneVerified", [], "any", false, false, false, 52) == true)) && (twig_get_attribute($this->env, $this->source,         // line 53
(isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 53, $this->source); })()), "validated", [], "any", false, false, false, 53) == true))) {
            // line 55
            echo "        <span class=\"badge bg-success\">Entréprise validée</span>
        ";
        }
        // line 57
        echo "        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Informations de l'entréprise</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 66, $this->source); })()), "imagePath", [], "any", false, false, false, 66) == null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/img/logo.png")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 66, $this->source); })()), "imagePath", [], "any", false, false, false, 66))))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 66, $this->source); })()), "name", [], "any", false, false, false, 66), "html", null, true);
        echo "\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\">
                    <h5 class=\"card-title mb-0\">";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 67, $this->source); })()), "name", [], "any", false, false, false, 67), "html", null, true);
        echo "</h5>
                    <div class=\"text-muted mb-2\">";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 68, $this->source); })()), "owner", [], "any", false, false, false, 68), "html", null, true);
        echo "</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier le logo</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos de l'entréprise</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 82, $this->source); })()), "owner", [], "any", false, false, false, 82), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>";
        // line 83
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 83, $this->source); })()), "email", [], "any", false, false, false, 83), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
        // line 84
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 84, $this->source); })()), "zipcode", [], "any", false, false, false, 84) . " - ") . twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 84, $this->source); })()), "address", [], "any", false, false, false, 84)), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>";
        // line 85
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 85, $this->source); })()), "phone", [], "any", false, false, false, 85), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map\"></i>";
        // line 86
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 86, $this->source); })()), "town", [], "any", false, false, false, 86), "name", [], "any", false, false, false, 86), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Mode de paiement</h5>
                    <ul class=\"list-unstyled mb-0\">
                    ";
        // line 93
        if ((twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 93, $this->source); })()), "payment", [], "any", false, false, false, 93) == 1)) {
            // line 94
            echo "                        <li class=\"mb-1\"><b>Paiement par chèque</b></li>
                    ";
        }
        // line 96
        echo "                    ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 96, $this->source); })()), "payment", [], "any", false, false, false, 96) == 2)) {
            // line 97
            echo "                        <li class=\"mb-1\"><b>Paiement direct</b></li>
                    ";
        }
        // line 99
        echo "                        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_paiement", ["profile" => 1]);
        echo "\" class=\"btn btn-primary\">Modifier</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Adresse de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
        // line 106
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 106, $this->source); })()), "facturezipcode", [], "any", false, false, false, 106) . " - ") . twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 106, $this->source); })()), "factureaddress", [], "any", false, false, false, 106)), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                ";
        // line 109
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 109, $this->source); })()), "signed", [], "any", false, false, false, 109) && (twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 109, $this->source); })()), "usercontract", [], "any", false, false, false, 109) != null))) {
            // line 110
            echo "                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <a href=\"";
            // line 114
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("contracts/society/pdf/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 114, $this->source); })()), "usercontract", [], "any", false, false, false, 114), "id", [], "any", false, false, false, 114))), "html", null, true);
            echo ".pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger votre contrat</a>
                    </ul>
                </div>
                ";
        }
        // line 118
        echo "            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 128
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 128, $this->source); })()), 'form_start');
        echo "
                        ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 129, $this->source); })()), "owner", [], "any", false, false, false, 129), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Responsable"]);
        // line 131
        echo "
                        ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 132, $this->source); })()), "owner", [], "any", false, false, false, 132), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>

                        ";
        // line 136
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 136, $this->source); })()), "address", [], "any", false, false, false, 136), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de l'entreprise"]);
        // line 138
        echo "
                        ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 139, $this->source); })()), "address", [], "any", false, false, false, 139), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 143
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 143, $this->source); })()), "zipcode", [], "any", false, false, false, 143), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal"]);
        // line 145
        echo "
                        ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 146, $this->source); })()), "zipcode", [], "any", false, false, false, 146), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 150, $this->source); })()), "town", [], "any", false, false, false, 150), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Ville"]);
        // line 152
        echo "
                        ";
        // line 153
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 153, $this->source); })()), "town", [], "any", false, false, false, 153), 'widget', ["attr" => ["class" => "form-control form-select"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 157
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 157, $this->source); })()), "submit", [], "any", false, false, false, 157), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')"]]);
        echo "

                    ";
        // line 159
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 159, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier l'adresse de facturation</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 167
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 167, $this->source); })()), 'form_start');
        echo "
                       
                        ";
        // line 169
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 169, $this->source); })()), "factureaddress", [], "any", false, false, false, 169), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de facturation"]);
        // line 171
        echo "
                        ";
        // line 172
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 172, $this->source); })()), "factureaddress", [], "any", false, false, false, 172), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 176
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 176, $this->source); })()), "facturezipcode", [], "any", false, false, false, 176), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal"]);
        // line 178
        echo "
                        ";
        // line 179
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 179, $this->source); })()), "facturezipcode", [], "any", false, false, false, 179), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        ";
        // line 182
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 182, $this->source); })()), "submit", [], "any", false, false, false, 182), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')"]]);
        echo "

                    ";
        // line 184
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 184, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
            ";
        // line 212
        echo "        </div>
    </div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/client/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  392 => 212,  386 => 184,  381 => 182,  375 => 179,  372 => 178,  370 => 176,  363 => 172,  360 => 171,  358 => 169,  353 => 167,  342 => 159,  337 => 157,  330 => 153,  327 => 152,  325 => 150,  318 => 146,  315 => 145,  313 => 143,  306 => 139,  303 => 138,  301 => 136,  294 => 132,  291 => 131,  289 => 129,  285 => 128,  273 => 118,  266 => 114,  260 => 110,  258 => 109,  252 => 106,  241 => 99,  237 => 97,  234 => 96,  230 => 94,  228 => 93,  218 => 86,  214 => 85,  210 => 84,  206 => 83,  202 => 82,  185 => 68,  181 => 67,  175 => 66,  164 => 57,  160 => 55,  158 => 53,  157 => 52,  156 => 51,  155 => 50,  154 => 49,  151 => 48,  147 => 46,  145 => 44,  144 => 43,  143 => 42,  142 => 41,  141 => 40,  138 => 39,  134 => 37,  132 => 36,  129 => 35,  125 => 33,  123 => 32,  120 => 31,  116 => 29,  114 => 28,  110 => 27,  104 => 23,  96 => 19,  88 => 15,  86 => 14,  76 => 7,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Detail de l'entréprise{% endblock %}

{% block body %}
    <div style=\"display:flex; align-items:center;justify-content:space-between\">
        <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
            <div class=\"card\" style=\"display:inline-block; margin:0\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
                </div>
            </div>
        </a>
        {% if client.validated != true %}
        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"{{ path('app_user_validate_client', {'id': client.id}) }}\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i> Valider</button>
        </form>
        {% else %}
        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"{{ path('app_user_unvalidate_client', {'id': client.id}) }}\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i> Bloquer</button>
        </form>
        {% endif %}
    </div>
    <br>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 style=\"display:inline; margin-right:10px\" class=\"card-title mb-0\">Details de l'entréprise {{ client.name }}</h5>
        {% if user.emailVerified != true %}
        <span class=\"badge bg-danger\">E-mail non vérifié</span>
        {% endif %}
        
        {% if user.phoneVerified != true %}
        <span class=\"badge bg-danger\">Phone non vérifié</span>
        {% endif %}
        
        {% if user.signed != true %}
        <span class=\"badge bg-danger\">Contrat non signé</span>
        {% endif %}
        
        {% if 
            user.signed == true
            and user.emailVerified == true 
            and user.phoneVerified == true
            and client.validated != true
        %}
        <span class=\"badge bg-warning\">En attente de validation</span>
        {% endif %}
        
        {% if 
            user.signed == true
            and user.emailVerified == true 
            and user.phoneVerified == true
            and client.validated == true
        %}
        <span class=\"badge bg-success\">Entréprise validée</span>
        {% endif %}
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Informations de l'entréprise</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"{{ user.imagePath == null ? asset('clientweb/img/logo.png') : asset('users/images/'~user.imagePath) }}\" alt=\"{{ client.name }}\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\">
                    <h5 class=\"card-title mb-0\">{{ client.name }}</h5>
                    <div class=\"text-muted mb-2\">{{ client.owner }}</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier le logo</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos de l'entréprise</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>{{ client.owner }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>{{ user.email }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ client.zipcode ~ ' - ' ~ client.address }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>{{ client.phone }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map\"></i>{{ client.town.name }}</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Mode de paiement</h5>
                    <ul class=\"list-unstyled mb-0\">
                    {% if client.payment == 1 %}
                        <li class=\"mb-1\"><b>Paiement par chèque</b></li>
                    {% endif %}
                    {% if client.payment == 2 %}
                        <li class=\"mb-1\"><b>Paiement direct</b></li>
                    {% endif %}
                        <a href=\"{{ path('app_client_web_auth_paiement', {'profile': 1}) }}\" class=\"btn btn-primary\">Modifier</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Adresse de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ client.facturezipcode ~ ' - ' ~ client.factureaddress }}</li>
                    </ul>
                </div>
                {% if user.signed and user.usercontract != null %}
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <a href=\"{{ asset(\"contracts/society/pdf/\"~user.usercontract.id) }}.pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger votre contrat</a>
                    </ul>
                </div>
                {% endif %}
            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(infoForm) }}
                        {{ form_label(infoForm.owner, 'Responsable', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.owner, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>

                        {{ form_label(infoForm.address, 'Adresse de l\\'entreprise', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.address, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(infoForm.zipcode, 'Code postal', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.zipcode, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(infoForm.town, 'Ville', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.town, { 'attr': {'class': 'form-control form-select'} }) }}
                        
                        <br>
                        
                        {{ form_widget(infoForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(infoForm) }}
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier l'adresse de facturation</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(factureForm) }}
                       
                        {{ form_label(factureForm.factureaddress, 'Adresse de facturation', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(factureForm.factureaddress, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(factureForm.facturezipcode, 'Code postal', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(factureForm.facturezipcode, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        {{ form_widget(factureForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(factureForm) }}
                </div>
            </div>
            {# <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier le mot de passe</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(passForm) }}
                       
                        {{ form_label(passForm.password, 'Mot de passe', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(passForm.password, { 'attr': {'class': 'form-control', 'placeholder': '*******'} }) }}
                        
                        <br>
                        
                        {{ form_label(passForm.password_c, 'Confirmer le mot de passe', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(passForm.password_c, { 'attr': {'class': 'form-control', 'placeholder': '*******'} }) }}
                        
                        <br>
                        {{ form_widget(passForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(passForm) }}
                </div>
            </div> #}
        </div>
    </div>
{% endblock %}
", "admin/client/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/client/detail.html.twig");
    }
}
