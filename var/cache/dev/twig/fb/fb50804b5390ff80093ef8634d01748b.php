<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/invoice/employee.html.twig */
class __TwigTemplate_80a9920d29cd0ff10734ad00124bfea7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/invoice/employee.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/invoice/employee.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Factures Travailleurs";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "
<style></style>

<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Factures Travailleurs</h1>
</div>
<hr>
<br>

<div class=\"row\">
    ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 40, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 40));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 41
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 43
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "    
    
    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Factures Payées</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Factures non payées</span>
        </li>
    </ul>
    <div class=\"container__inner\">
        <div class=\"row\">
            ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["all"]) || array_key_exists("all", $context) ? $context["all"] : (function () { throw new RuntimeError('Variable "all" does not exist.', 66, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
            // line 67
            echo "                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["invoice"], "id", [], "any", false, false, false, 68)]), "html", null, true);
            echo "\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">";
            // line 72
            echo twig_escape_filter($this->env, ("Facture - " . twig_get_attribute($this->env, $this->source, $context["invoice"], "code", [], "any", false, false, false, 72)), "html", null, true);
            echo "</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
            // line 73
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "date", [], "any", false, false, false, 73), "d/m/Y"), "html", null, true);
            echo "</p>
                                    <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">";
            // line 74
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 74), "firstname", [], "any", false, false, false, 74) . " ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 74), "lastname", [], "any", false, false, false, 74)), "html", null, true);
            echo "</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
            // line 81
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["invoice"], "total", [], "any", false, false, false, 81) . "\$"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        ";
            // line 84
            if (twig_get_attribute($this->env, $this->source, $context["invoice"], "paid", [], "any", false, false, false, 84)) {
                // line 85
                echo "                                        <div class=\"status-box approuved small\">Payé</div> 
                                        ";
            } else {
                // line 86
                echo "                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        ";
            }
            // line 89
            echo "                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            ";
        // line 101
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["paid"]) || array_key_exists("paid", $context) ? $context["paid"] : (function () { throw new RuntimeError('Variable "paid" does not exist.', 101, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
            // line 102
            echo "                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"";
            // line 103
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["invoice"], "id", [], "any", false, false, false, 103)]), "html", null, true);
            echo "\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">";
            // line 107
            echo twig_escape_filter($this->env, ("Facture - " . twig_get_attribute($this->env, $this->source, $context["invoice"], "code", [], "any", false, false, false, 107)), "html", null, true);
            echo "</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
            // line 108
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "date", [], "any", false, false, false, 108), "d/m/Y"), "html", null, true);
            echo "</p>
                                    <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">";
            // line 109
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 109), "firstname", [], "any", false, false, false, 109) . " ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 109), "lastname", [], "any", false, false, false, 109)), "html", null, true);
            echo "</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
            // line 116
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["invoice"], "total", [], "any", false, false, false, 116) . "\$"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        ";
            // line 119
            if (twig_get_attribute($this->env, $this->source, $context["invoice"], "paid", [], "any", false, false, false, 119)) {
                // line 120
                echo "                                        <div class=\"status-box approuved small\">Payé</div> 
                                        ";
            } else {
                // line 121
                echo "                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        ";
            }
            // line 124
            echo "                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            ";
        // line 136
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["wait"]) || array_key_exists("wait", $context) ? $context["wait"] : (function () { throw new RuntimeError('Variable "wait" does not exist.', 136, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
            // line 137
            echo "            <div class=\"col-12 col-md-6 col-lg-3\">
                <a href=\"";
            // line 138
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["invoice"], "id", [], "any", false, false, false, 138)]), "html", null, true);
            echo "\"  class='contract-card-link'> 
                    <div class=\"card contract-card\">
                        <div class=\"card-header\" style=\"display:flex; align-items:center\">
                            <div style=\"display:inline-block;margin-left:10px\">
                                <h5 class=\"card-title mb-0\" style=\"\">";
            // line 142
            echo twig_escape_filter($this->env, ("Facture - " . twig_get_attribute($this->env, $this->source, $context["invoice"], "code", [], "any", false, false, false, 142)), "html", null, true);
            echo "</h5>
                                <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
            // line 143
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "date", [], "any", false, false, false, 143), "d/m/Y"), "html", null, true);
            echo "</p>
                                <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">";
            // line 144
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 144), "firstname", [], "any", false, false, false, 144) . " ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["invoice"], "user", [], "any", false, false, false, 144), "lastname", [], "any", false, false, false, 144)), "html", null, true);
            echo "</p>
                            </div>
                        </div>
                        <div class=\"card-body\" style=\"padding-top:0\">
                            <hr style=\"opacity:0.1; margin-top:0\">
                            <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                <div>
                                    <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
            // line 151
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["invoice"], "total", [], "any", false, false, false, 151) . "\$"), "html", null, true);
            echo "</h4>
                                </div>
                                <div style=\"display:flex\">
                                    ";
            // line 154
            if (twig_get_attribute($this->env, $this->source, $context["invoice"], "paid", [], "any", false, false, false, 154)) {
                // line 155
                echo "                                    <div class=\"status-box approuved small\">Payé</div> 
                                    ";
            } else {
                // line 156
                echo "                                           
                                    <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                    ";
            }
            // line 159
            echo "                                </div>
                            </div>
                            
                        </div>
                    </div>
                </a>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/invoice/employee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  332 => 167,  319 => 159,  314 => 156,  310 => 155,  308 => 154,  302 => 151,  292 => 144,  288 => 143,  284 => 142,  277 => 138,  274 => 137,  270 => 136,  264 => 132,  251 => 124,  246 => 121,  242 => 120,  240 => 119,  234 => 116,  224 => 109,  220 => 108,  216 => 107,  209 => 103,  206 => 102,  202 => 101,  196 => 97,  183 => 89,  178 => 86,  174 => 85,  172 => 84,  166 => 81,  156 => 74,  152 => 73,  148 => 72,  141 => 68,  138 => 67,  134 => 66,  117 => 51,  103 => 43,  99 => 41,  95 => 40,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Factures Travailleurs{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}

<style></style>

<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Factures Travailleurs</h1>
</div>
<hr>
<br>

<div class=\"row\">
    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
    
    
    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Factures Payées</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Factures non payées</span>
        </li>
    </ul>
    <div class=\"container__inner\">
        <div class=\"row\">
            {% for invoice in all %}
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"{{ path('app_client_web_invoice_detail', {'id': invoice.id})}}\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">{{ \"Facture - \" ~ invoice.code }}</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ invoice.date | date('d/m/Y') }}</p>
                                    <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">{{ invoice.user.firstname ~ ' ' ~ invoice.user.lastname }}</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ invoice.total ~ \"\$\" }}</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        {% if invoice.paid %}
                                        <div class=\"status-box approuved small\">Payé</div> 
                                        {% else %}                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        {% endif %}
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            {% endfor %}
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            {% for invoice in paid %}
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"{{ path('app_client_web_invoice_detail', {'id': invoice.id})}}\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">{{ \"Facture - \" ~ invoice.code }}</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ invoice.date | date('d/m/Y') }}</p>
                                    <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">{{ invoice.user.firstname ~ ' ' ~ invoice.user.lastname }}</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ invoice.total ~ \"\$\" }}</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        {% if invoice.paid %}
                                        <div class=\"status-box approuved small\">Payé</div> 
                                        {% else %}                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        {% endif %}
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            {% endfor %}
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            {% for invoice in wait %}
            <div class=\"col-12 col-md-6 col-lg-3\">
                <a href=\"{{ path('app_client_web_invoice_detail', {'id': invoice.id})}}\"  class='contract-card-link'> 
                    <div class=\"card contract-card\">
                        <div class=\"card-header\" style=\"display:flex; align-items:center\">
                            <div style=\"display:inline-block;margin-left:10px\">
                                <h5 class=\"card-title mb-0\" style=\"\">{{ \"Facture - \" ~ invoice.code }}</h5>
                                <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ invoice.date | date('d/m/Y') }}</p>
                                <p class=\"mb-0\" style=\"font-size:15px; font-weight:bold\">{{ invoice.user.firstname ~ ' ' ~ invoice.user.lastname }}</p>
                            </div>
                        </div>
                        <div class=\"card-body\" style=\"padding-top:0\">
                            <hr style=\"opacity:0.1; margin-top:0\">
                            <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                <div>
                                    <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ invoice.total ~ \"\$\" }}</h4>
                                </div>
                                <div style=\"display:flex\">
                                    {% if invoice.paid %}
                                    <div class=\"status-box approuved small\">Payé</div> 
                                    {% else %}                                           
                                    <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                    {% endif %}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </a>
            </div>
        {% endfor %}
        </div>
    </div>
</div>
{% endblock %}
", "admin/invoice/employee.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/invoice/employee.html.twig");
    }
}
