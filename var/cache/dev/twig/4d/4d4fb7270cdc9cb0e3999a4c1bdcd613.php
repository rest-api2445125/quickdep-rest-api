<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/home/index.html.twig */
class __TwigTemplate_cc167d84e5f533d75f271d0c07b9680c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "client_web/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/home/index.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("client_web/base.html.twig", "client_web/home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Contrats publiés";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Contrats Publiés</h1>
";
        // line 32
        if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "session", [], "any", false, false, false, 32), "get", [0 => "user"], "method", false, false, false, 32), "client", [], "any", false, false, false, 32), "validated", [], "any", false, false, false, 32) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "session", [], "any", false, false, false, 32), "get", [0 => "user"], "method", false, false, false, 32), "signed", [], "any", false, false, false, 32) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "session", [], "any", false, false, false, 32), "get", [0 => "user"], "method", false, false, false, 32), "phoneVerified", [], "any", false, false, false, 32) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "session", [], "any", false, false, false, 32), "get", [0 => "user"], "method", false, false, false, 32), "emailVerified", [], "any", false, false, false, 32) == true))) {
            // line 33
            echo "    <a href=\"";
            echo ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 33, $this->source); })()), "session", [], "any", false, false, false, 33), "get", [0 => "user"], "method", false, false, false, 33), "client", [], "any", false, false, false, 33), "validated", [], "any", false, false, false, 33) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 33, $this->source); })()), "session", [], "any", false, false, false, 33), "get", [0 => "user"], "method", false, false, false, 33), "signed", [], "any", false, false, false, 33) == true))) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_new_shift")) : ("#"));
            echo "\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i> Publier un nouveau contrat</a>
";
        }
        // line 35
        echo "</div>
<hr>
<br>
<div class=\"row\">
    <div class=\"col-12\">

    <div class=\"row\">
        ";
        // line 42
        if ((twig_length_filter($this->env, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 42, $this->source); })())) == 1)) {
            // line 43
            echo "            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 43, $this->source); })()), 0, [], "array", false, false, false, 43), "html", null, true);
            echo "</h3>
        ";
        } else {
            // line 45
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 45, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["contract"]) {
                // line 46
                echo "                ";
                if (twig_test_iterable($context["contract"])) {
                    // line 47
                    echo "                    ";
                    // line 48
                    $context["month"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], 0, [], "array", false, false, false, 48), " ");
                    // line 50
                    echo "                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["months"]) || array_key_exists("months", $context) ? $context["months"] : (function () { throw new RuntimeError('Variable "months" does not exist.', 50, $this->source); })()), (twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 50, $this->source); })()), 0, [], "array", false, false, false, 50) - 1), [], "array", false, false, false, 50) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 50, $this->source); })()), 1, [], "array", false, false, false, 50)), "html", null, true);
                    echo "</h3>
                    <br>
                    <br>
                ";
                } else {
                    // line 54
                    echo "                    ";
                    echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("Contract", ["contractData" => $context["contract"], "user" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 54, $this->source); })()), "session", [], "any", false, false, false, 54), "get", [0 => "user"], "method", false, false, false, 54)]);
                    echo "
                ";
                }
                // line 56
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contract'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "        ";
        }
        // line 58
        echo "            
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 58,  144 => 57,  138 => 56,  132 => 54,  124 => 50,  122 => 48,  120 => 47,  117 => 46,  112 => 45,  106 => 43,  104 => 42,  95 => 35,  89 => 33,  87 => 32,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'client_web/base.html.twig' %}

{% block title %}Contrats publiés{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Contrats Publiés</h1>
{% if app.session.get('user').client.validated == true and app.session.get('user').signed == true and app.session.get('user').phoneVerified == true  and app.session.get('user').emailVerified == true  %}
    <a href=\"{{ (app.session.get('user').client.validated == true and app.session.get('user').signed == true) ? path('app_client_web_new_shift'): '#' }}\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i> Publier un nouveau contrat</a>
{% endif %}
</div>
<hr>
<br>
<div class=\"row\">
    <div class=\"col-12\">

    <div class=\"row\">
        {% if contracts|length == 1 %}
            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ contracts[0] }}</h3>
        {% else %}
            {% for contract in contracts %}
                {% if  contract is iterable %}
                    {% 
                        set month = contract[0]|split(' ')
                    %}
                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ months[month[0] - 1] ~ ' ' ~ month[1]}}</h3>
                    <br>
                    <br>
                {% else %}
                    {{ component('Contract', { contractData: contract, user: app.session.get('user') }) }}
                {% endif %}
            {% endfor %}
        {% endif %}
            
    </div>
</div>
{% endblock %}
", "client_web/home/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/home/index.html.twig");
    }
}
