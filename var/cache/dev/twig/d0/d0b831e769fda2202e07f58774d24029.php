<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* email/ShiftCreated.html.twig */
class __TwigTemplate_38b626e4923bb8effc9d228a708085f0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/ShiftCreated.html.twig"));

        // line 1
        ob_start();
        // line 2
        echo "<style>
\t* {
\t\tbox-sizing: border-box;
\t}
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3>Salut
\t\t\t\t\t<b>";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["username"]) || array_key_exists("username", $context) ? $context["username"] : (function () { throw new RuntimeError('Variable "username" does not exist.', 62, $this->source); })()), "html", null, true);
        echo ",</b>
\t\t\t\t</h3>
\t\t\t\t<p>Un shift vient d'être publié chez
\t\t\t\t\t<b>";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 65, $this->source); })()), "name", [], "any", false, false, false, 65), "html", null, true);
        echo "</b>
\t\t\t\t\tpour le
\t\t\t\t\t<b>";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 67, $this->source); })()), "date", [], "any", false, false, false, 67), "medium", "medium", "eeee dd MMMM yyyy", null, "gregorian", "fr"), "html", null, true);
        echo "</b>
\t\t\t\t\tde
\t\t\t\t\t<b>";
        // line 69
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 69, $this->source); })()), "startHoure", [], "any", false, false, false, 69), "H:i"), "html", null, true);
        echo "</b>
\t\t\t\t\tà
\t\t\t\t\t<b>";
        // line 71
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 71, $this->source); })()), "endHoure", [], "any", false, false, false, 71), "H:i"), "html", null, true);
        echo "</b>, payé
\t\t\t\t\t<b>";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 72, $this->source); })()), "hourlyRate", [], "any", false, false, false, 72), "html", null, true);
        echo "\$/heure</b>.</p>
\t\t\t\t<br>
\t\t\t\t<p>Veuillez lancer votre application pour avoir plus d'informations et accepter le contrat si cela vous convient.</p>
\t\t\t\t<hr>
\t\t\t\t<p>Merci d'avoir choisi
\t\t\t\t\t<b>QuickDep</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>

";
        $___internal_parse_6_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        echo Twig\Extra\CssInliner\twig_inline_css($___internal_parse_6_);
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "email/ShiftCreated.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 1,  129 => 72,  125 => 71,  120 => 69,  115 => 67,  110 => 65,  104 => 62,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% apply inline_css %}
<style>
\t* {
\t\tbox-sizing: border-box;
\t}
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3>Salut
\t\t\t\t\t<b>{{ username }},</b>
\t\t\t\t</h3>
\t\t\t\t<p>Un shift vient d'être publié chez
\t\t\t\t\t<b>{{ client.name }}</b>
\t\t\t\t\tpour le
\t\t\t\t\t<b>{{ contract.date | format_datetime(pattern=\"eeee dd MMMM yyyy\", locale='fr') }}</b>
\t\t\t\t\tde
\t\t\t\t\t<b>{{ contract.startHoure | date('H:i') }}</b>
\t\t\t\t\tà
\t\t\t\t\t<b>{{ contract.endHoure | date('H:i')  }}</b>, payé
\t\t\t\t\t<b>{{ contract.hourlyRate }}\$/heure</b>.</p>
\t\t\t\t<br>
\t\t\t\t<p>Veuillez lancer votre application pour avoir plus d'informations et accepter le contrat si cela vous convient.</p>
\t\t\t\t<hr>
\t\t\t\t<p>Merci d'avoir choisi
\t\t\t\t\t<b>QuickDep</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>

{% endapply %}
", "email/ShiftCreated.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/email/ShiftCreated.html.twig");
    }
}
