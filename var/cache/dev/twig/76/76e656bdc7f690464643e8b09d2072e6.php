<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/auth/paiement.html.twig */
class __TwigTemplate_37c1a69dcc871e0d8ad61f06f95c67c7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/auth/paiement.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "client_web/auth/paiement.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">";
        // line 7
        if ((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 7, $this->source); })())) {
            echo "Modifier votre <b>Mode de paiement</b>";
        } else {
            echo "Bienvenue chez <b>QuickDep</b>";
        }
        echo "</h1>
    <p class=\"lead\">
        Veuillez choisir votre mode de <b>paiement</b>
    </p>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "     ";
            // line 22
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    
    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 24));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 25
            echo "     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 27
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        ";
        // line 39
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["paiementForm"]) || array_key_exists("paiementForm", $context) ? $context["paiementForm"] : (function () { throw new RuntimeError('Variable "paiementForm" does not exist.', 39, $this->source); })()), 'form_start');
        echo "
        <div class=\"m-sm-4\">
            <div class=\"wrapper\">
                <input type=\"radio\" name=\"paiement\" value=\"1\" id=\"option-1\" checked>
                <input type=\"radio\" name=\"paiement\" value=\"2\" id=\"option-2\">
                <label for=\"option-1\" class=\"option option-1\">
                    <span>Paiement par chèque</span>
                </label>
                <label for=\"option-2\" class=\"option option-2\">
                    <span>Paiement direct</span>
                </label>
            </div>
            <br>
            <br>
            <div id=\"paiement1_box\" style=\"text-align:center\">
                <h3 style=\"
                    font-size: 16px;
                    font-weight: bold;
                \">Adresse QuickDep</h3>        
                <p>Compagnie QuickDep Inc.</p>
                <hr>
                <p>202-2955 Avenue Maricourt Québec, Québec G1W 4T8</p>
            </div>
            <div style=\"text-align:center; display:none\" id=\"paiement2_box\">    
                <br>    
                <a href=\"#\" style=\"margin-bottom:10px;\" class=\"btn btn-primary\">Télécharger le Spécimen de chèque</a>
                <a href=\"#\" class=\"btn btn-primary\">Télécharger l'attestation de revenue Québec</a>
                <br>
                <hr>
            </div>
            <br>
            <div class=\"text-center mt-3\">
            ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["paiementForm"]) || array_key_exists("paiementForm", $context) ? $context["paiementForm"] : (function () { throw new RuntimeError('Variable "paiementForm" does not exist.', 71, $this->source); })()), "submit", [], "any", false, false, false, 71), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
            </div>

        </div>
        ";
        // line 75
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["paiementForm"]) || array_key_exists("paiementForm", $context) ? $context["paiementForm"] : (function () { throw new RuntimeError('Variable "paiementForm" does not exist.', 75, $this->source); })()), 'form_end');
        echo "
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/auth/paiement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 75,  166 => 71,  131 => 39,  125 => 35,  111 => 27,  107 => 25,  103 => 24,  100 => 23,  94 => 22,  92 => 13,  88 => 12,  76 => 7,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">{% if (profile) %}Modifier votre <b>Mode de paiement</b>{% else %}Bienvenue chez <b>QuickDep</b>{% endif %}</h1>
    <p class=\"lead\">
        Veuillez choisir votre mode de <b>paiement</b>
    </p>
    
    {% for message in app.flashes('notice') %}
     {# <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br> #}
    {% endfor %}
    
    {% for message in app.flashes('success') %}
     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        {{ form_start(paiementForm) }}
        <div class=\"m-sm-4\">
            <div class=\"wrapper\">
                <input type=\"radio\" name=\"paiement\" value=\"1\" id=\"option-1\" checked>
                <input type=\"radio\" name=\"paiement\" value=\"2\" id=\"option-2\">
                <label for=\"option-1\" class=\"option option-1\">
                    <span>Paiement par chèque</span>
                </label>
                <label for=\"option-2\" class=\"option option-2\">
                    <span>Paiement direct</span>
                </label>
            </div>
            <br>
            <br>
            <div id=\"paiement1_box\" style=\"text-align:center\">
                <h3 style=\"
                    font-size: 16px;
                    font-weight: bold;
                \">Adresse QuickDep</h3>        
                <p>Compagnie QuickDep Inc.</p>
                <hr>
                <p>202-2955 Avenue Maricourt Québec, Québec G1W 4T8</p>
            </div>
            <div style=\"text-align:center; display:none\" id=\"paiement2_box\">    
                <br>    
                <a href=\"#\" style=\"margin-bottom:10px;\" class=\"btn btn-primary\">Télécharger le Spécimen de chèque</a>
                <a href=\"#\" class=\"btn btn-primary\">Télécharger l'attestation de revenue Québec</a>
                <br>
                <hr>
            </div>
            <br>
            <div class=\"text-center mt-3\">
            {{ form_widget(paiementForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary'} }) }}
            </div>

        </div>
        {{ form_end(paiementForm) }}
    </div>
</div>
{% endblock %}
", "client_web/auth/paiement.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/auth/paiement.html.twig");
    }
}
