<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/auth/index.html.twig */
class __TwigTemplate_4b9cfcb1a0492afc38579f8d8436761e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/auth/index.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "client_web/auth/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        Connectez-vous à votre compte <b>entréprise</b> pour continuer
    </p>
    
    
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 13, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 13));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 14
            echo "     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 16
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "    
    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 25));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 26
            echo "     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 28
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 41, $this->source); })()), 'form_start');
        echo "
                <div class=\"mb-3\">
                    ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 43, $this->source); })()), "email", [], "any", false, false, false, 43), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse Email"]);
        // line 45
        echo "
                    ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 46, $this->source); })()), "email", [], "any", false, false, false, 46), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "email", "placeholder" => "Entrez votre adresse e-mail"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 49, $this->source); })()), "email", [], "any", false, false, false, 49), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Mot de passe"]);
        // line 51
        echo "
                    ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 52, $this->source); })()), "password", [], "any", false, false, false, 52), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "password", "placeholder" => "Entrez votre mot de passe"]]);
        echo "
                    <br/>
                    <small>
                        <a href=\"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_forget_pass");
        echo "\">Mot de passe oublié ?</a>
                    </small>
                </div>
                <div class=\"text-center mt-3\">
                    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 59, $this->source); })()), "submit", [], "any", false, false, false, 59), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>
                <br>
                <p style=\"text-align: center\">Vous n'avez pas de comte ? <b><a href=\"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_register");
        echo "\">Inscrivez-vous</a></b></p>
            ";
        // line 63
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 63, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/auth/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 63,  176 => 62,  170 => 59,  163 => 55,  157 => 52,  154 => 51,  152 => 49,  146 => 46,  143 => 45,  141 => 43,  136 => 41,  129 => 36,  115 => 28,  111 => 26,  107 => 25,  104 => 24,  90 => 16,  86 => 14,  82 => 13,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        Connectez-vous à votre compte <b>entréprise</b> pour continuer
    </p>
    
    
    {% for message in app.flashes('success') %}
     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
    
    {% for message in app.flashes('notice') %}
     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            {{ form_start(loginForm) }}
                <div class=\"mb-3\">
                    {{ form_label(loginForm.email, 'Adresse Email', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(loginForm.email, { 'attr': {'class': 'form-control form-control-lg', 'type': 'email', 'placeholder': 'Entrez votre adresse e-mail'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(loginForm.email, 'Mot de passe', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(loginForm.password, { 'attr': {'class': 'form-control form-control-lg', 'type': 'password', 'placeholder': 'Entrez votre mot de passe'} }) }}
                    <br/>
                    <small>
                        <a href=\"{{ path('app_client_web_auth_forget_pass') }}\">Mot de passe oublié ?</a>
                    </small>
                </div>
                <div class=\"text-center mt-3\">
                    {{ form_widget(loginForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary'} }) }}
                </div>
                <br>
                <p style=\"text-align: center\">Vous n'avez pas de comte ? <b><a href=\"{{ path(\"app_client_web_auth_register\") }}\">Inscrivez-vous</a></b></p>
            {{ form_end(loginForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "client_web/auth/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/auth/index.html.twig");
    }
}
