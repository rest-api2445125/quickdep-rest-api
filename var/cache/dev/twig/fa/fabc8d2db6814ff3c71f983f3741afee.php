<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/contract/detail.html.twig */
class __TwigTemplate_54677e2e46a18d17c2239c8963100f03 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/contract/detail.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/contract/detail.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Detail du contrat";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <div>
        ";
        // line 16
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 16, $this->source); })()), "status", [], "any", false, false, false, 16) == "contract_published")) {
            // line 17
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_edit_shift", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 17, $this->source); })()), "id", [], "any", false, false, false, 17)]), "html", null, true);
            echo "\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        ";
        }
        // line 19
        echo "        ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 19, $this->source); })()), "status", [], "any", false, false, false, 19) == "contract_approuved")) {
            // line 20
            echo "        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" action=\"#\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que l'employé s'est pas présenté ?')\" type=\"submit\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"user-x\"></i> Employé absent</button>
        </form>
        ";
        }
        // line 24
        echo "        <form  method=\"POST\" style=\"display:inline\" action=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_delete_shift_detail", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 24, $this->source); })()), "id", [], "any", false, false, false, 24)]), "html", null, true);
        echo "\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce contrat ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i> Supprimer</button>
        </form>
    </div>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "request", [], "any", false, false, false, 34), "headers", [], "any", false, false, false, 34), "get", [0 => "referer"], "method", false, false, false, 34), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    ";
        // line 41
        echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("ContractDetail", ["contractData" => (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 41, $this->source); })()), "user" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 41, $this->source); })()), "session", [], "any", false, false, false, 41), "get", [0 => "user"], "method", false, false, false, 41)]);
        echo "
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/contract/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 41,  116 => 34,  102 => 24,  96 => 20,  93 => 19,  87 => 17,  85 => 16,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Detail du contrat{% endblock %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <div>
        {% if contract.status == \"contract_published\" %}
        <a href=\"{{ path('app_client_web_edit_shift', {'id' : contract.id}) }}\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        {% endif %}
        {% if contract.status == \"contract_approuved\" %}
        <form  method=\"POST\" style=\"display:inline; margin-right:5px\" action=\"#\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que l'employé s'est pas présenté ?')\" type=\"submit\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"user-x\"></i> Employé absent</button>
        </form>
        {% endif %}
        <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('app_client_web_delete_shift_detail', {'id': contract.id}) }}\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce contrat ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i> Supprimer</button>
        </form>
    </div>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    {{ component('ContractDetail', {contractData: contract, user : app.session.get('user')}) }}
</div>
{% endblock %}
", "admin/contract/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/contract/detail.html.twig");
    }
}
