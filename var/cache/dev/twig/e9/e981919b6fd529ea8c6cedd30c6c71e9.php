<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/user/new.html.twig */
class __TwigTemplate_ff59a191fe910c72e131a62d8ba6e35f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/user/new.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/user/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 3, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier les informations du travailleur : " . twig_get_attribute($this->env, $this->source, (isset($context["employee"]) || array_key_exists("employee", $context) ? $context["employee"] : (function () { throw new RuntimeError('Variable "employee" does not exist.', 3, $this->source); })()), "email", [], "any", false, false, false, 3)), "html", null, true))) : (print ("Enrégistrer un travailleur")));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "headers", [], "any", false, false, false, 8), "get", [0 => "referer"], "method", false, false, false, 8), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">";
        // line 17
        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 17, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier le shift : " . twig_get_attribute($this->env, $this->source, (isset($context["employee"]) || array_key_exists("employee", $context) ? $context["employee"] : (function () { throw new RuntimeError('Variable "employee" does not exist.', 17, $this->source); })()), "email", [], "any", false, false, false, 17)), "html", null, true))) : (print ("Enrégistrer un travailleur")));
        echo "</h5>
        </div>
        <div class=\"card-body\">
            ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 20, $this->source); })()), 'form_start');
        echo "
                <div class=\"row\">
                    <div class=\"col-6\">

                        ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 24, $this->source); })()), "firstname", [], "any", false, false, false, 24), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Prénom *"]);
        // line 26
        echo "
                        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 27, $this->source); })()), "firstname", [], "any", false, false, false, 27), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    
                        <br>
                        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 30, $this->source); })()), "lastname", [], "any", false, false, false, 30), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Nom *"]);
        // line 32
        echo "
                        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 33, $this->source); })()), "lastname", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                        <br>
                        ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 36, $this->source); })()), "work", [], "any", false, false, false, 36), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Proféssion *"]);
        // line 38
        echo "
                        ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 39, $this->source); })()), "work", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                        <br>
                        ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 42, $this->source); })()), "birthday", [], "any", false, false, false, 42), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date de naissance *"]);
        // line 44
        echo "
                        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 45, $this->source); })()), "birthday", [], "any", false, false, false, 45), 'widget', ["attr" => ["class" => "form-control", "style" => "width:100%"]]);
        echo "
                        
                        <br>
                        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 48, $this->source); })()), "address", [], "any", false, false, false, 48), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse *"]);
        // line 50
        echo "
                        ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 51, $this->source); })()), "address", [], "any", false, false, false, 51), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 54, $this->source); })()), "phone", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Téléphone *"]);
        // line 56
        echo "
                        ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 57, $this->source); })()), "phone", [], "any", false, false, false, 57), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 60, $this->source); })()), "email", [], "any", false, false, false, 60), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse E-mail *"]);
        // line 62
        echo "
                        ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 63, $this->source); })()), "email", [], "any", false, false, false, 63), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        ";
        // line 66
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 66, $this->source); })()), "password", [], "any", false, false, false, 66), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Mot de passe *"]);
        // line 68
        echo "
                        ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 69, $this->source); })()), "password", [], "any", false, false, false, 69), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        ";
        // line 72
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 72, $this->source); })()), "password_confirm", [], "any", false, false, false, 72), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Confirmer le mot de passe *"]);
        // line 74
        echo "
                        ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 75, $this->source); })()), "password_confirm", [], "any", false, false, false, 75), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <div style=\"position:absolute; bottom:0; right:0\">
                            ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 78, $this->source); })()), "submit", [], "any", false, false, false, 78), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => (("return confirm('Etes-vous sûr de vouloir " . (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 78, $this->source); })())) ? ("modifier") : ("enrégistrer"))) . " cet travailleur ?')")]]);
        echo "
                        </div>
                    </div>
                </div>
            ";
        // line 82
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["employeeForm"]) || array_key_exists("employeeForm", $context) ? $context["employeeForm"] : (function () { throw new RuntimeError('Variable "employeeForm" does not exist.', 82, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/user/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 82,  201 => 78,  195 => 75,  192 => 74,  190 => 72,  184 => 69,  181 => 68,  179 => 66,  173 => 63,  170 => 62,  168 => 60,  162 => 57,  159 => 56,  157 => 54,  151 => 51,  148 => 50,  146 => 48,  140 => 45,  137 => 44,  135 => 42,  129 => 39,  126 => 38,  124 => 36,  118 => 33,  115 => 32,  113 => 30,  107 => 27,  104 => 26,  102 => 24,  95 => 20,  89 => 17,  77 => 8,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}{{ edit ? 'Modifier les informations du travailleur : ' ~ employee.email : 'Enrégistrer un travailleur' }}{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">{{ edit ? 'Modifier le shift : ' ~ employee.email: 'Enrégistrer un travailleur' }}</h5>
        </div>
        <div class=\"card-body\">
            {{ form_start(employeeForm) }}
                <div class=\"row\">
                    <div class=\"col-6\">

                        {{ form_label(employeeForm.firstname, 'Prénom *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.firstname, { 'attr': {'class': 'form-control'} }) }}
    
                        <br>
                        {{ form_label(employeeForm.lastname, 'Nom *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.lastname, { 'attr': {'class': 'form-control'} }) }}

                        <br>
                        {{ form_label(employeeForm.work, 'Proféssion *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.work, { 'attr': {'class': 'form-control'} }) }}

                        <br>
                        {{ form_label(employeeForm.birthday, 'Date de naissance *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.birthday, { 'attr': {'class': 'form-control', 'style': 'width:100%'} }) }}
                        
                        <br>
                        {{ form_label(employeeForm.address, 'Adresse *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.address, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        {{ form_label(employeeForm.phone, 'Téléphone *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.phone, { 'attr': {'class': 'form-control'} }) }}
                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        {{ form_label(employeeForm.email, 'Adresse E-mail *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.email, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        {{ form_label(employeeForm.password, 'Mot de passe *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.password, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        {{ form_label(employeeForm.password_confirm, 'Confirmer le mot de passe *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(employeeForm.password_confirm, { 'attr': {'class': 'form-control'} }) }}
                        
                        <div style=\"position:absolute; bottom:0; right:0\">
                            {{ form_widget(employeeForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir \" ~ (edit ? \"modifier\" : \"enrégistrer\") ~ \" cet travailleur ?')\"} }) }}
                        </div>
                    </div>
                </div>
            {{ form_end(employeeForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "admin/user/new.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/user/new.html.twig");
    }
}
