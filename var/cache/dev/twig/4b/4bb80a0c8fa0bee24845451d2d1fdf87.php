<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* email/new.html */
class __TwigTemplate_a67f901cd97925e2d3450529cf7e4a0d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/new.html"));

        // line 1
        echo "<style>
\t* {
\t\tbox-sizing: border-box;
\t}
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-image: url(\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["email"]) || array_key_exists("email", $context) ? $context["email"] : (function () { throw new RuntimeError('Variable "email" does not exist.', 29, $this->source); })()), "image", [0 => "@images/bg_white.png"], "method", false, false, false, 29), "html", null, true);
        echo "\");
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"top\">
\t\t\t\t<div class=\"img\" style=\"width:80px; height:80px; background-color:white; padding: 15px; border-radius:50%; margin:auto\">
\t\t\t\t\t<img src=\"logo.png\" alt=\"logo\" style=\"width:100%\">
\t\t\t\t</div>
\t\t\t\t<br>
\t\t\t\t<h2 style=\"font-weight:700; margin:0; color:white\">
\t\t\t\t\t<b>Slack</b>
\t\t\t\t\toffre un shift au poste de
\t\t\t\t\t<b>plongeur</b>!
\t\t\t\t</h2>
\t\t\t</div>
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3>Salut
\t\t\t\t\t<b>Daniel Mwema,</b>
\t\t\t\t</h3>
\t\t\t\t<p>Un shift vient d'être publié chez Slack pour le lundi 26 septembre 2022 de 08h00 à 16h30, payé 20\$/h.</p>
\t\t\t\t<br>
\t\t\t\t<p>Veuillez lancez votre application pour avoir plus d'informations et accepter le contrat si cela vous convient.</p>
\t\t\t\t<hr>
\t\t\t\t<p>Merci d'avoir choisi
\t\t\t\t\t<b>QuickDep</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "email/new.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 29,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
\t* {
\t\tbox-sizing: border-box;
\t}
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-image: url(\"{{ email.image('@images/bg_white.png') }}\");
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"top\">
\t\t\t\t<div class=\"img\" style=\"width:80px; height:80px; background-color:white; padding: 15px; border-radius:50%; margin:auto\">
\t\t\t\t\t<img src=\"logo.png\" alt=\"logo\" style=\"width:100%\">
\t\t\t\t</div>
\t\t\t\t<br>
\t\t\t\t<h2 style=\"font-weight:700; margin:0; color:white\">
\t\t\t\t\t<b>Slack</b>
\t\t\t\t\toffre un shift au poste de
\t\t\t\t\t<b>plongeur</b>!
\t\t\t\t</h2>
\t\t\t</div>
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3>Salut
\t\t\t\t\t<b>Daniel Mwema,</b>
\t\t\t\t</h3>
\t\t\t\t<p>Un shift vient d'être publié chez Slack pour le lundi 26 septembre 2022 de 08h00 à 16h30, payé 20\$/h.</p>
\t\t\t\t<br>
\t\t\t\t<p>Veuillez lancez votre application pour avoir plus d'informations et accepter le contrat si cela vous convient.</p>
\t\t\t\t<hr>
\t\t\t\t<p>Merci d'avoir choisi
\t\t\t\t\t<b>QuickDep</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>
", "email/new.html", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/email/new.html");
    }
}
