<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/user/detail.html.twig */
class __TwigTemplate_e4633a5600ebd6912e1ced9cccf22e2a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/user/detail.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/user/detail.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Detail du travailleur";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "headers", [], "any", false, false, false, 8), "get", [0 => "referer"], "method", false, false, false, 8), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 style=\"display:inline; margin-right:10px\" class=\"card-title mb-0\">Details du travailleur ";
        // line 17
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "firstname", [], "any", false, false, false, 17) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "lastname", [], "any", false, false, false, 17)), "html", null, true);
        echo "</h5>
        ";
        // line 18
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 18, $this->source); })()), "getDocumentsStatus", [], "any", false, false, false, 18) == "document_not_sent")) {
            // line 19
            echo "            <span class=\"badge bg-danger\">Documents non envoyés</span>
        ";
        }
        // line 21
        echo "
        ";
        // line 22
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 22, $this->source); })()), "getDocumentsStatus", [], "any", false, false, false, 22) == "document_refused")) {
            // line 23
            echo "            <span class=\"badge bg-danger\">Documents refusés</span>
        ";
        }
        // line 25
        echo "
        ";
        // line 26
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 26, $this->source); })()), "signed", [], "any", false, false, false, 26) != true)) {
            // line 27
            echo "            <span class=\"badge bg-danger\">Contrat non signé</span>
        ";
        }
        // line 29
        echo "
        ";
        // line 30
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 30, $this->source); })()), "emailVerified", [], "any", false, false, false, 30) != true)) {
            // line 31
            echo "        <span class=\"badge bg-danger\">E-mail non vérifié</span>
        ";
        }
        // line 33
        echo "
        ";
        // line 34
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 34, $this->source); })()), "phoneVerified", [], "any", false, false, false, 34) != true)) {
            // line 35
            echo "        <span class=\"badge bg-danger\">Téléphone non vérifié</span>
        ";
        }
        // line 37
        echo "
        ";
        // line 38
        if (((((twig_get_attribute($this->env, $this->source,         // line 39
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 39, $this->source); })()), "getDocumentsStatus", [], "any", false, false, false, 39) == "document_waiting") && (twig_get_attribute($this->env, $this->source,         // line 40
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 40, $this->source); })()), "signed", [], "any", false, false, false, 40) == true)) && (twig_get_attribute($this->env, $this->source,         // line 41
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 41, $this->source); })()), "emailVerified", [], "any", false, false, false, 41) == true)) && (twig_get_attribute($this->env, $this->source,         // line 42
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "phoneVerified", [], "any", false, false, false, 42) == true))) {
            // line 44
            echo "        <span class=\"badge bg-warning\">En attente d'approbation</span>
        ";
        }
        // line 46
        echo "
        ";
        // line 47
        if (((((twig_get_attribute($this->env, $this->source,         // line 48
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 48, $this->source); })()), "getDocumentsStatus", [], "any", false, false, false, 48) == "document_validated") && (twig_get_attribute($this->env, $this->source,         // line 49
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 49, $this->source); })()), "signed", [], "any", false, false, false, 49) == true)) && (twig_get_attribute($this->env, $this->source,         // line 50
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 50, $this->source); })()), "emailVerified", [], "any", false, false, false, 50) == true)) && (twig_get_attribute($this->env, $this->source,         // line 51
(isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "phoneVerified", [], "any", false, false, false, 51) == true))) {
            // line 53
            echo "        <span class=\"badge bg-success\">Travailleur validé</span>
        ";
        }
        // line 55
        echo "        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Details du compte</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 65, $this->source); })()), "imagePath", [], "any", false, false, false, 65) == null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/img/logo.png")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 65, $this->source); })()), "imagePath", [], "any", false, false, false, 65))))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 65, $this->source); })()), "firstname", [], "any", false, false, false, 65), "html", null, true);
        echo "\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\" style=\"width:128px; height:128px\">
                    <h5 class=\"card-title mb-0\">";
        // line 66
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 66, $this->source); })()), "firstname", [], "any", false, false, false, 66) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 66, $this->source); })()), "lastname", [], "any", false, false, false, 66)), "html", null, true);
        echo "</h5>
                    <div class=\"text-muted mb-2\">";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 67, $this->source); })()), "email", [], "any", false, false, false, 67), "html", null, true);
        echo "</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier la photo de profile</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos du travailleur</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>";
        // line 81
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 81, $this->source); })()), "firstname", [], "any", false, false, false, 81) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 81, $this->source); })()), "lastname", [], "any", false, false, false, 81)), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 82, $this->source); })()), "email", [], "any", false, false, false, 82), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
        // line 83
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 83, $this->source); })()), "address", [], "any", false, false, false, 83), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 84, $this->source); })()), "phone", [], "any", false, false, false, 84), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Villes de travail</h5>
                    <ul class=\"list-unstyled mb-0\">
                    ";
        // line 91
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 91, $this->source); })()), "town", [], "any", false, false, false, 91));
        foreach ($context['_seq'] as $context["_key"] => $context["townU"]) {
            // line 92
            echo "                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["townU"], "name", [], "any", false, false, false, 92), "html", null, true);
            echo "</li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['townU'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                    <br>
                    <a href=\"#\" target=\"_blank\" class=\"btn btn-primary\">Modifier les villes de travaille</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                ";
        // line 99
        if ((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 99, $this->source); })()), "signed", [], "any", false, false, false, 99) && (twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 99, $this->source); })()), "usercontract", [], "any", false, false, false, 99) != null))) {
            // line 100
            echo "                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de travailleur</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <a href=\"";
            // line 103
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("contracts/society/pdf/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 103, $this->source); })()), "usercontract", [], "any", false, false, false, 103), "id", [], "any", false, false, false, 103))), "html", null, true);
            echo ".pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger le contrat</a>
                    </ul>
                </div>
                ";
        }
        // line 107
        echo "            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
           
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 118
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 118, $this->source); })()), 'form_start');
        echo "
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                        
                            ";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 122, $this->source); })()), "firstname", [], "any", false, false, false, 122), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Prénom *"]);
        // line 124
        echo "
                            ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 125, $this->source); })()), "firstname", [], "any", false, false, false, 125), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>

                            ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 129, $this->source); })()), "lastname", [], "any", false, false, false, 129), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Nom *"]);
        // line 131
        echo "
                            ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 132, $this->source); })()), "lastname", [], "any", false, false, false, 132), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                            
                            ";
        // line 136
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 136, $this->source); })()), "email", [], "any", false, false, false, 136), 'label', ["label_attr" => ["class" => "form-label"], "label" => "E-mail *"]);
        // line 138
        echo "
                            ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 139, $this->source); })()), "email", [], "any", false, false, false, 139), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                            
                            ";
        // line 143
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 143, $this->source); })()), "phone", [], "any", false, false, false, 143), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Téléphone *"]);
        // line 145
        echo "
                            ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 146, $this->source); })()), "phone", [], "any", false, false, false, 146), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                        </div>
                        <div class=\"col-md-6\">
                            ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 151, $this->source); })()), "address", [], "any", false, false, false, 151), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse *"]);
        // line 153
        echo "
                            ";
        // line 154
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 154, $this->source); })()), "address", [], "any", false, false, false, 154), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                            
                            ";
        // line 158
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 158, $this->source); })()), "work", [], "any", false, false, false, 158), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Profession"]);
        // line 160
        echo "
                            ";
        // line 161
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 161, $this->source); })()), "work", [], "any", false, false, false, 161), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                            
                            ";
        // line 165
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 165, $this->source); })()), "birthday", [], "any", false, false, false, 165), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date de naissance *"]);
        // line 167
        echo "
                            ";
        // line 168
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 168, $this->source); })()), "birthday", [], "any", false, false, false, 168), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                            
                            ";
        // line 172
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 172, $this->source); })()), "bio", [], "any", false, false, false, 172), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Bio"]);
        // line 174
        echo "
                            ";
        // line 175
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 175, $this->source); })()), "bio", [], "any", false, false, false, 175), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            
                            <br>
                        </div>
                    </div>
                    <div class=\"text-center\">
                        ";
        // line 181
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 181, $this->source); })()), "submit", [], "any", false, false, false, 181), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')"]]);
        echo "
                    </div>

                    ";
        // line 184
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 184, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Documents administratifs</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 192
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 192, $this->source); })()), "documents", [], "any", false, false, false, 192));
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 193
            echo "                    <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; display: flex; justify-content:space-between;align-items:center\">
                        <span>";
            // line 194
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["document"], "type", [], "any", false, false, false, 194), "title", [], "any", false, false, false, 194), "html", null, true);
            echo "</span>
                        <div>
                            ";
            // line 197
            $context["status"] = (((twig_get_attribute($this->env, $this->source,             // line 198
$context["document"], "statut", [], "any", false, false, false, 198) == "document_not_sent")) ? ("Document non envoyé") : ((((twig_get_attribute($this->env, $this->source,             // line 199
$context["document"], "statut", [], "any", false, false, false, 199) == "document_waiting")) ? ("En attente de validation") : ((((twig_get_attribute($this->env, $this->source,             // line 200
$context["document"], "statut", [], "any", false, false, false, 200) == "document_validated")) ? ("Validé") : ("Non valide"))))));
            // line 204
            echo "
                            ";
            // line 206
            $context["color"] = (((twig_get_attribute($this->env, $this->source,             // line 207
$context["document"], "statut", [], "any", false, false, false, 207) == "document_validated")) ? ("success") : ((((twig_get_attribute($this->env, $this->source,             // line 208
$context["document"], "statut", [], "any", false, false, false, 208) == "document_waiting")) ? ("warning") : ("danger"))));
            // line 211
            echo "                            ";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["document"], "type", [], "any", false, false, false, 211), "type", [], "any", false, false, false, 211) == "file")) {
                // line 212
                echo "                                <span class=\"badge bg-";
                echo twig_escape_filter($this->env, (isset($context["color"]) || array_key_exists("color", $context) ? $context["color"] : (function () { throw new RuntimeError('Variable "color" does not exist.', 212, $this->source); })()), "html", null, true);
                echo "\" style=\"margin-right:5px\">";
                echo twig_escape_filter($this->env, (isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 212, $this->source); })()), "html", null, true);
                echo "</span>
                                ";
                // line 213
                if ((twig_get_attribute($this->env, $this->source, $context["document"], "statut", [], "any", false, false, false, 213) != "document_not_sent")) {
                    // line 214
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("documents/files/" . twig_get_attribute($this->env, $this->source, $context["document"], "filePath", [], "any", false, false, false, 214))), "html", null, true);
                    echo "\" class=\"btn btn-info\" target=\"_blank\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                ";
                }
                // line 216
                echo "                                ";
                if (((twig_get_attribute($this->env, $this->source, $context["document"], "statut", [], "any", false, false, false, 216) != "document_refused") && (twig_get_attribute($this->env, $this->source, $context["document"], "statut", [], "any", false, false, false, 216) != "document_not_sent"))) {
                    // line 217
                    echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Réfuser\" action=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_document_unvalid", ["id" => twig_get_attribute($this->env, $this->source, $context["document"], "id", [], "any", false, false, false, 217)]), "html", null, true);
                    echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir refuser ce document ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                ";
                }
                // line 221
                echo "                                ";
                if (((twig_get_attribute($this->env, $this->source, $context["document"], "statut", [], "any", false, false, false, 221) != "document_validated") && (twig_get_attribute($this->env, $this->source, $context["document"], "statut", [], "any", false, false, false, 221) != "document_not_sent"))) {
                    // line 222
                    echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_document_valid", ["id" => twig_get_attribute($this->env, $this->source, $context["document"], "id", [], "any", false, false, false, 222)]), "html", null, true);
                    echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider ce document ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                ";
                }
                // line 226
                echo "                            ";
            } else {
                // line 227
                echo "                            <span style=\"margin-right:5px; font-weight:bold\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "text", [], "any", false, false, false, 227), "html", null, true);
                echo "</span>
                            ";
            }
            // line 229
            echo "                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 232
        echo "                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Statistiques</h5>
                </div>
                <div class=\"card-body h-100\">
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Shifts Complétés</span>
                                <div style=\"font-size:20px\">";
        // line 243
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 243, $this->source); })()), "completed", [], "any", false, false, false, 243), "html", null, true);
        echo "</div>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Shifts Annulés</span>
                                <div style=\"font-size:20px\">";
        // line 249
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 249, $this->source); })()), "canceled", [], "any", false, false, false, 249), "html", null, true);
        echo "</div>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Absences non justifiées</span>
                                <div style=\"font-size:20px\">";
        // line 255
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 255, $this->source); })()), "notworked", [], "any", false, false, false, 255), "html", null, true);
        echo "</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/user/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  504 => 255,  495 => 249,  486 => 243,  473 => 232,  465 => 229,  459 => 227,  456 => 226,  448 => 222,  445 => 221,  437 => 217,  434 => 216,  428 => 214,  426 => 213,  419 => 212,  416 => 211,  414 => 208,  413 => 207,  412 => 206,  409 => 204,  407 => 200,  406 => 199,  405 => 198,  404 => 197,  399 => 194,  396 => 193,  392 => 192,  381 => 184,  375 => 181,  366 => 175,  363 => 174,  361 => 172,  354 => 168,  351 => 167,  349 => 165,  342 => 161,  339 => 160,  337 => 158,  330 => 154,  327 => 153,  325 => 151,  317 => 146,  314 => 145,  312 => 143,  305 => 139,  302 => 138,  300 => 136,  293 => 132,  290 => 131,  288 => 129,  281 => 125,  278 => 124,  276 => 122,  269 => 118,  256 => 107,  249 => 103,  244 => 100,  242 => 99,  235 => 94,  226 => 92,  222 => 91,  212 => 84,  208 => 83,  204 => 82,  200 => 81,  183 => 67,  179 => 66,  173 => 65,  161 => 55,  157 => 53,  155 => 51,  154 => 50,  153 => 49,  152 => 48,  151 => 47,  148 => 46,  144 => 44,  142 => 42,  141 => 41,  140 => 40,  139 => 39,  138 => 38,  135 => 37,  131 => 35,  129 => 34,  126 => 33,  122 => 31,  120 => 30,  117 => 29,  113 => 27,  111 => 26,  108 => 25,  104 => 23,  102 => 22,  99 => 21,  95 => 19,  93 => 18,  89 => 17,  77 => 8,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Detail du travailleur{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 style=\"display:inline; margin-right:10px\" class=\"card-title mb-0\">Details du travailleur {{ user.firstname ~ ' ' ~ user.lastname }}</h5>
        {% if user.getDocumentsStatus == \"document_not_sent\" %}
            <span class=\"badge bg-danger\">Documents non envoyés</span>
        {% endif %}

        {% if user.getDocumentsStatus == \"document_refused\" %}
            <span class=\"badge bg-danger\">Documents refusés</span>
        {% endif %}

        {% if user.signed != true %}
            <span class=\"badge bg-danger\">Contrat non signé</span>
        {% endif %}

        {% if user.emailVerified != true %}
        <span class=\"badge bg-danger\">E-mail non vérifié</span>
        {% endif %}

        {% if user.phoneVerified != true %}
        <span class=\"badge bg-danger\">Téléphone non vérifié</span>
        {% endif %}

        {% if 
            user.getDocumentsStatus == \"document_waiting\" 
            and user.signed == true
            and user.emailVerified == true
            and user.phoneVerified == true 
        %}
        <span class=\"badge bg-warning\">En attente d'approbation</span>
        {% endif %}

        {% if 
            user.getDocumentsStatus == \"document_validated\" 
            and user.signed == true
            and user.emailVerified == true
            and user.phoneVerified == true 
        %}
        <span class=\"badge bg-success\">Travailleur validé</span>
        {% endif %}
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Details du compte</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"{{ user.imagePath == null ? asset('clientweb/img/logo.png') : asset('users/images/'~user.imagePath) }}\" alt=\"{{ user.firstname }}\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\" style=\"width:128px; height:128px\">
                    <h5 class=\"card-title mb-0\">{{ user.firstname ~ ' ' ~ user.lastname }}</h5>
                    <div class=\"text-muted mb-2\">{{ user.email }}</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier la photo de profile</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos du travailleur</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>{{ user.firstname ~ ' ' ~ user.lastname }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>{{ user.email }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ user.address }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>{{ user.phone }}</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Villes de travail</h5>
                    <ul class=\"list-unstyled mb-0\">
                    {% for townU in user.town %}
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ townU.name }}</li>
                    {% endfor %}
                    <br>
                    <a href=\"#\" target=\"_blank\" class=\"btn btn-primary\">Modifier les villes de travaille</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                {% if user.signed and user.usercontract != null%}
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de travailleur</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <a href=\"{{ asset(\"contracts/society/pdf/\"~user.usercontract.id) }}.pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger le contrat</a>
                    </ul>
                </div>
                {% endif %}
            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
           
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(infoForm) }}
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                        
                            {{ form_label(infoForm.firstname, 'Prénom *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.firstname, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>

                            {{ form_label(infoForm.lastname, 'Nom *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.lastname, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                            
                            {{ form_label(infoForm.email, 'E-mail *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.email, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                            
                            {{ form_label(infoForm.phone, 'Téléphone *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.phone, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                        </div>
                        <div class=\"col-md-6\">
                            {{ form_label(infoForm.address, 'Adresse *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.address, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                            
                            {{ form_label(infoForm.work, 'Profession', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.work, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                            
                            {{ form_label(infoForm.birthday, 'Date de naissance *', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.birthday, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                            
                            {{ form_label(infoForm.bio, 'Bio', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(infoForm.bio, { 'attr': {'class': 'form-control'} }) }}
                            
                            <br>
                        </div>
                    </div>
                    <div class=\"text-center\">
                        {{ form_widget(infoForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}
                    </div>

                    {{ form_end(infoForm) }}
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Documents administratifs</h5>
                </div>
                <div class=\"card-body h-100\">
                    {% for document in user.documents %}
                    <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; display: flex; justify-content:space-between;align-items:center\">
                        <span>{{ document.type.title }}</span>
                        <div>
                            {% 
                            set status =
                            document.statut == \"document_not_sent\" ? \"Document non envoyé\" : (
                                document.statut == \"document_waiting\" ? \"En attente de validation\" : (
                                    document.statut == \"document_validated\" ? \"Validé\" : \"Non valide\"
                                ) 
                            )
                            %}

                            {% 
                            set color =
                            document.statut == \"document_validated\" ? \"success\" : (
                                document.statut == \"document_waiting\" ? \"warning\" : \"danger\"  
                            ) 
                            %}
                            {% if document.type.type == 'file' %}
                                <span class=\"badge bg-{{ color }}\" style=\"margin-right:5px\">{{ status }}</span>
                                {% if document.statut != \"document_not_sent\" %}
                                    <a href=\"{{ asset('documents/files/' ~ document.filePath) }}\" class=\"btn btn-info\" target=\"_blank\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                {% endif %}
                                {% if document.statut != \"document_refused\" and document.statut != \"document_not_sent\" %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Réfuser\" action=\"{{ path('app_user_document_unvalid', {'id': document.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir refuser ce document ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                {% endif %}
                                {% if document.statut != \"document_validated\" and document.statut != \"document_not_sent\" %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"{{ path('app_user_document_valid', {'id': document.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider ce document ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                {% endif %}
                            {% else %}
                            <span style=\"margin-right:5px; font-weight:bold\">{{ document.text }}</span>
                            {% endif %}
                        </div>
                    </div>
                    {% endfor %}
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Statistiques</h5>
                </div>
                <div class=\"card-body h-100\">
                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Shifts Complétés</span>
                                <div style=\"font-size:20px\">{{ user.completed }}</div>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Shifts Annulés</span>
                                <div style=\"font-size:20px\">{{ user.canceled }}</div>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div style=\"border:1px solid #ccc; border-radius:5px; padding:10px 15px; margin-bottom:10px; text-align:center\">
                                <span style=\"font-weight:bold\">Absences non justifiées</span>
                                <div style=\"font-size:20px\">{{ user.notworked }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock %}
", "admin/user/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/user/detail.html.twig");
    }
}
