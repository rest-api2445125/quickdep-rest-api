<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/home/contact.html.twig */
class __TwigTemplate_eb2340127ff444bdbeb820f846daa9cc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "client_web/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/home/contact.html.twig"));

        $this->parent = $this->loadTemplate("client_web/base.html.twig", "client_web/home/contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Contact";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"mb-3\">
        <h1 class=\"h3 d-inline align-middle\">Nous contacter</h1>
    </div>
    <div class=\"row\">
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i><span>Téléphone</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">+1 581 777 1486</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i><span>E-mail</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">contact@quickdep.ca</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i><span>Adresse</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">202-2955 avenue maricourt, Quebec, QC</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/home/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'client_web/base.html.twig' %}

{% block title %}Contact{% endblock %}

{% block body %}
    <div class=\"mb-3\">
        <h1 class=\"h3 d-inline align-middle\">Nous contacter</h1>
    </div>
    <div class=\"row\">
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i><span>Téléphone</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">+1 581 777 1486</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i><span>E-mail</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">contact@quickdep.ca</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\" col-md-6\">
            <div class=\"card\">
                <div class=\"card-header\">
                    <div style=\"display:flex; align-items:center;justify-content:space-between\">
                        <h5 class=\"card-title mb-0\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i><span>Adresse</span></h5>
                        <p style=\"margin-bottom:0; font-weight:bold\">202-2955 avenue maricourt, Quebec, QC</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "client_web/home/contact.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/home/contact.html.twig");
    }
}
