<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/user/town.html.twig */
class __TwigTemplate_fc925af7e5329a65cc93848f96e50a8b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/user/town.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/user/town.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Villes de travaille";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "headers", [], "any", false, false, false, 8), "get", [0 => "referer"], "method", false, false, false, 8), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Villes de travaille</h5>
        </div>
        <div class=\"card-body\">
            <p>Veuillez séléctionner les villes où le travailleur <b>";
        // line 20
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["employee"]) || array_key_exists("employee", $context) ? $context["employee"] : (function () { throw new RuntimeError('Variable "employee" does not exist.', 20, $this->source); })()), "firstname", [], "any", false, false, false, 20) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["employee"]) || array_key_exists("employee", $context) ? $context["employee"] : (function () { throw new RuntimeError('Variable "employee" does not exist.', 20, $this->source); })()), "lastname", [], "any", false, false, false, 20)), "html", null, true);
        echo "</b> pourra travailler</p>
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["townForm"]) || array_key_exists("townForm", $context) ? $context["townForm"] : (function () { throw new RuntimeError('Variable "townForm" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <div class=\"container\">
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["towns"]) || array_key_exists("towns", $context) ? $context["towns"] : (function () { throw new RuntimeError('Variable "towns" does not exist.', 23, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["town"]) {
            // line 24
            echo "                    <div class=\"checkbox\">
                        <input type=\"checkbox\" name=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["town"], "id", [], "any", false, false, false, 25), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["town"], "name", [], "any", false, false, false, 25), "html", null, true);
            echo "\" />
                        <div class=\"box\">
                            <i class=\"align-middle\" data-feather=\"map-pin\"></i>
                            <p data-text=\"";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["town"], "name", [], "any", false, false, false, 28), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["town"], "name", [], "any", false, false, false, 28), "html", null, true);
            echo "</p>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['town'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "                </div>
                <br>
                <div style=\"display:flex; align-items:end;justify-content:end\">
                    ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["townForm"]) || array_key_exists("townForm", $context) ? $context["townForm"] : (function () { throw new RuntimeError('Variable "townForm" does not exist.', 35, $this->source); })()), "submit", [], "any", false, false, false, 35), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer ces villes "]]);
        echo "
                </div>
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["townForm"]) || array_key_exists("townForm", $context) ? $context["townForm"] : (function () { throw new RuntimeError('Variable "townForm" does not exist.', 37, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/user/town.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 37,  133 => 35,  128 => 32,  116 => 28,  108 => 25,  105 => 24,  101 => 23,  96 => 21,  92 => 20,  77 => 8,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}{{ 'Villes de travaille' }}{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-12\">
    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
            <div class=\"card-header\">
            <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
            </div>
        </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Villes de travaille</h5>
        </div>
        <div class=\"card-body\">
            <p>Veuillez séléctionner les villes où le travailleur <b>{{ employee.firstname ~ ' ' ~ employee.lastname }}</b> pourra travailler</p>
            {{ form_start(townForm) }}
                <div class=\"container\">
                {% for town in towns %}
                    <div class=\"checkbox\">
                        <input type=\"checkbox\" name=\"{{ town.id }}\" id=\"{{ town.name }}\" />
                        <div class=\"box\">
                            <i class=\"align-middle\" data-feather=\"map-pin\"></i>
                            <p data-text=\"{{ town.name }}\">{{ town.name }}</p>
                        </div>
                    </div>
                {% endfor %}
                </div>
                <br>
                <div style=\"display:flex; align-items:end;justify-content:end\">
                    {{ form_widget(townForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer ces villes \"} }) }}
                </div>
            {{ form_end(townForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "admin/user/town.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/user/town.html.twig");
    }
}
