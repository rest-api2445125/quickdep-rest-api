<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/auth/contract.html.twig */
class __TwigTemplate_1b6bfe1dec5c92623d1450dd3333a0d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/auth/contract.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "client_web/auth/contract.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">";
        // line 7
        if ((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 7, $this->source); })())) {
            echo "Signer le <b>contrat de facturation</b>";
        } else {
            echo "Bienvenue chez <b>QuickDep</b>";
        }
        echo "</h1>
    <p class=\"lead\">
        Veuillez lire attentivement puis signer le contrat
    </p>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 15
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    
    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 24));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 25
            echo "     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 27
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            <div class=\"text-center\">
                <p style=\"font-weight:bold\">Contract de facturation</p>
                <ol style=\"text-align: left;\">
                    <li>J’offre mes services à la Compagnie QuickDep Inc. en tant que travailleur autonome.</li>
                    <li>Le travailleur a le droit de travailler légalement au Canada.</li>
                    <li>Le travailleur autonome s’engage à respecter ses engagements et complété ses quarts de travail confirmés.</li>
                    <li>Il est interdit de partager ou de parler de son salaire sur son lieu de travail.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id=\"signature-pad\" class=\"signature-pad\">
    <div class=\"description\" style=\"font-size:13px\">Signer le contrat</div>
    <br>
    <div class=\"signature-pad--body\">
      <canvas></canvas> 
    </div>
    <div class=\"signature-pad--footer\">

      <div class=\"signature-pad--actions\">
        <div class=\"column\">
          <button type=\"button\" class=\"clear\" data-action=\"clear\">Effacer</button>
        </div>
      </div>
    </div>
</div>

<div class=\"text-center mt-3\">
    <button type=\"button\" class=\"button\" data-action=\"save-png\">
        <span class=\"button__text\">Enrégistrer</span>
    </button><br>
    <a class=\"btn btn-default mt-2\" href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home");
        echo "\" style=\"border:1px solid #ccc\">";
        echo (((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 73, $this->source); })())) ? ("Retour à l'accueil") : ("Ignorer pour le moment"));
        echo "</a>
    <br>
    <br>
</div>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/auth/contract.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 73,  135 => 35,  121 => 27,  117 => 25,  113 => 24,  110 => 23,  96 => 15,  92 => 13,  88 => 12,  76 => 7,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">{% if (profile) %}Signer le <b>contrat de facturation</b>{% else %}Bienvenue chez <b>QuickDep</b>{% endif %}</h1>
    <p class=\"lead\">
        Veuillez lire attentivement puis signer le contrat
    </p>
    
    {% for message in app.flashes('notice') %}
     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
    
    {% for message in app.flashes('success') %}
     <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            <div class=\"text-center\">
                <p style=\"font-weight:bold\">Contract de facturation</p>
                <ol style=\"text-align: left;\">
                    <li>J’offre mes services à la Compagnie QuickDep Inc. en tant que travailleur autonome.</li>
                    <li>Le travailleur a le droit de travailler légalement au Canada.</li>
                    <li>Le travailleur autonome s’engage à respecter ses engagements et complété ses quarts de travail confirmés.</li>
                    <li>Il est interdit de partager ou de parler de son salaire sur son lieu de travail.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id=\"signature-pad\" class=\"signature-pad\">
    <div class=\"description\" style=\"font-size:13px\">Signer le contrat</div>
    <br>
    <div class=\"signature-pad--body\">
      <canvas></canvas> 
    </div>
    <div class=\"signature-pad--footer\">

      <div class=\"signature-pad--actions\">
        <div class=\"column\">
          <button type=\"button\" class=\"clear\" data-action=\"clear\">Effacer</button>
        </div>
      </div>
    </div>
</div>

<div class=\"text-center mt-3\">
    <button type=\"button\" class=\"button\" data-action=\"save-png\">
        <span class=\"button__text\">Enrégistrer</span>
    </button><br>
    <a class=\"btn btn-default mt-2\" href=\"{{ path('app_client_web_home') }}\" style=\"border:1px solid #ccc\">{{ profile ? 'Retour à l\\'accueil': 'Ignorer pour le moment' }}</a>
    <br>
    <br>
</div>

{% endblock %}
", "client_web/auth/contract.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/auth/contract.html.twig");
    }
}
