<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/Contract.html.twig */
class __TwigTemplate_54db865331585e679fd6dc704ec41ece extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/Contract.html.twig"));

        // line 2
        echo "<div class=\"col-12 col-md-6 col-lg-3\">
    <a href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 3, $this->source); })()), ["id" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 3, $this->source); })()), "id", [], "any", false, false, false, 3)]), "html", null, true);
        echo "\"  class='contract-card-link'>
    <div class=\"card contract-card\" style=\"cursor:pointer\">
        <div class=\"card-header\" style=\"display:flex\">
            <img
                src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 7, $this->source); })())), "html", null, true);
        echo "\"
                class=\"avatar img-fluid rounded me-1\"
                alt=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 9, $this->source); })()), "html", null, true);
        echo "\"
                style=\"width:60px; height:60px\"
            />
            <div style=\"display:inline-block;margin-left:10px\">
                ";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 13, $this->source); })()), "session", [], "any", false, false, false, 13), "get", [0 => "type"], "method", false, false, false, 13) == "admin")) {
            // line 14
            echo "                <h5 class=\"mb-0\" style=\"font-size:20px; font-weight:bold\">";
            echo twig_escape_filter($this->env, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 14, $this->source); })()), "html", null, true);
            echo "</h5>
                ";
        }
        // line 16
        echo "                <h5 class=\"card-title mb-0\" style=\"\">";
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 16, $this->source); })()), "html", null, true);
        echo "</h5>
                <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 17, $this->source); })()), "code", [], "any", false, false, false, 17), "html", null, true);
        echo "</p>
            </div>
        </div>
        <div class=\"card-body\" style=\"padding-top:0\">
            <div class=\"mb-2\" style=\"display:flex\">
                <i class=\"align-middle\" data-feather=\"calendar\" style=\"";
        // line 22
        echo (((isset($context["today"]) || array_key_exists("today", $context) ? $context["today"] : (function () { throw new RuntimeError('Variable "today" does not exist.', 22, $this->source); })())) ? ("color:red") : (""));
        echo "\"></i>
                <h5 class=\"mb-0\" style=\"display:inline; margin-left:10px;; font-size:13px; ";
        // line 23
        echo (((isset($context["today"]) || array_key_exists("today", $context) ? $context["today"] : (function () { throw new RuntimeError('Variable "today" does not exist.', 23, $this->source); })())) ? ("color:red;") : (""));
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["date"]) || array_key_exists("date", $context) ? $context["date"] : (function () { throw new RuntimeError('Variable "date" does not exist.', 23, $this->source); })()), "html", null, true);
        echo "</h5>
            </div>
            <div style=\"display:flex; align-items:end; justify-content:space-between\">
                <div>
                    <div style=\"display:flex\">
                        <i class=\"align-middle\" data-feather=\"clock\"></i>
                        <h5 style=\"display:inline; margin-left:10px; font-size:13px\">";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["hours"]) || array_key_exists("hours", $context) ? $context["hours"] : (function () { throw new RuntimeError('Variable "hours" does not exist.', 29, $this->source); })()), "html", null, true);
        echo "</h5>
                    </div>
                </div>
                ";
        // line 32
        if ((isset($context["consecutive"]) || array_key_exists("consecutive", $context) ? $context["consecutive"] : (function () { throw new RuntimeError('Variable "consecutive" does not exist.', 32, $this->source); })())) {
            // line 33
            echo "                <div class=\"btn-success btn \" style=\"padding:5px 10px;color:white;border-radius:20px; font-size:10px\">
                    <p style=\"margin:0\">Shifts Consécutifs</p>
                </div>
                ";
        }
        // line 37
        echo "            </div>
            
                <hr style=\"opacity:0.1\">
            <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between;\">
                <div>
                    <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["rate"]) || array_key_exists("rate", $context) ? $context["rate"] : (function () { throw new RuntimeError('Variable "rate" does not exist.', 42, $this->source); })()), "html", null, true);
        echo "</h4>
                    <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["bonus"]) || array_key_exists("bonus", $context) ? $context["bonus"] : (function () { throw new RuntimeError('Variable "bonus" does not exist.', 43, $this->source); })()), "html", null, true);
        echo "</h6>
                </div>
                ";
        // line 45
        if ((isset($context["applied"]) || array_key_exists("applied", $context) ? $context["applied"] : (function () { throw new RuntimeError('Variable "applied" does not exist.', 45, $this->source); })())) {
            // line 46
            echo "                <div style=\"display:flex\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    <i class=\"align-middle\" data-feather=\"users\" style=\"margin-right:5px\"></i>
                    <p style=\"margin:0\">";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 49, $this->source); })()), "getAppliesCount", [], "any", false, false, false, 49), "html", null, true);
            echo "</p>
                </div>
                ";
        }
        // line 52
        echo "                ";
        if ((isset($context["approuved"]) || array_key_exists("approuved", $context) ? $context["approuved"] : (function () { throw new RuntimeError('Variable "approuved" does not exist.', 52, $this->source); })())) {
            // line 53
            echo "                <div style=\"display:flex\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                ";
        }
        // line 57
        echo "            </div>
            
        </div>
    </div>
    </a>
</div>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "components/Contract.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 57,  144 => 53,  141 => 52,  135 => 49,  130 => 46,  128 => 45,  123 => 43,  119 => 42,  112 => 37,  106 => 33,  104 => 32,  98 => 29,  87 => 23,  83 => 22,  75 => 17,  70 => 16,  64 => 14,  62 => 13,  55 => 9,  50 => 7,  43 => 3,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# templates/components/Alert.html.twig #}
<div class=\"col-12 col-md-6 col-lg-3\">
    <a href=\"{{ path(path, {'id': contract.id})}}\"  class='contract-card-link'>
    <div class=\"card contract-card\" style=\"cursor:pointer\">
        <div class=\"card-header\" style=\"display:flex\">
            <img
                src=\"{{asset(image)}}\"
                class=\"avatar img-fluid rounded me-1\"
                alt=\"{{ title }}\"
                style=\"width:60px; height:60px\"
            />
            <div style=\"display:inline-block;margin-left:10px\">
                {% if app.session.get('type') == 'admin' %}
                <h5 class=\"mb-0\" style=\"font-size:20px; font-weight:bold\">{{ client }}</h5>
                {% endif %}
                <h5 class=\"card-title mb-0\" style=\"\">{{ title }}</h5>
                <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ contract.code }}</p>
            </div>
        </div>
        <div class=\"card-body\" style=\"padding-top:0\">
            <div class=\"mb-2\" style=\"display:flex\">
                <i class=\"align-middle\" data-feather=\"calendar\" style=\"{{ today ? 'color:red':''}}\"></i>
                <h5 class=\"mb-0\" style=\"display:inline; margin-left:10px;; font-size:13px; {{ today ? 'color:red;': ''}}\">{{ date }}</h5>
            </div>
            <div style=\"display:flex; align-items:end; justify-content:space-between\">
                <div>
                    <div style=\"display:flex\">
                        <i class=\"align-middle\" data-feather=\"clock\"></i>
                        <h5 style=\"display:inline; margin-left:10px; font-size:13px\">{{ hours }}</h5>
                    </div>
                </div>
                {% if consecutive %}
                <div class=\"btn-success btn \" style=\"padding:5px 10px;color:white;border-radius:20px; font-size:10px\">
                    <p style=\"margin:0\">Shifts Consécutifs</p>
                </div>
                {% endif %}
            </div>
            
                <hr style=\"opacity:0.1\">
            <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between;\">
                <div>
                    <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ rate }}</h4>
                    <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">{{ bonus }}</h6>
                </div>
                {% if applied %}
                <div style=\"display:flex\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    <i class=\"align-middle\" data-feather=\"users\" style=\"margin-right:5px\"></i>
                    <p style=\"margin:0\">{{ contract.getAppliesCount }}</p>
                </div>
                {% endif %}
                {% if approuved %}
                <div style=\"display:flex\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                {% endif %}
            </div>
            
        </div>
    </div>
    </a>
</div>", "components/Contract.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/components/Contract.html.twig");
    }
}
