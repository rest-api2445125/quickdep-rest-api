<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/base.html.twig */
class __TwigTemplate_aecfea3c8676fc4b3d92df67531e4dbc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
    <meta
      name=\"viewport\"
      content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"
    />
    <meta
      name=\"description\"
      content=\"QuickDep &amp; Tableau de bord\"
    />
    <meta name=\"author\" content=\"QuickDep\" />

    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" />
    <link rel=\"shortcut icon\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/favicon.png"), "html", null, true);
        echo "\" />


    <title>QuickDep | ";
        // line 20
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/css/app.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link
      href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap\"
      rel=\"stylesheet\"
    />
    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/alerts/css/alerts-css.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />

    <link rel=\"stylesheet\" 
        href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\" 
        integrity=\"sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg\" 
        crossorigin=\"anonymous\"
     >

     <style>
        .contract-card.disabled {
            
        }

        .inputfile {
          width: 0.1px;
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          z-index: -1;
        }

        .contract-card.disabled:hover {
            border: none;
        }
        .contract-card:hover {
            border: 1px solid #29216B;
        }
        .contract-card-link:hover {
            text-decoration:none;
            color:inherit;
        }
        .btn {
            padding: 10px 20px;
        }
        .btn.small {
            padding: 10px;
        }
        .status-box {
            border-radius: 30px;
            padding: 10px 20px;
        }
        .status-box.small {
            font-size: 12px;
            font-weight: bold;
            border-radius: 20px;
            padding: 5px 10px;
        }
        .status-box.waiting {
            background-color: #fae7c0;
            color:  orange;
        }
        .status-box.approuved {
            background-color: #c0fad0;
            color:  green;
        }
        .status-box p {
            margin: 0;
            font-weight:bold
        }
        .apply-card {
            padding-right:20px
        }
        .apply-card:last-child {
            padding-right:0
        }
        .apply-card > div{
            padding:10px;
            border-radius:10px;
            border: 1px solid #ccc;
        }
        .apply-card > div > div{
            display: flex;
            justify-content:space-between;
        }
        .ex-class {
            font-size:10px; 
            font-weight:bold;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 3px 10px;
            display: inline-block !important;
            margin-right: 10px
        }

        /** TABS */
        .tab{
          position:relative;
        }
        .tab li{
          float:left;
          overflow:hidden;
          list-style-type: none;
          display: block;
          cursor: pointer;
          position: relative;
          border-radius:5px;
        }

        .tab li a{
          display:block;
          text-decoration: none;
          border-radius:5px;
          padding:10px 20px;
        }

        .tab section{
          z-index:0;
          width:70%;
          padding:5px;
          position: absolute;
          top:70px;
          background: #f5f7fb;
        }

        .tab li:hover{
          font-weight:bold;
          color:black;
        }

        .tab section:first-child{
          z-index:1;
        }

        .tab section:target{
          z-index:2;
          margin:0;
        }
        .tab section:target + li{
          background:#29216B;
          color:white;
        }
        .tab section:target + li a{
          color:white;
        }

        /** PROGRESS SETPS */

        .container {
            display: flex;
            justify-content: start;
            flex-wrap: wrap;
            margin: 1em 0 0 0;
        }
        .checkbox {
            width: 200px;
            height: 60px;
            position: relative;
            margin: 1em 1em;
        }
        .checkbox input {
            position: absolute;
            cursor: pointer;
            width: 100%;
            height: 100%;
            z-index: 2;
            appearance: none;
            -webkit-appearance: none;
        }
        .box {
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 1;
            background: #f7f7f7;
            border: 2px solid #d1d1d1;
            color: rgb(63, 63, 63);
            border-radius: 5px;
            display: flex;
            justify-content: center;
            align-items: center;
            overflow: hidden;
            transition: all 0.6s;
        }
        .box i {
            margin-right: 0.3em;
        }
        .box p {
            transition: all 0.2s;
            margin: 0 0 0 10px;
        }
        .checkbox input:checked ~ .box p {
            transform: translateY(-40px);
        }
        .box p::before {
            content: attr(data-text);
            position: absolute;
            transform: translateY(40px);
        }
        .checkbox input:checked ~ .box p::before {
            transform: translateY(40px);
        }
        .checkbox input:checked ~ .box {
            background: #f7f7f7;
            border: 2px solid #185ADB;
            color: #185ADB;
        }

        
     </style>
    ";
        // line 227
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 230
        echo "
    ";
        // line 231
        $this->displayBlock('javascripts', $context, $blocks);
        // line 234
        echo "  </head>

  <body>
    <div class=\"wrapper\">
      <nav id=\"sidebar\" class=\"sidebar js-sidebar\">
        <div class=\"sidebar-content js-simplebar\">
          <a class=\"sidebar-brand\" href=\"#\">
            <span class=\"align-middle\">QuickDep | Admin</span>
          </a>

          <ul class=\"sidebar-nav\">
            <li class=\"sidebar-header\">Contrats</li>

            <li class=\"sidebar-item ";
        // line 247
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 247, $this->source); })()), "request", [], "any", false, false, false, 247), "get", [0 => "_route"], "method", false, false, false, 247) == "app_admin_contract")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 248
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_contract");
        echo "\">
                <i class=\"align-middle\" data-feather=\"list\"></i>
                <span class=\"align-middle\">Publiés</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 254
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 254, $this->source); })()), "request", [], "any", false, false, false, 254), "get", [0 => "_route"], "method", false, false, false, 254) == "app_admin_contract_waiting")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link \" href=\"";
        // line 255
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_contract_waiting");
        echo "\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">En attente d'approbation</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 261
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 261, $this->source); })()), "request", [], "any", false, false, false, 261), "get", [0 => "_route"], "method", false, false, false, 261) == "app_admin_contract_approuved")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 262
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_contract_approuved");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvés</span>
              </a>
            </li>

            ";
        // line 281
        echo "
            <li class=\"sidebar-header\">Feuilles de temps</li>

            <li class=\"sidebar-item ";
        // line 284
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 284, $this->source); })()), "request", [], "any", false, false, false, 284), "get", [0 => "_route"], "method", false, false, false, 284) == "app_admin_timesheet_todo")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 285
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_timesheet_todo");
        echo "\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À déclarer</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 291
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 291, $this->source); })()), "request", [], "any", false, false, false, 291), "get", [0 => "_route"], "method", false, false, false, 291) == "app_admin_timesheet")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 292
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_timesheet");
        echo "\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À valider</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 298
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 298, $this->source); })()), "request", [], "any", false, false, false, 298), "get", [0 => "_route"], "method", false, false, false, 298) == "app_admin_timesheet_approuved")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 299
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_timesheet_approuved");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Facturation</li>

            <li class=\"sidebar-item ";
        // line 307
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 307, $this->source); })()), "request", [], "any", false, false, false, 307), "get", [0 => "_route"], "method", false, false, false, 307) == "app_admin_invoice_client")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 308
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_invoice_client");
        echo "\">
                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                <span class=\"align-middle\">Entréprise</span>
              </a>
            </li>

            <li class=\"sidebar-itemm ";
        // line 314
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 314, $this->source); })()), "request", [], "any", false, false, false, 314), "get", [0 => "_route"], "method", false, false, false, 314) == "app_admin_invoice_employee")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 315
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_invoice_employee");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Travailleur</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Utilisateurs</li>

            <li class=\"sidebar-item ";
        // line 323
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 323, $this->source); })()), "request", [], "any", false, false, false, 323), "get", [0 => "_route"], "method", false, false, false, 323) == "app_admin_user")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 324
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_user");
        echo "\">
                <i class=\"align-middle\" data-feather=\"users\"></i>
                <span class=\"align-middle\">Travailleurs</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 330
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 330, $this->source); })()), "request", [], "any", false, false, false, 330), "get", [0 => "_route"], "method", false, false, false, 330) == "app_admin_client")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 331
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_client");
        echo "\">
                <i class=\"align-middle\" data-feather=\"briefcase\"></i>
                <span class=\"align-middle\">Entréprises</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Performances</li>

            <li class=\"sidebar-itemm ";
        // line 339
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 339, $this->source); })()), "request", [], "any", false, false, false, 339), "get", [0 => "_route"], "method", false, false, false, 339) == "app_admin_finance")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 340
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_finance");
        echo "\">
                <i class=\"align-middle\" data-feather=\"dollar-sign\"></i>
                <span class=\"align-middle\">Finance</span>
              </a>
            </li>

            <li class=\"sidebar-itemm ";
        // line 346
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 346, $this->source); })()), "request", [], "any", false, false, false, 346), "get", [0 => "_route"], "method", false, false, false, 346) == "app_admin_stats")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 347
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_stats");
        echo "\">
                <i class=\"align-middle\" data-feather=\"bar-chart-2\"></i>
                <span class=\"align-middle\">Statistiaues</span>
              </a>
            </li>

            ";
        // line 361
        echo "          </ul>
        </div>
      </nav>

      <div class=\"main\">
        <nav class=\"navbar navbar-expand navbar-light navbar-bg\">
          <a class=\"sidebar-toggle js-sidebar-toggle\">
            <i class=\"hamburger align-self-center\"></i>
          </a>

          <div class=\"navbar-collapse collapse\">
            <ul class=\"navbar-nav navbar-align\">
              <li class=\"nav-item dropdown\">
                <a
                  class=\"nav-icon dropdown-toggle d-inline-block d-sm-none\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <i class=\"align-middle\" data-feather=\"settings\"></i>
                </a>

                <a
                  class=\"nav-link dropdown-toggle d-none d-sm-inline-block\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <img
                    src=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/img/logo.png"), "html", null, true);
        echo "\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"";
        // line 390
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 390, $this->source); })()), "session", [], "any", false, false, false, 390), "get", [0 => "user"], "method", false, false, false, 390), "firstname", [], "any", false, false, false, 390), "html", null, true);
        echo "\"
                  />
                  <span class=\"text-dark\">";
        // line 392
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 392, $this->source); })()), "session", [], "any", false, false, false, 392), "get", [0 => "user"], "method", false, false, false, 392), "firstname", [], "any", false, false, false, 392), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 392, $this->source); })()), "session", [], "any", false, false, false, 392), "get", [0 => "user"], "method", false, false, false, 392), "lastname", [], "any", false, false, false, 392), "html", null, true);
        echo "</span>
                </a>
                <div class=\"dropdown-menu dropdown-menu-end\">
                  <a class=\"dropdown-item\" href=\"#\"
                    ><i class=\"align-middle me-1\" data-feather=\"user\"></i>
                    Profil</a
                  >
                  <div class=\"dropdown-divider\"></div>
                    <form  method=\"POST\" style=\"display:inline\" action=\"";
        // line 400
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
        echo "\">
                        <button type=\"submit\" class=\"dropdown-item\">Se decconecter</button>
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <main class=\"content\">
          <div class=\"container-fluid p-0\">

            ";
        // line 412
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 412, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 412));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 413
            echo "            <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
                <div class=\"alert--content\">
                    ";
            // line 415
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                <div class=\"alert--close\">
                    <i class=\"far fa-times-circle\"></i>
                </div>
            </div>
            </br>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 423
        echo "
            ";
        // line 424
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 424, $this->source); })()), "flashes", [0 => "error"], "method", false, false, false, 424));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 425
            echo "            <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
                <div class=\"alert--content\">
                    ";
            // line 427
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>
                <div class=\"alert--close\">
                    <i class=\"far fa-times-circle\"></i>
                </div>
            </div>
            </br>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 435
        echo "            ";
        $this->displayBlock('body', $context, $blocks);
        echo "  
          </div>
        </main>
      </div>
    </div>

  <script src=\"https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js\"></script>
    <script src=\"";
        // line 442
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/js/app.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 443
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/alerts/js/alerts.min.js"), "html", null, true);
        echo "\"></script>

     <script>
        
        let diffAdress = document.getElementById(\"diffAdress\")
        let newAddress = document.getElementById(\"newAddress\")
        
        let clothCheck = document.getElementById(\"clothCheck\")
        let clothDiv = document.getElementById(\"clothDiv\")

        let editTimeSheetBtn = document.getElementById(\"edit-time-sheet-btn\")
        let subminTsBtn = document.getElementById(\"time_sheet_submit\")

        let tsStartdate = document.getElementById(\"time_sheet_startdate\")
        let tsEnddate = document.getElementById(\"time_sheet_enddate\")

        if (diffAdress != null) {
            diffAdress.addEventListener('change', function() {
                if (this.checked) {
                    newAddress.style.display = \"block\"
                } else {
                    newAddress.style.display = \"none\"
                }
            });
        }

        if (clothCheck != null) {
            clothCheck.addEventListener('change', function() {
                if (this.checked) {
                    clothDiv.style.display = \"block\"
                } else {
                    clothDiv.style.display = \"none\"
                }
            });
        }

        if (editTimeSheetBtn != null) {
            editTimeSheetBtn.addEventListener('click', function(e) {
                e.preventDefault()
                
                subminTsBtn.style.display = \"inline-block\"
                this.style.display = \"none\"
                tsStartdate.removeAttribute('readonly')
                tsEnddate.removeAttribute('readonly')
            });
        }

        ";
        // line 490
        if (array_key_exists("user", $context)) {
            // line 491
            echo "      var inputs = document.querySelectorAll( '.inputfile' );
      Array.prototype.forEach.call( inputs, function( input )
      {
        var label\t = input.nextElementSibling,
          labelVal = label.innerHTML;

        input.addEventListener( 'change', async function( e )
        {
          var fileName = '';
          if( this.files && this.files.length > 1 ) {
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
          } else {
            fileName = e.target.value.split( '\\\\' ).pop();
            let preview = document.getElementById('img-prev')
            var src = URL.createObjectURL(event.target.files[0]);

            const domaineName = 'https://quickdep.ca'
            // const domaineName = 'http://localhost:8000'

            if (isFileImage(event.target.files[0])) {
              let request = await axios.post(domaineName + '/api/users/' + ";
            // line 511
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 511, $this->source); })()), "id", [], "any", false, false, false, 511), "html", null, true);
            echo " + '/image', {
                image: event.target.files[0]
              }, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              }).then((response) => {
                  if (response.data.success) {
                    ";
            // line 519
            if ((array_key_exists("profile", $context) && ((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 519, $this->source); })()) == 1))) {
                // line 520
                echo "                      window.location.replace(domaineName + \"/entreprise/profile\");
                    ";
            } else {
                // line 522
                echo "                      window.location.replace(domaineName + \"/entreprise\");
                    ";
            }
            // line 524
            echo "
                  }
                  preview.src = src;
              });
            } else {
              alert(\"Format du fichier invalide\")
            }
          }
          
        });
      });
        
      ";
        }
        // line 537
        echo "
      function isFileImage(file) {
          return file && file['type'].split('/')[0] === 'image';
      }

      let tabContent = document.querySelectorAll(\".container__inner\");
      let tabItem = document.querySelectorAll(\".container__item\");

      // For each element with class 'container__item'
      for (let i = 0; i < tabItem.length; i++) {
        // if the element was hovered
        //you can replace mouseover with click
        tabItem[i].addEventListener(\"click\", () => {
          // Add to all containers class 'container__inner_hidden'
          tabContent.forEach((item) => {
            item.classList.add(\"container__inner_hidden\");
          });
          // Clean all links from class 'container__item_active'
          tabItem.forEach((item) => {
            item.classList.remove(\"container__item_active\");
          });
          // Make visible correct tab content and add class to item
          tabContent[i].classList.remove(\"container__inner_hidden\");
          tabItem[i].classList.add(\"container__item_active\");
        });
      }

      let tsPause = document.getElementById(\"time_sheet_pause\")
      let tsDiff = document.getElementById(\"tsDiff\")

      if (tsStartdate != null) {
          tsStartdate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsEnddate != null) {
          tsEnddate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsPause != null) {
          tsPause.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      function changeDiff() {
        let startDate = Date.parse('2023-01-01T' + tsStartdate.value + ':00')
        let endDate = Date.parse('2023-01-01T' + tsEnddate.value + ':00')

        const index = tsPause.selectedIndex

        let pause = tsPause.options[index].text

        let sDate = new Date(startDate)
        let eDate = new Date(endDate)

        let diffInMillis = eDate.getTime() - sDate.getTime();

        if (pause != 'Pas de pause') {
          diffInMillis = diffInMillis - (parseInt(pause.replace('min', '')) * 60 * 1000);
        }
        const diffInHours = Math.floor(diffInMillis / 3600000);
        const diffInMinutes = Math.round((diffInMillis % 3600000) / 60000);
                        
        const formattedHours = diffInHours.toString().padStart(2, '0');
        const formattedMinutes = diffInMinutes.toString().padStart(2, '0');

        let diff = (formattedHours == 'NaN' ? '-' : formattedHours) + \"H\" + (formattedMinutes == 'NaN' ? '-' : formattedMinutes)
        tsDiff.innerText = diff
      }

        
     </script>
  </body>
</html>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 20
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 227
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 228
        echo "        ";
        echo twig_escape_filter($this->env, $this->env->getFunction('encore_entry_link_tags')->getCallable()("app"), "html", null, true);
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 231
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 232
        echo "        ";
        echo twig_escape_filter($this->env, $this->env->getFunction('encore_entry_script_tags')->getCallable()("app"), "html", null, true);
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 435
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  842 => 435,  832 => 232,  825 => 231,  815 => 228,  808 => 227,  796 => 20,  712 => 537,  697 => 524,  693 => 522,  689 => 520,  687 => 519,  676 => 511,  654 => 491,  652 => 490,  602 => 443,  598 => 442,  587 => 435,  573 => 427,  569 => 425,  565 => 424,  562 => 423,  548 => 415,  544 => 413,  540 => 412,  525 => 400,  512 => 392,  507 => 390,  502 => 388,  473 => 361,  464 => 347,  460 => 346,  451 => 340,  447 => 339,  436 => 331,  432 => 330,  423 => 324,  419 => 323,  408 => 315,  404 => 314,  395 => 308,  391 => 307,  380 => 299,  376 => 298,  367 => 292,  363 => 291,  354 => 285,  350 => 284,  345 => 281,  336 => 262,  332 => 261,  323 => 255,  319 => 254,  310 => 248,  306 => 247,  291 => 234,  289 => 231,  286 => 230,  284 => 227,  81 => 27,  73 => 22,  68 => 20,  62 => 17,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
    <meta
      name=\"viewport\"
      content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"
    />
    <meta
      name=\"description\"
      content=\"QuickDep &amp; Tableau de bord\"
    />
    <meta name=\"author\" content=\"QuickDep\" />

    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" />
    <link rel=\"shortcut icon\" href=\"{{ asset('web/assets/favicon.png') }}\" />


    <title>QuickDep | {% block title %}{% endblock %}</title>

    <link href=\"{{asset('clientweb/css/app.css')}}\" rel=\"stylesheet\" />
    <link
      href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap\"
      rel=\"stylesheet\"
    />
    <link href=\"{{asset('clientweb/alerts/css/alerts-css.min.css')}}\" rel=\"stylesheet\" />

    <link rel=\"stylesheet\" 
        href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\" 
        integrity=\"sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg\" 
        crossorigin=\"anonymous\"
     >

     <style>
        .contract-card.disabled {
            
        }

        .inputfile {
          width: 0.1px;
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          z-index: -1;
        }

        .contract-card.disabled:hover {
            border: none;
        }
        .contract-card:hover {
            border: 1px solid #29216B;
        }
        .contract-card-link:hover {
            text-decoration:none;
            color:inherit;
        }
        .btn {
            padding: 10px 20px;
        }
        .btn.small {
            padding: 10px;
        }
        .status-box {
            border-radius: 30px;
            padding: 10px 20px;
        }
        .status-box.small {
            font-size: 12px;
            font-weight: bold;
            border-radius: 20px;
            padding: 5px 10px;
        }
        .status-box.waiting {
            background-color: #fae7c0;
            color:  orange;
        }
        .status-box.approuved {
            background-color: #c0fad0;
            color:  green;
        }
        .status-box p {
            margin: 0;
            font-weight:bold
        }
        .apply-card {
            padding-right:20px
        }
        .apply-card:last-child {
            padding-right:0
        }
        .apply-card > div{
            padding:10px;
            border-radius:10px;
            border: 1px solid #ccc;
        }
        .apply-card > div > div{
            display: flex;
            justify-content:space-between;
        }
        .ex-class {
            font-size:10px; 
            font-weight:bold;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 3px 10px;
            display: inline-block !important;
            margin-right: 10px
        }

        /** TABS */
        .tab{
          position:relative;
        }
        .tab li{
          float:left;
          overflow:hidden;
          list-style-type: none;
          display: block;
          cursor: pointer;
          position: relative;
          border-radius:5px;
        }

        .tab li a{
          display:block;
          text-decoration: none;
          border-radius:5px;
          padding:10px 20px;
        }

        .tab section{
          z-index:0;
          width:70%;
          padding:5px;
          position: absolute;
          top:70px;
          background: #f5f7fb;
        }

        .tab li:hover{
          font-weight:bold;
          color:black;
        }

        .tab section:first-child{
          z-index:1;
        }

        .tab section:target{
          z-index:2;
          margin:0;
        }
        .tab section:target + li{
          background:#29216B;
          color:white;
        }
        .tab section:target + li a{
          color:white;
        }

        /** PROGRESS SETPS */

        .container {
            display: flex;
            justify-content: start;
            flex-wrap: wrap;
            margin: 1em 0 0 0;
        }
        .checkbox {
            width: 200px;
            height: 60px;
            position: relative;
            margin: 1em 1em;
        }
        .checkbox input {
            position: absolute;
            cursor: pointer;
            width: 100%;
            height: 100%;
            z-index: 2;
            appearance: none;
            -webkit-appearance: none;
        }
        .box {
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 1;
            background: #f7f7f7;
            border: 2px solid #d1d1d1;
            color: rgb(63, 63, 63);
            border-radius: 5px;
            display: flex;
            justify-content: center;
            align-items: center;
            overflow: hidden;
            transition: all 0.6s;
        }
        .box i {
            margin-right: 0.3em;
        }
        .box p {
            transition: all 0.2s;
            margin: 0 0 0 10px;
        }
        .checkbox input:checked ~ .box p {
            transform: translateY(-40px);
        }
        .box p::before {
            content: attr(data-text);
            position: absolute;
            transform: translateY(40px);
        }
        .checkbox input:checked ~ .box p::before {
            transform: translateY(40px);
        }
        .checkbox input:checked ~ .box {
            background: #f7f7f7;
            border: 2px solid #185ADB;
            color: #185ADB;
        }

        
     </style>
    {% block stylesheets %}
        {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
        {{ encore_entry_script_tags('app') }}
    {% endblock %}
  </head>

  <body>
    <div class=\"wrapper\">
      <nav id=\"sidebar\" class=\"sidebar js-sidebar\">
        <div class=\"sidebar-content js-simplebar\">
          <a class=\"sidebar-brand\" href=\"#\">
            <span class=\"align-middle\">QuickDep | Admin</span>
          </a>

          <ul class=\"sidebar-nav\">
            <li class=\"sidebar-header\">Contrats</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_contract' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_admin_contract') }}\">
                <i class=\"align-middle\" data-feather=\"list\"></i>
                <span class=\"align-middle\">Publiés</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_contract_waiting' ? 'active': '' }}\">
              <a class=\"sidebar-link \" href=\"{{ path('app_admin_contract_waiting') }}\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">En attente d'approbation</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_contract_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_admin_contract_approuved') }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvés</span>
              </a>
            </li>

            {# <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_contract_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"#\">
                <i class=\"align-middle\" data-feather=\"trash-2\"></i>
                <span class=\"align-middle\">Annulés</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_contract_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"\">
                <i class=\"align-middle\" data-feather=\"user-x\"></i>
                <span class=\"align-middle\">Employée absent</span>
              </a>
            </li> #}

            <li class=\"sidebar-header\">Feuilles de temps</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_timesheet_todo' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_timesheet_todo\") }}\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À déclarer</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_timesheet' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_timesheet\") }}\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À valider</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_timesheet_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_timesheet_approuved\") }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Facturation</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_invoice_client' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_invoice_client\") }}\">
                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                <span class=\"align-middle\">Entréprise</span>
              </a>
            </li>

            <li class=\"sidebar-itemm {{ app.request.get('_route') == 'app_admin_invoice_employee' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_invoice_employee\") }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Travailleur</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Utilisateurs</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_user' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_user\") }}\">
                <i class=\"align-middle\" data-feather=\"users\"></i>
                <span class=\"align-middle\">Travailleurs</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_admin_client' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_client\") }}\">
                <i class=\"align-middle\" data-feather=\"briefcase\"></i>
                <span class=\"align-middle\">Entréprises</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Performances</li>

            <li class=\"sidebar-itemm {{ app.request.get('_route') == 'app_admin_finance' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_finance\") }}\">
                <i class=\"align-middle\" data-feather=\"dollar-sign\"></i>
                <span class=\"align-middle\">Finance</span>
              </a>
            </li>

            <li class=\"sidebar-itemm {{ app.request.get('_route') == 'app_admin_stats' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path(\"app_admin_stats\") }}\">
                <i class=\"align-middle\" data-feather=\"bar-chart-2\"></i>
                <span class=\"align-middle\">Statistiaues</span>
              </a>
            </li>

            {# <li class=\"sidebar-header\">Compte</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_home_profile' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"#\">
                <i class=\"align-middle\" data-feather=\"user\"></i>
                <span class=\"align-middle\">Profile</span>
              </a>
            </li> #}
          </ul>
        </div>
      </nav>

      <div class=\"main\">
        <nav class=\"navbar navbar-expand navbar-light navbar-bg\">
          <a class=\"sidebar-toggle js-sidebar-toggle\">
            <i class=\"hamburger align-self-center\"></i>
          </a>

          <div class=\"navbar-collapse collapse\">
            <ul class=\"navbar-nav navbar-align\">
              <li class=\"nav-item dropdown\">
                <a
                  class=\"nav-icon dropdown-toggle d-inline-block d-sm-none\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <i class=\"align-middle\" data-feather=\"settings\"></i>
                </a>

                <a
                  class=\"nav-link dropdown-toggle d-none d-sm-inline-block\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <img
                    src=\"{{asset('clientweb/img/logo.png')}}\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"{{ app.session.get('user').firstname }}\"
                  />
                  <span class=\"text-dark\">{{app.session.get('user').firstname}} {{app.session.get('user').lastname}}</span>
                </a>
                <div class=\"dropdown-menu dropdown-menu-end\">
                  <a class=\"dropdown-item\" href=\"#\"
                    ><i class=\"align-middle me-1\" data-feather=\"user\"></i>
                    Profil</a
                  >
                  <div class=\"dropdown-divider\"></div>
                    <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('logout') }}\">
                        <button type=\"submit\" class=\"dropdown-item\">Se decconecter</button>
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <main class=\"content\">
          <div class=\"container-fluid p-0\">

            {% for message in app.flashes('success') %}
            <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
                <div class=\"alert--content\">
                    {{ message }}
                </div>
                <div class=\"alert--close\">
                    <i class=\"far fa-times-circle\"></i>
                </div>
            </div>
            </br>
            {% endfor %}

            {% for message in app.flashes('error') %}
            <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
                <div class=\"alert--content\">
                    {{ message }}
                </div>
                <div class=\"alert--close\">
                    <i class=\"far fa-times-circle\"></i>
                </div>
            </div>
            </br>
            {% endfor %}
            {% block body %}{% endblock %}  
          </div>
        </main>
      </div>
    </div>

  <script src=\"https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js\"></script>
    <script src=\"{{ asset('clientweb/js/app.js') }}\"></script>
\t<script src=\"{{asset('clientweb/alerts/js/alerts.min.js')}}\"></script>

     <script>
        
        let diffAdress = document.getElementById(\"diffAdress\")
        let newAddress = document.getElementById(\"newAddress\")
        
        let clothCheck = document.getElementById(\"clothCheck\")
        let clothDiv = document.getElementById(\"clothDiv\")

        let editTimeSheetBtn = document.getElementById(\"edit-time-sheet-btn\")
        let subminTsBtn = document.getElementById(\"time_sheet_submit\")

        let tsStartdate = document.getElementById(\"time_sheet_startdate\")
        let tsEnddate = document.getElementById(\"time_sheet_enddate\")

        if (diffAdress != null) {
            diffAdress.addEventListener('change', function() {
                if (this.checked) {
                    newAddress.style.display = \"block\"
                } else {
                    newAddress.style.display = \"none\"
                }
            });
        }

        if (clothCheck != null) {
            clothCheck.addEventListener('change', function() {
                if (this.checked) {
                    clothDiv.style.display = \"block\"
                } else {
                    clothDiv.style.display = \"none\"
                }
            });
        }

        if (editTimeSheetBtn != null) {
            editTimeSheetBtn.addEventListener('click', function(e) {
                e.preventDefault()
                
                subminTsBtn.style.display = \"inline-block\"
                this.style.display = \"none\"
                tsStartdate.removeAttribute('readonly')
                tsEnddate.removeAttribute('readonly')
            });
        }

        {% if user is defined %}
      var inputs = document.querySelectorAll( '.inputfile' );
      Array.prototype.forEach.call( inputs, function( input )
      {
        var label\t = input.nextElementSibling,
          labelVal = label.innerHTML;

        input.addEventListener( 'change', async function( e )
        {
          var fileName = '';
          if( this.files && this.files.length > 1 ) {
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
          } else {
            fileName = e.target.value.split( '\\\\' ).pop();
            let preview = document.getElementById('img-prev')
            var src = URL.createObjectURL(event.target.files[0]);

            const domaineName = 'https://quickdep.ca'
            // const domaineName = 'http://localhost:8000'

            if (isFileImage(event.target.files[0])) {
              let request = await axios.post(domaineName + '/api/users/' + {{ user.id }} + '/image', {
                image: event.target.files[0]
              }, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              }).then((response) => {
                  if (response.data.success) {
                    {% if profile is defined and profile == 1 %}
                      window.location.replace(domaineName + \"/entreprise/profile\");
                    {% else %}
                      window.location.replace(domaineName + \"/entreprise\");
                    {% endif %}

                  }
                  preview.src = src;
              });
            } else {
              alert(\"Format du fichier invalide\")
            }
          }
          
        });
      });
        
      {% endif %}

      function isFileImage(file) {
          return file && file['type'].split('/')[0] === 'image';
      }

      let tabContent = document.querySelectorAll(\".container__inner\");
      let tabItem = document.querySelectorAll(\".container__item\");

      // For each element with class 'container__item'
      for (let i = 0; i < tabItem.length; i++) {
        // if the element was hovered
        //you can replace mouseover with click
        tabItem[i].addEventListener(\"click\", () => {
          // Add to all containers class 'container__inner_hidden'
          tabContent.forEach((item) => {
            item.classList.add(\"container__inner_hidden\");
          });
          // Clean all links from class 'container__item_active'
          tabItem.forEach((item) => {
            item.classList.remove(\"container__item_active\");
          });
          // Make visible correct tab content and add class to item
          tabContent[i].classList.remove(\"container__inner_hidden\");
          tabItem[i].classList.add(\"container__item_active\");
        });
      }

      let tsPause = document.getElementById(\"time_sheet_pause\")
      let tsDiff = document.getElementById(\"tsDiff\")

      if (tsStartdate != null) {
          tsStartdate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsEnddate != null) {
          tsEnddate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsPause != null) {
          tsPause.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      function changeDiff() {
        let startDate = Date.parse('2023-01-01T' + tsStartdate.value + ':00')
        let endDate = Date.parse('2023-01-01T' + tsEnddate.value + ':00')

        const index = tsPause.selectedIndex

        let pause = tsPause.options[index].text

        let sDate = new Date(startDate)
        let eDate = new Date(endDate)

        let diffInMillis = eDate.getTime() - sDate.getTime();

        if (pause != 'Pas de pause') {
          diffInMillis = diffInMillis - (parseInt(pause.replace('min', '')) * 60 * 1000);
        }
        const diffInHours = Math.floor(diffInMillis / 3600000);
        const diffInMinutes = Math.round((diffInMillis % 3600000) / 60000);
                        
        const formattedHours = diffInHours.toString().padStart(2, '0');
        const formattedMinutes = diffInMinutes.toString().padStart(2, '0');

        let diff = (formattedHours == 'NaN' ? '-' : formattedHours) + \"H\" + (formattedMinutes == 'NaN' ? '-' : formattedMinutes)
        tsDiff.innerText = diff
      }

        
     </script>
  </body>
</html>", "admin/base.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/base.html.twig");
    }
}
