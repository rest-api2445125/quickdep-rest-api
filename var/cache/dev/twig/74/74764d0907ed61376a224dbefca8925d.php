<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/invoice/detail.html.twig */
class __TwigTemplate_9c533946233275c0692f62095293ac18 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((((array_key_exists("type", $context) && ((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new RuntimeError('Variable "type" does not exist.', 1, $this->source); })()) == "admin"))) ? ("admin/base.html.twig") : ("client_web/base.html.twig")), "client_web/invoice/detail.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/invoice/detail.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Detail de la facture";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <div>
        <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("invoices/pdf/" . twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 16, $this->source); })()), "code", [], "any", false, false, false, 16)) . ".pdf")), "html", null, true);
        echo "\" target=\"_blank\" onclick=\"return confirm('Etes-vous sûr de vouloir télécharger cette facture ?')\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"file\"></i> Télécharger (.pdf)</a>
       
        ";
        // line 18
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "session", [], "any", false, false, false, 18), "get", [0 => "type"], "method", false, false, false, 18) == "admin") && (twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 18, $this->source); })()), "paid", [], "any", false, false, false, 18) != true))) {
            // line 19
            echo "        <form  method=\"POST\" style=\"display:inline;margin-left:10px\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_validate_invoice", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 19, $this->source); })()), "id", [], "any", false, false, false, 19)]), "html", null, true);
            echo "\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que cette facture est payée ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"check\"></i> Payer</button>
        </form>
        ";
        }
        // line 22
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "session", [], "any", false, false, false, 22), "get", [0 => "type"], "method", false, false, false, 22) == "admin") && (twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 22, $this->source); })()), "paid", [], "any", false, false, false, 22) == true))) {
            // line 23
            echo "        <form  method=\"POST\" style=\"display:inline;margin-left:10px\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_cancel_invoice", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 23, $this->source); })()), "id", [], "any", false, false, false, 23)]), "html", null, true);
            echo "\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que cette facture n\\'est pas payée ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"x\"></i> Non payée</button>
        </form>
        ";
        }
        // line 27
        echo "    </div>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "request", [], "any", false, false, false, 34), "headers", [], "any", false, false, false, 34), "get", [0 => "referer"], "method", false, false, false, 34), "html", null, true);
        echo "\" class=\"invoice-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Detail de la facture</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 49, $this->source); })()), "user", [], "any", false, false, false, 49), "imagePath", [], "any", false, false, false, 49))), "html", null, true);
        echo "\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"";
        // line 51
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 51, $this->source); })()), "user", [], "any", false, false, false, 51), "email", [], "any", false, false, false, 51), "html", null, true);
        echo "\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">\"Facture - \" ";
        // line 55
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 55, $this->source); })()), "code", [], "any", false, false, false, 55), "html", null, true);
        echo "</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
        // line 56
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 56, $this->source); })()), "code", [], "any", false, false, false, 56), "html", null, true);
        echo "</p>
                    </div>
                </div>
                ";
        // line 59
        if (twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 59, $this->source); })()), "paid", [], "any", false, false, false, 59)) {
            // line 60
            echo "                    <div style=\"display:flex; margin-left: 30px\">
                        <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                    </div>
                    <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                        <p>Payée</p>
                    </div>
                ";
        } else {
            // line 67
            echo "                    <div style=\"display:flex; margin-left: 30px\">
                        <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    </div>
                    <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                        <p>En attente du paiement</p>
                    </div>
                ";
        }
        // line 74
        echo "            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>";
        // line 82
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 82, $this->source); })()), "date", [], "any", false, false, false, 82), "d/m/Y"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Période</p>                    
                    <p>Semaine du ";
        // line 89
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 89, $this->source); })()), "start", [], "any", false, false, false, 89), "d/m/Y"), "html", null, true);
        echo " au ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 89, $this->source); })()), "end", [], "any", false, false, false, 89), "d/m/Y"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total à payer</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
        // line 98
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 98, $this->source); })()), "total", [], "any", false, false, false, 98) . "\$"), "html", null, true);
        echo "</h4>
                    </div>                
                </div>
            </div>
        </div>
    </div>
     <div class=\"card flex-fill\">
        <div class=\"card-header\">

            <h5 class=\"card-title mb-0\">Contrats</h5>
        </div>
        <table class=\"table table-hover my-0\">
            <thead>
                <tr>
                    <th>#</th>
                    <th class=\"d-none d-xl-table-cell\">Poste</th>
                    <th class=\"d-none d-xl-table-cell\">Date</th>
                    <th class=\"d-none d-xl-table-cell\">Nb Heures</th>
                    <th class=\"d-none d-md-table-cell\">Taux/H</th>
                    <th class=\"d-none d-md-table-cell\">Bonus</th>
                    <th class=\"d-none d-md-table-cell\">Total</th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 122
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["invoice"]) || array_key_exists("invoice", $context) ? $context["invoice"] : (function () { throw new RuntimeError('Variable "invoice" does not exist.', 122, $this->source); })()), "contracts", [], "any", false, false, false, 122));
        foreach ($context['_seq'] as $context["_key"] => $context["contract"]) {
            // line 123
            echo "                <tr>
                    <td>";
            // line 124
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "code", [], "any", false, false, false, 124), "html", null, true);
            echo "</td>
                    <td>";
            // line 125
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "job", [], "any", false, false, false, 125), "title", [], "any", false, false, false, 125), "html", null, true);
            echo "</td>
                    <td>";
            // line 126
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "startdate", [], "any", false, false, false, 126), "d/m/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 127
            echo twig_escape_filter($this->env, (int) floor((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "timesheet", [], "any", false, false, false, 127), "totaltimes", [], "any", false, false, false, 127) / 60)), "html", null, true);
            echo "H";
            ((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "timesheet", [], "any", false, false, false, 127), "totaltimes", [], "any", false, false, false, 127) % 60) == 0)) ? (print ("")) : (print (twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "timesheet", [], "any", false, false, false, 127), "totaltimes", [], "any", false, false, false, 127) / 60) - (int) floor((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "timesheet", [], "any", false, false, false, 127), "totaltimes", [], "any", false, false, false, 127) / 60))) * 60), "html", null, true))));
            echo "</td>
                    <td>";
            // line 128
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["contract"], "clientrate", [], "any", false, false, false, 128) . "\$/H "), "html", null, true);
            echo "</td>
                    <td>";
            // line 129
            (((twig_get_attribute($this->env, $this->source, $context["contract"], "bonus", [], "any", false, false, false, 129) != null)) ? (print (twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["contract"], "bonus", [], "any", false, false, false, 129) . "\$/H"), "html", null, true))) : (print ("-")));
            echo "</td>
                    <td>";
            // line 130
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "timesheet", [], "any", false, false, false, 130), "amount", [], "any", false, false, false, 130), "html", null, true);
            echo "\$</td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contract'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 133
        echo "            </tbody>
        </table>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/invoice/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 133,  272 => 130,  268 => 129,  264 => 128,  258 => 127,  254 => 126,  250 => 125,  246 => 124,  243 => 123,  239 => 122,  212 => 98,  198 => 89,  188 => 82,  178 => 74,  169 => 67,  160 => 60,  158 => 59,  152 => 56,  148 => 55,  141 => 51,  136 => 49,  118 => 34,  109 => 27,  101 => 23,  99 => 22,  91 => 19,  89 => 18,  84 => 16,  72 => 6,  65 => 5,  52 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends (type is defined and type == 'admin') ? 'admin/base.html.twig': 'client_web/base.html.twig' %}

{% block title %}Detail de la facture{% endblock %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <div>
        <a href=\"{{ asset(\"invoices/pdf/\" ~ invoice.code ~ \".pdf\") }}\" target=\"_blank\" onclick=\"return confirm('Etes-vous sûr de vouloir télécharger cette facture ?')\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"file\"></i> Télécharger (.pdf)</a>
       
        {% if app.session.get('type') == 'admin' and invoice.paid != true %}
        <form  method=\"POST\" style=\"display:inline;margin-left:10px\" action=\"{{ path('app_user_validate_invoice', {'id': invoice.id}) }}\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que cette facture est payée ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"check\"></i> Payer</button>
        </form>
        {% endif %}{% if app.session.get('type') == 'admin' and invoice.paid == true %}
        <form  method=\"POST\" style=\"display:inline;margin-left:10px\" action=\"{{ path('app_user_cancel_invoice', {'id': invoice.id}) }}\">
            <button onclick=\"return confirm('Etes-vous sûr de vouloir signaler que cette facture n\\'est pas payée ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"x\"></i> Non payée</button>
        </form>
        {% endif %}
    </div>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"invoice-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Detail de la facture</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"{{asset('users/images/'~invoice.user.imagePath)}}\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"{{ invoice.user.email }}\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">\"Facture - \" {{ invoice.code }}</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ invoice.code }}</p>
                    </div>
                </div>
                {% if invoice.paid %}
                    <div style=\"display:flex; margin-left: 30px\">
                        <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                    </div>
                    <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                        <p>Payée</p>
                    </div>
                {% else %}
                    <div style=\"display:flex; margin-left: 30px\">
                        <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    </div>
                    <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                        <p>En attente du paiement</p>
                    </div>
                {% endif %}
            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>{{ invoice.date | date('d/m/Y') }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Période</p>                    
                    <p>Semaine du {{ invoice.start | date('d/m/Y') }} au {{ invoice.end | date('d/m/Y') }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total à payer</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ invoice.total ~ \"\$\" }}</h4>
                    </div>                
                </div>
            </div>
        </div>
    </div>
     <div class=\"card flex-fill\">
        <div class=\"card-header\">

            <h5 class=\"card-title mb-0\">Contrats</h5>
        </div>
        <table class=\"table table-hover my-0\">
            <thead>
                <tr>
                    <th>#</th>
                    <th class=\"d-none d-xl-table-cell\">Poste</th>
                    <th class=\"d-none d-xl-table-cell\">Date</th>
                    <th class=\"d-none d-xl-table-cell\">Nb Heures</th>
                    <th class=\"d-none d-md-table-cell\">Taux/H</th>
                    <th class=\"d-none d-md-table-cell\">Bonus</th>
                    <th class=\"d-none d-md-table-cell\">Total</th>
                </tr>
            </thead>
            <tbody>
                {% for contract in invoice.contracts %}
                <tr>
                    <td>{{ contract.code }}</td>
                    <td>{{ contract.job.title }}</td>
                    <td>{{ contract.startdate | date('d/m/Y') }}</td>
                    <td>{{ contract.timesheet.totaltimes // 60 }}H{{contract.timesheet.totaltimes % 60 == 0 ? '': ((contract.timesheet.totaltimes / 60) - (contract.timesheet.totaltimes // 60)) * 60}}</td>
                    <td>{{ contract.clientrate ~ \"\$/H \" }}</td>
                    <td>{{ contract.bonus != null ? contract.bonus ~ \"\$/H\": \"-\"}}</td>
                    <td>{{ contract.timesheet.amount}}\$</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    </div>
</div>
{% endblock %}
", "client_web/invoice/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/invoice/detail.html.twig");
    }
}
