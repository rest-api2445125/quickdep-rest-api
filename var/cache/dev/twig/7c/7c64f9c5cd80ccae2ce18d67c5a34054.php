<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* web/home/index.html.twig */
class __TwigTemplate_d183e9105221ef07f9390f1658f57758 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "web/home/index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
\t<head>
\t\t<meta charset=\"utf-8\"/>
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/>
\t\t<meta name=\"description\" content=\"\"/>
\t\t<meta name=\"author\" content=\"\"/>
\t\t<title>QuickDep</title>
\t\t<link
\t\trel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/favicon.png"), "html", null, true);
        echo "\"/>
\t\t<!-- Bootstrap icons-->
\t\t<link
\t\thref=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css\" rel=\"stylesheet\"/>
\t\t<!-- Google fonts-->
\t\t<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\"/>
\t\t<link href=\"https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<link href=\"https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<link
\t\thref=\"https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<!-- Core theme CSS (includes Bootstrap)-->
\t\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/css/styles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
\t\t<link href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\"/>
\t</head>
\t<body
\t\tid=\"Accueil\">
\t\t<!-- Navigation-->
\t\t<nav class=\"navbar navbar-expand-lg navbar-light fixed-top shadow-sm\" id=\"mainNav\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<a class=\"navbar-brand fw-bold\" href=\"#Accueil\"><img style=\"width:40px;margin-right:10px\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/logo.png"), "html", null, true);
        echo "\" alt=\"logo\">
\t\t\t\t\tQuickDep</a>
\t\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

\t\t\t\t\t<i class=\"bi-list\"></i>
\t\t\t\t</button>
\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t\t<ul class=\"navbar-nav ms-auto me-4 my-3 my-lg-0\">
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Accueil\">Accueil</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#A-propos\">À propos</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Comment-ça-marche\">Comment ça marche</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Contact\">Contact</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<a class=\"btn btn-primary rounded-pill px-3 mb-2 mb-lg-0\" href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth");
        echo "\" style=\"margin-right:10px\">
\t\t\t\t\t\t<span class=\"d-flex align-items-center\">
\t\t\t\t\t\t\t<span class=\"small\">Se connecter</span>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t\t<a class=\"btn btn-primary rounded-pill px-3 mb-2 mb-lg-0\" href=\"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_register");
        echo "\">
\t\t\t\t\t\t<span class=\"d-flex align-items-center\">
\t\t\t\t\t\t\t<span class=\"small\">S'inscrire</span>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t<!-- Mashead header-->
\t\t<header class=\"masthead\" id=\"home\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"row gx-5 align-items-center\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-lg-6\">
\t\t\t\t\t\t<!-- Mashead text and app badges-->
\t\t\t\t\t\t<div class=\"mb-5 mb-lg-0 text-center text-lg-start\">
\t\t\t\t\t\t\t<h1 class=\"display-1 lh-1 mb-3\">Trouvez le travail idéal en quelques clics.</h1>
\t\t\t\t\t\t\t<p class=\"lead fw-normal text-muted mb-5\">QuickDep est la clé d'un emploi assuré et l'assurence d'avoir des bons travailleurs!</p>
\t\t\t\t\t\t\t<div class=\"d-flex flex-column flex-lg-row align-items-center\">
\t\t\t\t\t\t\t\t<a target=\"_blank\" class=\"me-lg-3 mb-4 mb-lg-0\" href=\"https://play.google.com/store/apps/details?id=com.dmwema.quickdep_mob&pli=1\"><img class=\"app-badge\" src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/img/google-play-badge.svg"), "html", null, true);
        echo "\" alt=\"...\"/></a>
\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"https://apps.apple.com/us/app/quickdep/id6449005697\"><img class=\"app-badge\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/img/app-store-badge.svg"), "html", null, true);
        echo "\" alt=\"...\"/></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-lg-6\">
\t\t\t\t\t\t<!-- Masthead device mockup feature-->
\t\t\t\t\t\t<div class=\"masthead-device-mockup\">
\t\t\t\t\t\t\t<svg class=\"circle\" viewbox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<defs>
\t\t\t\t\t\t\t\t\t<linearGradient id=\"circleGradient\" gradienttransform=\"rotate(45)\">
\t\t\t\t\t\t\t\t\t\t<stop class=\"gradient-start-color\" offset=\"0%\"></stop>
\t\t\t\t\t\t\t\t\t\t<stop class=\"gradient-end-color\" offset=\"100%\"></stop>
\t\t\t\t\t\t\t\t\t</linearGradient>
\t\t\t\t\t\t\t\t</defs>
\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"50\"></circle>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<svg class=\"shape-1 d-none d-sm-block\" viewbox=\"0 0 240.83 240.83\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<rect x=\"-32.54\" y=\"78.39\" width=\"305.92\" height=\"84.05\" rx=\"42.03\" transform=\"translate(120.42 -49.88) rotate(45)\"></rect>
\t\t\t\t\t\t\t\t<rect x=\"-32.54\" y=\"78.39\" width=\"305.92\" height=\"84.05\" rx=\"42.03\" transform=\"translate(-49.88 120.42) rotate(-45)\"></rect>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<svg class=\"shape-2 d-none d-sm-block\" viewbox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"50\"></circle>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<div class=\"device-wrapper\">
\t\t\t\t\t\t\t\t<div class=\"device\" data-device=\"iPhoneX\" data-orientation=\"portrait\" data-color=\"black\">
\t\t\t\t\t\t\t\t\t<div class=\"screen bg-black\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/phone.jpg"), "html", null, true);
        echo "\" style=\"width:100%\" alt=\"phone\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</header>
\t\t<!-- Quote/testimonial aside-->
\t\t<aside class=\"text-center bg-gradient-primary-to-secondary\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"row gx-5 justify-content-center\">
\t\t\t\t\t<div class=\"col-xl-8\">
\t\t\t\t\t\t<div class=\"h2 fs-1 text-white mb-4\">\"La recherche des Shifts, la selection des travailleurs, la gestion des feuilles de temps et des factures. Le tout embarqué dans une simple application!\"</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</aside>
\t\t<!-- ======= About Section ======= -->
\t\t<section id=\"A-propos\" class=\"about section-bg\">
\t\t\t<div class=\"container\" data-aos=\"fade-up\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>À propos de nous</span>
\t\t\t\t\t</h3>
\t\t\t\t\t<hr/>
\t\t\t\t\t<p>QuickDep a été fondée en 2023 par une équipe de professionnels passionnés par l'innovation et la technologie.</p>
\t\t\t\t</div>

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6\" data-aos=\"fade-right\" data-aos-delay=\"100\">
\t\t\t\t\t\t<img src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/about.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center\" data-aos=\"fade-up\" data-aos-delay=\"100\">
\t\t\t\t\t\t<h3>Notre mission est de révolutionner le monde du recrutement en offrant une solution rapide, facile et efficace pour les entreprises à la recherche de travailleurs qualifiés et pour les travailleurs à la recherche d'opportunités d'emploi.</h3>
\t\t\t\t\t\t<p class=\"fst-italic\">
\t\t\t\t\t\t\tNous croyons que notre application QuickDep peut aider à connecter les bons travailleurs aux bonnes entreprises, tout en offrant une expérience utilisateur exceptionnelle.
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<i class=\"bi bi-emoji-smile\"></i>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<p>Chez QuickDep, nous croyons en l'importance de la transparence, de l'intégrité et de l'empathie. Nous sommes déterminés à offrir une expérience utilisateur exceptionnelle à nos clients, tout en respectant les normes éthiques et en veillant à ce que nos utilisateurs soient traités avec respect et dignité.</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<i class=\"bi bi-search\"></i>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<p>Nous sommes constamment à la recherche de moyens d'améliorer notre application et de répondre aux besoins changeants du marché. Nous sommes déterminés à rester à la pointe de l'innovation technologique et à offrir une solution de placement de travailleurs de qualité supérieure à nos clients.</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\tNous avons constaté que le processus de recrutement traditionnel était souvent long, coûteux et inefficace. C'est pourquoi nous avons décidé de créer une application de placement de travailleurs qui simplifie et accélère le processus de recrutement pour les entreprises et les travailleurs.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</section>
\t\t<!-- End About Section -->
\t\t<!-- App features section-->
\t\t<section id=\"Comment-ça-marche\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Comment ça marche</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"tabs\">

\t\t\t\t\t<input type=\"radio\" id=\"tab1\" name=\"tab-control\" checked>
\t\t\t\t\t<input type=\"radio\" id=\"tab2\" name=\"tab-control\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li title=\"Pour Travailleurs\">
\t\t\t\t\t\t\t<label for=\"tab1\" role=\"button\">
\t\t\t\t\t\t\t\t<span>Pour Travailleurs</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li title=\"Pour Entréprises\">
\t\t\t\t\t\t\t<label for=\"tab2\" role=\"button\">
\t\t\t\t\t\t\t\t<span>Pour Entréprises</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>

\t\t\t\t\t<div class=\"slider\">
\t\t\t\t\t\t<div class=\"indicator\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t<section>
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t<div class=\"myprogress\">
\t\t\t\t\t\t\t\t\t<div class=\"bar\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/1_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Télécharger l'application</h4>
\t\t\t\t\t\t\t\t\t\t<span>Télécharger l'application gratuite QuickDep sur Play Store ou Apple Store</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/2_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Configurations</h4>
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\tInstallez-vous en quelques minutes, contrôle à la demande et conformité prête à l'emploi</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/3_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Acceptez des shift</h4>
\t\t\t\t\t\t\t\t\t\t<span>Si le poste vous convient, postulez, pas d'entretien, pas de CV, attendez l'approbation et lancez-vous.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/4_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Terminer le travail</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois le travail terminé avec succès, Une feuille de temps sera générée et vous devez le remplir puis valider.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1 last\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/5_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Être payé</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois la feuille de temps declarée et approuvé par l'employeur, le prochain lundi vous serez payé pour tous les shifts de la semaine passée !</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section>
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t<div class=\"myprogress\">
\t\t\t\t\t\t\t\t\t<div class=\"bar\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 237
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/1_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Télécharger l'application</h4>
\t\t\t\t\t\t\t\t\t\t<span>Télécharger l'application gratuite QuickDep sur Play Store ou Apple Store</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/2_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Configurations</h4>
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\tInstallez-vous en quelques minutes, contrôle à la demande et conformité prête à l'emploi</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/3_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>publier des shift</h4>
\t\t\t\t\t\t\t\t\t\t<span>Commencez à publier des shifts, suivez les candidatures des travailleurs et selectionnez le travailleurs</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/4_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Approuver feuilles de temps</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois le travailleurs a complété le shift et declarez la feuille de temps avec succès, vous devez vérifier les informations saisis et approuvé la feuille de temps.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1 last\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 262
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/images/5_circle.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<h4>Payer</h4>
\t\t\t\t\t\t\t\t\t\t<span>Chaque vendredi vous recevez la facture pour toutes les feuilles de temps approuvées dans la semaine</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>

\t\t<!-- App features section-->
\t\t<section id=\"testimonials\" class=\"section-bg\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Témoignages</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"testimonials text-center\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">Grâce à QuickDep, j'ai pu trouver un emploi temporaire rapidement et facilement. Le processus de candidature était simple et j'ai été contactée par une entreprise en moins de 24 heures. Je recommande vivement cette application pour tous ceux qui cherchent un emploi temporaire au Québec</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Marie-Claire Tremblay</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">QuickDep m'a permis de trouver des emplois temporaires qui correspondaient parfaitement à mes compétences et à mon expérience. J'ai apprécié la facilité d'utilisation de l'application et la rapidité avec laquelle j'ai été mis en contact avec des employeurs potentiels.
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Mathieu Dubois</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">QuickDep a été un outil précieux pour notre entreprise. Nous avons pu trouver des travailleurs temporaires qualifiés rapidement et facilement, ce qui nous a permis de répondre à nos besoins en matière de main-d'œuvre de manière efficace. Je recommande vivement QuickDep à toutes les entreprises qui cherchent à embaucher des travailleurs temporaires au Québec.</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Isabelle Gagnon</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</section>

\t\t<!-- ======= Counts Section ======= -->
\t\t<section id=\"counts\" class=\"counts\">
\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Statistiques</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t\t<br>
\t\t\t\t<br>
\t\t\t<div class=\"container\" data-aos=\"fade-up\">

\t\t\t\t<div class=\"row\">

\t\t\t\t\t<div class=\"col-lg-3 col-md-6\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-check-all\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"232\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Shifts complétés</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-md-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-journal-richtext\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"521\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Contrats publiés</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-lg-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-calendar-check\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"1463\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Heures complétées</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-lg-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-people\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"15\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Utilisateurs</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>
\t\t</section>
\t\t<!-- End Counts Section -->

\t\t<section class=\"cta\" id=\"download\">
\t\t\t<div class=\"cta-content\">
\t\t\t\t<div class=\"container px-5\">
\t\t\t\t\t<h2 class=\"text-white display-1 lh-1 mb-4\" style=\"text-align:center; font-size:2rem\">
\t\t\t\t\t\tArrêtez d'attendre.
\t\t\t\t\t\t<br/>
\t\t\t\t\t\tCommencez maintenant!
\t\t\t\t\t</h2>
\t\t\t\t\t<div class=\"d-flex flex-column flex-lg-row align-items-center justify-content-center\">
\t\t\t\t\t\t<a target=\"_blank\" class=\"me-lg-3 mb-4 mb-lg-0\" href=\"https://play.google.com/store/apps/details?id=com.dmwema.quickdep_mob&pli=1\"><img class=\"app-badge\" src=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/img/google-play-badge.svg"), "html", null, true);
        echo "\" alt=\"...\"/></a>
\t\t\t\t\t\t<a target=\"_blank\" href=\"https://apps.apple.com/us/app/quickdep/id6449005697\"><img class=\"app-badge\" src=\"";
        // line 389
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/img/app-store-badge.svg"), "html", null, true);
        echo "\" alt=\"...\"/></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
\t\t<section class=\"section-bg\" id=\"Contact\">
\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Nous contacter</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"containerz\">
\t\t\t\t<div class=\"contentz\">
\t\t\t\t\t<div class=\"left-side\">
\t\t\t\t\t\t<div class=\"address details\">
\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Adresse</div>
\t\t\t\t\t\t\t<div class=\"text-one\">2955 Avenue Maricourt.
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"text-two\">Quebec, Canada</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"phone details\">
\t\t\t\t\t\t\t<i class=\"fas fa-phone-alt\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Téléphone</div>
\t\t\t\t\t\t\t<div class=\"text-one\">+1 (581) 777-1486</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"email details\">
\t\t\t\t\t\t\t<i class=\"fas fa-envelope\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Email</div>
\t\t\t\t\t\t\t<div class=\"text-one\">contact@quickdep.ca</div>
\t\t\t\t\t\t\t<div class=\"text-tw\">facturation@quickdep.ca</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"right-side\">
\t\t\t\t\t\t<div class=\"topic-text\">Laissez-nous un message</div>
\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t<div class=\"input-box\">
\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Entrez votre nom\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"input-box\">
\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Entre votre adresse e-mail\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"input-box message-box\">
\t\t\t\t\t\t\t\t<textarea name=\"message\" id=\"message\" cols=\"30\" rows=\"10\" placeholder=\"Entrez le message\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"button\">
\t\t\t\t\t\t\t\t<input type=\"button\" value=\"Envoyer\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>


\t\t<!-- Footer-->
\t\t<footer class=\"bg-black text-center py-\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"text-white-50 small\">
\t\t\t\t\t<div class=\"py-2\">&copy; QuickDep 2023. Tous droits reservés.</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t<!-- Bootstrap core JS-->
\t\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\"></script>
\t\t<!-- Core theme JS-->
\t\t<script src=\"";
        // line 455
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/scripts.js"), "html", null, true);
        echo "\"></script>
\t\t<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
\t\t<!-- * *                               SB Forms JS                               * *-->
\t\t<!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
\t\t<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *--><script src=\"https://cdn.startbootstrap.com/sb-forms-latest.js\"> </script>


\t\t<!-- Vendor JS Files -->
\t\t<script src=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/purecounter/purecounter_vanilla.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 464
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/aos/aos.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 465
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 466
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/glightbox/js/glightbox.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 467
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/isotope-layout/isotope.pkgd.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 468
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/swiper/swiper-bundle.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 469
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/waypoints/noframework.waypoints.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/vendor/php-email-form/validate.js"), "html", null, true);
        echo "\"></script>

\t\t<!-- Template Main JS File -->
\t\t<script src=\"";
        // line 473
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/js/main.jsjs/main.js"), "html", null, true);
        echo "\"></script>
\t</body>
</html></body></html>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "web/home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  604 => 473,  598 => 470,  594 => 469,  590 => 468,  586 => 467,  582 => 466,  578 => 465,  574 => 464,  570 => 463,  559 => 455,  490 => 389,  486 => 388,  357 => 262,  348 => 256,  339 => 250,  329 => 243,  320 => 237,  305 => 225,  296 => 219,  287 => 213,  277 => 206,  268 => 200,  200 => 135,  164 => 102,  134 => 75,  130 => 74,  108 => 55,  100 => 50,  76 => 29,  65 => 21,  51 => 10,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
\t<head>
\t\t<meta charset=\"utf-8\"/>
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/>
\t\t<meta name=\"description\" content=\"\"/>
\t\t<meta name=\"author\" content=\"\"/>
\t\t<title>QuickDep</title>
\t\t<link
\t\trel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('web/assets/favicon.png') }}\"/>
\t\t<!-- Bootstrap icons-->
\t\t<link
\t\thref=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css\" rel=\"stylesheet\"/>
\t\t<!-- Google fonts-->
\t\t<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\"/>
\t\t<link href=\"https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<link href=\"https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<link
\t\thref=\"https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap\" rel=\"stylesheet\"/>
\t\t<!-- Core theme CSS (includes Bootstrap)-->
\t\t<link href=\"{{ asset('web/css/styles.css') }}\" rel=\"stylesheet\"/>
\t\t<link href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\"/>
\t</head>
\t<body
\t\tid=\"Accueil\">
\t\t<!-- Navigation-->
\t\t<nav class=\"navbar navbar-expand-lg navbar-light fixed-top shadow-sm\" id=\"mainNav\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<a class=\"navbar-brand fw-bold\" href=\"#Accueil\"><img style=\"width:40px;margin-right:10px\" src=\"{{ asset('web/images/logo.png') }}\" alt=\"logo\">
\t\t\t\t\tQuickDep</a>
\t\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">

\t\t\t\t\t<i class=\"bi-list\"></i>
\t\t\t\t</button>
\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t\t<ul class=\"navbar-nav ms-auto me-4 my-3 my-lg-0\">
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Accueil\">Accueil</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#A-propos\">À propos</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Comment-ça-marche\">Comment ça marche</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link me-lg-3\" href=\"#Contact\">Contact</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<a class=\"btn btn-primary rounded-pill px-3 mb-2 mb-lg-0\" href=\"{{ path('app_client_web_auth') }}\" style=\"margin-right:10px\">
\t\t\t\t\t\t<span class=\"d-flex align-items-center\">
\t\t\t\t\t\t\t<span class=\"small\">Se connecter</span>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t\t<a class=\"btn btn-primary rounded-pill px-3 mb-2 mb-lg-0\" href=\"{{ path('app_client_web_auth_register') }}\">
\t\t\t\t\t\t<span class=\"d-flex align-items-center\">
\t\t\t\t\t\t\t<span class=\"small\">S'inscrire</span>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t<!-- Mashead header-->
\t\t<header class=\"masthead\" id=\"home\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"row gx-5 align-items-center\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-lg-6\">
\t\t\t\t\t\t<!-- Mashead text and app badges-->
\t\t\t\t\t\t<div class=\"mb-5 mb-lg-0 text-center text-lg-start\">
\t\t\t\t\t\t\t<h1 class=\"display-1 lh-1 mb-3\">Trouvez le travail idéal en quelques clics.</h1>
\t\t\t\t\t\t\t<p class=\"lead fw-normal text-muted mb-5\">QuickDep est la clé d'un emploi assuré et l'assurence d'avoir des bons travailleurs!</p>
\t\t\t\t\t\t\t<div class=\"d-flex flex-column flex-lg-row align-items-center\">
\t\t\t\t\t\t\t\t<a target=\"_blank\" class=\"me-lg-3 mb-4 mb-lg-0\" href=\"https://play.google.com/store/apps/details?id=com.dmwema.quickdep_mob&pli=1\"><img class=\"app-badge\" src=\"{{ asset('web/assets/img/google-play-badge.svg') }}\" alt=\"...\"/></a>
\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"https://apps.apple.com/us/app/quickdep/id6449005697\"><img class=\"app-badge\" src=\"{{ asset('web/assets/img/app-store-badge.svg') }}\" alt=\"...\"/></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"col-lg-6\">
\t\t\t\t\t\t<!-- Masthead device mockup feature-->
\t\t\t\t\t\t<div class=\"masthead-device-mockup\">
\t\t\t\t\t\t\t<svg class=\"circle\" viewbox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<defs>
\t\t\t\t\t\t\t\t\t<linearGradient id=\"circleGradient\" gradienttransform=\"rotate(45)\">
\t\t\t\t\t\t\t\t\t\t<stop class=\"gradient-start-color\" offset=\"0%\"></stop>
\t\t\t\t\t\t\t\t\t\t<stop class=\"gradient-end-color\" offset=\"100%\"></stop>
\t\t\t\t\t\t\t\t\t</linearGradient>
\t\t\t\t\t\t\t\t</defs>
\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"50\"></circle>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<svg class=\"shape-1 d-none d-sm-block\" viewbox=\"0 0 240.83 240.83\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<rect x=\"-32.54\" y=\"78.39\" width=\"305.92\" height=\"84.05\" rx=\"42.03\" transform=\"translate(120.42 -49.88) rotate(45)\"></rect>
\t\t\t\t\t\t\t\t<rect x=\"-32.54\" y=\"78.39\" width=\"305.92\" height=\"84.05\" rx=\"42.03\" transform=\"translate(-49.88 120.42) rotate(-45)\"></rect>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<svg class=\"shape-2 d-none d-sm-block\" viewbox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"50\"></circle>
\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t<div class=\"device-wrapper\">
\t\t\t\t\t\t\t\t<div class=\"device\" data-device=\"iPhoneX\" data-orientation=\"portrait\" data-color=\"black\">
\t\t\t\t\t\t\t\t\t<div class=\"screen bg-black\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/phone.jpg') }}\" style=\"width:100%\" alt=\"phone\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</header>
\t\t<!-- Quote/testimonial aside-->
\t\t<aside class=\"text-center bg-gradient-primary-to-secondary\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"row gx-5 justify-content-center\">
\t\t\t\t\t<div class=\"col-xl-8\">
\t\t\t\t\t\t<div class=\"h2 fs-1 text-white mb-4\">\"La recherche des Shifts, la selection des travailleurs, la gestion des feuilles de temps et des factures. Le tout embarqué dans une simple application!\"</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</aside>
\t\t<!-- ======= About Section ======= -->
\t\t<section id=\"A-propos\" class=\"about section-bg\">
\t\t\t<div class=\"container\" data-aos=\"fade-up\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>À propos de nous</span>
\t\t\t\t\t</h3>
\t\t\t\t\t<hr/>
\t\t\t\t\t<p>QuickDep a été fondée en 2023 par une équipe de professionnels passionnés par l'innovation et la technologie.</p>
\t\t\t\t</div>

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6\" data-aos=\"fade-right\" data-aos-delay=\"100\">
\t\t\t\t\t\t<img src=\"{{ asset('web/images/about.jpg') }}\" class=\"img-fluid\" alt=\"\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center\" data-aos=\"fade-up\" data-aos-delay=\"100\">
\t\t\t\t\t\t<h3>Notre mission est de révolutionner le monde du recrutement en offrant une solution rapide, facile et efficace pour les entreprises à la recherche de travailleurs qualifiés et pour les travailleurs à la recherche d'opportunités d'emploi.</h3>
\t\t\t\t\t\t<p class=\"fst-italic\">
\t\t\t\t\t\t\tNous croyons que notre application QuickDep peut aider à connecter les bons travailleurs aux bonnes entreprises, tout en offrant une expérience utilisateur exceptionnelle.
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<i class=\"bi bi-emoji-smile\"></i>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<p>Chez QuickDep, nous croyons en l'importance de la transparence, de l'intégrité et de l'empathie. Nous sommes déterminés à offrir une expérience utilisateur exceptionnelle à nos clients, tout en respectant les normes éthiques et en veillant à ce que nos utilisateurs soient traités avec respect et dignité.</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<i class=\"bi bi-search\"></i>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<p>Nous sommes constamment à la recherche de moyens d'améliorer notre application et de répondre aux besoins changeants du marché. Nous sommes déterminés à rester à la pointe de l'innovation technologique et à offrir une solution de placement de travailleurs de qualité supérieure à nos clients.</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\tNous avons constaté que le processus de recrutement traditionnel était souvent long, coûteux et inefficace. C'est pourquoi nous avons décidé de créer une application de placement de travailleurs qui simplifie et accélère le processus de recrutement pour les entreprises et les travailleurs.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</section>
\t\t<!-- End About Section -->
\t\t<!-- App features section-->
\t\t<section id=\"Comment-ça-marche\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Comment ça marche</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"tabs\">

\t\t\t\t\t<input type=\"radio\" id=\"tab1\" name=\"tab-control\" checked>
\t\t\t\t\t<input type=\"radio\" id=\"tab2\" name=\"tab-control\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li title=\"Pour Travailleurs\">
\t\t\t\t\t\t\t<label for=\"tab1\" role=\"button\">
\t\t\t\t\t\t\t\t<span>Pour Travailleurs</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li title=\"Pour Entréprises\">
\t\t\t\t\t\t\t<label for=\"tab2\" role=\"button\">
\t\t\t\t\t\t\t\t<span>Pour Entréprises</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>

\t\t\t\t\t<div class=\"slider\">
\t\t\t\t\t\t<div class=\"indicator\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t<section>
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t<div class=\"myprogress\">
\t\t\t\t\t\t\t\t\t<div class=\"bar\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/1_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Télécharger l'application</h4>
\t\t\t\t\t\t\t\t\t\t<span>Télécharger l'application gratuite QuickDep sur Play Store ou Apple Store</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/2_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Configurations</h4>
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\tInstallez-vous en quelques minutes, contrôle à la demande et conformité prête à l'emploi</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/3_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Acceptez des shift</h4>
\t\t\t\t\t\t\t\t\t\t<span>Si le poste vous convient, postulez, pas d'entretien, pas de CV, attendez l'approbation et lancez-vous.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/4_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Terminer le travail</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois le travail terminé avec succès, Une feuille de temps sera générée et vous devez le remplir puis valider.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1 last\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/5_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Être payé</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois la feuille de temps declarée et approuvé par l'employeur, le prochain lundi vous serez payé pour tous les shifts de la semaine passée !</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section>
\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t<div class=\"myprogress\">
\t\t\t\t\t\t\t\t\t<div class=\"bar\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/1_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Télécharger l'application</h4>
\t\t\t\t\t\t\t\t\t\t<span>Télécharger l'application gratuite QuickDep sur Play Store ou Apple Store</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/2_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Configurations</h4>
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\tInstallez-vous en quelques minutes, contrôle à la demande et conformité prête à l'emploi</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/3_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>publier des shift</h4>
\t\t\t\t\t\t\t\t\t\t<span>Commencez à publier des shifts, suivez les candidatures des travailleurs et selectionnez le travailleurs</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/4_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Approuver feuilles de temps</h4>
\t\t\t\t\t\t\t\t\t\t<span>Une fois le travailleurs a complété le shift et declarez la feuille de temps avec succès, vous devez vérifier les informations saisis et approuvé la feuille de temps.</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"pro1 last\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('web/images/5_circle.png') }}\">
\t\t\t\t\t\t\t\t\t\t<h4>Payer</h4>
\t\t\t\t\t\t\t\t\t\t<span>Chaque vendredi vous recevez la facture pour toutes les feuilles de temps approuvées dans la semaine</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>

\t\t<!-- App features section-->
\t\t<section id=\"testimonials\" class=\"section-bg\">

\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Témoignages</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"testimonials text-center\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">Grâce à QuickDep, j'ai pu trouver un emploi temporaire rapidement et facilement. Le processus de candidature était simple et j'ai été contactée par une entreprise en moins de 24 heures. Je recommande vivement cette application pour tous ceux qui cherchent un emploi temporaire au Québec</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Marie-Claire Tremblay</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">QuickDep m'a permis de trouver des emplois temporaires qui correspondaient parfaitement à mes compétences et à mon expérience. J'ai apprécié la facilité d'utilisation de l'application et la rapidité avec laquelle j'ai été mis en contact avec des employeurs potentiels.
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Mathieu Dubois</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-md-6 col-lg-4\">
\t\t\t\t\t\t\t<div class=\"card border-light bg-light text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-quote-left fa-3x card-img-top rounded-circle\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t<div class=\"card-body blockquote\">
\t\t\t\t\t\t\t\t\t<p class=\"card-text\">QuickDep a été un outil précieux pour notre entreprise. Nous avons pu trouver des travailleurs temporaires qualifiés rapidement et facilement, ce qui nous a permis de répondre à nos besoins en matière de main-d'œuvre de manière efficace. Je recommande vivement QuickDep à toutes les entreprises qui cherchent à embaucher des travailleurs temporaires au Québec.</p>
\t\t\t\t\t\t\t\t\t<footer class=\"blockquote-footer\">
\t\t\t\t\t\t\t\t\t\t<cite title=\"Source Title\">Isabelle Gagnon</cite>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</section>

\t\t<!-- ======= Counts Section ======= -->
\t\t<section id=\"counts\" class=\"counts\">
\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Statistiques</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t\t<br>
\t\t\t\t<br>
\t\t\t<div class=\"container\" data-aos=\"fade-up\">

\t\t\t\t<div class=\"row\">

\t\t\t\t\t<div class=\"col-lg-3 col-md-6\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-check-all\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"232\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Shifts complétés</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-md-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-journal-richtext\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"521\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Contrats publiés</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-lg-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-calendar-check\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"1463\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Heures complétées</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-lg-3 col-md-6 mt-5 mt-lg-0\">
\t\t\t\t\t\t<div class=\"count-box\">
\t\t\t\t\t\t\t<i class=\"bi bi-people\"></i>
\t\t\t\t\t\t\t<span data-purecounter-start=\"0\" data-purecounter-end=\"15\" data-purecounter-duration=\"1\" class=\"purecounter\"></span>
\t\t\t\t\t\t\t<p>Utilisateurs</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>
\t\t</section>
\t\t<!-- End Counts Section -->

\t\t<section class=\"cta\" id=\"download\">
\t\t\t<div class=\"cta-content\">
\t\t\t\t<div class=\"container px-5\">
\t\t\t\t\t<h2 class=\"text-white display-1 lh-1 mb-4\" style=\"text-align:center; font-size:2rem\">
\t\t\t\t\t\tArrêtez d'attendre.
\t\t\t\t\t\t<br/>
\t\t\t\t\t\tCommencez maintenant!
\t\t\t\t\t</h2>
\t\t\t\t\t<div class=\"d-flex flex-column flex-lg-row align-items-center justify-content-center\">
\t\t\t\t\t\t<a target=\"_blank\" class=\"me-lg-3 mb-4 mb-lg-0\" href=\"https://play.google.com/store/apps/details?id=com.dmwema.quickdep_mob&pli=1\"><img class=\"app-badge\" src=\"{{ asset('web/assets/img/google-play-badge.svg') }}\" alt=\"...\"/></a>
\t\t\t\t\t\t<a target=\"_blank\" href=\"https://apps.apple.com/us/app/quickdep/id6449005697\"><img class=\"app-badge\" src=\"{{ asset('web/assets/img/app-store-badge.svg') }}\" alt=\"...\"/></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
\t\t<section class=\"section-bg\" id=\"Contact\">
\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<h3>
\t\t\t\t\t\t<span>Nous contacter</span>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t<div class=\"containerz\">
\t\t\t\t<div class=\"contentz\">
\t\t\t\t\t<div class=\"left-side\">
\t\t\t\t\t\t<div class=\"address details\">
\t\t\t\t\t\t\t<i class=\"fas fa-map-marker-alt\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Adresse</div>
\t\t\t\t\t\t\t<div class=\"text-one\">2955 Avenue Maricourt.
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"text-two\">Quebec, Canada</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"phone details\">
\t\t\t\t\t\t\t<i class=\"fas fa-phone-alt\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Téléphone</div>
\t\t\t\t\t\t\t<div class=\"text-one\">+1 (581) 777-1486</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"email details\">
\t\t\t\t\t\t\t<i class=\"fas fa-envelope\"></i>
\t\t\t\t\t\t\t<div class=\"topic\">Email</div>
\t\t\t\t\t\t\t<div class=\"text-one\">contact@quickdep.ca</div>
\t\t\t\t\t\t\t<div class=\"text-tw\">facturation@quickdep.ca</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"right-side\">
\t\t\t\t\t\t<div class=\"topic-text\">Laissez-nous un message</div>
\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t<div class=\"input-box\">
\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Entrez votre nom\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"input-box\">
\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Entre votre adresse e-mail\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"input-box message-box\">
\t\t\t\t\t\t\t\t<textarea name=\"message\" id=\"message\" cols=\"30\" rows=\"10\" placeholder=\"Entrez le message\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"button\">
\t\t\t\t\t\t\t\t<input type=\"button\" value=\"Envoyer\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>


\t\t<!-- Footer-->
\t\t<footer class=\"bg-black text-center py-\">
\t\t\t<div class=\"container px-5\">
\t\t\t\t<div class=\"text-white-50 small\">
\t\t\t\t\t<div class=\"py-2\">&copy; QuickDep 2023. Tous droits reservés.</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t<!-- Bootstrap core JS-->
\t\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\"></script>
\t\t<!-- Core theme JS-->
\t\t<script src=\"{{ asset('web/js/scripts.js') }}\"></script>
\t\t<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
\t\t<!-- * *                               SB Forms JS                               * *-->
\t\t<!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
\t\t<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *--><script src=\"https://cdn.startbootstrap.com/sb-forms-latest.js\"> </script>


\t\t<!-- Vendor JS Files -->
\t\t<script src=\"{{ asset('web/assets/vendor/purecounter/purecounter_vanilla.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/aos/aos.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/glightbox/js/glightbox.min.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/swiper/swiper-bundle.min.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/waypoints/noframework.waypoints.js') }}\"></script>
\t\t<script src=\"{{ asset('web/assets/vendor/php-email-form/validate.js') }}\"></script>

\t\t<!-- Template Main JS File -->
\t\t<script src=\"{{ asset('web/js/main.jsjs/main.js') }}\"></script>
\t</body>
</html></body></html>
", "web/home/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/web/home/index.html.twig");
    }
}
