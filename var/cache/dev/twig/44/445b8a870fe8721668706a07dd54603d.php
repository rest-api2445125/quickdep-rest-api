<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/stats/finance.html.twig */
class __TwigTemplate_70784a4a0f3ae825159812f8de7074bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/stats/finance.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/stats/finance.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Finance";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Statistiques de finance</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 39));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 40
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 42
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
    <div class=\"row\">
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Payé aux employés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["paid_employee"]) || array_key_exists("paid_employee", $context) ? $context["paid_employee"] : (function () { throw new RuntimeError('Variable "paid_employee" does not exist.', 66, $this->source); })()), "html", null, true);
        echo "\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">À payer aux employés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["unpaid_employee"]) || array_key_exists("unpaid_employee", $context) ? $context["unpaid_employee"] : (function () { throw new RuntimeError('Variable "unpaid_employee" does not exist.', 87, $this->source); })()), "html", null, true);
        echo "\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">À recevoir des entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["unpaid_clients"]) || array_key_exists("unpaid_clients", $context) ? $context["unpaid_clients"] : (function () { throw new RuntimeError('Variable "unpaid_clients" does not exist.', 108, $this->source); })()), "html", null, true);
        echo "\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Réçu des entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 129
        echo twig_escape_filter($this->env, (isset($context["paid_clients"]) || array_key_exists("paid_clients", $context) ? $context["paid_clients"] : (function () { throw new RuntimeError('Variable "paid_clients" does not exist.', 129, $this->source); })()), "html", null, true);
        echo "\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Bénéfices</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"target\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 150
        echo twig_escape_filter($this->env, (isset($context["benefits"]) || array_key_exists("benefits", $context) ? $context["benefits"] : (function () { throw new RuntimeError('Variable "benefits" does not exist.', 150, $this->source); })()), "html", null, true);
        echo "\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/stats/finance.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 150,  206 => 129,  182 => 108,  158 => 87,  134 => 66,  116 => 50,  102 => 42,  98 => 40,  94 => 39,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Finance{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Statistiques de finance</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <div class=\"row\">
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Payé aux employés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ paid_employee }}\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">À payer aux employés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ unpaid_employee }}\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">À recevoir des entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ unpaid_clients }}\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Réçu des entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ paid_clients }}\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Bénéfices</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"target\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ benefits }}\$</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
{% endblock %}
", "admin/stats/finance.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/stats/finance.html.twig");
    }
}
