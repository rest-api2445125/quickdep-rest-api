<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/invoice/paid.html.twig */
class __TwigTemplate_c1f4fb4eed30cb07f420c58e089f8572 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "client_web/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/invoice/paid.html.twig"));

        $this->parent = $this->loadTemplate("client_web/base.html.twig", "client_web/invoice/paid.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Factures payées";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Factures payées</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 22));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 23
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 25
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    <div class=\"row\">
        
        ";
        // line 36
        if ((twig_length_filter($this->env, (isset($context["invoices"]) || array_key_exists("invoices", $context) ? $context["invoices"] : (function () { throw new RuntimeError('Variable "invoices" does not exist.', 36, $this->source); })())) == 0)) {
            // line 37
            echo "            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">Aucune facture payées</h3>
        ";
        } else {
            // line 39
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["invoices"]) || array_key_exists("invoices", $context) ? $context["invoices"] : (function () { throw new RuntimeError('Variable "invoices" does not exist.', 39, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
                // line 40
                echo "                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["invoice"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <img
                                    src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 45, $this->source); })()), "session", [], "any", false, false, false, 45), "get", [0 => "user"], "method", false, false, false, 45), "imagePath", [], "any", false, false, false, 45))), "html", null, true);
                echo "\"
                                    class=\"avatar img-fluid rounded me-1\"
                                    alt=\"";
                // line 47
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "session", [], "any", false, false, false, 47), "get", [0 => "user"], "method", false, false, false, 47), "client", [], "any", false, false, false, 47), "name", [], "any", false, false, false, 47), "html", null, true);
                echo "\"
                                />
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">";
                // line 50
                echo twig_escape_filter($this->env, ("Facture - " . twig_get_attribute($this->env, $this->source, $context["invoice"], "code", [], "any", false, false, false, 50)), "html", null, true);
                echo "</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
                // line 51
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "date", [], "any", false, false, false, 51), "d/m/Y"), "html", null, true);
                echo "</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
                // line 58
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["invoice"], "total", [], "any", false, false, false, 58) . "\$"), "html", null, true);
                echo "</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        ";
                // line 61
                if (twig_get_attribute($this->env, $this->source, $context["invoice"], "paid", [], "any", false, false, false, 61)) {
                    // line 62
                    echo "                                        <div class=\"status-box approuved small\">Payée</div> 
                                        ";
                } else {
                    // line 63
                    echo "                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        ";
                }
                // line 66
                echo "                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "        ";
        }
        // line 75
        echo "            
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/invoice/paid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 75,  194 => 74,  181 => 66,  176 => 63,  172 => 62,  170 => 61,  164 => 58,  154 => 51,  150 => 50,  144 => 47,  139 => 45,  132 => 41,  129 => 40,  124 => 39,  120 => 37,  118 => 36,  113 => 33,  99 => 25,  95 => 23,  91 => 22,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'client_web/base.html.twig' %}

{% block title %}Factures payées{% endblock %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Factures payées</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <div class=\"row\">
        
        {% if invoices|length == 0 %}
            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">Aucune facture payées</h3>
        {% else %}
            {% for invoice in invoices %}
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <a href=\"{{ path('app_client_web_invoice_detail', {'id': invoice.id})}}\"  class='contract-card-link'> 
                        <div class=\"card contract-card\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <img
                                    src=\"{{asset('users/images/'~app.session.get('user').imagePath)}}\"
                                    class=\"avatar img-fluid rounded me-1\"
                                    alt=\"{{ app.session.get('user').client.name }}\"
                                />
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">{{ \"Facture - \" ~ invoice.code }}</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ invoice.date | date('d/m/Y') }}</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <hr style=\"opacity:0.1; margin-top:0\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ invoice.total ~ \"\$\" }}</h4>
                                    </div>
                                    <div style=\"display:flex\">
                                        {% if invoice.paid %}
                                        <div class=\"status-box approuved small\">Payée</div> 
                                        {% else %}                                           
                                        <div class=\"status-box waiting small\">En attente du paiement</div>                                            
                                        {% endif %}
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </a>
                </div>
            {% endfor %}
        {% endif %}
            
    </div>
</div>
{% endblock %}
", "client_web/invoice/paid.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/invoice/paid.html.twig");
    }
}
