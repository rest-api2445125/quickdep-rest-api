<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/auth/confirm-code.html.twig */
class __TwigTemplate_cb24f2d9e4415a6f1f467b4f99ac927f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/auth/confirm-code.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "client_web/auth/confirm-code.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        recuperation du mot de passe
    </p>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 15
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            <p>Un code a été envoyé par mail à l'adresse ";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reset"]) || array_key_exists("reset", $context) ? $context["reset"] : (function () { throw new RuntimeError('Variable "reset" does not exist.', 28, $this->source); })()), "email", [], "any", false, false, false, 28), "html", null, true);
        echo ". Veuillez le saisir pour continuer</p>
            ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["codeForm"]) || array_key_exists("codeForm", $context) ? $context["codeForm"] : (function () { throw new RuntimeError('Variable "codeForm" does not exist.', 29, $this->source); })()), 'form_start', ["attr" => ["class" => "opt-form"]]);
        echo "

                <div style=\"display:flex;align-items:center;justify-content:center\">
                    <input class=\"otp\" name=\"1\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(1)' maxlength=1 >
                    <input class=\"otp\" name=\"2\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(2)' maxlength=1 >
                    <input class=\"otp\" name=\"3\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(3)' maxlength=1 >
                    <input class=\"otp\" name=\"4\" type=\"text\" oninput='digitValidate(this)'onkeyup='tabChange(4)' maxlength=1 >
                    <input class=\"otp\" name=\"5\" type=\"text\" oninput='digitValidate(this)'onkeyup='tabChange(5)' maxlength=1 >
                </div>
                <div class=\"text-center mt-3\">
                    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["codeForm"]) || array_key_exists("codeForm", $context) ? $context["codeForm"] : (function () { throw new RuntimeError('Variable "codeForm" does not exist.', 39, $this->source); })()), "submit", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>
                <div class=\"text-center\">
                    <a class=\"btn btn-default mt-2\" href=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home");
        echo "\" style=\"border:1px solid #ccc\">Retour à l'accueil</a>
                </div>
           ";
        // line 44
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["codeForm"]) || array_key_exists("codeForm", $context) ? $context["codeForm"] : (function () { throw new RuntimeError('Variable "codeForm" does not exist.', 44, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/auth/confirm-code.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 44,  133 => 42,  127 => 39,  114 => 29,  110 => 28,  103 => 23,  89 => 15,  85 => 13,  81 => 12,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        recuperation du mot de passe
    </p>
    
    {% for message in app.flashes('notice') %}
     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            <p>Un code a été envoyé par mail à l'adresse {{ reset.email }}. Veuillez le saisir pour continuer</p>
            {{ form_start(codeForm, {'attr': {'class': 'opt-form'}}) }}

                <div style=\"display:flex;align-items:center;justify-content:center\">
                    <input class=\"otp\" name=\"1\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(1)' maxlength=1 >
                    <input class=\"otp\" name=\"2\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(2)' maxlength=1 >
                    <input class=\"otp\" name=\"3\" type=\"text\" oninput='digitValidate(this)' onkeyup='tabChange(3)' maxlength=1 >
                    <input class=\"otp\" name=\"4\" type=\"text\" oninput='digitValidate(this)'onkeyup='tabChange(4)' maxlength=1 >
                    <input class=\"otp\" name=\"5\" type=\"text\" oninput='digitValidate(this)'onkeyup='tabChange(5)' maxlength=1 >
                </div>
                <div class=\"text-center mt-3\">
                    {{ form_widget(codeForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary'} }) }}
                </div>
                <div class=\"text-center\">
                    <a class=\"btn btn-default mt-2\" href=\"{{ path('app_client_web_home') }}\" style=\"border:1px solid #ccc\">Retour à l'accueil</a>
                </div>
           {{ form_end(codeForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "client_web/auth/confirm-code.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/auth/confirm-code.html.twig");
    }
}
