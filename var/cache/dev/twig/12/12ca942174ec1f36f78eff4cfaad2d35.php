<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/base.html.twig */
class __TwigTemplate_9a52c1e57bebc1a2c2e519ac8ccc9e92 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
    <meta
      name=\"viewport\"
      content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"
    />
    <meta
      name=\"description\"
      content=\"QuickDep &amp; Tableau de bord\"
    />
    <meta name=\"author\" content=\"QuickDep\" />

    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" />
    <link rel=\"shortcut icon\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("web/assets/favicon.png"), "html", null, true);
        echo "\" />


    <title>QuickDep | ";
        // line 20
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/css/app.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link
      href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap\"
      rel=\"stylesheet\"
    />
    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/alerts/css/alerts-css.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />

    <link rel=\"stylesheet\" 
        href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\" 
        integrity=\"sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg\" 
        crossorigin=\"anonymous\"
     >

     <style>
        .contract-card.disabled {
            
        }
        .inputfile {
          width: 0.1px;
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          z-index: -1;
        }
        .contract-card.disabled:hover {
            border: none;
        }
        .contract-card:hover {
            border: 1px solid #29216B;
        }
        .contract-card-link:hover {
            text-decoration:none;
            color:inherit;
        }
        .btn {
            padding: 10px 20px;
        }
        .btn.small {
            padding: 5px 10px;
        }
        .status-box {
            border-radius: 30px;
            padding: 10px 20px;
        }
        .status-box.small {
            font-size: 12px;
            font-weight: bold;
            border-radius: 20px;
            padding: 5px 10px;
        }
        .status-box.waiting {
            background-color: #fae7c0;
            color:  orange;
        }
        .status-box.approuved {
            background-color: #c0fad0;
            color:  green;
        }
        .status-box p {
            margin: 0;
            font-weight:bold
        }
        .apply-card {
            padding-right:20px
        }
        .apply-card:last-child {
            padding-right:0
        }
        .apply-card > div{
            padding:10px;
            border-radius:10px;
            border: 1px solid #ccc;
        }
        .apply-card > div > div{
            display: flex;
            justify-content:space-between;
        }
        .ex-class {
            font-size:10px; 
            font-weight:bold;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 3px 10px;
            display: inline-block !important;
            margin-right: 10px
        }
     </style>
    ";
        // line 110
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 113
        echo "
    ";
        // line 114
        $this->displayBlock('javascripts', $context, $blocks);
        // line 117
        echo "  </head>

  <body>
    <div class=\"wrapper\">
      <nav id=\"sidebar\" class=\"sidebar js-sidebar\">
        <div class=\"sidebar-content js-simplebar\">
          <a class=\"sidebar-brand\" href=\"#\">
            <span class=\"align-middle\">QuickDep | Entreprise</span>
          </a>

          <ul class=\"sidebar-nav\">
            <li class=\"sidebar-header\">Contrats</li>

            <li class=\"sidebar-item ";
        // line 130
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 130, $this->source); })()), "request", [], "any", false, false, false, 130), "get", [0 => "_route"], "method", false, false, false, 130) == "app_client_web_home")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 131
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home");
        echo "\">
                <i class=\"align-middle\" data-feather=\"list\"></i>
                <span class=\"align-middle\">Publiés</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 137
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 137, $this->source); })()), "request", [], "any", false, false, false, 137), "get", [0 => "_route"], "method", false, false, false, 137) == "app_client_web_contract_waiting")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link \" href=\"";
        // line 138
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_contract_waiting");
        echo "\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">En attente d'approbation</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 144
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 144, $this->source); })()), "request", [], "any", false, false, false, 144), "get", [0 => "_route"], "method", false, false, false, 144) == "app_client_web_contract_approuved")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 145
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_contract_approuved");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvés</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Feuilles de temps</li>

            <li class=\"sidebar-item ";
        // line 153
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 153, $this->source); })()), "request", [], "any", false, false, false, 153), "get", [0 => "_route"], "method", false, false, false, 153) == "app_client_web_timesheet")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 154
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_timesheet");
        echo "\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À valider</span>
              </a>
            </li>

            <li class=\"sidebar-item ";
        // line 160
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 160, $this->source); })()), "request", [], "any", false, false, false, 160), "get", [0 => "_route"], "method", false, false, false, 160) == "app_client_web_timesheet_approuved")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 161
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_timesheet_approuved");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Factures</li>

            <li class=\"sidebar-item ";
        // line 169
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 169, $this->source); })()), "request", [], "any", false, false, false, 169), "get", [0 => "_route"], "method", false, false, false, 169) == "app_client_web_invoice")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 170
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice");
        echo "\">
                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                <span class=\"align-middle\">À payer</span>
              </a>
            </li>

            <li class=\"sidebar-itemm ";
        // line 176
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 176, $this->source); })()), "request", [], "any", false, false, false, 176), "get", [0 => "_route"], "method", false, false, false, 176) == "app_client_web_invoice_paid")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 177
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_invoice_paid");
        echo "\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Payées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Compte</li>

            <li class=\"sidebar-item ";
        // line 185
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 185, $this->source); })()), "request", [], "any", false, false, false, 185), "get", [0 => "_route"], "method", false, false, false, 185) == "app_client_web_home_profile")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 186
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home_profile");
        echo "\">
                <i class=\"align-middle\" data-feather=\"user\"></i>
                <span class=\"align-middle\">Profil</span>
              </a>
            </li>
            <li class=\"sidebar-item ";
        // line 191
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 191, $this->source); })()), "request", [], "any", false, false, false, 191), "get", [0 => "_route"], "method", false, false, false, 191) == "app_client_web_home_contact")) ? ("active") : (""));
        echo "\">
              <a class=\"sidebar-link\" href=\"";
        // line 192
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home_contact");
        echo "\">
                <i class=\"align-middle\" data-feather=\"phone\"></i>
                <span class=\"align-middle\">Nous contacter</span>
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class=\"main\">
        <nav class=\"navbar navbar-expand navbar-light navbar-bg\">
          <a class=\"sidebar-toggle js-sidebar-toggle\">
            <i class=\"hamburger align-self-center\"></i>
          </a>

          <div class=\"navbar-collapse collapse\">
            <ul class=\"navbar-nav navbar-align\">
              <li class=\"nav-item dropdown\">
                <a
                  class=\"nav-icon dropdown-toggle d-inline-block d-sm-none\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <i class=\"align-middle\" data-feather=\"settings\"></i>
                </a>

                <a
                  class=\"nav-link dropdown-toggle d-none d-sm-inline-block\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <img
                    src=\"";
        // line 224
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 224, $this->source); })()), "session", [], "any", false, false, false, 224), "get", [0 => "user"], "method", false, false, false, 224), "imagePath", [], "any", false, false, false, 224) == null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/img/logo.png")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 224, $this->source); })()), "session", [], "any", false, false, false, 224), "get", [0 => "user"], "method", false, false, false, 224), "imagePath", [], "any", false, false, false, 224))))), "html", null, true);
        echo "\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"";
        // line 226
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 226, $this->source); })()), "session", [], "any", false, false, false, 226), "get", [0 => "user"], "method", false, false, false, 226), "client", [], "any", false, false, false, 226), "name", [], "any", false, false, false, 226), "html", null, true);
        echo "\"
                  />
                  <span class=\"text-dark\">";
        // line 228
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 228, $this->source); })()), "session", [], "any", false, false, false, 228), "get", [0 => "user"], "method", false, false, false, 228), "client", [], "any", false, false, false, 228), "name", [], "any", false, false, false, 228), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 228, $this->source); })()), "session", [], "any", false, false, false, 228), "get", [0 => "user"], "method", false, false, false, 228), "client", [], "any", false, false, false, 228), "owner", [], "any", false, false, false, 228), "html", null, true);
        echo "</span>
                </a>
                <div class=\"dropdown-menu dropdown-menu-end\">
                  <a class=\"dropdown-item\" href=\"";
        // line 231
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_home_profile");
        echo "\"
                    ><i class=\"align-middle me-1\" data-feather=\"user\"></i>
                    Profil</a
                  >
                  <div class=\"dropdown-divider\"></div>
                    <form  method=\"POST\" style=\"display:inline\" action=\"";
        // line 236
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
        echo "\">
                        <button type=\"submit\" class=\"dropdown-item\">Se decconecter</button>
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <main class=\"content\">

        ";
        // line 247
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 247, $this->source); })()), "session", [], "any", false, false, false, 247), "get", [0 => "user"], "method", false, false, false, 247), "emailVerified", [], "any", false, false, false, 247) != true)) {
            // line 248
            echo "            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                <span>Veuillez vérifier votre adresse E-mail</span>
                <form  method=\"POST\" style=\"display:inline\" action=\"";
            // line 251
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("mailVerif");
            echo "\">
                    <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('Un mail sera envoyé à votre adresse e-mail avec un code de vérification. Voulez vous continuer')\">Vérifier maintenant</button>
                </form>
              </div>
            </div>
            <br>
        ";
        }
        // line 258
        echo "
        ";
        // line 259
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 259, $this->source); })()), "session", [], "any", false, false, false, 259), "get", [0 => "user"], "method", false, false, false, 259), "phoneVerified", [], "any", false, false, false, 259) != true)) {
            // line 260
            echo "            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                <span>Veuillez vérifier votre numéro de téléphone</span> 
                <form  method=\"POST\" style=\"display:inline\" action=\"";
            // line 263
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("phoneVerif");
            echo "\">
                    <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('Un SMS sera envoyé à votre adresse numéro de téléphone avec un code de vérification. Voulez vous continuer')\">Vérifier maintenant</button>
                </form>
              </div>
            </div>
            <br>
        ";
        }
        // line 270
        echo "        ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 270, $this->source); })()), "session", [], "any", false, false, false, 270), "get", [0 => "user"], "method", false, false, false, 270), "signed", [], "any", false, false, false, 270) != true)) {
            // line 271
            echo "            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                  <span>Veuillez signer le contrat de facturation</span> <a href=\"";
            // line 273
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_contract", ["profile" => 1]);
            echo "\" class=\"btn btn-danger\" style=\"margin-left:10px\">Signer maintenant</a>
              </div>
            </div>
            <br>
        ";
        }
        // line 278
        echo "        ";
        if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 278, $this->source); })()), "session", [], "any", false, false, false, 278), "get", [0 => "user"], "method", false, false, false, 278), "emailVerified", [], "any", false, false, false, 278) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 279
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 279, $this->source); })()), "session", [], "any", false, false, false, 279), "get", [0 => "user"], "method", false, false, false, 279), "phoneVerified", [], "any", false, false, false, 279) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 280
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 280, $this->source); })()), "session", [], "any", false, false, false, 280), "get", [0 => "user"], "method", false, false, false, 280), "signed", [], "any", false, false, false, 280) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 281
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 281, $this->source); })()), "session", [], "any", false, false, false, 281), "get", [0 => "user"], "method", false, false, false, 281), "client", [], "any", false, false, false, 281), "validated", [], "any", false, false, false, 281) != true))) {
            // line 283
            echo "            <div class=\"alert alert_warning\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                <b>En cours de validation</b><br>
                Votre compte est en cours de validation. Un email de validation vous sera envoyé dans les prochaines 24h, vous notifiant du résultat de la validation. Si vous ne recevez par l’email veuillez nous contacter. 
                <hr>
                Pour l'instant vous ne pouvez pas encore publier de contrat, une fois le compte validé vous pourrez le faire. Merci de patienter
              </div>
            </div>
            <br>
        ";
        }
        // line 293
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 293, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 293));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 294
            echo "          <div class=\"alert alert_success\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                  ";
            // line 296
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
              </div>
              <div class=\"alert--close\">
                  <i class=\"far fa-times-circle\"></i>
              </div>
          </div>
          </br>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 304
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 304, $this->source); })()), "flashes", [0 => "error"], "method", false, false, false, 304));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 305
            echo "          <div class=\"alert alert_danger\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                  ";
            // line 307
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
              </div>
              <div class=\"alert--close\">
                  <i class=\"far fa-times-circle\"></i>
              </div>
          </div>
          </br>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 315
        echo "          <div class=\"container-fluid p-0\">
            ";
        // line 316
        $this->displayBlock('body', $context, $blocks);
        echo "  
          </div>
        </main>
      </div>
    </div>
  <script src=\"https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js\"></script>
    <script src=\"";
        // line 322
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/js/app.js"), "html", null, true);
        echo "\"></script>
\t  <script src=\"";
        // line 323
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/alerts/js/alerts.min.js"), "html", null, true);
        echo "\"></script>
    <script>
      
      let diffAdress = document.getElementById(\"diffAdress\")
      let newAddress = document.getElementById(\"newAddress\")
      
      let clothCheck = document.getElementById(\"clothCheck\")
      let clothDiv = document.getElementById(\"clothDiv\")

      let editTimeSheetBtn = document.getElementById(\"edit-time-sheet-btn\")
      let subminTsBtn = document.getElementById(\"time_sheet_submit\")

      let tsStartdate = document.getElementById(\"time_sheet_startdate\")
      let tsEnddate = document.getElementById(\"time_sheet_enddate\")
      let tsPause = document.getElementById(\"time_sheet_pause\")
      let tsDiff = document.getElementById(\"tsDiff\")

      if (diffAdress != null) {
          diffAdress.addEventListener('change', function() {
              if (this.checked) {
                  newAddress.style.display = \"block\"
              } else {
                  newAddress.style.display = \"none\"
              }
          });
      }

      if (clothCheck != null) {
          clothCheck.addEventListener('change', function() {
              if (this.checked) {
                  clothDiv.style.display = \"block\"
              } else {
                  clothDiv.style.display = \"none\"
              }
          });
      }

      if (editTimeSheetBtn != null) {
          editTimeSheetBtn.addEventListener('click', function(e) {
              e.preventDefault()
              
              subminTsBtn.style.display = \"inline-block\"
              this.style.display = \"none\"
              tsStartdate.removeAttribute('readonly')
              tsEnddate.removeAttribute('readonly')
          });
      }

      if (tsStartdate != null) {
          tsStartdate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsEnddate != null) {
          tsEnddate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsPause != null) {
          tsPause.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      function changeDiff() {
        let startDate = Date.parse('2023-01-01T' + tsStartdate.value + ':00')
        let endDate = Date.parse('2023-01-01T' + tsEnddate.value + ':00')

        const index = tsPause.selectedIndex

        let pause = tsPause.options[index].text

        let sDate = new Date(startDate)
        let eDate = new Date(endDate)

        let diffInMillis = eDate.getTime() - sDate.getTime();

        if (pause != 'Pas de pause') {
          diffInMillis = diffInMillis - (parseInt(pause.replace('min', '')) * 60 * 1000);
        }
        const diffInHours = Math.floor(diffInMillis / 3600000);
        const diffInMinutes = Math.round((diffInMillis % 3600000) / 60000);
                        
        const formattedHours = diffInHours.toString().padStart(2, '0');
        const formattedMinutes = diffInMinutes.toString().padStart(2, '0');

        let diff = formattedHours + \"H\" + formattedMinutes
        tsDiff.innerText = diff
      }
      
      // FILE UPLOAD

      ";
        // line 417
        if (array_key_exists("user", $context)) {
            // line 418
            echo "      var inputs = document.querySelectorAll( '.inputfile' );
      Array.prototype.forEach.call( inputs, function( input )
      {
        var label\t = input.nextElementSibling,
          labelVal = label.innerHTML;

        input.addEventListener( 'change', async function( e )
        {
          var fileName = '';
          if( this.files && this.files.length > 1 ) {
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
          } else {
            fileName = e.target.value.split( '\\\\' ).pop();
            let preview = document.getElementById('img-prev')
            var src = URL.createObjectURL(event.target.files[0]);

            const domaineName = 'https://quickdep.ca'
            // const domaineName = 'http://localhost:8000'

            if (isFileImage(event.target.files[0])) {
              let request = await axios.post(domaineName + '/api/users/' + ";
            // line 438
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 438, $this->source); })()), "id", [], "any", false, false, false, 438), "html", null, true);
            echo " + '/image', {
                image: event.target.files[0]
              }, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              }).then((response) => {
                  if (response.data.success) {
                    ";
            // line 446
            if ((array_key_exists("profile", $context) && ((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 446, $this->source); })()) == 1))) {
                // line 447
                echo "                      window.location.replace(domaineName + \"/entreprise/profile\");
                    ";
            } else {
                // line 449
                echo "                      window.location.replace(domaineName + \"/entreprise\");
                    ";
            }
            // line 451
            echo "
                  }
                  preview.src = src;
              });
            } else {
              alert(\"Format du fichier invalide\")
            }
          }
          
        });
      });
        
      ";
        }
        // line 464
        echo "
      function isFileImage(file) {
          return file && file['type'].split('/')[0] === 'image';
      }
    </script>
  </body>
</html>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 20
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 110
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 111
        echo "        ";
        echo twig_escape_filter($this->env, $this->env->getFunction('encore_entry_link_tags')->getCallable()("app"), "html", null, true);
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 114
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 115
        echo "        ";
        echo twig_escape_filter($this->env, $this->env->getFunction('encore_entry_script_tags')->getCallable()("app"), "html", null, true);
        echo "
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 316
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  725 => 316,  715 => 115,  708 => 114,  698 => 111,  691 => 110,  679 => 20,  666 => 464,  651 => 451,  647 => 449,  643 => 447,  641 => 446,  630 => 438,  608 => 418,  606 => 417,  509 => 323,  505 => 322,  496 => 316,  493 => 315,  479 => 307,  475 => 305,  470 => 304,  456 => 296,  452 => 294,  447 => 293,  435 => 283,  433 => 281,  432 => 280,  431 => 279,  429 => 278,  421 => 273,  417 => 271,  414 => 270,  404 => 263,  399 => 260,  397 => 259,  394 => 258,  384 => 251,  379 => 248,  377 => 247,  363 => 236,  355 => 231,  347 => 228,  342 => 226,  337 => 224,  302 => 192,  298 => 191,  290 => 186,  286 => 185,  275 => 177,  271 => 176,  262 => 170,  258 => 169,  247 => 161,  243 => 160,  234 => 154,  230 => 153,  219 => 145,  215 => 144,  206 => 138,  202 => 137,  193 => 131,  189 => 130,  174 => 117,  172 => 114,  169 => 113,  167 => 110,  81 => 27,  73 => 22,  68 => 20,  62 => 17,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
    <meta
      name=\"viewport\"
      content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"
    />
    <meta
      name=\"description\"
      content=\"QuickDep &amp; Tableau de bord\"
    />
    <meta name=\"author\" content=\"QuickDep\" />

    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" />
    <link rel=\"shortcut icon\" href=\"{{ asset('web/assets/favicon.png') }}\" />


    <title>QuickDep | {% block title %}{% endblock %}</title>

    <link href=\"{{asset('clientweb/css/app.css')}}\" rel=\"stylesheet\" />
    <link
      href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap\"
      rel=\"stylesheet\"
    />
    <link href=\"{{asset('clientweb/alerts/css/alerts-css.min.css')}}\" rel=\"stylesheet\" />

    <link rel=\"stylesheet\" 
        href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\" 
        integrity=\"sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg\" 
        crossorigin=\"anonymous\"
     >

     <style>
        .contract-card.disabled {
            
        }
        .inputfile {
          width: 0.1px;
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          z-index: -1;
        }
        .contract-card.disabled:hover {
            border: none;
        }
        .contract-card:hover {
            border: 1px solid #29216B;
        }
        .contract-card-link:hover {
            text-decoration:none;
            color:inherit;
        }
        .btn {
            padding: 10px 20px;
        }
        .btn.small {
            padding: 5px 10px;
        }
        .status-box {
            border-radius: 30px;
            padding: 10px 20px;
        }
        .status-box.small {
            font-size: 12px;
            font-weight: bold;
            border-radius: 20px;
            padding: 5px 10px;
        }
        .status-box.waiting {
            background-color: #fae7c0;
            color:  orange;
        }
        .status-box.approuved {
            background-color: #c0fad0;
            color:  green;
        }
        .status-box p {
            margin: 0;
            font-weight:bold
        }
        .apply-card {
            padding-right:20px
        }
        .apply-card:last-child {
            padding-right:0
        }
        .apply-card > div{
            padding:10px;
            border-radius:10px;
            border: 1px solid #ccc;
        }
        .apply-card > div > div{
            display: flex;
            justify-content:space-between;
        }
        .ex-class {
            font-size:10px; 
            font-weight:bold;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 3px 10px;
            display: inline-block !important;
            margin-right: 10px
        }
     </style>
    {% block stylesheets %}
        {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
        {{ encore_entry_script_tags('app') }}
    {% endblock %}
  </head>

  <body>
    <div class=\"wrapper\">
      <nav id=\"sidebar\" class=\"sidebar js-sidebar\">
        <div class=\"sidebar-content js-simplebar\">
          <a class=\"sidebar-brand\" href=\"#\">
            <span class=\"align-middle\">QuickDep | Entreprise</span>
          </a>

          <ul class=\"sidebar-nav\">
            <li class=\"sidebar-header\">Contrats</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_home' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_home') }}\">
                <i class=\"align-middle\" data-feather=\"list\"></i>
                <span class=\"align-middle\">Publiés</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_contract_waiting' ? 'active': '' }}\">
              <a class=\"sidebar-link \" href=\"{{ path('app_client_web_contract_waiting') }}\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">En attente d'approbation</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_contract_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_contract_approuved') }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvés</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Feuilles de temps</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_timesheet' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_timesheet') }}\">
                <i class=\"align-middle\" data-feather=\"clock\"></i>
                <span class=\"align-middle\">À valider</span>
              </a>
            </li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_timesheet_approuved' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_timesheet_approuved') }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Approuvées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Factures</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_invoice' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_invoice') }}\">
                <i class=\"align-middle\" data-feather=\"credit-card\"></i>
                <span class=\"align-middle\">À payer</span>
              </a>
            </li>

            <li class=\"sidebar-itemm {{ app.request.get('_route') == 'app_client_web_invoice_paid' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_invoice_paid') }}\">
                <i class=\"align-middle\" data-feather=\"check-circle\"></i>
                <span class=\"align-middle\">Payées</span>
              </a>
            </li>

            <li class=\"sidebar-header\">Compte</li>

            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_home_profile' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_home_profile') }}\">
                <i class=\"align-middle\" data-feather=\"user\"></i>
                <span class=\"align-middle\">Profil</span>
              </a>
            </li>
            <li class=\"sidebar-item {{ app.request.get('_route') == 'app_client_web_home_contact' ? 'active': '' }}\">
              <a class=\"sidebar-link\" href=\"{{ path('app_client_web_home_contact') }}\">
                <i class=\"align-middle\" data-feather=\"phone\"></i>
                <span class=\"align-middle\">Nous contacter</span>
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class=\"main\">
        <nav class=\"navbar navbar-expand navbar-light navbar-bg\">
          <a class=\"sidebar-toggle js-sidebar-toggle\">
            <i class=\"hamburger align-self-center\"></i>
          </a>

          <div class=\"navbar-collapse collapse\">
            <ul class=\"navbar-nav navbar-align\">
              <li class=\"nav-item dropdown\">
                <a
                  class=\"nav-icon dropdown-toggle d-inline-block d-sm-none\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <i class=\"align-middle\" data-feather=\"settings\"></i>
                </a>

                <a
                  class=\"nav-link dropdown-toggle d-none d-sm-inline-block\"
                  href=\"#\"
                  data-bs-toggle=\"dropdown\"
                >
                  <img
                    src=\"{{ app.session.get('user').imagePath == null ? asset('clientweb/img/logo.png') : asset('users/images/'~app.session.get('user').imagePath)}}\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"{{ app.session.get('user').client.name }}\"
                  />
                  <span class=\"text-dark\">{{app.session.get('user').client.name}} - {{app.session.get('user').client.owner}}</span>
                </a>
                <div class=\"dropdown-menu dropdown-menu-end\">
                  <a class=\"dropdown-item\" href=\"{{ path('app_client_web_home_profile') }}\"
                    ><i class=\"align-middle me-1\" data-feather=\"user\"></i>
                    Profil</a
                  >
                  <div class=\"dropdown-divider\"></div>
                    <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('logout') }}\">
                        <button type=\"submit\" class=\"dropdown-item\">Se decconecter</button>
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <main class=\"content\">

        {% if app.session.get('user').emailVerified != true %}
            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                <span>Veuillez vérifier votre adresse E-mail</span>
                <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('mailVerif') }}\">
                    <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('Un mail sera envoyé à votre adresse e-mail avec un code de vérification. Voulez vous continuer')\">Vérifier maintenant</button>
                </form>
              </div>
            </div>
            <br>
        {% endif %}

        {% if app.session.get('user').phoneVerified != true %}
            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                <span>Veuillez vérifier votre numéro de téléphone</span> 
                <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('phoneVerif') }}\">
                    <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('Un SMS sera envoyé à votre adresse numéro de téléphone avec un code de vérification. Voulez vous continuer')\">Vérifier maintenant</button>
                </form>
              </div>
            </div>
            <br>
        {% endif %}
        {% if app.session.get('user').signed != true %}
            <div class=\"alert alert_danger\" data-fade-time=\"3\">
              <div class=\"alert--content\" style=\"display:flex;align-items: center;justify-content: space-between;\">
                  <span>Veuillez signer le contrat de facturation</span> <a href=\"{{ path(\"app_client_web_auth_contract\", {'profile': 1}) }}\" class=\"btn btn-danger\" style=\"margin-left:10px\">Signer maintenant</a>
              </div>
            </div>
            <br>
        {% endif %}
        {% if app.session.get('user').emailVerified == true 
            and app.session.get('user').phoneVerified == true 
            and app.session.get('user').signed == true
            and app.session.get('user').client.validated != true 
        %}
            <div class=\"alert alert_warning\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                <b>En cours de validation</b><br>
                Votre compte est en cours de validation. Un email de validation vous sera envoyé dans les prochaines 24h, vous notifiant du résultat de la validation. Si vous ne recevez par l’email veuillez nous contacter. 
                <hr>
                Pour l'instant vous ne pouvez pas encore publier de contrat, une fois le compte validé vous pourrez le faire. Merci de patienter
              </div>
            </div>
            <br>
        {% endif %}
          {% for message in app.flashes('success') %}
          <div class=\"alert alert_success\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                  {{ message }}
              </div>
              <div class=\"alert--close\">
                  <i class=\"far fa-times-circle\"></i>
              </div>
          </div>
          </br>
          {% endfor %}
          {% for message in app.flashes('error') %}
          <div class=\"alert alert_danger\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
              <div class=\"alert--content\">
                  {{ message }}
              </div>
              <div class=\"alert--close\">
                  <i class=\"far fa-times-circle\"></i>
              </div>
          </div>
          </br>
          {% endfor %}
          <div class=\"container-fluid p-0\">
            {% block body %}{% endblock %}  
          </div>
        </main>
      </div>
    </div>
  <script src=\"https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js\"></script>
    <script src=\"{{ asset('clientweb/js/app.js') }}\"></script>
\t  <script src=\"{{asset('clientweb/alerts/js/alerts.min.js')}}\"></script>
    <script>
      
      let diffAdress = document.getElementById(\"diffAdress\")
      let newAddress = document.getElementById(\"newAddress\")
      
      let clothCheck = document.getElementById(\"clothCheck\")
      let clothDiv = document.getElementById(\"clothDiv\")

      let editTimeSheetBtn = document.getElementById(\"edit-time-sheet-btn\")
      let subminTsBtn = document.getElementById(\"time_sheet_submit\")

      let tsStartdate = document.getElementById(\"time_sheet_startdate\")
      let tsEnddate = document.getElementById(\"time_sheet_enddate\")
      let tsPause = document.getElementById(\"time_sheet_pause\")
      let tsDiff = document.getElementById(\"tsDiff\")

      if (diffAdress != null) {
          diffAdress.addEventListener('change', function() {
              if (this.checked) {
                  newAddress.style.display = \"block\"
              } else {
                  newAddress.style.display = \"none\"
              }
          });
      }

      if (clothCheck != null) {
          clothCheck.addEventListener('change', function() {
              if (this.checked) {
                  clothDiv.style.display = \"block\"
              } else {
                  clothDiv.style.display = \"none\"
              }
          });
      }

      if (editTimeSheetBtn != null) {
          editTimeSheetBtn.addEventListener('click', function(e) {
              e.preventDefault()
              
              subminTsBtn.style.display = \"inline-block\"
              this.style.display = \"none\"
              tsStartdate.removeAttribute('readonly')
              tsEnddate.removeAttribute('readonly')
          });
      }

      if (tsStartdate != null) {
          tsStartdate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsEnddate != null) {
          tsEnddate.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      if (tsPause != null) {
          tsPause.addEventListener('change', function(e) {
              changeDiff()
          });
      }

      function changeDiff() {
        let startDate = Date.parse('2023-01-01T' + tsStartdate.value + ':00')
        let endDate = Date.parse('2023-01-01T' + tsEnddate.value + ':00')

        const index = tsPause.selectedIndex

        let pause = tsPause.options[index].text

        let sDate = new Date(startDate)
        let eDate = new Date(endDate)

        let diffInMillis = eDate.getTime() - sDate.getTime();

        if (pause != 'Pas de pause') {
          diffInMillis = diffInMillis - (parseInt(pause.replace('min', '')) * 60 * 1000);
        }
        const diffInHours = Math.floor(diffInMillis / 3600000);
        const diffInMinutes = Math.round((diffInMillis % 3600000) / 60000);
                        
        const formattedHours = diffInHours.toString().padStart(2, '0');
        const formattedMinutes = diffInMinutes.toString().padStart(2, '0');

        let diff = formattedHours + \"H\" + formattedMinutes
        tsDiff.innerText = diff
      }
      
      // FILE UPLOAD

      {% if user is defined %}
      var inputs = document.querySelectorAll( '.inputfile' );
      Array.prototype.forEach.call( inputs, function( input )
      {
        var label\t = input.nextElementSibling,
          labelVal = label.innerHTML;

        input.addEventListener( 'change', async function( e )
        {
          var fileName = '';
          if( this.files && this.files.length > 1 ) {
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
          } else {
            fileName = e.target.value.split( '\\\\' ).pop();
            let preview = document.getElementById('img-prev')
            var src = URL.createObjectURL(event.target.files[0]);

            const domaineName = 'https://quickdep.ca'
            // const domaineName = 'http://localhost:8000'

            if (isFileImage(event.target.files[0])) {
              let request = await axios.post(domaineName + '/api/users/' + {{ user.id }} + '/image', {
                image: event.target.files[0]
              }, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              }).then((response) => {
                  if (response.data.success) {
                    {% if profile is defined and profile == 1 %}
                      window.location.replace(domaineName + \"/entreprise/profile\");
                    {% else %}
                      window.location.replace(domaineName + \"/entreprise\");
                    {% endif %}

                  }
                  preview.src = src;
              });
            } else {
              alert(\"Format du fichier invalide\")
            }
          }
          
        });
      });
        
      {% endif %}

      function isFileImage(file) {
          return file && file['type'].split('/')[0] === 'image';
      }
    </script>
  </body>
</html>", "client_web/base.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/base.html.twig");
    }
}
