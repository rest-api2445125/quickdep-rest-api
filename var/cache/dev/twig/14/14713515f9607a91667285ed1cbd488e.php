<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/stats/stats.html.twig */
class __TwigTemplate_f8041c79b32f10f5941d95bcb569c265 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/stats/stats.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/stats/stats.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Statistiques";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Statistiques</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 39));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 40
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 42
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
    <div class=\"row\">
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Shifts Complétés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"check-square\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["completed_shifts"]) || array_key_exists("completed_shifts", $context) ? $context["completed_shifts"] : (function () { throw new RuntimeError('Variable "completed_shifts" does not exist.', 66, $this->source); })()), "html", null, true);
        echo "</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Shifts non Complétés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"x-circle\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["uncompleted_shifts"]) || array_key_exists("uncompleted_shifts", $context) ? $context["uncompleted_shifts"] : (function () { throw new RuntimeError('Variable "uncompleted_shifts" does not exist.', 87, $this->source); })()), "html", null, true);
        echo "</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Utilisateurs</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"users\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["total_users"]) || array_key_exists("total_users", $context) ? $context["total_users"] : (function () { throw new RuntimeError('Variable "total_users" does not exist.', 108, $this->source); })()), "html", null, true);
        echo "</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"briefcase\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 129
        echo twig_escape_filter($this->env, (isset($context["clients"]) || array_key_exists("clients", $context) ? $context["clients"] : (function () { throw new RuntimeError('Variable "clients" does not exist.', 129, $this->source); })()), "html", null, true);
        echo "</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Heures complétées</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 150
        echo twig_escape_filter($this->env, (isset($context["completed_hours"]) || array_key_exists("completed_hours", $context) ? $context["completed_hours"] : (function () { throw new RuntimeError('Variable "completed_hours" does not exist.', 150, $this->source); })()), "html", null, true);
        echo "H</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Demandes comblées</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"user-check\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">";
        // line 171
        echo twig_escape_filter($this->env, (isset($context["completed_requests"]) || array_key_exists("completed_requests", $context) ? $context["completed_requests"] : (function () { throw new RuntimeError('Variable "completed_requests" does not exist.', 171, $this->source); })()), "html", null, true);
        echo "%</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/stats/stats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 171,  230 => 150,  206 => 129,  182 => 108,  158 => 87,  134 => 66,  116 => 50,  102 => 42,  98 => 40,  94 => 39,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Statistiques{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Statistiques</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <div class=\"row\">
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Shifts Complétés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"check-square\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ completed_shifts }}</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Shifts non Complétés</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"x-circle\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ uncompleted_shifts }}</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Utilisateurs</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"users\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ total_users }}</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Entréprises</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"briefcase\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ clients }}</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Heures complétées</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"clock\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ completed_hours }}H</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
        <div class=\"col-md-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col mt-0\">
                            <h5 class=\"card-title\">Demandes comblées</h5>
                        </div>

                        <div class=\"col-auto\">
                            <div class=\"stat text-primary\">
                                <i class=\"align-middle\" data-feather=\"user-check\"></i>
                            </div>
                        </div>
                    </div>
                    <h1 class=\"mt-1 mb-3\">{{ completed_requests }}%</h1>
                    <div class=\"mb-0\">
                        <span class=\"text-muted\">Depuis de début de l'entreprise</span>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
{% endblock %}
", "admin/stats/stats.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/stats/stats.html.twig");
    }
}
