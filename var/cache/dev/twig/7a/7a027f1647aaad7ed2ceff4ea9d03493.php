<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/time_sheet/detail.html.twig */
class __TwigTemplate_45b6cb889b9167134b990c4c50d6f9c2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/time_sheet/detail.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/time_sheet/detail.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Feuille de temps";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 6, $this->source); })()), 'form_start');
        echo "
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <di style=\"display:flex\">
        <a href=\"#\" id=\"edit-time-sheet-btn\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 18, $this->source); })()), "submit", [], "any", false, false, false, 18), 'widget', ["attr" => ["class" => "btn btn-lg btn-success", "onclick" => "return confirm('Etes-vous sûr de vouloir approuver ces heures ?')", "style" => ("display:" . (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 18, $this->source); })()), "status", [], "any", false, false, false, 18) == "contract_time_approuved")) ? ("none") : ("inline-block"))), "id" => "submit-ts-btn"]]);
        echo "
    </di>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "request", [], "any", false, false, false, 26), "headers", [], "any", false, false, false, 26), "get", [0 => "referer"], "method", false, false, false, 26), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Feuille de temps</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 41, $this->source); })()))), "html", null, true);
        echo "\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 43, $this->source); })()), "client", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 47, $this->source); })()), "job", [], "any", false, false, false, 47), "title", [], "any", false, false, false, 47), "html", null, true);
        echo "</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 48, $this->source); })()), "code", [], "any", false, false, false, 48), "html", null, true);
        echo "</p>
                    </div>
                </div>
                ";
        // line 51
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 51, $this->source); })()), "status", [], "any", false, false, false, 51) == "contract_time_sent")) {
            // line 52
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>À valider</p>
                </div>
                ";
        }
        // line 59
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 59, $this->source); })()), "status", [], "any", false, false, false, 59) == "contract_time_approuved")) {
            // line 60
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>Approuvé</p>
                </div>
                ";
        }
        // line 67
        echo "            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>";
        // line 75
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 75, $this->source); })()), "startdate", [], "any", false, false, false, 75), "d/m/Y"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total heures</p>                    
                    <p></p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
        // line 91
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 91, $this->source); })()), "clientrate", [], "any", false, false, false, 91) . "\$/H"), "html", null, true);
        echo "</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">";
        // line 92
        (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 92, $this->source); })()), "bonus", [], "any", false, false, false, 92) != null)) ? (print (twig_escape_filter($this->env, (("+" . twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 92, $this->source); })()), "bonus", [], "any", false, false, false, 92)) . "\$/H"), "html", null, true))) : (print ("")));
        echo "</h6>
                    </div>                
                </div>
            </div>
            <hr style=\"opacity:0.1\">

            <br>

            <div class=\"row\">
                <div class=\"col-4\">
                    ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 102, $this->source); })()), "startdate", [], "any", false, false, false, 102), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Heure du début *"]);
        // line 104
        echo "<br>
                    ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 105, $this->source); })()), "startdate", [], "any", false, false, false, 105), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%", "readonly" => (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 105, $this->source); })()), "status", [], "any", false, false, false, 105) != "contract_worked")) ? ("") : ("readonly"))]]);
        echo "
                </div>
                <div class=\"col-4\">
                    ";
        // line 108
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 108, $this->source); })()), "enddate", [], "any", false, false, false, 108), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Heure de la fin *"]);
        // line 110
        echo "<br>
                    ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 111, $this->source); })()), "enddate", [], "any", false, false, false, 111), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%", "readonly" => (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 111, $this->source); })()), "status", [], "any", false, false, false, 111) != "contract_worked")) ? ("") : ("readonly"))]]);
        echo "
                </div>

                <div class=\"col-4\">
                    ";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 115, $this->source); })()), "pause", [], "any", false, false, false, 115), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Durée de la pause"]);
        // line 117
        echo "
                    ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 118, $this->source); })()), "pause", [], "any", false, false, false, 118), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "
                </div>

            </div>

            ";
        // line 123
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 123, $this->source); })()), "status", [], "any", false, false, false, 123) != "contract_published")) {
            // line 124
            echo "            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            
            <div class=\"row\">
                ";
            // line 131
            echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("EmployeeCard", ["employee" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 131, $this->source); })()), "employee", [], "any", false, false, false, 131), "contract" => (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 131, $this->source); })())]);
            echo "
            </div>
            ";
        }
        // line 134
        echo "        </div>
    </div>
</div>
";
        // line 137
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 137, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/time_sheet/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 137,  261 => 134,  255 => 131,  246 => 124,  244 => 123,  236 => 118,  233 => 117,  231 => 115,  224 => 111,  221 => 110,  219 => 108,  213 => 105,  210 => 104,  208 => 102,  195 => 92,  191 => 91,  172 => 75,  162 => 67,  153 => 60,  150 => 59,  141 => 52,  139 => 51,  133 => 48,  129 => 47,  122 => 43,  117 => 41,  99 => 26,  88 => 18,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Feuille de temps{% endblock %}

{% block body %}
{{ form_start(timeSheetForm) }}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <di style=\"display:flex\">
        <a href=\"#\" id=\"edit-time-sheet-btn\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        {{ form_widget(timeSheetForm.submit, { 'attr': {'class': 'btn btn-lg btn-success', 'onclick': \"return confirm('Etes-vous sûr de vouloir approuver ces heures ?')\", 'style':'display:' ~ (contract.status == 'contract_time_approuved' ? 'none': 'inline-block'), 'id': 'submit-ts-btn'} }) }}
    </di>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Feuille de temps</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"{{asset('users/images/'~image)}}\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"{{ contract.client.name }}\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">{{ contract.job.title }}</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ contract.code }}</p>
                    </div>
                </div>
                {% if contract.status == 'contract_time_sent' %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>À valider</p>
                </div>
                {% endif %}
                {% if contract.status == 'contract_time_approuved' %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>Approuvé</p>
                </div>
                {% endif %}
            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>{{ contract.startdate | date('d/m/Y') }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total heures</p>                    
                    <p></p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ contract.clientrate ~ \"\$/H\" }}</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">{{ contract.bonus != null ? \"+\" ~ contract.bonus ~ \"\$/H\": \"\" }}</h6>
                    </div>                
                </div>
            </div>
            <hr style=\"opacity:0.1\">

            <br>

            <div class=\"row\">
                <div class=\"col-4\">
                    {{ form_label(timeSheetForm.startdate, 'Heure du début *', {
                        'label_attr': {'class': 'form-label'}
                    }) }}<br>
                    {{ form_widget(timeSheetForm.startdate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%', 'readonly':contract.status != 'contract_worked' ? '' : 'readonly'} }) }}
                </div>
                <div class=\"col-4\">
                    {{ form_label(timeSheetForm.enddate, 'Heure de la fin *', {
                        'label_attr': {'class': 'form-label'}
                    }) }}<br>
                    {{ form_widget(timeSheetForm.enddate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%', 'readonly':contract.status != 'contract_worked' ? '' : 'readonly'} }) }}
                </div>

                <div class=\"col-4\">
                    {{ form_label(timeSheetForm.pause, 'Durée de la pause', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(timeSheetForm.pause, { 'attr': {'class': 'form-select form-control-lg'} }) }}
                </div>

            </div>

            {% if contract.status != 'contract_published' %}
            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            
            <div class=\"row\">
                {{ component('EmployeeCard', {employee: contract.employee, contract: contract}) }}
            </div>
            {% endif %}
        </div>
    </div>
</div>
{{ form_end(timeSheetForm) }}
{% endblock %}
", "admin/time_sheet/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/time_sheet/detail.html.twig");
    }
}
