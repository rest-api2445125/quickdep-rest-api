<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/contract/index.html.twig */
class __TwigTemplate_8fb403423ba96ce2688eceadf25dcaaa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/contract/index.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/contract/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Contrats publiés";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Contrats Publiés</h1>
    <a href=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_new_shift");
        echo "\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i> Publier un nouveau contrat</a>
</div>
<hr>
<br>
<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 39));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 40
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
        <div class=\"alert--content\">
            ";
            // line 42
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
    <div class=\"row\">
        ";
        // line 52
        if ((twig_length_filter($this->env, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 52, $this->source); })())) == 1)) {
            // line 53
            echo "            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 53, $this->source); })()), 0, [], "array", false, false, false, 53), "html", null, true);
            echo "</h3>
        ";
        } else {
            // line 55
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 55, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["contract"]) {
                // line 56
                echo "                ";
                if (twig_test_iterable($context["contract"])) {
                    // line 57
                    echo "                    ";
                    // line 58
                    $context["month"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], 0, [], "array", false, false, false, 58), " ");
                    // line 60
                    echo "                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["months"]) || array_key_exists("months", $context) ? $context["months"] : (function () { throw new RuntimeError('Variable "months" does not exist.', 60, $this->source); })()), (twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 60, $this->source); })()), 0, [], "array", false, false, false, 60) - 1), [], "array", false, false, false, 60) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 60, $this->source); })()), 1, [], "array", false, false, false, 60)), "html", null, true);
                    echo "</h3>
                    <br>
                    <br>
                ";
                } else {
                    // line 64
                    echo "                   ";
                    echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("Contract", ["contractData" => $context["contract"], "user" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 64, $this->source); })()), "session", [], "any", false, false, false, 64), "get", [0 => "user"], "method", false, false, false, 64)]);
                    echo "
                ";
                }
                // line 66
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contract'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "        ";
        }
        // line 68
        echo "            
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/contract/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 68,  163 => 67,  157 => 66,  151 => 64,  143 => 60,  141 => 58,  139 => 57,  136 => 56,  131 => 55,  125 => 53,  123 => 52,  119 => 50,  105 => 42,  101 => 40,  97 => 39,  87 => 32,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Contrats publiés{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Contrats Publiés</h1>
    <a href=\"{{ path('app_admin_new_shift') }}\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i> Publier un nouveau contrat</a>
</div>
<hr>
<br>
<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\" data-fade-time=\"3\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <div class=\"row\">
        {% if contracts|length == 1 %}
            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ contracts[0] }}</h3>
        {% else %}
            {% for contract in contracts %}
                {% if  contract is iterable %}
                    {% 
                        set month = contract[0]|split(' ')
                    %}
                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ months[month[0] - 1] ~ ' ' ~ month[1]}}</h3>
                    <br>
                    <br>
                {% else %}
                   {{ component('Contract', { contractData: contract, user: app.session.get('user') }) }}
                {% endif %}
            {% endfor %}
        {% endif %}
            
    </div>
</div>
{% endblock %}
", "admin/contract/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/contract/index.html.twig");
    }
}
