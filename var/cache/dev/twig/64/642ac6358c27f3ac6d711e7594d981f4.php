<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/ContractDetail.html.twig */
class __TwigTemplate_75bcd4c3bb0093a75121d514ec28f05c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/ContractDetail.html.twig"));

        // line 1
        echo "<div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Detail du contrat</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 9, $this->source); })()))), "html", null, true);
        echo "\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 11, $this->source); })()), "html", null, true);
        echo "\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        ";
        // line 15
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "session", [], "any", false, false, false, 15), "get", [0 => "type"], "method", false, false, false, 15) == "admin")) {
            // line 16
            echo "                        <h5 class=\"mb-0\" style=\"font-size:20px; font-weight:bold\">";
            echo twig_escape_filter($this->env, (isset($context["client"]) || array_key_exists("client", $context) ? $context["client"] : (function () { throw new RuntimeError('Variable "client" does not exist.', 16, $this->source); })()), "html", null, true);
            echo "</h5>
                        ";
        }
        // line 18
        echo "                        <h5 class=\"card-title mb-0\" style=\"\">";
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 18, $this->source); })()), "html", null, true);
        echo "</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 19, $this->source); })()), "code", [], "any", false, false, false, 19), "html", null, true);
        echo "</p>
                    </div>
                    ";
        // line 21
        if ((isset($context["consecutive"]) || array_key_exists("consecutive", $context) ? $context["consecutive"] : (function () { throw new RuntimeError('Variable "consecutive" does not exist.', 21, $this->source); })())) {
            // line 22
            echo "                    <div class=\"btn-success btn \" style=\"padding:5px 10px;color:white;border-radius:20px; font-size:14px; margin-left:20px\">
                        <p style=\"margin:0\">Shifts Consécutifs</p>
                    </div>
                    ";
        }
        // line 26
        echo "                </div>
                ";
        // line 27
        if ((isset($context["applied"]) || array_key_exists("applied", $context) ? $context["applied"] : (function () { throw new RuntimeError('Variable "applied" does not exist.', 27, $this->source); })())) {
            // line 28
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    <i class=\"align-middle\" data-feather=\"users\" style=\"margin-right:5px\"></i>
                    <p style=\"margin:0\">";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 31, $this->source); })()), "getAppliesCount", [], "any", false, false, false, 31), "html", null, true);
            echo "</p>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>En attente d'approbation</p>
                </div>
                ";
        }
        // line 37
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 37, $this->source); })()), "status", [], "any", false, false, false, 37) == "contract_approuved")) {
            // line 38
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["status"]) || array_key_exists("status", $context) ? $context["status"] : (function () { throw new RuntimeError('Variable "status" does not exist.', 42, $this->source); })()), "html", null, true);
            echo "</p>
                </div>
                ";
        }
        // line 45
        echo "            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                        ";
        // line 52
        echo (((isset($context["today"]) || array_key_exists("today", $context) ? $context["today"] : (function () { throw new RuntimeError('Variable "today" does not exist.', 52, $this->source); })())) ? ("color:red") : (""));
        echo "
                    \">Date</p>                    
                    <p style=\"";
        // line 54
        echo (((isset($context["today"]) || array_key_exists("today", $context) ? $context["today"] : (function () { throw new RuntimeError('Variable "today" does not exist.', 54, $this->source); })())) ? ("color:red") : (""));
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["date"]) || array_key_exists("date", $context) ? $context["date"] : (function () { throw new RuntimeError('Variable "date" does not exist.', 54, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Heures</p>                    
                    <p>";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["hours"]) || array_key_exists("hours", $context) ? $context["hours"] : (function () { throw new RuntimeError('Variable "hours" does not exist.', 61, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Pause</p>                    
                    <p>";
        // line 68
        echo twig_escape_filter($this->env, (isset($context["pause"]) || array_key_exists("pause", $context) ? $context["pause"] : (function () { throw new RuntimeError('Variable "pause" does not exist.', 68, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Stationnement</p>                    
                    <p>";
        // line 75
        echo twig_escape_filter($this->env, (isset($context["parking"]) || array_key_exists("parking", $context) ? $context["parking"] : (function () { throw new RuntimeError('Variable "parking" does not exist.', 75, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Adresse</p>                    
                    <p>";
        // line 82
        echo twig_escape_filter($this->env, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 82, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
                ";
        // line 84
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 84, $this->source); })()), "cloth", [], "any", false, false, false, 84) != null)) {
            // line 85
            echo "                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Ténue exigée</p>                    
                    <p>";
            // line 90
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 90, $this->source); })()), "cloth", [], "any", false, false, false, 90), "html", null, true);
            echo "</p>
                </div>
                ";
        }
        // line 93
        echo "                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Description</p>                    
                    <p>";
        // line 98
        (((twig_get_attribute($this->env, $this->source, ($context["contract"] ?? null), "description", [], "any", true, true, false, 98) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["contract"] ?? null), "description", [], "any", false, false, false, 98)))) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["contract"] ?? null), "description", [], "any", false, false, false, 98), "html", null, true))) : (print ("Pas de description")));
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
        // line 107
        echo twig_escape_filter($this->env, (isset($context["rate"]) || array_key_exists("rate", $context) ? $context["rate"] : (function () { throw new RuntimeError('Variable "rate" does not exist.', 107, $this->source); })()), "html", null, true);
        echo "</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["bonus"]) || array_key_exists("bonus", $context) ? $context["bonus"] : (function () { throw new RuntimeError('Variable "bonus" does not exist.', 108, $this->source); })()), "html", null, true);
        echo "</h6>
                    </div>                
                </div>
            </div>

            ";
        // line 113
        if (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 113, $this->source); })()), "getAppliesCount", [], "any", false, false, false, 113) > 0) && (twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 113, $this->source); })()), "status", [], "any", false, false, false, 113) == "contract_published"))) {
            // line 114
            echo "            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Candidats</p>  
            <div class=\"row\">
                ";
            // line 120
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 120, $this->source); })()), "applies", [], "any", false, false, false, 120));
            foreach ($context['_seq'] as $context["_key"] => $context["apply"]) {
                // line 121
                echo "
                ";
                // line 122
                echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("EmployeeCard", ["employee" => twig_get_attribute($this->env, $this->source, $context["apply"], "user", [], "any", false, false, false, 122), "contract" => (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 122, $this->source); })())]);
                echo "

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['apply'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "            </div>
            ";
        }
        // line 127
        echo "
            ";
        // line 128
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 128, $this->source); })()), "status", [], "any", false, false, false, 128) == "contract_approuved")) {
            // line 129
            echo "            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            <div class=\"row\">
                ";
            // line 135
            if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 135, $this->source); })()), "employee", [], "any", false, false, false, 135) == null)) {
                // line 136
                echo "                    <h4 class=\"mb-0\" style=\"color:red; font-weight:bold\">Utilusateur supprimé</h4>
                ";
            } else {
                // line 138
                echo "                    ";
                echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("EmployeeCard", ["employee" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 138, $this->source); })()), "employee", [], "any", false, false, false, 138), "contract" => (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 138, $this->source); })())]);
                echo "
                ";
            }
            // line 140
            echo "            </div>
            ";
        }
        // line 142
        echo "        </div>
    </div>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "components/ContractDetail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 142,  284 => 140,  278 => 138,  274 => 136,  272 => 135,  264 => 129,  262 => 128,  259 => 127,  255 => 125,  246 => 122,  243 => 121,  239 => 120,  231 => 114,  229 => 113,  221 => 108,  217 => 107,  205 => 98,  198 => 93,  192 => 90,  185 => 85,  183 => 84,  178 => 82,  168 => 75,  158 => 68,  148 => 61,  136 => 54,  131 => 52,  122 => 45,  116 => 42,  110 => 38,  107 => 37,  98 => 31,  93 => 28,  91 => 27,  88 => 26,  82 => 22,  80 => 21,  75 => 19,  70 => 18,  64 => 16,  62 => 15,  55 => 11,  50 => 9,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Detail du contrat</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"{{asset('users/images/'~image)}}\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"{{ client }}\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        {% if app.session.get('type') == 'admin' %}
                        <h5 class=\"mb-0\" style=\"font-size:20px; font-weight:bold\">{{ client }}</h5>
                        {% endif %}
                        <h5 class=\"card-title mb-0\" style=\"\">{{ title }}</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ contract.code }}</p>
                    </div>
                    {% if consecutive %}
                    <div class=\"btn-success btn \" style=\"padding:5px 10px;color:white;border-radius:20px; font-size:14px; margin-left:20px\">
                        <p style=\"margin:0\">Shifts Consécutifs</p>
                    </div>
                    {% endif %}
                </div>
                {% if applied %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange; margin-right:10px\"></i>
                    <i class=\"align-middle\" data-feather=\"users\" style=\"margin-right:5px\"></i>
                    <p style=\"margin:0\">{{ contract.getAppliesCount }}</p>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>En attente d'approbation</p>
                </div>
                {% endif %}
                {% if contract.status == 'contract_approuved' %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>{{ status }}</p>
                </div>
                {% endif %}
            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                        {{ today ? 'color:red': '' }}
                    \">Date</p>                    
                    <p style=\"{{ today ? 'color:red': '' }}\">{{ date }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Heures</p>                    
                    <p>{{ hours }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Pause</p>                    
                    <p>{{ pause }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Stationnement</p>                    
                    <p>{{ parking }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Adresse</p>                    
                    <p>{{ address }}</p>
                </div>
                {% if contract.cloth != null %}
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Ténue exigée</p>                    
                    <p>{{ contract.cloth }}</p>
                </div>
                {% endif %}
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Description</p>                    
                    <p>{{ contract.description ?? 'Pas de description' }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ rate }}</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">{{ bonus }}</h6>
                    </div>                
                </div>
            </div>

            {% if contract.getAppliesCount > 0 and contract.status == 'contract_published' %}
            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Candidats</p>  
            <div class=\"row\">
                {% for apply in contract.applies %}

                {{ component('EmployeeCard', {employee: apply.user, contract: contract}) }}

                {% endfor %}
            </div>
            {% endif %}

            {% if contract.status == 'contract_approuved' %}
            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            <div class=\"row\">
                {% if contract.employee == null %}
                    <h4 class=\"mb-0\" style=\"color:red; font-weight:bold\">Utilusateur supprimé</h4>
                {% else %}
                    {{ component('EmployeeCard', {employee: contract.employee, contract: contract}) }}
                {% endif %}
            </div>
            {% endif %}
        </div>
    </div>", "components/ContractDetail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/components/ContractDetail.html.twig");
    }
}
