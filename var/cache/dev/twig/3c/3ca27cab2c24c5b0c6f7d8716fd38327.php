<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/user/index.html.twig */
class __TwigTemplate_b86440e85f3a56b32e847cfb9cf6d8f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/user/index.html.twig"));

        // line 3
        $context["i"] = 1;
        // line 8
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/user/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Travailleurs";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 24
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Liste des travailleurs</h1>
    <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_new_user");
        echo "\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i>Enrégistrer un utilisateur</a>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 42));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 43
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 45
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "    
    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Validés</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Non validés</span>
        </li>
    </ul>
    <div class=\"container__inner\">
       <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card flex-fill\">
                    <table class=\"table table-hover my-0\">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class=\"d-none d-xl-table-cell\">Nom</th>
                                <th class=\"d-none d-xl-table-cell\">Prénom</th>
                                <th class=\"d-none d-xl-table-cell\">Adresse</th>
                                <th class=\"d-none d-md-table-cell\">Email</th>
                                <th class=\"d-none d-md-table-cell\">Statut</th>
                                <th class=\"d-none d-md-table-cell\">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        ";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["all"]) || array_key_exists("all", $context) ? $context["all"] : (function () { throw new RuntimeError('Variable "all" does not exist.', 82, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 83
            echo "                            <tr>
                                <td>";
            // line 84
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 84, $this->source); })()), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 85), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 86
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 86), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "address", [], "any", false, false, false, 87), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 88
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 88), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 90
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 90) == "document_not_sent")) {
                // line 91
                echo "                                        <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                    ";
            }
            // line 93
            echo "
                                    ";
            // line 94
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 94) == "document_refused")) {
                // line 95
                echo "                                        <span class=\"badge bg-danger\">Documents refusés</span><br>
                                    ";
            }
            // line 97
            echo "
                                    ";
            // line 98
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "signed", [], "any", false, false, false, 98) != true)) {
                // line 99
                echo "                                        <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                    ";
            }
            // line 101
            echo "
                                    ";
            // line 102
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "emailVerified", [], "any", false, false, false, 102) != true)) {
                // line 103
                echo "                                    <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                    ";
            }
            // line 105
            echo "
                                    ";
            // line 106
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "phoneVerified", [], "any", false, false, false, 106) != true)) {
                // line 107
                echo "                                    <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                    ";
            }
            // line 109
            echo "
                                    ";
            // line 110
            if (((((twig_get_attribute($this->env, $this->source,             // line 111
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 111) == "document_waiting") && (twig_get_attribute($this->env, $this->source,             // line 112
$context["user"], "signed", [], "any", false, false, false, 112) == true)) && (twig_get_attribute($this->env, $this->source,             // line 113
$context["user"], "emailVerified", [], "any", false, false, false, 113) == true)) && (twig_get_attribute($this->env, $this->source,             // line 114
$context["user"], "phoneVerified", [], "any", false, false, false, 114) == true))) {
                // line 116
                echo "                                    <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                    ";
            }
            // line 118
            echo "
                                    ";
            // line 119
            if (((((twig_get_attribute($this->env, $this->source,             // line 120
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 120) == "document_validated") && (twig_get_attribute($this->env, $this->source,             // line 121
$context["user"], "signed", [], "any", false, false, false, 121) == true)) && (twig_get_attribute($this->env, $this->source,             // line 122
$context["user"], "emailVerified", [], "any", false, false, false, 122) == true)) && (twig_get_attribute($this->env, $this->source,             // line 123
$context["user"], "phoneVerified", [], "any", false, false, false, 123) == true))) {
                // line 125
                echo "                                    <span class=\"badge bg-success\">Travailleur validé</span><br>
                                    ";
            }
            // line 127
            echo "                                </td>
                                <td class=\"d-none d-xl-table-cell\">
                                    <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 129)]), "html", null, true);
            echo "\">
                                        <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                    </form>
                                    ";
            // line 133
            echo "                                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 133)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                </td>
                            </tr>
                            ";
            // line 136
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 136, $this->source); })()) + 1);
            // line 137
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card flex-fill\">
                    <table class=\"table table-hover my-0\">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class=\"d-none d-xl-table-cell\">Nom</th>
                                <th class=\"d-none d-xl-table-cell\">Prénom</th>
                                <th class=\"d-none d-xl-table-cell\">Adresse</th>
                                <th class=\"d-none d-md-table-cell\">Email</th>
                                <th class=\"d-none d-md-table-cell\">Statut</th>
                                <th class=\"d-none d-md-table-cell\">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        ";
        // line 161
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["validated"]) || array_key_exists("validated", $context) ? $context["validated"] : (function () { throw new RuntimeError('Variable "validated" does not exist.', 161, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 162
            echo "                            <tr>
                                <td>";
            // line 163
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 163, $this->source); })()), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 164
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 164), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 165
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 165), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 166
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "address", [], "any", false, false, false, 166), "html", null, true);
            echo "</td>
                                <td class=\"d-none d-xl-table-cell\">";
            // line 167
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 167), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 169
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 169) == "document_not_sent")) {
                // line 170
                echo "                                        <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                    ";
            }
            // line 172
            echo "
                                    ";
            // line 173
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 173) == "document_refused")) {
                // line 174
                echo "                                        <span class=\"badge bg-danger\">Documents refusés</span><br>
                                    ";
            }
            // line 176
            echo "
                                    ";
            // line 177
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "signed", [], "any", false, false, false, 177) != true)) {
                // line 178
                echo "                                        <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                    ";
            }
            // line 180
            echo "
                                    ";
            // line 181
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "emailVerified", [], "any", false, false, false, 181) != true)) {
                // line 182
                echo "                                    <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                    ";
            }
            // line 184
            echo "
                                    ";
            // line 185
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "phoneVerified", [], "any", false, false, false, 185) != true)) {
                // line 186
                echo "                                    <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                    ";
            }
            // line 188
            echo "
                                    ";
            // line 189
            if (((((twig_get_attribute($this->env, $this->source,             // line 190
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 190) == "document_waiting") && (twig_get_attribute($this->env, $this->source,             // line 191
$context["user"], "signed", [], "any", false, false, false, 191) == true)) && (twig_get_attribute($this->env, $this->source,             // line 192
$context["user"], "emailVerified", [], "any", false, false, false, 192) == true)) && (twig_get_attribute($this->env, $this->source,             // line 193
$context["user"], "phoneVerified", [], "any", false, false, false, 193) == true))) {
                // line 195
                echo "                                    <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                    ";
            }
            // line 197
            echo "
                                    ";
            // line 198
            if (((((twig_get_attribute($this->env, $this->source,             // line 199
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 199) == "document_validated") && (twig_get_attribute($this->env, $this->source,             // line 200
$context["user"], "signed", [], "any", false, false, false, 200) == true)) && (twig_get_attribute($this->env, $this->source,             // line 201
$context["user"], "emailVerified", [], "any", false, false, false, 201) == true)) && (twig_get_attribute($this->env, $this->source,             // line 202
$context["user"], "phoneVerified", [], "any", false, false, false, 202) == true))) {
                // line 204
                echo "                                    <span class=\"badge bg-success\">Travailleur validé</span><br>
                                    ";
            }
            // line 206
            echo "                                </td>
                                <td class=\"d-none d-xl-table-cell\">
                                    <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 208
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 208)]), "html", null, true);
            echo "\">
                                        <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                    </form>
                                    ";
            // line 212
            echo "                                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 212)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                </td>
                            </tr>
                            ";
            // line 215
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 215, $this->source); })()) + 1);
            // line 216
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 217
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\"><div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom</th>
                            <th class=\"d-none d-xl-table-cell\">Prénom</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Email</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 239
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["waiting"]) || array_key_exists("waiting", $context) ? $context["waiting"] : (function () { throw new RuntimeError('Variable "waiting" does not exist.', 239, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 240
            echo "                        <tr>
                            <td>";
            // line 241
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 241, $this->source); })()), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 242
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 242), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 243
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 243), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 244
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "address", [], "any", false, false, false, 244), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 245
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 245), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 247
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 247) == "document_not_sent")) {
                // line 248
                echo "                                    <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                ";
            }
            // line 250
            echo "
                                ";
            // line 251
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "getDocumentsStatus", [], "any", false, false, false, 251) == "document_refused")) {
                // line 252
                echo "                                    <span class=\"badge bg-danger\">Documents refusés</span><br>
                                ";
            }
            // line 254
            echo "
                                ";
            // line 255
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "signed", [], "any", false, false, false, 255) != true)) {
                // line 256
                echo "                                    <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                ";
            }
            // line 258
            echo "
                                ";
            // line 259
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "emailVerified", [], "any", false, false, false, 259) != true)) {
                // line 260
                echo "                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                ";
            }
            // line 262
            echo "
                                ";
            // line 263
            if ((twig_get_attribute($this->env, $this->source, $context["user"], "phoneVerified", [], "any", false, false, false, 263) != true)) {
                // line 264
                echo "                                <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                ";
            }
            // line 266
            echo "
                                ";
            // line 267
            if (((((twig_get_attribute($this->env, $this->source,             // line 268
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 268) == "document_waiting") && (twig_get_attribute($this->env, $this->source,             // line 269
$context["user"], "signed", [], "any", false, false, false, 269) == true)) && (twig_get_attribute($this->env, $this->source,             // line 270
$context["user"], "emailVerified", [], "any", false, false, false, 270) == true)) && (twig_get_attribute($this->env, $this->source,             // line 271
$context["user"], "phoneVerified", [], "any", false, false, false, 271) == true))) {
                // line 273
                echo "                                <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                ";
            }
            // line 275
            echo "
                                ";
            // line 276
            if (((((twig_get_attribute($this->env, $this->source,             // line 277
$context["user"], "getDocumentsStatus", [], "any", false, false, false, 277) == "document_validated") && (twig_get_attribute($this->env, $this->source,             // line 278
$context["user"], "signed", [], "any", false, false, false, 278) == true)) && (twig_get_attribute($this->env, $this->source,             // line 279
$context["user"], "emailVerified", [], "any", false, false, false, 279) == true)) && (twig_get_attribute($this->env, $this->source,             // line 280
$context["user"], "phoneVerified", [], "any", false, false, false, 280) == true))) {
                // line 282
                echo "                                <span class=\"badge bg-success\">Travailleur validé</span><br>
                                ";
            }
            // line 284
            echo "                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 286
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_user", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 286)]), "html", null, true);
            echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                ";
            // line 290
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 290)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                            </td>
                        </tr>
                        ";
            // line 293
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 293, $this->source); })()) + 1);
            // line 294
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 295
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/user/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  565 => 295,  559 => 294,  557 => 293,  550 => 290,  544 => 286,  540 => 284,  536 => 282,  534 => 280,  533 => 279,  532 => 278,  531 => 277,  530 => 276,  527 => 275,  523 => 273,  521 => 271,  520 => 270,  519 => 269,  518 => 268,  517 => 267,  514 => 266,  510 => 264,  508 => 263,  505 => 262,  501 => 260,  499 => 259,  496 => 258,  492 => 256,  490 => 255,  487 => 254,  483 => 252,  481 => 251,  478 => 250,  474 => 248,  472 => 247,  467 => 245,  463 => 244,  459 => 243,  455 => 242,  451 => 241,  448 => 240,  444 => 239,  420 => 217,  414 => 216,  412 => 215,  405 => 212,  399 => 208,  395 => 206,  391 => 204,  389 => 202,  388 => 201,  387 => 200,  386 => 199,  385 => 198,  382 => 197,  378 => 195,  376 => 193,  375 => 192,  374 => 191,  373 => 190,  372 => 189,  369 => 188,  365 => 186,  363 => 185,  360 => 184,  356 => 182,  354 => 181,  351 => 180,  347 => 178,  345 => 177,  342 => 176,  338 => 174,  336 => 173,  333 => 172,  329 => 170,  327 => 169,  322 => 167,  318 => 166,  314 => 165,  310 => 164,  306 => 163,  303 => 162,  299 => 161,  274 => 138,  268 => 137,  266 => 136,  259 => 133,  253 => 129,  249 => 127,  245 => 125,  243 => 123,  242 => 122,  241 => 121,  240 => 120,  239 => 119,  236 => 118,  232 => 116,  230 => 114,  229 => 113,  228 => 112,  227 => 111,  226 => 110,  223 => 109,  219 => 107,  217 => 106,  214 => 105,  210 => 103,  208 => 102,  205 => 101,  201 => 99,  199 => 98,  196 => 97,  192 => 95,  190 => 94,  187 => 93,  183 => 91,  181 => 90,  176 => 88,  172 => 87,  168 => 86,  164 => 85,  160 => 84,  157 => 83,  153 => 82,  122 => 53,  108 => 45,  104 => 43,  100 => 42,  89 => 34,  78 => 25,  71 => 24,  58 => 5,  50 => 1,  48 => 8,  46 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% set i = 1 %}

{% block title %}Travailleurs{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Liste des travailleurs</h1>
    <a href=\"{{ path('app_user_new_user') }}\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i>Enrégistrer un utilisateur</a>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
    
    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Validés</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Non validés</span>
        </li>
    </ul>
    <div class=\"container__inner\">
       <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card flex-fill\">
                    <table class=\"table table-hover my-0\">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class=\"d-none d-xl-table-cell\">Nom</th>
                                <th class=\"d-none d-xl-table-cell\">Prénom</th>
                                <th class=\"d-none d-xl-table-cell\">Adresse</th>
                                <th class=\"d-none d-md-table-cell\">Email</th>
                                <th class=\"d-none d-md-table-cell\">Statut</th>
                                <th class=\"d-none d-md-table-cell\">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for user in all %}
                            <tr>
                                <td>{{ i }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.lastname }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.firstname }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.address }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.email }}</td>
                                <td>
                                    {% if user.getDocumentsStatus == \"document_not_sent\" %}
                                        <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                    {% endif %}

                                    {% if user.getDocumentsStatus == \"document_refused\" %}
                                        <span class=\"badge bg-danger\">Documents refusés</span><br>
                                    {% endif %}

                                    {% if user.signed != true %}
                                        <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                    {% endif %}

                                    {% if user.emailVerified != true %}
                                    <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                    {% endif %}

                                    {% if user.phoneVerified != true %}
                                    <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                    {% endif %}

                                    {% if 
                                        user.getDocumentsStatus == \"document_waiting\" 
                                        and user.signed == true
                                        and user.emailVerified == true
                                        and user.phoneVerified == true 
                                    %}
                                    <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                    {% endif %}

                                    {% if 
                                        user.getDocumentsStatus == \"document_validated\" 
                                        and user.signed == true
                                        and user.emailVerified == true
                                        and user.phoneVerified == true 
                                    %}
                                    <span class=\"badge bg-success\">Travailleur validé</span><br>
                                    {% endif %}
                                </td>
                                <td class=\"d-none d-xl-table-cell\">
                                    <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_user', {'id': user.id}) }}\">
                                        <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                    </form>
                                    {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                    <a href=\"{{ path('app_user_detail', {'id': user.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                </td>
                            </tr>
                            {% set i=i+1 %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"card flex-fill\">
                    <table class=\"table table-hover my-0\">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class=\"d-none d-xl-table-cell\">Nom</th>
                                <th class=\"d-none d-xl-table-cell\">Prénom</th>
                                <th class=\"d-none d-xl-table-cell\">Adresse</th>
                                <th class=\"d-none d-md-table-cell\">Email</th>
                                <th class=\"d-none d-md-table-cell\">Statut</th>
                                <th class=\"d-none d-md-table-cell\">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for user in validated %}
                            <tr>
                                <td>{{ i }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.lastname }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.firstname }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.address }}</td>
                                <td class=\"d-none d-xl-table-cell\">{{ user.email }}</td>
                                <td>
                                    {% if user.getDocumentsStatus == \"document_not_sent\" %}
                                        <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                    {% endif %}

                                    {% if user.getDocumentsStatus == \"document_refused\" %}
                                        <span class=\"badge bg-danger\">Documents refusés</span><br>
                                    {% endif %}

                                    {% if user.signed != true %}
                                        <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                    {% endif %}

                                    {% if user.emailVerified != true %}
                                    <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                    {% endif %}

                                    {% if user.phoneVerified != true %}
                                    <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                    {% endif %}

                                    {% if 
                                        user.getDocumentsStatus == \"document_waiting\" 
                                        and user.signed == true
                                        and user.emailVerified == true
                                        and user.phoneVerified == true 
                                    %}
                                    <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                    {% endif %}

                                    {% if 
                                        user.getDocumentsStatus == \"document_validated\" 
                                        and user.signed == true
                                        and user.emailVerified == true
                                        and user.phoneVerified == true 
                                    %}
                                    <span class=\"badge bg-success\">Travailleur validé</span><br>
                                    {% endif %}
                                </td>
                                <td class=\"d-none d-xl-table-cell\">
                                    <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_user', {'id': user.id}) }}\">
                                        <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                    </form>
                                    {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                    <a href=\"{{ path('app_user_detail', {'id': user.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                </td>
                            </tr>
                            {% set i=i+1 %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container__inner container__inner_hidden\"><div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom</th>
                            <th class=\"d-none d-xl-table-cell\">Prénom</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Email</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for user in waiting %}
                        <tr>
                            <td>{{ i }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ user.lastname }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ user.firstname }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ user.address }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ user.email }}</td>
                            <td>
                                {% if user.getDocumentsStatus == \"document_not_sent\" %}
                                    <span class=\"badge bg-danger\">Documents non envoyés</span><br>
                                {% endif %}

                                {% if user.getDocumentsStatus == \"document_refused\" %}
                                    <span class=\"badge bg-danger\">Documents refusés</span><br>
                                {% endif %}

                                {% if user.signed != true %}
                                    <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                {% endif %}

                                {% if user.emailVerified != true %}
                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                {% endif %}

                                {% if user.phoneVerified != true %}
                                <span class=\"badge bg-danger\">Téléphone non vérifié</span><br>
                                {% endif %}

                                {% if 
                                    user.getDocumentsStatus == \"document_waiting\" 
                                    and user.signed == true
                                    and user.emailVerified == true
                                    and user.phoneVerified == true 
                                %}
                                <span class=\"badge bg-warning\">En attente d'approbation</span><br>
                                {% endif %}

                                {% if 
                                    user.getDocumentsStatus == \"document_validated\" 
                                    and user.signed == true
                                    and user.emailVerified == true
                                    and user.phoneVerified == true 
                                %}
                                <span class=\"badge bg-success\">Travailleur validé</span><br>
                                {% endif %}
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_user', {'id': user.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce travailleur ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                <a href=\"{{ path('app_user_detail', {'id': user.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                            </td>
                        </tr>
                        {% set i=i+1 %}
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
{% endblock %}
", "admin/user/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/user/index.html.twig");
    }
}
