<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* email/validation.html.twig */
class __TwigTemplate_d8cdb0d19cb973d3c4ac9552319d18b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/validation.html.twig"));

        // line 1
        ob_start();
        // line 2
        echo "<style>
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3 style=\"font-weight:700; margin:0;\">Validation du compte
\t\t\t\t\t";
        // line 59
        echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 59, $this->source); })()), "html", null, true);
        echo "</h3>
\t\t\t\t<hr>
\t\t\t\t<h3>Salut</h3>
\t\t\t\t<h1 style=\"font-weight:900; margin-bottom:25px\">
\t\t\t\t\tLe compte
\t\t\t\t\t";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 64, $this->source); })()), "html", null, true);
        echo "
\t\t\t\t\tvient d'être validé.</h1>
\t\t\t\t<hr>
\t\t\t\t<p>Vous pouvez maintenant publier des Shifts avec QuickDep.</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>

";
        $___internal_parse_3_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        echo Twig\Extra\CssInliner\twig_inline_css($___internal_parse_3_);
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "email/validation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 1,  109 => 64,  101 => 59,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% apply inline_css %}
<style>
\tmain {
\t\tpadding: 50px 0;
\t\tbackground: #eee;
\t\tmin-height: 100vh;
\t\tcolor: #000;
\t}

\tp {
\t\tmargin: 0 !important;
\t}

\tmain > .container {
\t\twidth: 700px;
\t\tmax-width: 90%;
\t\tborder-radius: 5px;
\t\tpadding: 0;
\t\tmargin: 0 auto;
\t\ttext-align: center;
\t}

\tmain > .container .top {
\t\tpadding: 50px;
\t\tbackground-color: #29216B;
\t\tcolor: white;
\t\tbackground-size: cover;
\t}

\t.mail-body {
\t\tpadding: 30px 40px;
\t\ttext-align: left;
\t\tbackground-color: #f7f7f7;
\t\tmargin-bottom: 18px;
\t}

\t.bottom {
\t\tpadding: 30px 40px;
\t\tbackground-color: #f7f7f7;
\t}

\t.links {
\t\ttext-align: center;
\t\tpadding-top: 30px;
\t\tfont-size: 13px
\t}

\t.links a {
\t\ttext-decoration: none;
\t\tcolor: #29216B;
\t\tfont-weight: 900
\t}
</style>
<body>
\t<main>
\t\t<div class=\"container\">
\t\t\t<div class=\"mail-body\">
\t\t\t\t<h3 style=\"font-weight:700; margin:0;\">Validation du compte
\t\t\t\t\t{{name}}</h3>
\t\t\t\t<hr>
\t\t\t\t<h3>Salut</h3>
\t\t\t\t<h1 style=\"font-weight:900; margin-bottom:25px\">
\t\t\t\t\tLe compte
\t\t\t\t\t{{ name }}
\t\t\t\t\tvient d'être validé.</h1>
\t\t\t\t<hr>
\t\t\t\t<p>Vous pouvez maintenant publier des Shifts avec QuickDep.</p>
\t\t\t</div>
\t\t\t<div class=\"bottom\">
\t\t\t\t<p>Ceci est un message automatique envoyé par notre serveur.
\t\t\t\t\t<b>Ne repondez pas à ce message!</b>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"links\">
\t\t\t\tEnvoyé par
\t\t\t\t<a href=\"http://quickdep.ca\" target=\"_blank\">Compagnie QuickDep inc.</a>
\t\t\t\t<p>2955 avenue Maricourt, Québec, QC G1W 4T8</p>
\t\t\t</div>
\t\t</div>

\t</main>


\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js\" integrity=\"sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK\" crossorigin=\"anonymous\"></script>
</body>

{% endapply %}
", "email/validation.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/email/validation.html.twig");
    }
}
