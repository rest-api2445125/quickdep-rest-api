<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/contract/new-shift.html.twig */
class __TwigTemplate_afce8096e44e226b7ca9e1e18d08ceed extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/contract/new-shift.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/contract/new-shift.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 3, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier le shift : " . twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 3, $this->source); })()), "code", [], "any", false, false, false, 3)), "html", null, true))) : (print ("Publier un shift")));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "request", [], "any", false, false, false, 9), "headers", [], "any", false, false, false, 9), "get", [0 => "referer"], "method", false, false, false, 9), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">";
        // line 18
        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 18, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier le shift : " . twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 18, $this->source); })()), "code", [], "any", false, false, false, 18)), "html", null, true))) : (print ("Publier un shift")));
        echo "</h5>
        </div>
        <div class=\"card-body\">
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <div class=\"row\">
                    <div class=\"col-6\">

                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 25, $this->source); })()), "job", [], "any", false, false, false, 25), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Poste *"]);
        // line 27
        echo "
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 28, $this->source); })()), "job", [], "any", false, false, false, 28), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "
                        
                        <br>
                        <label class=\"form-check\">
                            <input id=\"consecutiveCheck\" class=\"form-check-input\" type=\"checkbox\" name=\"consecutive\">
                            <span class=\"form-check-label\">
                                Shifts consécutifs ?
                            </span>
                        </label>
                        <br>
                        <div class=\"row\">
                            <div class=\"col-6\">
                                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 40, $this->source); })()), "startdate", [], "any", false, false, false, 40), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date et heure du début *"]);
        // line 42
        echo "<br>
                                ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 43, $this->source); })()), "startdate", [], "any", false, false, false, 43), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%"]]);
        echo "
                            </div>
                            <div class=\"col-6\">
                                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 46, $this->source); })()), "enddate", [], "any", false, false, false, 46), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date et heure de la fin *"]);
        // line 48
        echo "<br>
                                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 49, $this->source); })()), "enddate", [], "any", false, false, false, 49), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%"]]);
        echo "
                            </div>
                        </div>

                        <br>

                        ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 55, $this->source); })()), "pause", [], "any", false, false, false, 55), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Durée de la pause"]);
        // line 57
        echo "
                        ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 58, $this->source); })()), "pause", [], "any", false, false, false, 58), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "

                        <br>
                        <label class=\"form-check\">
                            <input id=\"diffAdress\" ";
        // line 62
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 62, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 62, $this->source); })()), "address", [], "any", false, false, false, 62) != (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 62, $this->source); })()))) ? ("checked") : (""))) : (""));
        echo " class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Addresse différente de l'adresse de l'entreprise ?
                            </span>
                        </label>   

                        <div style='display:";
        // line 68
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 68, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 68, $this->source); })()), "address", [], "any", false, false, false, 68) != (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 68, $this->source); })()))) ? ("block") : ("none"))) : ("none"));
        echo "' id='newAddress'>
                            <br>

                            ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 71, $this->source); })()), "address", [], "any", false, false, false, 71), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse"]);
        // line 73
        echo "
                            ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 74, $this->source); })()), "address", [], "any", false, false, false, 74), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                
                                <br>

                                ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 78, $this->source); })()), "town", [], "any", false, false, false, 78), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Ville"]);
        // line 80
        echo "
                                ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 81, $this->source); })()), "town", [], "any", false, false, false, 81), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "
                        </div>
                        
                        <br>
                                     
                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 88, $this->source); })()), "client", [], "any", false, false, false, 88), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Entréprise"]);
        // line 90
        echo "
                        ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 91, $this->source); })()), "client", [], "any", false, false, false, 91), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "

                        <br >

                        <label class=\"form-check\">
                            <input id=\"clothCheck\" ";
        // line 96
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 96, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 96, $this->source); })()), "cloth", [], "any", false, false, false, 96) != null)) ? ("checked") : (""))) : (""));
        echo " class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Tenue exigée
                            </span>
                        </label>
                        <div style=\"display:";
        // line 101
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 101, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 101, $this->source); })()), "cloth", [], "any", false, false, false, 101) != null)) ? ("block") : ("none"))) : ("none"));
        echo "\" id=\"clothDiv\">
                            <br> 
                            ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 103, $this->source); })()), "cloth", [], "any", false, false, false, 103), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Entrez la tenue exigée"]]);
        echo "
                        </div>
                        <br>

                        <label class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 109, $this->source); })()), "parking", [], "any", false, false, false, 109), 'widget', ["attr" => ["class" => "form-check-input"]]);
        echo "
                            <span class=\"form-check-label\">
                                ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 111, $this->source); })()), "parking", [], "any", false, false, false, 111), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Stationnement"]);
        // line 113
        echo "
                            </span>
                        </label>

                        <br>

                        ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 119, $this->source); })()), "description", [], "any", false, false, false, 119), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Description"]);
        // line 121
        echo "
                        ";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 122, $this->source); })()), "description", [], "any", false, false, false, 122), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                        <br>

                        ";
        // line 126
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 126, $this->source); })()), "bonus", [], "any", false, false, false, 126), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Bonus"]);
        // line 128
        echo "
                        ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 129, $this->source); })()), "bonus", [], "any", false, false, false, 129), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "

                        <br>
                        <br>
                        <br>

                        <div style=\"position:absolute; bottom:0; right:20\">
                            ";
        // line 136
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 136, $this->source); })()), "submit", [], "any", false, false, false, 136), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => (("return confirm('Etes-vous sûr de vouloir " . (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 136, $this->source); })())) ? ("modifier") : ("publier"))) . " ce shift ?')")]]);
        echo "
                        </div>
                    </div>
                </div>
            ";
        // line 140
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 140, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/contract/new-shift.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  286 => 140,  279 => 136,  269 => 129,  266 => 128,  264 => 126,  257 => 122,  254 => 121,  252 => 119,  244 => 113,  242 => 111,  237 => 109,  228 => 103,  223 => 101,  215 => 96,  207 => 91,  204 => 90,  202 => 88,  192 => 81,  189 => 80,  187 => 78,  180 => 74,  177 => 73,  175 => 71,  169 => 68,  160 => 62,  153 => 58,  150 => 57,  148 => 55,  139 => 49,  136 => 48,  134 => 46,  128 => 43,  125 => 42,  123 => 40,  108 => 28,  105 => 27,  103 => 25,  96 => 21,  90 => 18,  78 => 9,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}{{ edit ? 'Modifier le shift : ' ~ data.code : 'Publier un shift' }}{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">{{ edit ? 'Modifier le shift : ' ~ data.code: 'Publier un shift' }}</h5>
        </div>
        <div class=\"card-body\">
            {{ form_start(contractForm) }}
                <div class=\"row\">
                    <div class=\"col-6\">

                        {{ form_label(contractForm.job, 'Poste *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.job, { 'attr': {'class': 'form-select form-control-lg'} }) }}
                        
                        <br>
                        <label class=\"form-check\">
                            <input id=\"consecutiveCheck\" class=\"form-check-input\" type=\"checkbox\" name=\"consecutive\">
                            <span class=\"form-check-label\">
                                Shifts consécutifs ?
                            </span>
                        </label>
                        <br>
                        <div class=\"row\">
                            <div class=\"col-6\">
                                {{ form_label(contractForm.startdate, 'Date et heure du début *', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}<br>
                                {{ form_widget(contractForm.startdate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%'} }) }}
                            </div>
                            <div class=\"col-6\">
                                {{ form_label(contractForm.enddate, 'Date et heure de la fin *', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}<br>
                                {{ form_widget(contractForm.enddate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%'} }) }}
                            </div>
                        </div>

                        <br>

                        {{ form_label(contractForm.pause, 'Durée de la pause', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.pause, { 'attr': {'class': 'form-select form-control-lg'} }) }}

                        <br>
                        <label class=\"form-check\">
                            <input id=\"diffAdress\" {{ edit ? (data.address != address ? 'checked' : '' ) : '' }} class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Addresse différente de l'adresse de l'entreprise ?
                            </span>
                        </label>   

                        <div style='display:{{ edit ? (data.address != address ? 'block' : 'none' ) : 'none' }}' id='newAddress'>
                            <br>

                            {{ form_label(contractForm.address, 'Adresse', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(contractForm.address, { 'attr': {'class': 'form-control'} }) }}
                                
                                <br>

                                {{ form_label(contractForm.town, 'Ville', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}
                                {{ form_widget(contractForm.town, { 'attr': {'class': 'form-select form-control-lg'} }) }}
                        </div>
                        
                        <br>
                                     
                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        {{ form_label(contractForm.client, 'Entréprise', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.client, { 'attr': {'class': 'form-select form-control-lg'} }) }}

                        <br >

                        <label class=\"form-check\">
                            <input id=\"clothCheck\" {{ edit ? (data.cloth != null ? 'checked' : '' ) : ''}} class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Tenue exigée
                            </span>
                        </label>
                        <div style=\"display:{{ edit ? (data.cloth != null ? 'block' : 'none' ) : 'none'}}\" id=\"clothDiv\">
                            <br> 
                            {{ form_widget(contractForm.cloth, { 'attr': {'class': 'form-control', 'placeholder': 'Entrez la tenue exigée'} }) }}
                        </div>
                        <br>

                        <label class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            {{ form_widget(contractForm.parking, { 'attr': {'class': 'form-check-input'} }) }}
                            <span class=\"form-check-label\">
                                {{ form_label(contractForm.parking, 'Stationnement', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            </span>
                        </label>

                        <br>

                        {{ form_label(contractForm.description, 'Description', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.description,  { 'attr': {'class': 'form-control'} }) }}

                        <br>

                        {{ form_label(contractForm.bonus, 'Bonus', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.bonus, { 'attr': {'class': 'form-select form-control-lg'} }) }}

                        <br>
                        <br>
                        <br>

                        <div style=\"position:absolute; bottom:0; right:20\">
                            {{ form_widget(contractForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir \" ~ (edit ? \"modifier\" : \"publier\") ~ \" ce shift ?')\"} }) }}
                        </div>
                    </div>
                </div>
            {{ form_end(contractForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "admin/contract/new-shift.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/contract/new-shift.html.twig");
    }
}
