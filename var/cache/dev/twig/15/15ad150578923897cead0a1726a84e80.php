<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/EmployeeCard.html.twig */
class __TwigTemplate_1748c7e3e9f1e5a180c6d277908866a3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/EmployeeCard.html.twig"));

        // line 1
        echo "
<div class=\"apply-card col-md-4\">
    <div>
        <div style=\"margin-bottom:10px\">
            <div style=\"display:flex;align-items:center\">
                <img
                    src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 7, $this->source); })()))), "html", null, true);
        echo "\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 9, $this->source); })()), "html", null, true);
        echo "\"
                    style=\"width:50px !important;height:50px;object-fit: cover\"
                />
                <div style=\"display:inline-block;margin-left:10px\">
                    <h5 class=\"card-title mb-0\" style=\"\">";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 13, $this->source); })()), "html", null, true);
        echo "</h5>
                    ";
        // line 14
        if ((isset($context["showDetails"]) || array_key_exists("showDetails", $context) ? $context["showDetails"] : (function () { throw new RuntimeError('Variable "showDetails" does not exist.', 14, $this->source); })())) {
            // line 15
            echo "                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
            echo twig_escape_filter($this->env, (isset($context["email"]) || array_key_exists("email", $context) ? $context["email"] : (function () { throw new RuntimeError('Variable "email" does not exist.', 15, $this->source); })()), "html", null, true);
            echo "</p>
                    <p class=\"mb-0\" style=\"font-size:11px; font-weight:bold\">";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["phone"]) || array_key_exists("phone", $context) ? $context["phone"] : (function () { throw new RuntimeError('Variable "phone" does not exist.', 16, $this->source); })()), "html", null, true);
            echo "</p>
                    ";
        }
        // line 18
        echo "                    <p class=\"card-title mb-0 text-success\" style=\"font-size:13px\">";
        echo twig_escape_filter($this->env, (isset($context["applyStatus"]) || array_key_exists("applyStatus", $context) ? $context["applyStatus"] : (function () { throw new RuntimeError('Variable "applyStatus" does not exist.', 18, $this->source); })()), "html", null, true);
        echo "</p>
                </div>
            </div>
        </div>
        <hr style=\"
            margin: 5px 0;
            opacity: .1;
        \">
        <div class=\"row\" style=\"text-align:center; font-size:10px;\">
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Complétés</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 29, $this->source); })()), "completed", [], "any", false, false, false, 29), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Annulés</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 33, $this->source); })()), "canceled", [], "any", false, false, false, 33), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Absences</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 37, $this->source); })()), "notWorked", [], "any", false, false, false, 37), "html", null, true);
        echo "</p>

            </div>
        </div>
        ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["experiences"]) || array_key_exists("experiences", $context) ? $context["experiences"] : (function () { throw new RuntimeError('Variable "experiences" does not exist.', 41, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
            // line 42
            echo "            <div class=\"ex-class\">
                <div>";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "title", [], "any", false, false, false, 43), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["experience"], "time", [], "any", false, false, false, 43), "html", null, true);
            echo "</div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo " 
        ";
        // line 46
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 46, $this->source); })()), "status", [], "any", false, false, false, 46) == "contract_published")) {
            // line 47
            echo "            <form  method=\"POST\" style=\"display:inline\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_contract_confirm", ["user_id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 47, $this->source); })()), "employee", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47), "contract_id" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 47, $this->source); })()), "id", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\">
                <button type=\"submit\" onclick=\"return confirm('Etes-vous sûr de vouloir attribuer ce shift à cet travailleur ?')\" type=\"submit\" style=\"display:block; margin-top:10px; padding: 5px 15px; width:100%\" class=\"btn btn-primary\">Approuver</button>
            </form>
        ";
        }
        // line 51
        echo "        ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 51, $this->source); })()), "status", [], "any", false, false, false, 51) == "contract_approuved")) {
            // line 52
            echo "            <form  method=\"POST\" style=\"display:inline\" action=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_contract_cancel", ["user_id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 52, $this->source); })()), "employee", [], "any", false, false, false, 52), "id", [], "any", false, false, false, 52), "contract_id" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 52, $this->source); })()), "id", [], "any", false, false, false, 52)]), "html", null, true);
            echo "\">
                <button type=\"submit\" onclick=\"return confirm('Etes-vous sûr de vouloir rétirer ce shift à cet travailleur ?')\" type=\"submit\" style=\"display:block; margin-top:10px; padding: 5px 15px; width:100%\" class=\"btn btn-danger\">rétirer</button>
            </form>
        ";
        }
        // line 56
        echo "    </div>
</div>";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "components/EmployeeCard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 56,  146 => 52,  143 => 51,  135 => 47,  133 => 46,  130 => 45,  119 => 43,  116 => 42,  112 => 41,  105 => 37,  98 => 33,  91 => 29,  76 => 18,  71 => 16,  66 => 15,  64 => 14,  60 => 13,  53 => 9,  48 => 7,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<div class=\"apply-card col-md-4\">
    <div>
        <div style=\"margin-bottom:10px\">
            <div style=\"display:flex;align-items:center\">
                <img
                    src=\"{{asset('users/images/'~image)}}\"
                    class=\"avatar img-fluid rounded me-1\"
                    alt=\"{{ name }}\"
                    style=\"width:50px !important;height:50px;object-fit: cover\"
                />
                <div style=\"display:inline-block;margin-left:10px\">
                    <h5 class=\"card-title mb-0\" style=\"\">{{ name }}</h5>
                    {% if showDetails %}
                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ email }}</p>
                    <p class=\"mb-0\" style=\"font-size:11px; font-weight:bold\">{{ phone }}</p>
                    {% endif %}
                    <p class=\"card-title mb-0 text-success\" style=\"font-size:13px\">{{ applyStatus }}</p>
                </div>
            </div>
        </div>
        <hr style=\"
            margin: 5px 0;
            opacity: .1;
        \">
        <div class=\"row\" style=\"text-align:center; font-size:10px;\">
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Complétés</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">{{ stats.completed }}</p>
            </div>
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Annulés</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">{{ stats.canceled }}</p>
            </div>
            <div class=\"col-4\">
                <p style=\"opacity:.7; margin:0\">Absences</p>
                <p style=\"font-weight:bold; font-size:13; margin-bottom:5px\">{{ stats.notWorked }}</p>

            </div>
        </div>
        {% for experience in experiences %}
            <div class=\"ex-class\">
                <div>{{ experience.title }} | {{ experience.time }}</div>
            </div>
        {% endfor %} 
        {% if contract.status == 'contract_published' %}
            <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('app_client_web_contract_confirm', {'user_id': contract.employee.id, 'contract_id': contract.id}) }}\">
                <button type=\"submit\" onclick=\"return confirm('Etes-vous sûr de vouloir attribuer ce shift à cet travailleur ?')\" type=\"submit\" style=\"display:block; margin-top:10px; padding: 5px 15px; width:100%\" class=\"btn btn-primary\">Approuver</button>
            </form>
        {% endif %}
        {% if contract.status == 'contract_approuved' %}
            <form  method=\"POST\" style=\"display:inline\" action=\"{{ path('app_client_web_contract_cancel', {'user_id': contract.employee.id, 'contract_id': contract.id}) }}\">
                <button type=\"submit\" onclick=\"return confirm('Etes-vous sûr de vouloir rétirer ce shift à cet travailleur ?')\" type=\"submit\" style=\"display:block; margin-top:10px; padding: 5px 15px; width:100%\" class=\"btn btn-danger\">rétirer</button>
            </form>
        {% endif %}
    </div>
</div>", "components/EmployeeCard.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/components/EmployeeCard.html.twig");
    }
}
