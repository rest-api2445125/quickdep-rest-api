<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/time_sheet/index.html.twig */
class __TwigTemplate_46d85b4013b025f9232284bc575faf79 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "client_web/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/time_sheet/index.html.twig"));

        // line 6
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("client_web/base.html.twig", "client_web/time_sheet/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Feuilles de temps";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Feuilles de temps à valider</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 39));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 40
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 42
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
    <div class=\"row\">
        
        ";
        // line 53
        if ((twig_length_filter($this->env, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 53, $this->source); })())) == 1)) {
            // line 54
            echo "            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 54, $this->source); })()), 0, [], "array", false, false, false, 54), "html", null, true);
            echo "</h3>
        ";
        } else {
            // line 56
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contracts"]) || array_key_exists("contracts", $context) ? $context["contracts"] : (function () { throw new RuntimeError('Variable "contracts" does not exist.', 56, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["contract"]) {
                // line 57
                echo "                ";
                if (twig_test_iterable($context["contract"])) {
                    // line 58
                    echo "                    ";
                    // line 59
                    $context["month"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], 0, [], "array", false, false, false, 59), " ");
                    // line 61
                    echo "                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">";
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["months"]) || array_key_exists("months", $context) ? $context["months"] : (function () { throw new RuntimeError('Variable "months" does not exist.', 61, $this->source); })()), (twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 61, $this->source); })()), 0, [], "array", false, false, false, 61) - 1), [], "array", false, false, false, 61) . " ") . twig_get_attribute($this->env, $this->source, (isset($context["month"]) || array_key_exists("month", $context) ? $context["month"] : (function () { throw new RuntimeError('Variable "month" does not exist.', 61, $this->source); })()), 1, [], "array", false, false, false, 61)), "html", null, true);
                    echo "</h3>
                    <br>
                    <br>
                ";
                } else {
                    // line 65
                    echo "                    <div class=\"col-12 col-md-6 col-lg-3\">
                        <a href=\"";
                    // line 66
                    (((twig_get_attribute($this->env, $this->source, $context["contract"], "status", [], "any", false, false, false, 66) == "contract_worked")) ? (print ("#")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_timesheet_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["contract"], "id", [], "any", false, false, false, 66)]), "html", null, true))));
                    echo "\"  class='contract-card-link'>
                        <div class=\"card contract-card ";
                    // line 67
                    echo (((twig_get_attribute($this->env, $this->source, $context["contract"], "status", [], "any", false, false, false, 67) == "contract_worked")) ? ("disabled") : (""));
                    echo "\" style=\"cursor:pointer;";
                    echo (((twig_get_attribute($this->env, $this->source, $context["contract"], "status", [], "any", false, false, false, 67) == "contract_worked")) ? ("opacity:.6") : (""));
                    echo "\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <img
                                    src=\"";
                    // line 70
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 70, $this->source); })()), "session", [], "any", false, false, false, 70), "get", [0 => "user"], "method", false, false, false, 70), "imagePath", [], "any", false, false, false, 70))), "html", null, true);
                    echo "\"
                                    class=\"avatar img-fluid rounded me-1\"
                                    alt=\"";
                    // line 72
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 72, $this->source); })()), "session", [], "any", false, false, false, 72), "get", [0 => "user"], "method", false, false, false, 72), "client", [], "any", false, false, false, 72), "name", [], "any", false, false, false, 72), "html", null, true);
                    echo "\"
                                />
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">";
                    // line 75
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["contract"], "job", [], "any", false, false, false, 75), "title", [], "any", false, false, false, 75), "html", null, true);
                    echo "</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
                    // line 76
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "code", [], "any", false, false, false, 76), "html", null, true);
                    echo "</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <div class=\"mb-2\">
                                    <i class=\"align-middle\" data-feather=\"calendar\"></i>
                                    <h5 class=\"mb-0\" style=\"display:inline; margin-left:10px\">";
                    // line 82
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "startdate", [], "any", false, false, false, 82), "d/m/Y"), "html", null, true);
                    echo "</h5>
                                </div>
                                <div>
                                    <i class=\"align-middle\" data-feather=\"clock\"></i>
                                    <h5 style=\"display:inline; margin-left:10px\">";
                    // line 86
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "startdate", [], "any", false, false, false, 86), "G:i"), "html", null, true);
                    echo " à ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contract"], "enddate", [], "any", false, false, false, 86), "G:i"), "html", null, true);
                    echo "</h5>
                                </div>
                                    <hr style=\"opacity:0.1\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
                    // line 91
                    echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["contract"], "clientrate", [], "any", false, false, false, 91) . "\$/H"), "html", null, true);
                    echo "</h4>
                                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">";
                    // line 92
                    (((twig_get_attribute($this->env, $this->source, $context["contract"], "bonus", [], "any", false, false, false, 92) != null)) ? (print (twig_escape_filter($this->env, (("+" . twig_get_attribute($this->env, $this->source, $context["contract"], "bonus", [], "any", false, false, false, 92)) . "\$/H"), "html", null, true))) : (print ("")));
                    echo "</h6>
                                    </div>
                                    <div style=\"display:flex\">
                                        ";
                    // line 95
                    if ((twig_get_attribute($this->env, $this->source, $context["contract"], "status", [], "any", false, false, false, 95) == "contract_worked")) {
                        // line 96
                        echo "                                        <div class=\"status-box waiting small\">En attente du travailleur</div>                                            
                                        ";
                    }
                    // line 98
                    echo "                                        ";
                    if ((twig_get_attribute($this->env, $this->source, $context["contract"], "status", [], "any", false, false, false, 98) == "contract_time_sent")) {
                        // line 99
                        echo "                                        <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange;\"></i>                                        
                                        ";
                    }
                    // line 101
                    echo "                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        </a>
                    </div>
                ";
                }
                // line 109
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contract'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "        ";
        }
        // line 111
        echo "            
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/time_sheet/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 111,  244 => 110,  238 => 109,  228 => 101,  224 => 99,  221 => 98,  217 => 96,  215 => 95,  209 => 92,  205 => 91,  195 => 86,  188 => 82,  179 => 76,  175 => 75,  169 => 72,  164 => 70,  156 => 67,  152 => 66,  149 => 65,  141 => 61,  139 => 59,  137 => 58,  134 => 57,  129 => 56,  123 => 54,  121 => 53,  116 => 50,  102 => 42,  98 => 40,  94 => 39,  76 => 23,  69 => 22,  56 => 3,  48 => 1,  46 => 6,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'client_web/base.html.twig' %}

{% block title %}Feuilles de temps{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Feuilles de temps à valider</h1>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <div class=\"row\">
        
        {% if contracts|length == 1 %}
            <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ contracts[0] }}</h3>
        {% else %}
            {% for contract in contracts %}
                {% if  contract is iterable %}
                    {% 
                        set month = contract[0]|split(' ')
                    %}
                    <h3 class=\"form-label\" style=\"font-size:15px; font-weight:bold\">{{ months[month[0] - 1] ~ ' ' ~ month[1]}}</h3>
                    <br>
                    <br>
                {% else %}
                    <div class=\"col-12 col-md-6 col-lg-3\">
                        <a href=\"{{contract.status == 'contract_worked' ? '#': path('app_client_web_timesheet_detail', {'id': contract.id})}}\"  class='contract-card-link'>
                        <div class=\"card contract-card {{ contract.status == 'contract_worked' ? 'disabled': '' }}\" style=\"cursor:pointer;{{ contract.status == 'contract_worked' ? 'opacity:.6': '' }}\">
                            <div class=\"card-header\" style=\"display:flex; align-items:center\">
                                <img
                                    src=\"{{asset('users/images/'~app.session.get('user').imagePath)}}\"
                                    class=\"avatar img-fluid rounded me-1\"
                                    alt=\"{{ app.session.get('user').client.name }}\"
                                />
                                <div style=\"display:inline-block;margin-left:10px\">
                                    <h5 class=\"card-title mb-0\" style=\"\">{{ contract.job.title }}</h5>
                                    <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ contract.code }}</p>
                                </div>
                            </div>
                            <div class=\"card-body\" style=\"padding-top:0\">
                                <div class=\"mb-2\">
                                    <i class=\"align-middle\" data-feather=\"calendar\"></i>
                                    <h5 class=\"mb-0\" style=\"display:inline; margin-left:10px\">{{ contract.startdate | date('d/m/Y') }}</h5>
                                </div>
                                <div>
                                    <i class=\"align-middle\" data-feather=\"clock\"></i>
                                    <h5 style=\"display:inline; margin-left:10px\">{{ contract.startdate | date(\"G:i\") }} à {{ contract.enddate | date(\"G:i\") }}</h5>
                                </div>
                                    <hr style=\"opacity:0.1\">
                                <div style=\"cursor:pointer; position:relative; display:flex;justify-content: space-between; align-items:center\">
                                    <div>
                                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ contract.clientrate ~ \"\$/H\" }}</h4>
                                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">{{ contract.bonus != null ? \"+\" ~ contract.bonus ~ \"\$/H\": \"\" }}</h6>
                                    </div>
                                    <div style=\"display:flex\">
                                        {% if contract.status == 'contract_worked' %}
                                        <div class=\"status-box waiting small\">En attente du travailleur</div>                                            
                                        {% endif %}
                                        {% if contract.status == 'contract_time_sent' %}
                                        <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange;\"></i>                                        
                                        {% endif %}
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        </a>
                    </div>
                {% endif %}
            {% endfor %}
        {% endif %}
            
    </div>
</div>
{% endblock %}
", "client_web/time_sheet/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/time_sheet/index.html.twig");
    }
}
