<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/auth/register.html.twig */
class __TwigTemplate_650887cb47e1c40819e7c9a1a71c08d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/auth/register.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "client_web/auth/register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        Inscrivez-vous comme une <b>entréprise</b> 
    </p>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 15
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 28, $this->source); })()), 'form_start');
        echo "
                <div class=\"mb-3\">
                    ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 30, $this->source); })()), "name", [], "any", false, false, false, 30), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Nom de l'entreprise"]);
        // line 32
        echo "
                    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 33, $this->source); })()), "name", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez le nom de l'entreprise"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 36, $this->source); })()), "owner", [], "any", false, false, false, 36), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Nom du responsable"]);
        // line 38
        echo "
                    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 39, $this->source); })()), "owner", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez le nom du responsable"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 42, $this->source); })()), "phone", [], "any", false, false, false, 42), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Numéro de téléphone"]);
        // line 44
        echo "
                    ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 45, $this->source); })()), "phone", [], "any", false, false, false, 45), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez le numéro de téléphone"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 48, $this->source); })()), "address", [], "any", false, false, false, 48), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de l'entreprise"]);
        // line 50
        echo "
                    ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 51, $this->source); })()), "address", [], "any", false, false, false, 51), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez l'adresse de l'entreprise"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 54, $this->source); })()), "town", [], "any", false, false, false, 54), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Ville de l'entreprise"]);
        // line 56
        echo "
                    ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 57, $this->source); })()), "town", [], "any", false, false, false, 57), 'widget', ["attr" => ["class" => "form-select form-control", "type" => "text", "placeholder" => "Entrez la ville de l'entreprise"]]);
        echo "
                </div>
                <div class=\"mb-4\">
                    ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 60, $this->source); })()), "zipcode", [], "any", false, false, false, 60), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal"]);
        // line 62
        echo "
                    ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 63, $this->source); })()), "zipcode", [], "any", false, false, false, 63), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez le code postal"]]);
        echo "
                </div>
                <label class=\"form-check mb-2\">
                    <input id=\"diffFacture\" class=\"form-check-input\" type=\"checkbox\" value=\"\">
                    <span class=\"form-check-label\">
                        Addresse de facturation différente ?
                    </span>
                </label>
                
                <div id=\"factureAddress\" style=\"display:none\">
                    <div class=\"mb-3\">
                        ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 74, $this->source); })()), "factureaddress", [], "any", false, false, false, 74), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de facturation"]);
        // line 76
        echo "
                        ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 77, $this->source); })()), "factureaddress", [], "any", false, false, false, 77), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez l'adresse de facturatioln"]]);
        echo "
                    </div>
                    <div class=\"mb-3\">
                        ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 80, $this->source); })()), "facturezipcode", [], "any", false, false, false, 80), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal (Facturation)"]);
        // line 82
        echo "
                        ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 83, $this->source); })()), "facturezipcode", [], "any", false, false, false, 83), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "text", "placeholder" => "Entrez le code postal (de facturation)"]]);
        echo "
                    </div>
                </div>
                <hr style=\"margin-bottom:10px; opacity:.1\">
                <div class=\"mb-3\">
                    ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 88, $this->source); })()), "email", [], "any", false, false, false, 88), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse E-mail"]);
        // line 90
        echo "
                    ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 91, $this->source); })()), "email", [], "any", false, false, false, 91), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "email", "placeholder" => "Entrez votre adresse E-mail"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 94, $this->source); })()), "password", [], "any", false, false, false, 94), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Mot de passe"]);
        // line 96
        echo "
                    ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 97, $this->source); })()), "password", [], "any", false, false, false, 97), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "password", "placeholder" => "Créer un mot de passe"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 100, $this->source); })()), "passwordconfirm", [], "any", false, false, false, 100), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Confirmer le mot de passe"]);
        // line 102
        echo "
                    ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 103, $this->source); })()), "passwordconfirm", [], "any", false, false, false, 103), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "password", "placeholder" => "Confirmer le mot de passe"]]);
        echo "
                </div>
                <div class=\"text-center mt-3\">
                    ";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 106, $this->source); })()), "submit", [], "any", false, false, false, 106), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>
                <br>
                <p style=\"text-align: center\">Vous avez déjà un comte ? <b><a href=\"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth");
        echo "\">Connectez-vous</a></b></p>
            ";
        // line 110
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registerForm"]) || array_key_exists("registerForm", $context) ? $context["registerForm"] : (function () { throw new RuntimeError('Variable "registerForm" does not exist.', 110, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/auth/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 110,  252 => 109,  246 => 106,  240 => 103,  237 => 102,  235 => 100,  229 => 97,  226 => 96,  224 => 94,  218 => 91,  215 => 90,  213 => 88,  205 => 83,  202 => 82,  200 => 80,  194 => 77,  191 => 76,  189 => 74,  175 => 63,  172 => 62,  170 => 60,  164 => 57,  161 => 56,  159 => 54,  153 => 51,  150 => 50,  148 => 48,  142 => 45,  139 => 44,  137 => 42,  131 => 39,  128 => 38,  126 => 36,  120 => 33,  117 => 32,  115 => 30,  110 => 28,  103 => 23,  89 => 15,  85 => 13,  81 => 12,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Bienvenue chez <b>QuickDep</b></h1>
    <p class=\"lead\">
        Inscrivez-vous comme une <b>entréprise</b> 
    </p>
    
    {% for message in app.flashes('notice') %}
     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            {{ form_start(registerForm) }}
                <div class=\"mb-3\">
                    {{ form_label(registerForm.name, 'Nom de l\\'entreprise', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.name, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez le nom de l\\'entreprise'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.owner, 'Nom du responsable', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.owner, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez le nom du responsable'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.phone, 'Numéro de téléphone', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.phone, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez le numéro de téléphone'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.address, 'Adresse de l\\'entreprise', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.address, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez l\\'adresse de l\\'entreprise'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.town, 'Ville de l\\'entreprise', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.town, { 'attr': {'class': 'form-select form-control', 'type': 'text', 'placeholder': 'Entrez la ville de l\\'entreprise'} }) }}
                </div>
                <div class=\"mb-4\">
                    {{ form_label(registerForm.zipcode, 'Code postal', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.zipcode, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez le code postal'} }) }}
                </div>
                <label class=\"form-check mb-2\">
                    <input id=\"diffFacture\" class=\"form-check-input\" type=\"checkbox\" value=\"\">
                    <span class=\"form-check-label\">
                        Addresse de facturation différente ?
                    </span>
                </label>
                
                <div id=\"factureAddress\" style=\"display:none\">
                    <div class=\"mb-3\">
                        {{ form_label(registerForm.factureaddress, 'Adresse de facturation', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(registerForm.factureaddress, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez l\\'adresse de facturatioln'} }) }}
                    </div>
                    <div class=\"mb-3\">
                        {{ form_label(registerForm.facturezipcode, 'Code postal (Facturation)', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(registerForm.facturezipcode, { 'attr': {'class': 'form-control form-control-lg', 'type': 'text', 'placeholder': 'Entrez le code postal (de facturation)'} }) }}
                    </div>
                </div>
                <hr style=\"margin-bottom:10px; opacity:.1\">
                <div class=\"mb-3\">
                    {{ form_label(registerForm.email, 'Adresse E-mail', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.email, { 'attr': {'class': 'form-control form-control-lg', 'type': 'email', 'placeholder': 'Entrez votre adresse E-mail'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.password, 'Mot de passe', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.password, { 'attr': {'class': 'form-control form-control-lg', 'type': 'password', 'placeholder': 'Créer un mot de passe'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(registerForm.passwordconfirm, 'Confirmer le mot de passe', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(registerForm.passwordconfirm, { 'attr': {'class': 'form-control form-control-lg', 'type': 'password', 'placeholder': 'Confirmer le mot de passe'} }) }}
                </div>
                <div class=\"text-center mt-3\">
                    {{ form_widget(registerForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary'} }) }}
                </div>
                <br>
                <p style=\"text-align: center\">Vous avez déjà un comte ? <b><a href=\"{{ path(\"app_client_web_auth\") }}\">Connectez-vous</a></b></p>
            {{ form_end(registerForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "client_web/auth/register.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/auth/register.html.twig");
    }
}
