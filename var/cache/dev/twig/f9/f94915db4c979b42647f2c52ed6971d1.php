<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/time_sheet/detail.html.twig */
class __TwigTemplate_189156d0c1f9a00df5ac6e555a764863 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new RuntimeError('Variable "type" does not exist.', 1, $this->source); })()), "client_web/time_sheet/detail.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/time_sheet/detail.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Feuille de temps";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 6, $this->source); })()), 'form_start');
        echo "
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <di style=\"display:flex\">
        ";
        // line 17
        if (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 17, $this->source); })()), "status", [], "any", false, false, false, 17) != "contract_time_approuved") || ((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new RuntimeError('Variable "type" does not exist.', 17, $this->source); })()) != "client"))) {
            // line 18
            echo "        <a href=\"#\" id=\"edit-time-sheet-btn\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        ";
        }
        // line 20
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 20, $this->source); })()), "submit", [], "any", false, false, false, 20), 'widget', ["attr" => ["class" => "btn btn-lg btn-success", "onclick" => "return confirm('Etes-vous sûr de vouloir approuver ces heures ?')", "style" => ("display:" . (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 20, $this->source); })()), "status", [], "any", false, false, false, 20) == "contract_time_approuved")) ? ("none") : ("inline-block"))), "id" => "submit-ts-btn"]]);
        echo "
    </di>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 28, $this->source); })()), "request", [], "any", false, false, false, 28), "headers", [], "any", false, false, false, 28), "get", [0 => "referer"], "method", false, false, false, 28), "html", null, true);
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Feuille de temps</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, (isset($context["userClient"]) || array_key_exists("userClient", $context) ? $context["userClient"] : (function () { throw new RuntimeError('Variable "userClient" does not exist.', 43, $this->source); })()), "imagePath", [], "any", false, false, false, 43))), "html", null, true);
        echo "\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["userClient"]) || array_key_exists("userClient", $context) ? $context["userClient"] : (function () { throw new RuntimeError('Variable "userClient" does not exist.', 45, $this->source); })()), "client", [], "any", false, false, false, 45), "name", [], "any", false, false, false, 45), "html", null, true);
        echo "\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 49, $this->source); })()), "job", [], "any", false, false, false, 49), "title", [], "any", false, false, false, 49), "html", null, true);
        echo "</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 50, $this->source); })()), "code", [], "any", false, false, false, 50), "html", null, true);
        echo "</p>
                    </div>
                </div>
                ";
        // line 53
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 53, $this->source); })()), "status", [], "any", false, false, false, 53) == "contract_time_sent")) {
            // line 54
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>À valider</p>
                </div>
                ";
        }
        // line 61
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 61, $this->source); })()), "status", [], "any", false, false, false, 61) == "contract_time_approuved")) {
            // line 62
            echo "                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>Approuvé</p>
                </div>
                ";
        }
        // line 69
        echo "            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>";
        // line 77
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 77, $this->source); })()), "startdate", [], "any", false, false, false, 77), "d/m/Y"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">";
        // line 86
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 86, $this->source); })()), "clientrate", [], "any", false, false, false, 86) . "\$/H"), "html", null, true);
        echo "</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">";
        // line 87
        (((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 87, $this->source); })()), "bonus", [], "any", false, false, false, 87) != null)) ? (print (twig_escape_filter($this->env, (("+" . twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 87, $this->source); })()), "bonus", [], "any", false, false, false, 87)) . "\$/H"), "html", null, true))) : (print ("")));
        echo "</h6>
                    </div>                
                </div>
            </div>
            <hr style=\"opacity:0.1\">

            <br>

            <div style=\"display:flex; align-items:center\">
                <div style=\"width: 100px; margin:auto;margin-right:15px; text-align:right\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total heures</p>                    
                    <p id=\"tsDiff\" style=\"font-size:20px; font-weight:bold; margin-bottom:0\">";
        // line 101
        ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101)) ? (print (twig_escape_filter($this->env, (int) floor((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101), "totaltimes", [], "any", false, false, false, 101) / 60)), "html", null, true))) : (print ("00")));
        echo "H";
        ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101)) ? (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101), "totaltimes", [], "any", false, false, false, 101) % 60) == 0)) ? (print ("")) : (print (twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101), "totaltimes", [], "any", false, false, false, 101) / 60) - (int) floor((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 101, $this->source); })()), "timeSheet", [], "any", false, false, false, 101), "totaltimes", [], "any", false, false, false, 101) / 60))) * 60), "html", null, true))))) : (print ("00")));
        echo "</p>
                </div>
                <div class=\"row\" style=\"width:100%\">
                    <div class=\"col-4\">
                        ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 105, $this->source); })()), "startdate", [], "any", false, false, false, 105), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Heure du début *"]);
        // line 107
        echo "<br>
                        ";
        // line 108
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 108, $this->source); })()), "startdate", [], "any", false, false, false, 108), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%", "readonly" => "readonly"]]);
        echo "
                    </div>
                    <div class=\"col-4\">
                        ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 111, $this->source); })()), "enddate", [], "any", false, false, false, 111), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Heure de la fin *"]);
        // line 113
        echo "<br>
                        ";
        // line 114
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 114, $this->source); })()), "enddate", [], "any", false, false, false, 114), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%", "readonly" => "readonly"]]);
        echo "
                    </div>

                    <div class=\"col-4\">
                        ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 118, $this->source); })()), "pause", [], "any", false, false, false, 118), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Durée de la pause"]);
        // line 120
        echo "
                        ";
        // line 121
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 121, $this->source); })()), "pause", [], "any", false, false, false, 121), 'widget', ["attr" => ["class" => "form-select form-control-lg", "readonly" => "readonly"]]);
        echo "
                    </div>
                </div>

            </div>

            ";
        // line 127
        if ((twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 127, $this->source); })()), "status", [], "any", false, false, false, 127) != "contract_published")) {
            // line 128
            echo "            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            
            <div class=\"row\">
                ";
            // line 135
            echo $this->extensions['Symfony\UX\TwigComponent\Twig\ComponentExtension']->render("EmployeeCard", ["employee" => twig_get_attribute($this->env, $this->source, (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 135, $this->source); })()), "employee", [], "any", false, false, false, 135), "contract" => (isset($context["contract"]) || array_key_exists("contract", $context) ? $context["contract"] : (function () { throw new RuntimeError('Variable "contract" does not exist.', 135, $this->source); })())]);
            echo "
            </div>
            ";
        }
        // line 138
        echo "        </div>
    </div>
</div>
";
        // line 141
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["timeSheetForm"]) || array_key_exists("timeSheetForm", $context) ? $context["timeSheetForm"] : (function () { throw new RuntimeError('Variable "timeSheetForm" does not exist.', 141, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/time_sheet/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 141,  273 => 138,  267 => 135,  258 => 128,  256 => 127,  247 => 121,  244 => 120,  242 => 118,  235 => 114,  232 => 113,  230 => 111,  224 => 108,  221 => 107,  219 => 105,  210 => 101,  193 => 87,  189 => 86,  177 => 77,  167 => 69,  158 => 62,  155 => 61,  146 => 54,  144 => 53,  138 => 50,  134 => 49,  127 => 45,  122 => 43,  104 => 28,  92 => 20,  88 => 18,  86 => 17,  72 => 6,  65 => 5,  52 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends type %}

{% block title %}Feuille de temps{% endblock %}

{% block body %}
{{ form_start(timeSheetForm) }}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
        display: inline-block;
        margin-bottom: 0 !important
    \"></h1>
    <di style=\"display:flex\">
        {% if contract.status != 'contract_time_approuved' or type != 'client' %}
        <a href=\"#\" id=\"edit-time-sheet-btn\" style=\"margin-right:10px\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"edit-3\"></i> Modifier</a>
        {% endif %}
        {{ form_widget(timeSheetForm.submit, { 'attr': {'class': 'btn btn-lg btn-success', 'onclick': \"return confirm('Etes-vous sûr de vouloir approuver ces heures ?')\", 'style':'display:' ~ (contract.status == 'contract_time_approuved' ? 'none': 'inline-block'), 'id': 'submit-ts-btn'} }) }}
    </di>
</div>
    <br>

<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ app.request.headers.get('referer') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">Feuille de temps</h5>
        </div>
        <div class=\"card-body\">
            <div style=\"display:flex; align-items:center\">
                <div style=\"display:flex; align-items:center\">
                    <img
                        src=\"{{asset('users/images/'~userClient.imagePath)}}\"
                        class=\"avatar img-fluid rounded me-1\"
                        alt=\"{{ userClient.client.name }}\"
                        style=\"width:60px !important;height:60px\"
                    />
                    <div style=\"display:inline-block;margin-left:10px\">
                        <h5 class=\"card-title mb-0\" style=\"\">{{ contract.job.title }}</h5>
                        <p class=\"mb-0\" style=\"display:inline;font-size:11px; font-weight:bold\">{{ contract.code }}</p>
                    </div>
                </div>
                {% if contract.status == 'contract_time_sent' %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"clock\" style=\"color:orange\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box waiting\">
                    <p>À valider</p>
                </div>
                {% endif %}
                {% if contract.status == 'contract_time_approuved' %}
                <div style=\"display:flex; margin-left: 30px\">
                    <i class=\"align-middle\" data-feather=\"check-circle\" style=\"color:green; margin-right:10px\"></i>
                </div>
                <div style=\"display:flex; margin-left: 30px\" class=\"status-box approuved\">
                    <p>Approuvé</p>
                </div>
                {% endif %}
            </div>
            <hr style=\"opacity:0.1\">
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Date</p>                    
                    <p>{{ contract.startdate | date('d/m/Y') }}</p>
                </div>
                <div class=\"col-12 col-md-6 col-lg-3\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Taux horraire</p>    
                    
                    <div>
                        <h4 class=\"mb-0\" style=\"display:inline; font-weight:bold\">{{ contract.clientrate ~ \"\$/H\" }}</h4>
                        <h6 class=\"mb-0\" style=\"display:inline; color:green; font-weight:bold\">{{ contract.bonus != null ? \"+\" ~ contract.bonus ~ \"\$/H\": \"\" }}</h6>
                    </div>                
                </div>
            </div>
            <hr style=\"opacity:0.1\">

            <br>

            <div style=\"display:flex; align-items:center\">
                <div style=\"width: 100px; margin:auto;margin-right:15px; text-align:right\">
                    <p style=\"
                        font-weight: bold;
                        margin-bottom: 5px;
                    \">Total heures</p>                    
                    <p id=\"tsDiff\" style=\"font-size:20px; font-weight:bold; margin-bottom:0\">{{ contract.timeSheet ? contract.timeSheet.totaltimes // 60 : '00'}}H{{ contract.timeSheet ? (contract.timeSheet.totaltimes % 60 == 0 ? '': ((contract.timeSheet.totaltimes / 60 - contract.timeSheet.totaltimes // 60) * 60)) : '00'}}</p>
                </div>
                <div class=\"row\" style=\"width:100%\">
                    <div class=\"col-4\">
                        {{ form_label(timeSheetForm.startdate, 'Heure du début *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}<br>
                        {{ form_widget(timeSheetForm.startdate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%', 'readonly':'readonly'} }) }}
                    </div>
                    <div class=\"col-4\">
                        {{ form_label(timeSheetForm.enddate, 'Heure de la fin *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}<br>
                        {{ form_widget(timeSheetForm.enddate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%', 'readonly':'readonly'} }) }}
                    </div>

                    <div class=\"col-4\">
                        {{ form_label(timeSheetForm.pause, 'Durée de la pause', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(timeSheetForm.pause, { 'attr': {'class': 'form-select form-control-lg', 'readonly':'readonly'} }) }}
                    </div>
                </div>

            </div>

            {% if contract.status != 'contract_published' %}
            <hr style=\"opacity:0.1\">
            <p style=\"
                font-weight: bold;
                margin-bottom: 5px;
            \">Employé</p>  
            
            <div class=\"row\">
                {{ component('EmployeeCard', {employee: contract.employee, contract: contract}) }}
            </div>
            {% endif %}
        </div>
    </div>
</div>
{{ form_end(timeSheetForm) }}
{% endblock %}
", "client_web/time_sheet/detail.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/time_sheet/detail.html.twig");
    }
}
