<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/home/profile.html.twig */
class __TwigTemplate_515ec37672f2cebb030c845d769c909f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "client_web/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/home/profile.html.twig"));

        $this->parent = $this->loadTemplate("client_web/base.html.twig", "client_web/home/profile.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Profile";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"mb-3\">
        <h1 class=\"h3 d-inline align-middle\">Profile</h1>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Details du compte</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "imagePath", [], "any", false, false, false, 16) == null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("clientweb/img/logo.png")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("users/images/" . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "imagePath", [], "any", false, false, false, 16))))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "client", [], "any", false, false, false, 16), "name", [], "any", false, false, false, 16), "html", null, true);
        echo "\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\">
                    <h5 class=\"card-title mb-0\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "client", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17), "html", null, true);
        echo "</h5>
                    <div class=\"text-muted mb-2\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 18, $this->source); })()), "client", [], "any", false, false, false, 18), "owner", [], "any", false, false, false, 18), "html", null, true);
        echo "</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier le logo</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos de l'entréprise</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 32, $this->source); })()), "client", [], "any", false, false, false, 32), "owner", [], "any", false, false, false, 32), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 33, $this->source); })()), "email", [], "any", false, false, false, 33), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
        // line 34
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 34, $this->source); })()), "client", [], "any", false, false, false, 34), "zipcode", [], "any", false, false, false, 34) . " - ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 34, $this->source); })()), "client", [], "any", false, false, false, 34), "address", [], "any", false, false, false, 34)), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 35, $this->source); })()), "client", [], "any", false, false, false, 35), "phone", [], "any", false, false, false, 35), "html", null, true);
        echo "</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map\"></i>";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 36, $this->source); })()), "client", [], "any", false, false, false, 36), "town", [], "any", false, false, false, 36), "name", [], "any", false, false, false, 36), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Mode de paiement</h5>
                    <ul class=\"list-unstyled mb-0\">
                    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 43, $this->source); })()), "client", [], "any", false, false, false, 43), "payment", [], "any", false, false, false, 43) == 1)) {
            // line 44
            echo "                        <li class=\"mb-1\"><b>Paiement par chèque</b></li>
                    ";
        }
        // line 46
        echo "                    ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 46, $this->source); })()), "client", [], "any", false, false, false, 46), "payment", [], "any", false, false, false, 46) == 2)) {
            // line 47
            echo "                        <li class=\"mb-1\"><b>Paiement direct</b></li>
                    ";
        }
        // line 49
        echo "                        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_paiement", ["profile" => 1]);
        echo "\" class=\"btn btn-primary\">Modifier</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Adresse de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>";
        // line 56
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 56, $this->source); })()), "client", [], "any", false, false, false, 56), "facturezipcode", [], "any", false, false, false, 56) . " - ") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 56, $this->source); })()), "client", [], "any", false, false, false, 56), "factureaddress", [], "any", false, false, false, 56)), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                    ";
        // line 63
        if (twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 63, $this->source); })()), "signed", [], "any", false, false, false, 63)) {
            // line 64
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("contracts/society/pdf/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 64, $this->source); })()), "usercontract", [], "any", false, false, false, 64), "id", [], "any", false, false, false, 64))), "html", null, true);
            echo ".pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger votre contrat</a>
                    ";
        } else {
            // line 66
            echo "                        <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_contract", ["profile" => 1]);
            echo "\" class=\"btn btn-primary\">Signer le contrat</a>
                    ";
        }
        // line 68
        echo "                    </ul>
                </div>
                <hr class=\"my-0\">
            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 81
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 81, $this->source); })()), 'form_start');
        echo "
                        ";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 82, $this->source); })()), "owner", [], "any", false, false, false, 82), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Responsable"]);
        // line 84
        echo "
                        ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 85, $this->source); })()), "owner", [], "any", false, false, false, 85), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>

                        ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 89, $this->source); })()), "address", [], "any", false, false, false, 89), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de l'entreprise"]);
        // line 91
        echo "
                        ";
        // line 92
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 92, $this->source); })()), "address", [], "any", false, false, false, 92), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 96, $this->source); })()), "zipcode", [], "any", false, false, false, 96), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal"]);
        // line 98
        echo "
                        ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 99, $this->source); })()), "zipcode", [], "any", false, false, false, 99), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 103, $this->source); })()), "town", [], "any", false, false, false, 103), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Ville"]);
        // line 105
        echo "
                        ";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 106, $this->source); })()), "town", [], "any", false, false, false, 106), 'widget', ["attr" => ["class" => "form-control form-select"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 110
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 110, $this->source); })()), "submit", [], "any", false, false, false, 110), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')"]]);
        echo "

                    ";
        // line 112
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["infoForm"]) || array_key_exists("infoForm", $context) ? $context["infoForm"] : (function () { throw new RuntimeError('Variable "infoForm" does not exist.', 112, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier l'adresse de facturation</h5>
                </div>
                <div class=\"card-body h-100\">
                    ";
        // line 120
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 120, $this->source); })()), 'form_start');
        echo "
                       
                        ";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 122, $this->source); })()), "factureaddress", [], "any", false, false, false, 122), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse de facturation"]);
        // line 124
        echo "
                        ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 125, $this->source); })()), "factureaddress", [], "any", false, false, false, 125), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        
                        ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 129, $this->source); })()), "facturezipcode", [], "any", false, false, false, 129), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Code postal"]);
        // line 131
        echo "
                        ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 132, $this->source); })()), "facturezipcode", [], "any", false, false, false, 132), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        
                        <br>
                        ";
        // line 135
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 135, $this->source); })()), "submit", [], "any", false, false, false, 135), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => "return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')"]]);
        echo "

                    ";
        // line 137
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["factureForm"]) || array_key_exists("factureForm", $context) ? $context["factureForm"] : (function () { throw new RuntimeError('Variable "factureForm" does not exist.', 137, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
            ";
        // line 165
        echo "        </div>
    </div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/home/profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 165,  302 => 137,  297 => 135,  291 => 132,  288 => 131,  286 => 129,  279 => 125,  276 => 124,  274 => 122,  269 => 120,  258 => 112,  253 => 110,  246 => 106,  243 => 105,  241 => 103,  234 => 99,  231 => 98,  229 => 96,  222 => 92,  219 => 91,  217 => 89,  210 => 85,  207 => 84,  205 => 82,  201 => 81,  186 => 68,  180 => 66,  174 => 64,  172 => 63,  162 => 56,  151 => 49,  147 => 47,  144 => 46,  140 => 44,  138 => 43,  128 => 36,  124 => 35,  120 => 34,  116 => 33,  112 => 32,  95 => 18,  91 => 17,  85 => 16,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'client_web/base.html.twig' %}

{% block title %}Profile{% endblock %}

{% block body %}
    <div class=\"mb-3\">
        <h1 class=\"h3 d-inline align-middle\">Profile</h1>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4 col-xl-3\">
            <div class=\"card mb-3\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Details du compte</h5>
                </div>
                <div class=\"card-body text-center\">
                    <img id=\"img-prev\" src=\"{{ user.imagePath == null ? asset('clientweb/img/logo.png') : asset('users/images/'~user.imagePath) }}\" alt=\"{{ user.client.name }}\" class=\"img-fluid rounded-circle mb-2\" width=\"128\" height=\"128\">
                    <h5 class=\"card-title mb-0\">{{ user.client.name }}</h5>
                    <div class=\"text-muted mb-2\">{{ user.client.owner }}</div>

                    <div>
                        <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />
                        <label for=\"file\" class=\"btn btn-info small\">
                            <i class=\"align-middle\" style=\"margin-right:10px\" data-feather=\"upload\"></i>
                            <span class=\"align-middle\"> Modifier le logo</span>
                        </label>
                    </div>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Apropos de l'entréprise</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"user\"></i>{{ user.client.owner }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"mail\"></i>{{ user.email }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ user.client.zipcode ~ ' - ' ~ user.client.address }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"phone\"></i>{{ user.client.phone }}</li>
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map\"></i>{{ user.client.town.name }}</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Mode de paiement</h5>
                    <ul class=\"list-unstyled mb-0\">
                    {% if user.client.payment == 1 %}
                        <li class=\"mb-1\"><b>Paiement par chèque</b></li>
                    {% endif %}
                    {% if user.client.payment == 2 %}
                        <li class=\"mb-1\"><b>Paiement direct</b></li>
                    {% endif %}
                        <a href=\"{{ path('app_client_web_auth_paiement', {'profile': 1}) }}\" class=\"btn btn-primary\">Modifier</a>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Adresse de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                        <li class=\"mb-1\"><i class=\"align-middl\" style=\"margin-right:10px\" data-feather=\"map-pin\"></i>{{ user.client.facturezipcode ~ ' - ' ~ user.client.factureaddress }}</li>
                    </ul>
                </div>
                <hr class=\"my-0\">
                <div class=\"card-body\">
                    <h5 class=\"h6 card-title\">Contrat de facturation</h5>
                    <ul class=\"list-unstyled mb-0\">
                    {% if user.signed %}
                        <a href=\"{{ asset(\"contracts/society/pdf/\"~user.usercontract.id) }}.pdf\" target=\"_blank\" class=\"btn btn-primary\">Télécharger votre contrat</a>
                    {% else %}
                        <a href=\"{{ path(\"app_client_web_auth_contract\", {'profile': 1}) }}\" class=\"btn btn-primary\">Signer le contrat</a>
                    {% endif %}
                    </ul>
                </div>
                <hr class=\"my-0\">
            </div>
        </div>

        <div class=\"col-md-8 col-xl-9\">
            <div class=\"card\">
                <div class=\"card-header\">

                    <h5 class=\"card-title mb-0\">Modifier les information générales</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(infoForm) }}
                        {{ form_label(infoForm.owner, 'Responsable', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.owner, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>

                        {{ form_label(infoForm.address, 'Adresse de l\\'entreprise', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.address, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(infoForm.zipcode, 'Code postal', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.zipcode, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(infoForm.town, 'Ville', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(infoForm.town, { 'attr': {'class': 'form-control form-select'} }) }}
                        
                        <br>
                        
                        {{ form_widget(infoForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(infoForm) }}
                </div>
            </div>
            <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier l'adresse de facturation</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(factureForm) }}
                       
                        {{ form_label(factureForm.factureaddress, 'Adresse de facturation', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(factureForm.factureaddress, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        
                        {{ form_label(factureForm.facturezipcode, 'Code postal', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(factureForm.facturezipcode, { 'attr': {'class': 'form-control'} }) }}
                        
                        <br>
                        {{ form_widget(factureForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(factureForm) }}
                </div>
            </div>
            {# <div class=\"card\">
                <div class=\"card-header\">
                    <h5 class=\"card-title mb-0\">Modifier le mot de passe</h5>
                </div>
                <div class=\"card-body h-100\">
                    {{ form_start(passForm) }}
                       
                        {{ form_label(passForm.password, 'Mot de passe', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(passForm.password, { 'attr': {'class': 'form-control', 'placeholder': '*******'} }) }}
                        
                        <br>
                        
                        {{ form_label(passForm.password_c, 'Confirmer le mot de passe', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(passForm.password_c, { 'attr': {'class': 'form-control', 'placeholder': '*******'} }) }}
                        
                        <br>
                        {{ form_widget(passForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir enrégistrer les modification ?')\"} }) }}

                    {{ form_end(passForm) }}
                </div>
            </div> #}
        </div>
    </div>
{% endblock %}
", "client_web/home/profile.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/home/profile.html.twig");
    }
}
