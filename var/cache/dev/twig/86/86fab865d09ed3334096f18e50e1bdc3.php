<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* client_web/home/new-shift.html.twig */
class __TwigTemplate_b6a003984d5068335650ee585c8c1b4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((((array_key_exists("type", $context) && ((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new RuntimeError('Variable "type" does not exist.', 1, $this->source); })()) == "admin"))) ? ("admin/base.html.twig") : ("client_web/base.html.twig")), "client_web/home/new-shift.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "client_web/home/new-shift.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 3, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier le shift : " . twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 3, $this->source); })()), "code", [], "any", false, false, false, 3)), "html", null, true))) : (print ("Publier un shift")));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((((array_key_exists("type", $context) && ((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new RuntimeError('Variable "type" does not exist.', 9, $this->source); })()) == "admin"))) ? ("app_admin_contract") : ("app_client_web_home")));
        echo "\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">";
        // line 18
        (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 18, $this->source); })())) ? (print (twig_escape_filter($this->env, ("Modifier le shift : " . twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 18, $this->source); })()), "code", [], "any", false, false, false, 18)), "html", null, true))) : (print ("Publier un shift")));
        echo "</h5>
        </div>
        <div class=\"card-body\">
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <div class=\"row\">
                    <div class=\"col-6\">

                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 25, $this->source); })()), "job", [], "any", false, false, false, 25), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Poste *"]);
        // line 27
        echo "
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 28, $this->source); })()), "job", [], "any", false, false, false, 28), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "
                        
                        <br>
                        <label class=\"form-check\">
                            <input id=\"consecutiveCheck\" class=\"form-check-input\" type=\"checkbox\" name=\"consecutive\">
                            <span class=\"form-check-label\">
                                Shifts consécutifs ?
                            </span>
                        </label>
                        <br>
                        <div class=\"row\">
                            <div class=\"col-6\">
                                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 40, $this->source); })()), "startdate", [], "any", false, false, false, 40), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date et heure du début *"]);
        // line 42
        echo "<br>
                                ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 43, $this->source); })()), "startdate", [], "any", false, false, false, 43), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%"]]);
        echo "
                            </div>
                            <div class=\"col-6\">
                                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 46, $this->source); })()), "enddate", [], "any", false, false, false, 46), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Date et heure de la fin *"]);
        // line 48
        echo "<br>
                                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 49, $this->source); })()), "enddate", [], "any", false, false, false, 49), 'widget', ["attr" => ["class" => "form-control-lg", "style" => "width:100%"]]);
        echo "
                            </div>
                        </div>

                        <br>

                        ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 55, $this->source); })()), "pause", [], "any", false, false, false, 55), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Durée de la pause"]);
        // line 57
        echo "
                        ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 58, $this->source); })()), "pause", [], "any", false, false, false, 58), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "

                        <br>
                        <label class=\"form-label\">Adresse</label>
                        <textarea disabled class=\"form-control\" rows=\"2\">";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 62, $this->source); })()), "html", null, true);
        echo "</textarea>    
                        <br>
                        <label class=\"form-label\">Ville</label> 
                        <input disabled type=\"text\" class=\"form-control form-select\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["town"]) || array_key_exists("town", $context) ? $context["town"] : (function () { throw new RuntimeError('Variable "town" does not exist.', 65, $this->source); })()), "name", [], "any", false, false, false, 65), "html", null, true);
        echo "\">
                        <br>
                        <label class=\"form-check\">
                            <input id=\"diffAdress\" ";
        // line 68
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 68, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 68, $this->source); })()), "address", [], "any", false, false, false, 68) != (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 68, $this->source); })()))) ? ("checked") : (""))) : (""));
        echo " class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Addresse différente
                            </span>
                        </label>   

                        <div style='display:";
        // line 74
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 74, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 74, $this->source); })()), "address", [], "any", false, false, false, 74) != (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 74, $this->source); })()))) ? ("block") : ("none"))) : ("none"));
        echo "' id='newAddress'>
                            <br>

                            ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 77, $this->source); })()), "address", [], "any", false, false, false, 77), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse"]);
        // line 79
        echo "
                            ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 80, $this->source); })()), "address", [], "any", false, false, false, 80), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                
                                <br>

                                ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 84, $this->source); })()), "town", [], "any", false, false, false, 84), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Ville"]);
        // line 86
        echo "
                                ";
        // line 87
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 87, $this->source); })()), "town", [], "any", false, false, false, 87), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "
                        </div>
                        
                        <br>

                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        <label class=\"form-check\">
                            <input id=\"clothCheck\" ";
        // line 95
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 95, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 95, $this->source); })()), "cloth", [], "any", false, false, false, 95) != null)) ? ("checked") : (""))) : (""));
        echo " class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Tenue exigée
                            </span>
                        </label>
                        <div style=\"display:";
        // line 100
        echo (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 100, $this->source); })())) ? ((((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 100, $this->source); })()), "cloth", [], "any", false, false, false, 100) != null)) ? ("block") : ("none"))) : ("none"));
        echo "\" id=\"clothDiv\">
                            <br> 
                            ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 102, $this->source); })()), "cloth", [], "any", false, false, false, 102), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Entrez la tenue exigée"]]);
        echo "
                        </div>
                        <br>

                        <label class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            ";
        // line 108
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 108, $this->source); })()), "parking", [], "any", false, false, false, 108), 'widget', ["attr" => ["class" => "form-check-input"]]);
        echo "
                            <span class=\"form-check-label\">
                                ";
        // line 110
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 110, $this->source); })()), "parking", [], "any", false, false, false, 110), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Stationnement"]);
        // line 112
        echo "
                            </span>
                        </label>

                        <br>

                        ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 118, $this->source); })()), "description", [], "any", false, false, false, 118), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Description"]);
        // line 120
        echo "
                        ";
        // line 121
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 121, $this->source); })()), "description", [], "any", false, false, false, 121), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "

                        <br>

                        ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 125, $this->source); })()), "bonus", [], "any", false, false, false, 125), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Bonus"]);
        // line 127
        echo "
                        ";
        // line 128
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 128, $this->source); })()), "bonus", [], "any", false, false, false, 128), 'widget', ["attr" => ["class" => "form-select form-control-lg"]]);
        echo "

                        <br>

                        <div style=\"position:absolute; bottom:0; right:0\">
                            ";
        // line 133
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 133, $this->source); })()), "submit", [], "any", false, false, false, 133), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary", "onclick" => (("return confirm('Etes-vous sûr de vouloir " . (((isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new RuntimeError('Variable "edit" does not exist.', 133, $this->source); })())) ? ("modifier") : ("publier"))) . " ce shift ?')")]]);
        echo "
                        </div>
                    </div>
                </div>
            ";
        // line 137
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["contractForm"]) || array_key_exists("contractForm", $context) ? $context["contractForm"] : (function () { throw new RuntimeError('Variable "contractForm" does not exist.', 137, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "client_web/home/new-shift.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 137,  276 => 133,  268 => 128,  265 => 127,  263 => 125,  256 => 121,  253 => 120,  251 => 118,  243 => 112,  241 => 110,  236 => 108,  227 => 102,  222 => 100,  214 => 95,  203 => 87,  200 => 86,  198 => 84,  191 => 80,  188 => 79,  186 => 77,  180 => 74,  171 => 68,  165 => 65,  159 => 62,  152 => 58,  149 => 57,  147 => 55,  138 => 49,  135 => 48,  133 => 46,  127 => 43,  124 => 42,  122 => 40,  107 => 28,  104 => 27,  102 => 25,  95 => 21,  89 => 18,  77 => 9,  72 => 6,  65 => 5,  52 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends (type is defined and type == 'admin') ? 'admin/base.html.twig': 'client_web/base.html.twig' %}

{% block title %}{{ edit ? 'Modifier le shift : ' ~ data.code : 'Publier un shift' }}{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-12\">

    <a href=\"{{ path((type is defined and type == 'admin') ? 'app_admin_contract' : 'app_client_web_home') }}\" class=\"contract-card-link\">
        <div class=\"card\" style=\"display:inline-block\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\"><i class=\"align-middle\" data-feather=\"arrow-left\"></i></h5>
        </div>
    </div>
    </a>
    <div class=\"card\">
        <div class=\"card-header\">
        <h5 class=\"card-title mb-0\">{{ edit ? 'Modifier le shift : ' ~ data.code: 'Publier un shift' }}</h5>
        </div>
        <div class=\"card-body\">
            {{ form_start(contractForm) }}
                <div class=\"row\">
                    <div class=\"col-6\">

                        {{ form_label(contractForm.job, 'Poste *', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.job, { 'attr': {'class': 'form-select form-control-lg'} }) }}
                        
                        <br>
                        <label class=\"form-check\">
                            <input id=\"consecutiveCheck\" class=\"form-check-input\" type=\"checkbox\" name=\"consecutive\">
                            <span class=\"form-check-label\">
                                Shifts consécutifs ?
                            </span>
                        </label>
                        <br>
                        <div class=\"row\">
                            <div class=\"col-6\">
                                {{ form_label(contractForm.startdate, 'Date et heure du début *', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}<br>
                                {{ form_widget(contractForm.startdate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%'} }) }}
                            </div>
                            <div class=\"col-6\">
                                {{ form_label(contractForm.enddate, 'Date et heure de la fin *', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}<br>
                                {{ form_widget(contractForm.enddate, { 'attr': {'class': 'form-control-lg', 'style': 'width:100%'} }) }}
                            </div>
                        </div>

                        <br>

                        {{ form_label(contractForm.pause, 'Durée de la pause', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.pause, { 'attr': {'class': 'form-select form-control-lg'} }) }}

                        <br>
                        <label class=\"form-label\">Adresse</label>
                        <textarea disabled class=\"form-control\" rows=\"2\">{{ address }}</textarea>    
                        <br>
                        <label class=\"form-label\">Ville</label> 
                        <input disabled type=\"text\" class=\"form-control form-select\" value=\"{{ town.name }}\">
                        <br>
                        <label class=\"form-check\">
                            <input id=\"diffAdress\" {{ edit ? (data.address != address ? 'checked' : '' ) : '' }} class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Addresse différente
                            </span>
                        </label>   

                        <div style='display:{{ edit ? (data.address != address ? 'block' : 'none' ) : 'none' }}' id='newAddress'>
                            <br>

                            {{ form_label(contractForm.address, 'Adresse', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            {{ form_widget(contractForm.address, { 'attr': {'class': 'form-control'} }) }}
                                
                                <br>

                                {{ form_label(contractForm.town, 'Ville', {
                                    'label_attr': {'class': 'form-label'}
                                }) }}
                                {{ form_widget(contractForm.town, { 'attr': {'class': 'form-select form-control-lg'} }) }}
                        </div>
                        
                        <br>

                    </div>
                    <div class=\"col-6\" style=\"position:relative\">
                        <label class=\"form-check\">
                            <input id=\"clothCheck\" {{ edit ? (data.cloth != null ? 'checked' : '' ) : ''}} class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            <span class=\"form-check-label\">
                                Tenue exigée
                            </span>
                        </label>
                        <div style=\"display:{{ edit ? (data.cloth != null ? 'block' : 'none' ) : 'none'}}\" id=\"clothDiv\">
                            <br> 
                            {{ form_widget(contractForm.cloth, { 'attr': {'class': 'form-control', 'placeholder': 'Entrez la tenue exigée'} }) }}
                        </div>
                        <br>

                        <label class=\"form-check\">
                            <input class=\"form-check-input\" type=\"checkbox\" value=\"\">
                            {{ form_widget(contractForm.parking, { 'attr': {'class': 'form-check-input'} }) }}
                            <span class=\"form-check-label\">
                                {{ form_label(contractForm.parking, 'Stationnement', {
                                'label_attr': {'class': 'form-label'}
                            }) }}
                            </span>
                        </label>

                        <br>

                        {{ form_label(contractForm.description, 'Description', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.description,  { 'attr': {'class': 'form-control'} }) }}

                        <br>

                        {{ form_label(contractForm.bonus, 'Bonus', {
                            'label_attr': {'class': 'form-label'}
                        }) }}
                        {{ form_widget(contractForm.bonus, { 'attr': {'class': 'form-select form-control-lg'} }) }}

                        <br>

                        <div style=\"position:absolute; bottom:0; right:0\">
                            {{ form_widget(contractForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary', 'onclick': \"return confirm('Etes-vous sûr de vouloir \" ~ (edit ? \"modifier\" : \"publier\") ~ \" ce shift ?')\"} }) }}
                        </div>
                    </div>
                </div>
            {{ form_end(contractForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "client_web/home/new-shift.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/client_web/home/new-shift.html.twig");
    }
}
