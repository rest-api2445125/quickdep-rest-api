<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/auth/login.html.twig */
class __TwigTemplate_64343f6babafb2e2857851ed474bc1cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "auth_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/auth/login.html.twig"));

        $this->parent = $this->loadTemplate("auth_base.html.twig", "admin/auth/login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Connexion!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Administration <b>QuickDep</b></h1>
    <p class=\"lead\">
        Connectez-vous à votre compte <b>admin</b> pour continuer
    </p>
    
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "notice"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 15
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 28, $this->source); })()), 'form_start');
        echo "
                <div class=\"mb-3\">
                    ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 30, $this->source); })()), "email", [], "any", false, false, false, 30), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Adresse Email"]);
        // line 32
        echo "
                    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 33, $this->source); })()), "email", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "email", "placeholder" => "Entrez votre adresse e-mail"]]);
        echo "
                </div>
                <div class=\"mb-3\">
                    ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 36, $this->source); })()), "email", [], "any", false, false, false, 36), 'label', ["label_attr" => ["class" => "form-label"], "label" => "Mot de passe"]);
        // line 38
        echo "
                    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 39, $this->source); })()), "password", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "form-control form-control-lg", "type" => "password", "placeholder" => "Entrez votre mot de passe"]]);
        echo "
                </div>
                <div class=\"text-center mt-3\">
                    ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 42, $this->source); })()), "submit", [], "any", false, false, false, 42), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>
            ";
        // line 44
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["loginForm"]) || array_key_exists("loginForm", $context) ? $context["loginForm"] : (function () { throw new RuntimeError('Variable "loginForm" does not exist.', 44, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/auth/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 44,  137 => 42,  131 => 39,  128 => 38,  126 => 36,  120 => 33,  117 => 32,  115 => 30,  110 => 28,  103 => 23,  89 => 15,  85 => 13,  81 => 12,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'auth_base.html.twig' %}

{% block title %}Connexion!{% endblock %}

{% block body %}
<div class=\"text-center mt-4\">
    <h1 class=\"h2\">Administration <b>QuickDep</b></h1>
    <p class=\"lead\">
        Connectez-vous à votre compte <b>admin</b> pour continuer
    </p>
    
    {% for message in app.flashes('notice') %}
     <div class=\"alert alert_danger\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}
</div>

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"m-sm-4\">
            {{ form_start(loginForm) }}
                <div class=\"mb-3\">
                    {{ form_label(loginForm.email, 'Adresse Email', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(loginForm.email, { 'attr': {'class': 'form-control form-control-lg', 'type': 'email', 'placeholder': 'Entrez votre adresse e-mail'} }) }}
                </div>
                <div class=\"mb-3\">
                    {{ form_label(loginForm.email, 'Mot de passe', {
                        'label_attr': {'class': 'form-label'}
                    }) }}
                    {{ form_widget(loginForm.password, { 'attr': {'class': 'form-control form-control-lg', 'type': 'password', 'placeholder': 'Entrez votre mot de passe'} }) }}
                </div>
                <div class=\"text-center mt-3\">
                    {{ form_widget(loginForm.submit, { 'attr': {'class': 'btn btn-lg btn-primary'} }) }}
                </div>
            {{ form_end(loginForm) }}
        </div>
    </div>
</div>
{% endblock %}
", "admin/auth/login.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/auth/login.html.twig");
    }
}
