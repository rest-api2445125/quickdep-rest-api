<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/client/index.html.twig */
class __TwigTemplate_1eaefeb91da1870ff4f3db1ba7305209 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/client/index.html.twig"));

        // line 3
        $context["i"] = 1;
        // line 8
        $context["months"] = [0 => "Janvier", 1 => "Février", 2 => "Mars", 3 => "Avril", 4 => "Mai", 5 => "Juin", 6 => "Juillet", 7 => "Août", 8 => "Septembre", 9 => "Octobre", 10 => "Novembre", 11 => "Decembre"];
        // line 1
        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/client/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Entreprises";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 24
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Liste des entréprises</h1>
    <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_client_web_auth_register");
        echo "\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i>Enrégistrer une entreprise</a>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 42));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 43
            echo "    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            ";
            // line 45
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "
    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Validés</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Non validés</span>
        </li>
    </ul>
    <div class=\"container__inner\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["all"]) || array_key_exists("all", $context) ? $context["all"] : (function () { throw new RuntimeError('Variable "all" does not exist.', 83, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 84
            echo "                        <tr>
                            <td>";
            // line 85
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 85, $this->source); })()), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 86
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "name", [], "any", false, false, false, 86), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "owner", [], "any", false, false, false, 87), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 88
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["emails"]) || array_key_exists("emails", $context) ? $context["emails"] : (function () { throw new RuntimeError('Variable "emails" does not exist.', 88, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 88), [], "array", false, false, false, 88), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "address", [], "any", false, false, false, 89), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 90
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "phone", [], "any", false, false, false, 90), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 92
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 92, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 92), [], "array", false, false, false, 92), "emailVerified", [], "any", false, false, false, 92) != true)) {
                // line 93
                echo "                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                ";
            }
            // line 95
            echo "                                
                                ";
            // line 96
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 96, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 96), [], "array", false, false, false, 96), "phoneVerified", [], "any", false, false, false, 96) != true)) {
                // line 97
                echo "                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                ";
            }
            // line 99
            echo "                                
                                ";
            // line 100
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 100, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 100), [], "array", false, false, false, 100), "signed", [], "any", false, false, false, 100) != true)) {
                // line 101
                echo "                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                ";
            }
            // line 103
            echo "                                
                                ";
            // line 104
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 105
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 105, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 105), [], "array", false, false, false, 105), "signed", [], "any", false, false, false, 105) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 106
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 106, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 106), [], "array", false, false, false, 106), "emailVerified", [], "any", false, false, false, 106) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 107
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 107, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 107), [], "array", false, false, false, 107), "phoneVerified", [], "any", false, false, false, 107) == true)) && (twig_get_attribute($this->env, $this->source,             // line 108
$context["client"], "validated", [], "any", false, false, false, 108) != true))) {
                // line 110
                echo "                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                ";
            }
            // line 112
            echo "                                
                                ";
            // line 113
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 114
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 114, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 114), [], "array", false, false, false, 114), "signed", [], "any", false, false, false, 114) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 115
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 115, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 115), [], "array", false, false, false, 115), "emailVerified", [], "any", false, false, false, 115) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 116
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 116, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 116), [], "array", false, false, false, 116), "phoneVerified", [], "any", false, false, false, 116) == true)) && (twig_get_attribute($this->env, $this->source,             // line 117
$context["client"], "validated", [], "any", false, false, false, 117) == true))) {
                // line 119
                echo "                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                ";
            }
            // line 121
            echo "                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 124
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 124)]), "html", null, true);
            echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                ";
            // line 128
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_client_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 128)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                ";
            // line 129
            if ((twig_get_attribute($this->env, $this->source, $context["client"], "validated", [], "any", false, false, false, 129) != true)) {
                // line 130
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_validate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 130)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                ";
            } else {
                // line 134
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_unvalidate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 134)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                ";
            }
            // line 138
            echo "                            </td>
                        </tr>
                        ";
            // line 140
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 140, $this->source); })()) + 1);
            // line 141
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 142
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 166
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["validated"]) || array_key_exists("validated", $context) ? $context["validated"] : (function () { throw new RuntimeError('Variable "validated" does not exist.', 166, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 167
            echo "                        <tr>
                            <td>";
            // line 168
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 168, $this->source); })()), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 169
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "name", [], "any", false, false, false, 169), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 170
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "owner", [], "any", false, false, false, 170), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 171
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["emails"]) || array_key_exists("emails", $context) ? $context["emails"] : (function () { throw new RuntimeError('Variable "emails" does not exist.', 171, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 171), [], "array", false, false, false, 171), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 172
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "address", [], "any", false, false, false, 172), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 173
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "phone", [], "any", false, false, false, 173), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 175
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 175, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 175), [], "array", false, false, false, 175), "emailVerified", [], "any", false, false, false, 175) != true)) {
                // line 176
                echo "                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                ";
            }
            // line 178
            echo "                                
                                ";
            // line 179
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 179, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 179), [], "array", false, false, false, 179), "phoneVerified", [], "any", false, false, false, 179) != true)) {
                // line 180
                echo "                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                ";
            }
            // line 182
            echo "                                
                                ";
            // line 183
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 183, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 183), [], "array", false, false, false, 183), "signed", [], "any", false, false, false, 183) != true)) {
                // line 184
                echo "                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                ";
            }
            // line 186
            echo "                                
                                ";
            // line 187
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 188
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 188, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 188), [], "array", false, false, false, 188), "signed", [], "any", false, false, false, 188) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 189
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 189, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 189), [], "array", false, false, false, 189), "emailVerified", [], "any", false, false, false, 189) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 190
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 190, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 190), [], "array", false, false, false, 190), "phoneVerified", [], "any", false, false, false, 190) == true)) && (twig_get_attribute($this->env, $this->source,             // line 191
$context["client"], "validated", [], "any", false, false, false, 191) != true))) {
                // line 193
                echo "                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                ";
            }
            // line 195
            echo "                                
                                ";
            // line 196
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 197
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 197, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 197), [], "array", false, false, false, 197), "signed", [], "any", false, false, false, 197) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 198
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 198, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 198), [], "array", false, false, false, 198), "emailVerified", [], "any", false, false, false, 198) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 199
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 199, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 199), [], "array", false, false, false, 199), "phoneVerified", [], "any", false, false, false, 199) == true)) && (twig_get_attribute($this->env, $this->source,             // line 200
$context["client"], "validated", [], "any", false, false, false, 200) == true))) {
                // line 202
                echo "                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                ";
            }
            // line 204
            echo "                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 207
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 207)]), "html", null, true);
            echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                ";
            // line 211
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_client_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 211)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                ";
            // line 212
            if ((twig_get_attribute($this->env, $this->source, $context["client"], "validated", [], "any", false, false, false, 212) != true)) {
                // line 213
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_validate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 213)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                ";
            } else {
                // line 217
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_unvalidate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 217)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                ";
            }
            // line 221
            echo "                            </td>
                        </tr>
                        ";
            // line 223
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 223, $this->source); })()) + 1);
            // line 224
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 225
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 249
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["waiting"]) || array_key_exists("waiting", $context) ? $context["waiting"] : (function () { throw new RuntimeError('Variable "waiting" does not exist.', 249, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 250
            echo "                        <tr>
                            <td>";
            // line 251
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 251, $this->source); })()), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 252
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "name", [], "any", false, false, false, 252), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 253
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "owner", [], "any", false, false, false, 253), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 254
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["emails"]) || array_key_exists("emails", $context) ? $context["emails"] : (function () { throw new RuntimeError('Variable "emails" does not exist.', 254, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 254), [], "array", false, false, false, 254), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 255
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "address", [], "any", false, false, false, 255), "html", null, true);
            echo "</td>
                            <td class=\"d-none d-xl-table-cell\">";
            // line 256
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["client"], "phone", [], "any", false, false, false, 256), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 258
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 258, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 258), [], "array", false, false, false, 258), "emailVerified", [], "any", false, false, false, 258) != true)) {
                // line 259
                echo "                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                ";
            }
            // line 261
            echo "                                
                                ";
            // line 262
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 262, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 262), [], "array", false, false, false, 262), "phoneVerified", [], "any", false, false, false, 262) != true)) {
                // line 263
                echo "                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                ";
            }
            // line 265
            echo "                                
                                ";
            // line 266
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 266, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 266), [], "array", false, false, false, 266), "signed", [], "any", false, false, false, 266) != true)) {
                // line 267
                echo "                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                ";
            }
            // line 269
            echo "                                
                                ";
            // line 270
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 271
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 271, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 271), [], "array", false, false, false, 271), "signed", [], "any", false, false, false, 271) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 272
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 272, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 272), [], "array", false, false, false, 272), "emailVerified", [], "any", false, false, false, 272) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 273
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 273, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 273), [], "array", false, false, false, 273), "phoneVerified", [], "any", false, false, false, 273) == true)) && (twig_get_attribute($this->env, $this->source,             // line 274
$context["client"], "validated", [], "any", false, false, false, 274) != true))) {
                // line 276
                echo "                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                ";
            }
            // line 278
            echo "                                
                                ";
            // line 279
            if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 280
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 280, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 280), [], "array", false, false, false, 280), "signed", [], "any", false, false, false, 280) == true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 281
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 281, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 281), [], "array", false, false, false, 281), "emailVerified", [], "any", false, false, false, 281) == true)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 282
(isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 282, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 282), [], "array", false, false, false, 282), "phoneVerified", [], "any", false, false, false, 282) == true)) && (twig_get_attribute($this->env, $this->source,             // line 283
$context["client"], "validated", [], "any", false, false, false, 283) == true))) {
                // line 285
                echo "                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                ";
            }
            // line 287
            echo "                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"";
            // line 290
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_delete_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 290)]), "html", null, true);
            echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                ";
            // line 294
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_client_detail", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 294)]), "html", null, true);
            echo "\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                ";
            // line 295
            if ((twig_get_attribute($this->env, $this->source, $context["client"], "validated", [], "any", false, false, false, 295) != true)) {
                // line 296
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_validate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 296)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                ";
            } else {
                // line 300
                echo "                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_user_unvalidate_client", ["id" => twig_get_attribute($this->env, $this->source, $context["client"], "id", [], "any", false, false, false, 300)]), "html", null, true);
                echo "\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                ";
            }
            // line 304
            echo "                            </td>
                        </tr>
                        ";
            // line 306
            $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 306, $this->source); })()) + 1);
            // line 307
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 308
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "admin/client/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  590 => 308,  584 => 307,  582 => 306,  578 => 304,  570 => 300,  562 => 296,  560 => 295,  555 => 294,  549 => 290,  544 => 287,  540 => 285,  538 => 283,  537 => 282,  536 => 281,  535 => 280,  534 => 279,  531 => 278,  527 => 276,  525 => 274,  524 => 273,  523 => 272,  522 => 271,  521 => 270,  518 => 269,  514 => 267,  512 => 266,  509 => 265,  505 => 263,  503 => 262,  500 => 261,  496 => 259,  494 => 258,  489 => 256,  485 => 255,  481 => 254,  477 => 253,  473 => 252,  469 => 251,  466 => 250,  462 => 249,  436 => 225,  430 => 224,  428 => 223,  424 => 221,  416 => 217,  408 => 213,  406 => 212,  401 => 211,  395 => 207,  390 => 204,  386 => 202,  384 => 200,  383 => 199,  382 => 198,  381 => 197,  380 => 196,  377 => 195,  373 => 193,  371 => 191,  370 => 190,  369 => 189,  368 => 188,  367 => 187,  364 => 186,  360 => 184,  358 => 183,  355 => 182,  351 => 180,  349 => 179,  346 => 178,  342 => 176,  340 => 175,  335 => 173,  331 => 172,  327 => 171,  323 => 170,  319 => 169,  315 => 168,  312 => 167,  308 => 166,  282 => 142,  276 => 141,  274 => 140,  270 => 138,  262 => 134,  254 => 130,  252 => 129,  247 => 128,  241 => 124,  236 => 121,  232 => 119,  230 => 117,  229 => 116,  228 => 115,  227 => 114,  226 => 113,  223 => 112,  219 => 110,  217 => 108,  216 => 107,  215 => 106,  214 => 105,  213 => 104,  210 => 103,  206 => 101,  204 => 100,  201 => 99,  197 => 97,  195 => 96,  192 => 95,  188 => 93,  186 => 92,  181 => 90,  177 => 89,  173 => 88,  169 => 87,  165 => 86,  161 => 85,  158 => 84,  154 => 83,  122 => 53,  108 => 45,  104 => 43,  100 => 42,  89 => 34,  78 => 25,  71 => 24,  58 => 5,  50 => 1,  48 => 8,  46 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% set i = 1 %}

{% block title %}Entreprises{% endblock %}

{% 
    set months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    ]   
 %}

{% block body %}
<div style=\"
    display: flex;
    align-items: center;
    justify-content: space-between;
\">
    <h1 class=\"h3 mb-3\" style=\"
    display: inline-block;
    margin-bottom: 0 !important
\">Liste des entréprises</h1>
    <a href=\"{{ path('app_client_web_auth_register') }}\"=\"\" class=\"btn btn-primary\"><i class=\"align-middle\" data-feather=\"plus\"></i>Enrégistrer une entreprise</a>
</div>
<hr>
<br>

<div class=\"row\">
    <div class=\"col-12\">

    {% for message in app.flashes('success') %}
    <div class=\"alert alert_success\" style=\"animation-delay: .2s\">
        <div class=\"alert--content\">
            {{ message }}
        </div>
        <div class=\"alert--close\">
            <i class=\"far fa-times-circle\"></i>
        </div>
    </div>
    </br>
    {% endfor %}

    <ul class=\"container__list\">
        <li class=\"container__item container__item_active\">
        <span class=\"container__link\">Tous</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Validés</span>
        </li>
        <li class=\"container__item\">
        <span class=\"container__link\">Non validés</span>
        </li>
    </ul>
    <div class=\"container__inner\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for client in all %}
                        <tr>
                            <td>{{ i }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.name }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.owner }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ emails[client.id] }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.address }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.phone }}</td>
                            <td>
                                {% if users[client.id].emailVerified != true %}
                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].phoneVerified != true %}
                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].signed != true %}
                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated != true
                                %}
                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated == true
                                %}
                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                {% endif %}
                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                <a href=\"{{ path('app_user_client_detail', {'id': client.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                {% if client.validated != true %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"{{ path('app_user_validate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                {% else %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"{{ path('app_user_unvalidate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                {% endif %}
                            </td>
                        </tr>
                        {% set i=i+1 %}
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for client in validated %}
                        <tr>
                            <td>{{ i }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.name }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.owner }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ emails[client.id] }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.address }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.phone }}</td>
                            <td>
                                {% if users[client.id].emailVerified != true %}
                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].phoneVerified != true %}
                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].signed != true %}
                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated != true
                                %}
                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated == true
                                %}
                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                {% endif %}
                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                <a href=\"{{ path('app_user_client_detail', {'id': client.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                {% if client.validated != true %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"{{ path('app_user_validate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                {% else %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"{{ path('app_user_unvalidate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                {% endif %}
                            </td>
                        </tr>
                        {% set i=i+1 %}
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <div class=\"container__inner container__inner_hidden\">
        <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"card flex-fill\">
                <table class=\"table table-hover my-0\">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class=\"d-none d-xl-table-cell\">Nom de l'entreprise</th>
                            <th class=\"d-none d-xl-table-cell\">Nom du responsable</th>
                            <th class=\"d-none d-xl-table-cell\">Email</th>
                            <th class=\"d-none d-xl-table-cell\">Adresse</th>
                            <th class=\"d-none d-md-table-cell\">Phone</th>
                            <th class=\"d-none d-md-table-cell\">Statut</th>
                            <th class=\"d-none d-md-table-cell\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for client in waiting %}
                        <tr>
                            <td>{{ i }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.name }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.owner }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ emails[client.id] }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.address }}</td>
                            <td class=\"d-none d-xl-table-cell\">{{ client.phone }}</td>
                            <td>
                                {% if users[client.id].emailVerified != true %}
                                <span class=\"badge bg-danger\">E-mail non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].phoneVerified != true %}
                                <span class=\"badge bg-danger\">Phone non vérifié</span><br>
                                {% endif %}
                                
                                {% if users[client.id].signed != true %}
                                <span class=\"badge bg-danger\">Contrat non signé</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated != true
                                %}
                                <span class=\"badge bg-warning\">En attente de validation</span><br>
                                {% endif %}
                                
                                {% if 
                                    users[client.id].signed == true
                                    and users[client.id].emailVerified == true 
                                    and users[client.id].phoneVerified == true
                                    and client.validated == true
                                %}
                                <span class=\"badge bg-success\">Entréprise validée</span><br>
                                {% endif %}
                                
                            </td>
                            <td class=\"d-none d-xl-table-cell\">
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Supprimer\" action=\"{{ path('app_user_delete_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir supprimer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"trash-2\"></i></button>
                                </form>
                                {# <a href=\"#\" class=\"btn btn-primary\" title=\"Modifier\"><i class=\"align-middle\" data-feather=\"edit\"></i></a> #}
                                <a href=\"{{ path('app_user_client_detail', {'id': client.id}) }}\" class=\"btn btn-info\" title=\"Voir\"><i class=\"align-middle\" data-feather=\"eye\"></i></a>
                                {% if client.validated != true %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Valider\" action=\"{{ path('app_user_validate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir valider cette entréprise ?')\" type=\"submit\" class=\"btn btn-success\"><i class=\"align-middle\" data-feather=\"check\"></i></button>
                                </form>
                                {% else %}
                                <form  method=\"POST\" style=\"display:inline; margin-right:5px\" title=\"Bloquer\" action=\"{{ path('app_user_unvalidate_client', {'id': client.id}) }}\">
                                    <button onclick=\"return confirm('Etes-vous sûr de vouloir bloquer cette entréprise ?')\" type=\"submit\" class=\"btn btn-danger\"><i class=\"align-middle\" data-feather=\"x\"></i></button>
                                </form>
                                {% endif %}
                            </td>
                        </tr>
                        {% set i=i+1 %}
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
{% endblock %}
", "admin/client/index.html.twig", "/Users/quickdepquickdep/Desktop/QuickDep/v2/quickdep-rest-api/templates/admin/client/index.html.twig");
    }
}
