<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/clients/register' => [[['_route' => 'api_clients_register_collection', '_controller' => 'App\\Controller\\ClientRegisterController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_register_collection', '_api_collection_operation_name' => 'register'], null, ['POST' => 0], null, false, false, null]],
        '/api/contracts' => [
            [['_route' => 'api_contracts_post_collection', '_controller' => 'App\\Controller\\ContractCreationController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_post_collection', '_api_collection_operation_name' => 'post'], null, ['POST' => 0], null, false, false, null],
            [['_route' => 'api_contracts_get_collection', '_controller' => 'App\\Controller\\ContractListingController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_get_collection', '_api_collection_operation_name' => 'get'], null, ['GET' => 0], null, false, false, null],
        ],
        '/api/contracts/with-applies' => [[['_route' => 'api_contracts_waiting_collection', '_controller' => 'App\\Controller\\ContractListWithAppliesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_waiting_collection', '_api_collection_operation_name' => 'waiting'], null, ['GET' => 0], null, false, false, null]],
        '/api/contracts/notify-unaccepted' => [[['_route' => 'api_contracts_unaccepted_notification_collection', '_controller' => 'App\\Controller\\NotifyUnAcceptedContractController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_unaccepted_notification_collection', '_api_collection_operation_name' => 'unaccepted_notification'], null, ['POST' => 0], null, false, false, null]],
        '/api/contracts/stats' => [[['_route' => 'api_contracts_stats_collection', '_controller' => 'App\\Controller\\StatsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_stats_collection', '_api_collection_operation_name' => 'stats'], null, ['GET' => 0], null, false, false, null]],
        '/api/contracts/finance' => [[['_route' => 'api_contracts_finance_collection', '_controller' => 'App\\Controller\\FinanceController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_finance_collection', '_api_collection_operation_name' => 'finance'], null, ['GET' => 0], null, false, false, null]],
        '/api/invoices/generate' => [[['_route' => 'api_invoices_generatte_collection', '_controller' => 'App\\Controller\\InvoiceGenerateController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_generatte_collection', '_api_collection_operation_name' => 'generatte'], null, ['POST' => 0], null, false, false, null]],
        '/api/invoices/employee' => [[['_route' => 'api_invoices_employee_collection', '_controller' => 'App\\Controller\\InvoicesGetForEmployeesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_employee_collection', '_api_collection_operation_name' => 'employee'], null, ['GET' => 0], null, false, false, null]],
        '/api/invoices/client' => [[['_route' => 'api_invoices_client_collection', '_controller' => 'App\\Controller\\InvoiceGetForClientControllre', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_client_collection', '_api_collection_operation_name' => 'client'], null, ['GET' => 0], null, false, false, null]],
        '/api/password-reset/reset-request' => [[['_route' => 'api_password_resets_reset-request_collection', '_controller' => 'App\\Controller\\ResetPasswordController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_reset-request_collection', '_api_collection_operation_name' => 'reset-request'], null, ['POST' => 0], null, false, false, null]],
        '/api/password-reset/reset-confirm' => [[['_route' => 'api_password_resets_reset-confirm_collection', '_controller' => 'App\\Controller\\ResetPasswordConfirmController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_reset-confirm_collection', '_api_collection_operation_name' => 'reset-confirm'], null, ['POST' => 0], null, false, false, null]],
        '/api/time_sheets' => [[['_route' => 'api_time_sheets_get_collection', '_controller' => 'App\\Controller\\TimeSheetListingController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_get_collection', '_api_collection_operation_name' => 'get'], null, ['GET' => 0], null, false, false, null]],
        '/api/time_sheets_to_send' => [[['_route' => 'api_time_sheets_wait_collection', '_controller' => 'App\\Controller\\TimeSheetToSendController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_wait_collection', '_api_collection_operation_name' => 'wait'], null, ['GET' => 0], null, false, false, null]],
        '/api/register' => [[['_route' => 'api_users_post_collection', '_controller' => 'App\\Controller\\UserRegisterController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_post_collection', '_api_collection_operation_name' => 'post'], null, ['POST' => 0], null, false, false, null]],
        '/api/login' => [[['_route' => 'api_users_login_collection', '_controller' => 'App\\Controller\\UserLoginController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_login_collection', '_api_collection_operation_name' => 'login'], null, ['POST' => 0], null, false, false, null]],
        '/api/admin/login' => [[['_route' => 'api_users_login-admin_collection', '_controller' => 'App\\Controller\\UserLoginAdminController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_login-admin_collection', '_api_collection_operation_name' => 'login-admin'], null, ['POST' => 0], null, false, false, null]],
        '/api/users/check-notifications' => [[['_route' => 'api_users_check-notifications_collection', '_controller' => 'App\\Controller\\NotificationCheckController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_check-notifications_collection', '_api_collection_operation_name' => 'check-notifications'], null, ['POST' => 0], null, false, false, null]],
        '/api/users/register' => [[['_route' => 'api_users_register_collection', '_controller' => 'App\\Controller\\UserCompleteProfileController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_register_collection', '_api_collection_operation_name' => 'register'], null, ['POST' => 0], null, false, false, null]],
        '/admin/login' => [[['_route' => 'app_admin_auth', '_controller' => 'App\\Controller\\Admin\\AuthController::index'], null, null, null, false, false, null]],
        '/admin/new-password' => [[['_route' => 'app_admin_new_password', '_controller' => 'App\\Controller\\Admin\\AuthController::newPassword'], null, null, null, false, false, null]],
        '/admin/entreprises' => [[['_route' => 'app_admin_client', '_controller' => 'App\\Controller\\Admin\\ClientController::index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'app_admin_contract', '_controller' => 'App\\Controller\\Admin\\ContractController::index'], null, null, null, false, false, null]],
        '/admin/shift' => [[['_route' => 'app_admin_new_shift', '_controller' => 'App\\Controller\\Admin\\ContractController::newShift'], null, null, null, false, false, null]],
        '/admin/waiting' => [[['_route' => 'app_admin_contract_waiting', '_controller' => 'App\\Controller\\Admin\\ContractController::waiting'], null, null, null, false, false, null]],
        '/amin/approuved' => [[['_route' => 'app_admin_contract_approuved', '_controller' => 'App\\Controller\\Admin\\ContractController::approuved'], null, null, null, false, false, null]],
        '/admin/client' => [[['_route' => 'app_admin_invoice_client', '_controller' => 'App\\Controller\\Admin\\InvoiceController::client'], null, null, null, false, false, null]],
        '/admin/employee' => [[['_route' => 'app_admin_invoice_employee', '_controller' => 'App\\Controller\\Admin\\InvoiceController::employee'], null, null, null, false, false, null]],
        '/admin/stats' => [[['_route' => 'app_admin_stats', '_controller' => 'App\\Controller\\Admin\\StatsController::employee'], null, null, null, false, false, null]],
        '/admin/finance' => [[['_route' => 'app_admin_finance', '_controller' => 'App\\Controller\\Admin\\StatsController::finance'], null, null, null, false, false, null]],
        '/admin/todo' => [[['_route' => 'app_admin_timesheet_todo', '_controller' => 'App\\Controller\\Admin\\TimeSheetController::todo'], null, null, null, false, false, null]],
        '/admin/timesheet' => [[['_route' => 'app_admin_timesheet', '_controller' => 'App\\Controller\\Admin\\TimeSheetController::index'], null, null, null, false, false, null]],
        '/admin/timesheet-approuved' => [[['_route' => 'app_admin_timesheet_approuved', '_controller' => 'App\\Controller\\Admin\\TimeSheetController::approuved'], null, null, null, false, false, null]],
        '/admin/travailleurs' => [[['_route' => 'app_admin_user', '_controller' => 'App\\Controller\\Admin\\UserController::index'], null, null, null, false, false, null]],
        '/admin/employee/new' => [[['_route' => 'app_user_new_user', '_controller' => 'App\\Controller\\Admin\\UserController::newUser'], null, ['POST' => 0, 'GET' => 1], null, false, false, null]],
        '/entreprise/login' => [[['_route' => 'app_client_web_auth', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::index'], null, null, null, false, false, null]],
        '/entreprise/register' => [[['_route' => 'app_client_web_auth_register', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::register'], null, null, null, false, false, null]],
        '/entreprise/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::logout'], null, ['POST' => 0], null, false, false, null]],
        '/entreprise/forget-pass' => [[['_route' => 'app_client_web_auth_forget_pass', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::forgetPass'], null, null, null, false, false, null]],
        '/entreprise/email-verif' => [[['_route' => 'mailVerif', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::mailVerif'], null, ['POST' => 0], null, false, false, null]],
        '/entreprise/confirm-code-verif' => [[['_route' => 'app_client_web_auth_confirm_code_verif', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::confirmCodeVerif'], null, null, null, false, false, null]],
        '/entreprise/phone-verif' => [[['_route' => 'phoneVerif', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::phoneVerif'], null, ['POST' => 0], null, false, false, null]],
        '/entreprise/confirm-phone-verif' => [[['_route' => 'app_client_web_auth_confirm_code_verif_phone', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::confirmPhoneCodeVerif'], null, null, null, false, false, null]],
        '/entreprise/waiting' => [[['_route' => 'app_client_web_contract_waiting', '_controller' => 'App\\Controller\\ClientWeb\\ContractController::index'], null, null, null, false, false, null]],
        '/entreprise/approuved' => [[['_route' => 'app_client_web_contract_approuved', '_controller' => 'App\\Controller\\ClientWeb\\ContractController::approuved'], null, null, null, false, false, null]],
        '/entreprise' => [[['_route' => 'app_client_web_home', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::index'], null, null, null, false, false, null]],
        '/entreprise/shift' => [[['_route' => 'app_client_web_new_shift', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::newShift'], null, null, null, false, false, null]],
        '/entreprise/profile' => [[['_route' => 'app_client_web_home_profile', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::profile'], null, null, null, false, false, null]],
        '/entreprise/contact' => [[['_route' => 'app_client_web_home_contact', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::contact'], null, null, null, false, false, null]],
        '/entreprise/invoice' => [[['_route' => 'app_client_web_invoice', '_controller' => 'App\\Controller\\ClientWeb\\InvoiceController::index'], null, null, null, false, false, null]],
        '/entreprise/invoice/paid' => [[['_route' => 'app_client_web_invoice_paid', '_controller' => 'App\\Controller\\ClientWeb\\InvoiceController::paid'], null, null, null, false, false, null]],
        '/entreprise/timesheet' => [[['_route' => 'app_client_web_timesheet', '_controller' => 'App\\Controller\\ClientWeb\\TimeSheetController::index'], null, null, null, false, false, null]],
        '/entreprise/timesheet-approuved' => [[['_route' => 'app_client_web_timesheet_approuved', '_controller' => 'App\\Controller\\ClientWeb\\TimeSheetController::approuved'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_web_home', '_controller' => 'App\\Controller\\Web\\HomeController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/a(?'
                    .'|pi(?'
                        .'|/\\.well\\-known/genid/([^/]++)(*:46)'
                        .'|(?:/(index)(?:\\.([^/]++))?)?(*:81)'
                        .'|/(?'
                            .'|c(?'
                                .'|ont(?'
                                    .'|exts/([^.]+)(?:\\.(jsonld))?(*:129)'
                                    .'|racts/([^/]++)(?'
                                        .'|(*:154)'
                                        .'|/(?'
                                            .'|apply(*:171)'
                                            .'|time\\-send(*:189)'
                                            .'|c(?'
                                                .'|ancel(*:206)'
                                                .'|onfirm(*:220)'
                                            .')'
                                            .'|edit(*:233)'
                                            .'|worked(*:247)'
                                            .'|not\\-worked(*:266)'
                                            .'|bonus(*:279)'
                                            .'|late(*:291)'
                                        .')'
                                    .')'
                                .')'
                                .'|lients(?'
                                    .'|(?:\\.([^/]++))?(?'
                                        .'|(*:329)'
                                    .')'
                                    .'|/(?'
                                        .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                            .'|(*:370)'
                                        .')'
                                        .'|([^/]++)/(?'
                                            .'|image(*:396)'
                                            .'|contracts(*:413)'
                                            .'|app(?'
                                                .'|lies(*:431)'
                                                .'|rouved(*:445)'
                                            .')'
                                            .'|validate(*:462)'
                                        .')'
                                    .')'
                                .')'
                            .')'
                            .'|accepted_contracts(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:513)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:551)'
                                .')'
                            .')'
                            .'|d(?'
                                .'|evices(?'
                                    .'|(?:\\.([^/]++))?(?'
                                        .'|(*:592)'
                                    .')'
                                    .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                        .'|(*:630)'
                                    .')'
                                .')'
                                .'|ocument(?'
                                    .'|s(?'
                                        .'|(?:\\.([^/]++))?(?'
                                            .'|(*:672)'
                                        .')'
                                        .'|/(?'
                                            .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                                .'|(*:713)'
                                            .')'
                                            .'|([^/]++)/(?'
                                                .'|valid(*:739)'
                                                .'|unvalid(*:754)'
                                                .'|file(*:766)'
                                                .'|text(*:778)'
                                            .')'
                                        .')'
                                    .')'
                                    .'|_types(?'
                                        .'|(?:\\.([^/]++))?(?'
                                            .'|(*:816)'
                                        .')'
                                        .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                            .'|(*:854)'
                                        .')'
                                    .')'
                                .')'
                            .')'
                            .'|invoices(?'
                                .'|(?:\\.([^/]++))?(*:892)'
                                .'|/(?'
                                    .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                        .'|(*:932)'
                                    .')'
                                    .'|([^/]++)/(?'
                                        .'|pay(*:956)'
                                        .'|download(*:972)'
                                    .')'
                                .')'
                            .')'
                            .'|jobs(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:1008)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:1047)'
                                .')'
                            .')'
                            .'|likes(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:1084)'
                                .')'
                                .'|/(?'
                                    .'|([^/\\.]++)(?:\\.([^/]++))?(*:1123)'
                                    .'|([^/]++)/delete(*:1147)'
                                .')'
                            .')'
                            .'|notifications(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:1192)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:1231)'
                                .')'
                            .')'
                            .'|password_resets(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:1278)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:1317)'
                                .')'
                            .')'
                            .'|t(?'
                                .'|ime_sheets(?'
                                    .'|(?:\\.([^/]++))?(*:1360)'
                                    .'|/(?'
                                        .'|([^/\\.]++)(?:\\.([^/]++))?(*:1398)'
                                        .'|([^/]++)/(?'
                                            .'|approuve(*:1427)'
                                            .'|edit(*:1440)'
                                        .')'
                                        .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                            .'|(*:1478)'
                                        .')'
                                    .')'
                                .')'
                                .'|owns(?'
                                    .'|(?:\\.([^/]++))?(*:1512)'
                                    .'|(*:1521)'
                                    .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                        .'|(*:1559)'
                                    .')'
                                .')'
                            .')'
                            .'|user(?'
                                .'|s(?'
                                    .'|(?:\\.([^/]++))?(*:1597)'
                                    .'|/(?'
                                        .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                            .'|(*:1638)'
                                        .')'
                                        .'|([^/]++)/(?'
                                            .'|app(?'
                                                .'|lies(*:1670)'
                                                .'|rouved(*:1685)'
                                            .')'
                                            .'|i(?'
                                                .'|n(?'
                                                    .'|itiate\\-delete(*:1717)'
                                                    .'|voices(*:1732)'
                                                .')'
                                                .'|mage(*:1746)'
                                            .')'
                                            .'|t(?'
                                                .'|ime\\-sheets(?'
                                                    .'|(*:1774)'
                                                    .'|\\-(?'
                                                        .'|approuved(*:1797)'
                                                        .'|todo(*:1810)'
                                                    .')'
                                                .')'
                                                .'|owns(?'
                                                    .'|(*:1828)'
                                                    .'|\\-list(*:1843)'
                                                .')'
                                            .')'
                                            .'|files(*:1859)'
                                            .'|e(?'
                                                .'|xperiences(?'
                                                    .'|\\-list(*:1891)'
                                                    .'|(*:1900)'
                                                .')'
                                                .'|mail\\-verifivation(?'
                                                    .'|(*:1931)'
                                                    .'|\\-confirm(*:1949)'
                                                .')'
                                            .')'
                                            .'|likes(*:1965)'
                                            .'|notifications(*:1987)'
                                            .'|p(?'
                                                .'|hone\\-verifivation(?'
                                                    .'|(*:2021)'
                                                    .'|\\-confirm(*:2039)'
                                                .')'
                                                .'|assword(*:2056)'
                                            .')'
                                            .'|contract\\-file\\-download(*:2090)'
                                        .')'
                                    .')'
                                .')'
                                .'|_contracts(?'
                                    .'|(?:\\.([^/]++))?(?'
                                        .'|(*:2133)'
                                    .')'
                                    .'|/(?'
                                        .'|([^/\\.]++)(?:\\.([^/]++))?(?'
                                            .'|(*:2175)'
                                        .')'
                                        .'|([^/]++)/sign(*:2198)'
                                    .')'
                                .')'
                            .')'
                        .')'
                    .')'
                    .'|dmin/(?'
                        .'|d(?'
                            .'|elete\\-(?'
                                .'|client\\-([^/]++)(*:2250)'
                                .'|user\\-([^/]++)(*:2273)'
                            .')'
                            .'|ocument(?'
                                .'|s/([^/]++)(*:2303)'
                                .'|/(?'
                                    .'|unvalid/([^/]++)(*:2332)'
                                    .'|valid/([^/]++)(*:2355)'
                                .')'
                            .')'
                        .')'
                        .'|c(?'
                            .'|lient/detail/([^/]++)(*:2392)'
                            .'|ancel\\-invoice\\-([^/]++)(*:2425)'
                        .')'
                        .'|validate\\-(?'
                            .'|client\\-([^/]++)(*:2464)'
                            .'|invoice\\-([^/]++)(*:2490)'
                        .')'
                        .'|unvalidate\\-client\\-([^/]++)(*:2528)'
                        .'|shift\\-([^/]++)(*:2552)'
                        .'|t(?'
                            .'|own/([^/]++)(*:2577)'
                            .'|ravailleur/([^/]++)(*:2605)'
                        .')'
                    .')'
                .')'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:2645)'
                .'|/entreprise/(?'
                    .'|paiement(?:/([^/]++))?(*:2691)'
                    .'|c(?'
                        .'|on(?'
                            .'|tract(?:/([^/]++))?(*:2728)'
                            .'|firm(?'
                                .'|\\-code/([^/]++)(*:2759)'
                                .'|/([^/]++)/([^/]++)(*:2786)'
                            .')'
                        .')'
                        .'|ancel/([^/]++)(*:2811)'
                    .')'
                    .'|new\\-pass/([^/]++)(*:2839)'
                    .'|shift\\-([^/]++)(*:2863)'
                    .'|delete\\-shift\\-([^/]++)(*:2895)'
                .')'
                .'|/c(?'
                    .'|ontract/edit\\-shift/([^/]++)(*:2938)'
                    .'|ron/check/(?'
                        .'|([^/]++)(*:2968)'
                        .'|client\\-ts(*:2987)'
                    .')'
                .')'
                .'|/facturation/invoice/([^/]++)(*:3027)'
                .'|/timesheet/detail\\-([^/]++)(*:3063)'
                .'|/download/invoice/([^/]++)(*:3098)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        46 => [[['_route' => 'api_genid', '_controller' => 'api_platform.action.not_exposed', '_api_respond' => 'true'], ['id'], null, null, false, true, null]],
        81 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        129 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null]],
        154 => [
            [['_route' => 'api_contracts_delete_item', '_controller' => 'App\\Controller\\ContractDeleteController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_delete_item', '_api_item_operation_name' => 'delete'], ['id'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_contracts_get_item', '_controller' => 'App\\Controller\\ContractDetailController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_get_item', '_api_item_operation_name' => 'get'], ['id'], ['GET' => 0], null, false, true, null],
        ],
        171 => [[['_route' => 'api_contracts_apply_item', '_controller' => 'App\\Controller\\ContractApplyController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_apply_item', '_api_item_operation_name' => 'apply'], ['id'], ['POST' => 0], null, false, false, null]],
        189 => [[['_route' => 'api_contracts_send_time_item', '_controller' => 'App\\Controller\\ContractSendTimeController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_send_time_item', '_api_item_operation_name' => 'send_time'], ['id'], ['POST' => 0], null, false, false, null]],
        206 => [[['_route' => 'api_contracts_cancel_item', '_controller' => 'App\\Controller\\ContractCanceledController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_cancel_item', '_api_item_operation_name' => 'cancel'], ['id'], ['POST' => 0], null, false, false, null]],
        220 => [[['_route' => 'api_contracts_confirm_item', '_controller' => 'App\\Controller\\ContractConfirmController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_confirm_item', '_api_item_operation_name' => 'confirm'], ['id'], ['POST' => 0], null, false, false, null]],
        233 => [[['_route' => 'api_contracts_edit_item', '_controller' => 'App\\Controller\\ContractEditController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_edit_item', '_api_item_operation_name' => 'edit'], ['id'], ['POST' => 0], null, false, false, null]],
        247 => [[['_route' => 'api_contracts_worked_item', '_controller' => 'App\\Controller\\ContractWorkedController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_worked_item', '_api_item_operation_name' => 'worked'], ['id'], ['POST' => 0], null, false, false, null]],
        266 => [[['_route' => 'api_contracts_not_worked_item', '_controller' => 'App\\Controller\\ContractNotWorkedController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_not_worked_item', '_api_item_operation_name' => 'not_worked'], ['id'], ['POST' => 0], null, false, false, null]],
        279 => [[['_route' => 'api_contracts_bonus_item', '_controller' => 'App\\Controller\\ContractBonusController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_bonus_item', '_api_item_operation_name' => 'bonus'], ['id'], ['POST' => 0], null, false, false, null]],
        291 => [[['_route' => 'api_contracts_late_item', '_controller' => 'App\\Controller\\ContractSetLateController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Contract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_contracts_late_item', '_api_item_operation_name' => 'late'], ['id'], ['POST' => 0], null, false, false, null]],
        329 => [
            [['_route' => 'api_clients_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_clients_post_collection', '_controller' => 'App\\Controller\\ClientRegisterController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        370 => [
            [['_route' => 'api_clients_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_clients_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
            [['_route' => 'api_clients_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        396 => [[['_route' => 'api_clients_image_item', '_controller' => 'App\\Controller\\ClientImageController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_image_item', '_api_item_operation_name' => 'image'], ['id'], ['POST' => 0], null, false, false, null]],
        413 => [[['_route' => 'api_clients_contracts_item', '_controller' => 'App\\Controller\\ClientGetContractsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_contracts_item', '_api_item_operation_name' => 'contracts'], ['id'], ['GET' => 0], null, false, false, null]],
        431 => [[['_route' => 'api_clients_applies_item', '_controller' => 'App\\Controller\\ClientGetAppliedContractsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_applies_item', '_api_item_operation_name' => 'applies'], ['id'], ['GET' => 0], null, false, false, null]],
        445 => [[['_route' => 'api_clients_approuved_item', '_controller' => 'App\\Controller\\ClientGetApprouvedContractController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_approuved_item', '_api_item_operation_name' => 'approuved'], ['id'], ['GET' => 0], null, false, false, null]],
        462 => [[['_route' => 'api_clients_validate_item', '_controller' => 'App\\Controller\\ClientValidationController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Client', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_clients_validate_item', '_api_item_operation_name' => 'validate'], ['id'], ['POST' => 0], null, false, false, null]],
        513 => [
            [['_route' => 'api_accepted_contracts_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\AcceptedContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_accepted_contracts_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_accepted_contracts_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\AcceptedContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_accepted_contracts_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        551 => [
            [['_route' => 'api_accepted_contracts_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\AcceptedContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_accepted_contracts_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_accepted_contracts_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\AcceptedContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_accepted_contracts_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        592 => [
            [['_route' => 'api_devices_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_devices_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        630 => [
            [['_route' => 'api_devices_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_devices_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_devices_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_put_item', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_devices_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Device', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_devices_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        672 => [
            [['_route' => 'api_documents_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_documents_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        713 => [
            [['_route' => 'api_documents_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_documents_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_documents_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        739 => [[['_route' => 'api_documents_validate_item', '_controller' => 'App\\Controller\\DocumentValidatedController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_validate_item', '_api_item_operation_name' => 'validate'], ['id'], ['POST' => 0], null, false, false, null]],
        754 => [[['_route' => 'api_documents_unvalidate_item', '_controller' => 'App\\Controller\\DocumentUnValidatedController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_unvalidate_item', '_api_item_operation_name' => 'unvalidate'], ['id'], ['POST' => 0], null, false, false, null]],
        766 => [[['_route' => 'api_documents_file_item', '_controller' => 'App\\Controller\\DocumentSendFileController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_file_item', '_api_item_operation_name' => 'file'], ['id'], ['POST' => 0], null, false, false, null]],
        778 => [[['_route' => 'api_documents_text_item', '_controller' => 'App\\Controller\\DocumentSendTextController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Document', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_documents_text_item', '_api_item_operation_name' => 'text'], ['id'], ['POST' => 0], null, false, false, null]],
        816 => [
            [['_route' => 'api_document_types_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\DocumentType', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_document_types_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_document_types_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\DocumentType', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_document_types_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        854 => [
            [['_route' => 'api_document_types_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\DocumentType', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_document_types_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_document_types_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\DocumentType', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_document_types_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
            [['_route' => 'api_document_types_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\DocumentType', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_document_types_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
        ],
        892 => [[['_route' => 'api_invoices_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null]],
        932 => [
            [['_route' => 'api_invoices_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_invoices_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        956 => [[['_route' => 'api_invoices_pay_item', '_controller' => 'App\\Controller\\InvoicePayController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_pay_item', '_api_item_operation_name' => 'pay'], ['id'], ['POST' => 0], null, false, false, null]],
        972 => [[['_route' => 'api_invoices_download_item', '_controller' => 'App\\Controller\\InvoiceDownloadController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Invoice', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_invoices_download_item', '_api_item_operation_name' => 'download'], ['id'], ['POST' => 0], null, false, false, null]],
        1008 => [
            [['_route' => 'api_jobs_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_jobs_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        1047 => [
            [['_route' => 'api_jobs_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_jobs_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_jobs_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_put_item', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_jobs_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Job', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_jobs_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        1084 => [
            [['_route' => 'api_likes_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Like', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_likes_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_likes_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Like', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_likes_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        1123 => [[['_route' => 'api_likes_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Like', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_likes_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null]],
        1147 => [[['_route' => 'api_likes_remove_item', '_controller' => 'App\\Controller\\RemoveLikeController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Like', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_likes_remove_item', '_api_item_operation_name' => 'remove'], ['id'], ['POST' => 0], null, false, false, null]],
        1192 => [
            [['_route' => 'api_notifications_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_notifications_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        1231 => [
            [['_route' => 'api_notifications_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_notifications_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_notifications_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_put_item', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_notifications_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Notification', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_notifications_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        1278 => [
            [['_route' => 'api_password_resets_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
            [['_route' => 'api_password_resets_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
        ],
        1317 => [
            [['_route' => 'api_password_resets_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_password_resets_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_password_resets_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_put_item', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_password_resets_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\PasswordReset', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_password_resets_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        1360 => [[['_route' => 'api_time_sheets_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null]],
        1398 => [[['_route' => 'api_time_sheets_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null]],
        1427 => [[['_route' => 'api_time_sheets_approuve_item', '_controller' => 'App\\Controller\\TimeSheetApprouveController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_approuve_item', '_api_item_operation_name' => 'approuve'], ['id'], ['POST' => 0], null, false, false, null]],
        1440 => [[['_route' => 'api_time_sheets_edit_item', '_controller' => 'App\\Controller\\TimeSheetEditController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_edit_item', '_api_item_operation_name' => 'edit'], ['id'], ['POST' => 0], null, false, false, null]],
        1478 => [
            [['_route' => 'api_time_sheets_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_time_sheets_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\TimeSheet', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_time_sheets_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        1512 => [[['_route' => 'api_towns_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null]],
        1521 => [[['_route' => 'api_towns_get_collection', '_controller' => 'App\\Controller\\TownsListingController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_get_collection', '_api_collection_operation_name' => 'get'], [], ['GET' => 0], null, false, false, null]],
        1559 => [
            [['_route' => 'api_towns_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_towns_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_towns_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_put_item', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_towns_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\Town', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_towns_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        1597 => [[['_route' => 'api_users_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null]],
        1638 => [
            [['_route' => 'api_users_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_users_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
            [['_route' => 'api_users_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
        ],
        1670 => [[['_route' => 'api_users_contracts_item', '_controller' => 'App\\Controller\\UserGetAppliesContracts', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_contracts_item', '_api_item_operation_name' => 'contracts'], ['id'], ['GET' => 0], null, false, false, null]],
        1685 => [[['_route' => 'api_users_approuvments_item', '_controller' => 'App\\Controller\\UserGetApprouvedContractsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_approuvments_item', '_api_item_operation_name' => 'approuvments'], ['id'], ['GET' => 0], null, false, false, null]],
        1717 => [[['_route' => 'api_users_initiate_deletion_item', '_controller' => 'App\\Controller\\UserInitiateDeleteController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_initiate_deletion_item', '_api_item_operation_name' => 'initiate_deletion'], ['id'], ['POST' => 0], null, false, false, null]],
        1732 => [[['_route' => 'api_users_invoices_item', '_controller' => 'App\\Controller\\UserGetInvoicesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_invoices_item', '_api_item_operation_name' => 'invoices'], ['id'], ['GET' => 0], null, false, false, null]],
        1746 => [[['_route' => 'api_users_image_item', '_controller' => 'App\\Controller\\UserImageController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_image_item', '_api_item_operation_name' => 'image'], ['id'], ['POST' => 0], null, false, false, null]],
        1774 => [[['_route' => 'api_users_time_sheet_item', '_controller' => 'App\\Controller\\UserGetTimeSheetController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_time_sheet_item', '_api_item_operation_name' => 'time_sheet'], ['id'], ['GET' => 0], null, false, false, null]],
        1797 => [[['_route' => 'api_users_time_sheet_approuved_item', '_controller' => 'App\\Controller\\UserGetTimeSheetsApprouvedController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_time_sheet_approuved_item', '_api_item_operation_name' => 'time_sheet_approuved'], ['id'], ['GET' => 0], null, false, false, null]],
        1810 => [[['_route' => 'api_users_time_sheet_todo_item', '_controller' => 'App\\Controller\\UserGetTimeSheetTodoController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_time_sheet_todo_item', '_api_item_operation_name' => 'time_sheet_todo'], ['id'], ['GET' => 0], null, false, false, null]],
        1828 => [[['_route' => 'api_users_towns_item', '_controller' => 'App\\Controller\\UserSetTownsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_towns_item', '_api_item_operation_name' => 'towns'], ['id'], ['POST' => 0], null, false, false, null]],
        1843 => [[['_route' => 'api_users_towns-list_item', '_controller' => 'App\\Controller\\UserGetTownsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_towns-list_item', '_api_item_operation_name' => 'towns-list'], ['id'], ['GET' => 0], null, false, false, null]],
        1859 => [[['_route' => 'api_users_files_item', '_controller' => 'App\\Controller\\UserGetDocumentController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_files_item', '_api_item_operation_name' => 'files'], ['id'], ['GET' => 0], null, false, false, null]],
        1891 => [[['_route' => 'api_users_experiences-list_item', '_controller' => 'App\\Controller\\UserGetExperiencesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_experiences-list_item', '_api_item_operation_name' => 'experiences-list'], ['id'], ['GET' => 0], null, false, false, null]],
        1900 => [[['_route' => 'api_users_experiences_item', '_controller' => 'App\\Controller\\UserAddExperiencesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_experiences_item', '_api_item_operation_name' => 'experiences'], ['id'], ['POST' => 0], null, false, false, null]],
        1931 => [[['_route' => 'api_users_email_verification_item', '_controller' => 'App\\Controller\\UserEmailVerificationController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_email_verification_item', '_api_item_operation_name' => 'email_verification'], ['id'], ['POST' => 0], null, false, false, null]],
        1949 => [[['_route' => 'api_users_email_verification_confirm_item', '_controller' => 'App\\Controller\\UserEmailVerificationConfirmController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_email_verification_confirm_item', '_api_item_operation_name' => 'email_verification_confirm'], ['id'], ['POST' => 0], null, false, false, null]],
        1965 => [[['_route' => 'api_users_likes_item', '_controller' => 'App\\Controller\\UserGetLikesController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_likes_item', '_api_item_operation_name' => 'likes'], ['id'], ['GET' => 0], null, false, false, null]],
        1987 => [[['_route' => 'api_users_notifications_item', '_controller' => 'App\\Controller\\UserGetNotificationsController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_notifications_item', '_api_item_operation_name' => 'notifications'], ['id'], ['GET' => 0], null, false, false, null]],
        2021 => [[['_route' => 'api_users_phone_verification_item', '_controller' => 'App\\Controller\\UserPhoneVerificationController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_phone_verification_item', '_api_item_operation_name' => 'phone_verification'], ['id'], ['POST' => 0], null, false, false, null]],
        2039 => [[['_route' => 'api_users_phone_verification_confirm_item', '_controller' => 'App\\Controller\\UserPhoneVerificationConfirmController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_phone_verification_confirm_item', '_api_item_operation_name' => 'phone_verification_confirm'], ['id'], ['POST' => 0], null, false, false, null]],
        2056 => [[['_route' => 'api_users_new-password_item', '_controller' => 'App\\Controller\\UserNewPasswordController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_new-password_item', '_api_item_operation_name' => 'new-password'], ['id'], ['POST' => 0], null, false, false, null]],
        2090 => [[['_route' => 'api_users_download_file_contract_item', '_controller' => 'App\\Controller\\UserDownloadContractFile', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_users_download_file_contract_item', '_api_item_operation_name' => 'download_file_contract'], ['id'], ['POST' => 0], null, false, false, null]],
        2133 => [
            [['_route' => 'api_user_contracts_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_get_collection', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_user_contracts_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_post_collection', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        2175 => [
            [['_route' => 'api_user_contracts_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_patch_item', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
            [['_route' => 'api_user_contracts_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_get_item', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_user_contracts_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_delete_item', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        2198 => [[['_route' => 'api_user_contracts_sign_item', '_controller' => 'App\\Controller\\SignatureController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserContract', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_exception_to_status' => [], '_api_operation_name' => 'api_user_contracts_sign_item', '_api_item_operation_name' => 'sign'], ['id'], ['POST' => 0], null, false, false, null]],
        2250 => [[['_route' => 'app_user_delete_client', '_controller' => 'App\\Controller\\Admin\\ClientController::deleteClient'], ['id'], ['POST' => 0], null, false, true, null]],
        2273 => [[['_route' => 'app_user_delete_user', '_controller' => 'App\\Controller\\Admin\\UserController::deleteUser'], ['id'], ['POST' => 0], null, false, true, null]],
        2303 => [[['_route' => 'app_user_documents', '_controller' => 'App\\Controller\\Admin\\UserController::documents'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null]],
        2332 => [[['_route' => 'app_user_document_unvalid', '_controller' => 'App\\Controller\\Admin\\UserController::unvalidDoc'], ['id'], ['POST' => 0], null, false, true, null]],
        2355 => [[['_route' => 'app_user_document_valid', '_controller' => 'App\\Controller\\Admin\\UserController::validDoc'], ['id'], ['POST' => 0], null, false, true, null]],
        2392 => [[['_route' => 'app_user_client_detail', '_controller' => 'App\\Controller\\Admin\\ClientController::profile'], ['id'], null, null, false, true, null]],
        2425 => [[['_route' => 'app_user_cancel_invoice', '_controller' => 'App\\Controller\\Admin\\InvoiceController::cancel'], ['id'], ['POST' => 0], null, false, true, null]],
        2464 => [[['_route' => 'app_user_validate_client', '_controller' => 'App\\Controller\\Admin\\ClientController::validateClient'], ['id'], ['POST' => 0], null, false, true, null]],
        2490 => [[['_route' => 'app_user_validate_invoice', '_controller' => 'App\\Controller\\Admin\\InvoiceController::paid'], ['id'], ['POST' => 0], null, false, true, null]],
        2528 => [[['_route' => 'app_user_unvalidate_client', '_controller' => 'App\\Controller\\Admin\\ClientController::unValidateClient'], ['id'], ['POST' => 0], null, false, true, null]],
        2552 => [[['_route' => 'app_admin_shift_detail', '_controller' => 'App\\Controller\\Admin\\ContractController::detail'], ['id'], null, null, false, true, null]],
        2577 => [[['_route' => 'app_user_town', '_controller' => 'App\\Controller\\Admin\\UserController::town'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null]],
        2605 => [[['_route' => 'app_user_detail', '_controller' => 'App\\Controller\\Admin\\UserController::detail'], ['id'], ['POST' => 0, 'GET' => 1], null, false, true, null]],
        2645 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        2691 => [[['_route' => 'app_client_web_auth_paiement', 'profile' => null, '_controller' => 'App\\Controller\\ClientWeb\\AuthController::paiement'], ['profile'], null, null, false, true, null]],
        2728 => [[['_route' => 'app_client_web_auth_contract', 'profile' => null, '_controller' => 'App\\Controller\\ClientWeb\\AuthController::contract'], ['profile'], null, null, false, true, null]],
        2759 => [[['_route' => 'app_client_web_auth_confirm_code', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::confirmCode'], ['resetId'], null, null, false, true, null]],
        2786 => [[['_route' => 'app_client_web_contract_confirm', '_controller' => 'App\\Controller\\ClientWeb\\ContractController::confirm'], ['user_id', 'contract_id'], ['POST' => 0], null, false, true, null]],
        2811 => [[['_route' => 'app_client_web_contract_cancel', '_controller' => 'App\\Controller\\ClientWeb\\ContractController::cancel'], ['contract_id'], ['POST' => 0], null, false, true, null]],
        2839 => [[['_route' => 'app_client_web_auth_new_pass', '_controller' => 'App\\Controller\\ClientWeb\\AuthController::newPass'], ['userId'], null, null, false, true, null]],
        2863 => [[['_route' => 'app_client_web_shift_detail', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::detail'], ['id'], null, null, false, true, null]],
        2895 => [[['_route' => 'app_client_web_delete_shift_detail', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::deleteContract'], ['id'], ['POST' => 0], null, false, true, null]],
        2938 => [[['_route' => 'app_client_web_edit_shift', '_controller' => 'App\\Controller\\ClientWeb\\HomeController::editShift'], ['id'], null, null, false, true, null]],
        2968 => [[['_route' => 'app_cron', '_controller' => 'App\\Controller\\CronController::check24'], ['h'], null, null, false, true, null]],
        2987 => [[['_route' => 'app_cron_ts', '_controller' => 'App\\Controller\\CronController::checkTClient'], [], null, null, false, false, null]],
        3027 => [[['_route' => 'app_client_web_invoice_detail', '_controller' => 'App\\Controller\\ClientWeb\\InvoiceController::detail'], ['id'], null, null, false, true, null]],
        3063 => [[['_route' => 'app_client_web_timesheet_detail', '_controller' => 'App\\Controller\\ClientWeb\\TimeSheetController::detail'], ['id'], null, null, false, true, null]],
        3098 => [
            [['_route' => 'app_download_invoice', '_controller' => 'App\\Controller\\DownloadInvoiceController::index'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
