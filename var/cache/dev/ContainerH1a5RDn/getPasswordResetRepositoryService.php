<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getPasswordResetRepositoryService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Repository\PasswordResetRepository' shared autowired service.
     *
     * @return \App\Repository\PasswordResetRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Repository/PasswordResetRepository.php';

        return $container->privates['App\\Repository\\PasswordResetRepository'] = new \App\Repository\PasswordResetRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
