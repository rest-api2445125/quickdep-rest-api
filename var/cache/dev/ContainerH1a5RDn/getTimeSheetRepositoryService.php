<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getTimeSheetRepositoryService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Repository\TimeSheetRepository' shared autowired service.
     *
     * @return \App\Repository\TimeSheetRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Repository/TimeSheetRepository.php';

        return $container->privates['App\\Repository\\TimeSheetRepository'] = new \App\Repository\TimeSheetRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
