<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getTimeSheetService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Entity\TimeSheet' shared autowired service.
     *
     * @return \App\Entity\TimeSheet
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Entity/TimeSheet.php';

        return $container->privates['App\\Entity\\TimeSheet'] = new \App\Entity\TimeSheet();
    }
}
