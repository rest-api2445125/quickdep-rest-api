<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getTimeSheetController2Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\ClientWeb\TimeSheetController' shared autowired service.
     *
     * @return \App\Controller\ClientWeb\TimeSheetController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/ClientWeb/TimeSheetController.php';

        $container->services['App\\Controller\\ClientWeb\\TimeSheetController'] = $instance = new \App\Controller\ClientWeb\TimeSheetController();

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\ClientWeb\\TimeSheetController', $container));

        return $instance;
    }
}
