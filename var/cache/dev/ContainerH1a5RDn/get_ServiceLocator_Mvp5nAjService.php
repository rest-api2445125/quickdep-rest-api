<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_Mvp5nAjService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.Mvp5nAj' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.Mvp5nAj'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'contractRepository' => ['privates', 'App\\Repository\\ContractRepository', 'getContractRepositoryService', true],
            'data' => ['privates', '.errored..service_locator.Mvp5nAj.ApiPlatform\\Core\\Bridge\\Doctrine\\Orm\\Paginator', NULL, 'Cannot autowire service ".service_locator.Mvp5nAj": it references class "ApiPlatform\\Core\\Bridge\\Doctrine\\Orm\\Paginator" but no such service exists.'],
        ], [
            'contractRepository' => 'App\\Repository\\ContractRepository',
            'data' => 'ApiPlatform\\Core\\Bridge\\Doctrine\\Orm\\Paginator',
        ]);
    }
}
