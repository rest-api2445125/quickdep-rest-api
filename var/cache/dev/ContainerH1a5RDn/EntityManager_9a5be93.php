<?php

namespace ContainerH1a5RDn;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder11fa5 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer7564c = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties848e4 = [
        
    ];

    public function getConnection()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getConnection', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getMetadataFactory', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getExpressionBuilder', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'beginTransaction', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getCache', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getCache();
    }

    public function transactional($func)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'transactional', array('func' => $func), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'wrapInTransaction', array('func' => $func), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'commit', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->commit();
    }

    public function rollback()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'rollback', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getClassMetadata', array('className' => $className), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'createQuery', array('dql' => $dql), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'createNamedQuery', array('name' => $name), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'createQueryBuilder', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'flush', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'clear', array('entityName' => $entityName), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->clear($entityName);
    }

    public function close()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'close', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->close();
    }

    public function persist($entity)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'persist', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'remove', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->remove($entity);
    }

    public function refresh($entity, ?int $lockMode = null)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'refresh', array('entity' => $entity, 'lockMode' => $lockMode), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->refresh($entity, $lockMode);
    }

    public function detach($entity)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'detach', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'merge', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getRepository', array('entityName' => $entityName), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'contains', array('entity' => $entity), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getEventManager', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getConfiguration', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'isOpen', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getUnitOfWork', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getProxyFactory', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'initializeObject', array('obj' => $obj), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'getFilters', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'isFiltersStateClean', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'hasFilters', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return $this->valueHolder11fa5->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer7564c = $initializer;

        return $instance;
    }

    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, ?\Doctrine\Common\EventManager $eventManager = null)
    {
        static $reflection;

        if (! $this->valueHolder11fa5) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder11fa5 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder11fa5->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__get', ['name' => $name], $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        if (isset(self::$publicProperties848e4[$name])) {
            return $this->valueHolder11fa5->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder11fa5;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder11fa5;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder11fa5;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder11fa5;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__isset', array('name' => $name), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder11fa5;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder11fa5;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__unset', array('name' => $name), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder11fa5;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder11fa5;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__clone', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        $this->valueHolder11fa5 = clone $this->valueHolder11fa5;
    }

    public function __sleep()
    {
        $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, '__sleep', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;

        return array('valueHolder11fa5');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer7564c = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer7564c;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer7564c && ($this->initializer7564c->__invoke($valueHolder11fa5, $this, 'initializeProxy', array(), $this->initializer7564c) || 1) && $this->valueHolder11fa5 = $valueHolder11fa5;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder11fa5;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder11fa5;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
