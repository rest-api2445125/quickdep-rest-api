<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getTesteRepositoryService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Repository\TesteRepository' shared autowired service.
     *
     * @return \App\Repository\TesteRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Repository/TesteRepository.php';

        return $container->privates['App\\Repository\\TesteRepository'] = new \App\Repository\TesteRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
