<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_3PFLNK4Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.3PFLNK4' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.3PFLNK4'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'contractRepository' => ['privates', 'App\\Repository\\ContractRepository', 'getContractRepositoryService', true],
            'invoiceRepository' => ['privates', 'App\\Repository\\InvoiceRepository', 'getInvoiceRepositoryService', true],
            'timeSheetRepository' => ['privates', 'App\\Repository\\TimeSheetRepository', 'getTimeSheetRepositoryService', true],
            'userRepository' => ['privates', 'App\\Repository\\UserRepository', 'getUserRepositoryService', false],
        ], [
            'contractRepository' => 'App\\Repository\\ContractRepository',
            'invoiceRepository' => 'App\\Repository\\InvoiceRepository',
            'timeSheetRepository' => 'App\\Repository\\TimeSheetRepository',
            'userRepository' => 'App\\Repository\\UserRepository',
        ]);
    }
}
