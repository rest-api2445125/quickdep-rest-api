<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getUserDownloadContractFileService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\UserDownloadContractFile' shared autowired service.
     *
     * @return \App\Controller\UserDownloadContractFile
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/UserDownloadContractFile.php';

        $container->services['App\\Controller\\UserDownloadContractFile'] = $instance = new \App\Controller\UserDownloadContractFile();

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\UserDownloadContractFile', $container));

        return $instance;
    }
}
