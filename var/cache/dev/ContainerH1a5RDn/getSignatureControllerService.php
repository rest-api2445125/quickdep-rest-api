<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getSignatureControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\SignatureController' shared autowired service.
     *
     * @return \App\Controller\SignatureController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/SignatureController.php';

        $container->services['App\\Controller\\SignatureController'] = $instance = new \App\Controller\SignatureController();

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\SignatureController', $container));

        return $instance;
    }
}
