<?php

namespace ContainerH1a5RDn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_PapFtacService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.papFtac' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.papFtac'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'applyRepository' => ['privates', 'App\\Repository\\ApplyRepository', 'getApplyRepositoryService', true],
            'contractRepository' => ['privates', 'App\\Repository\\ContractRepository', 'getContractRepositoryService', true],
            'data' => ['privates', 'App\\Entity\\Client', 'getClientService', true],
            'userRepository' => ['privates', 'App\\Repository\\UserRepository', 'getUserRepositoryService', false],
        ], [
            'applyRepository' => 'App\\Repository\\ApplyRepository',
            'contractRepository' => 'App\\Repository\\ContractRepository',
            'data' => 'App\\Entity\\Client',
            'userRepository' => 'App\\Repository\\UserRepository',
        ]);
    }
}
